/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.abricot.client;

import org.cocktail.application.client.tools.ToolsCocktailPrivileges;

public class Privileges extends ToolsCocktailPrivileges {

	/*
	 * pour genere les FON_ID_INTERNE : select 'public static String '||upper(FON_ID_INTERNE)||' = "'||FON_ID_INTERNE||'";' from jefy_admin.fonction
	 * where tyap_id = 18
	 */
	// Privileges table jefy_admin.fonction
	public static String DEP_1 = "DEP_1";
	public static String DEP_2 = "DEP_2";
	public static String DEP_3 = "DEP_3";
	public static String DEP_4 = "DEP_4";
	public static String DEP_5 = "DEP_5";
	public static String DEP_8 = "DEP_8";
	public static String DEP_18 = "DEP_18";

	// PAF
	public static String ORV_PAF = "DEP_21";
	public static String DEP_PAF = "DEP_20";
	public static String CAP_PAF = "DEP_23";

	// BORDEREAU DE REGULARISATION DE SALAIRES
	public static String DEP_19 = "DEP_19";
	// BORDEREAU DE REGULARISATION DE SALAIRES PAF
	public static String DEP_REGUL_PAF = "DEP_22";

	public static String REC_PAF = "a definir";

	public static String PID_11 = "PID_11";
	public static String PIR_200 = "PIR_200";
	public static String PID_201 = "PID_201";

	// redution de prestations internes TODO
	//public static String PIR_200 = "PIRR_20x";
	//public static String PID_201 = "PIDR_20x";

	public static String REC_12 = "REC_12";
	public static String REC_13 = "REC_13";
	public static String REC_6 = "REC_6";
	public static String REC_7 = "REC_7";
	public static String REC_9 = "REC_9";
	public static String VIS_101 = "VIS_101";
	public static String VIS_102 = "VIS_102";
	public static String IMP = "IMP";
	//public static String REI = "REI";
	public static String IMPGEST = "IMPGEST";

	// Privileges Interface
	public static String DEPENSE = "DEPENSE";
	public static String RECETTE = "RECETTE";
	public static String PI = "PI";
	//public static String REIMPUT = "REIMPUT";
	public static String VISA = "VISA";
	public static String IMPRESSION = "IMPRESSION";
	public static String PAYE = "PAYE";

	public Privileges(String key) {
		super(key);
	}

}
