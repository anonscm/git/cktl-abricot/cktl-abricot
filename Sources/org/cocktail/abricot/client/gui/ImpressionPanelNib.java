/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.abricot.client.gui;

import javax.swing.JCheckBox;
import javax.swing.JLabel;

import org.cocktail.application.palette.JPanelCocktail;
import org.cocktail.application.palette.JTextFieldCocktail;

public class ImpressionPanelNib extends JPanelCocktail {
    
    /** Creates new form ImpressionPanelNibnb */
    public ImpressionPanelNib() {
        initComponents();
    }
    
    public org.cocktail.application.palette.JButtonCocktail getJButtonCocktailFermer() {
		return jButtonCocktailFermer;
	}

	public void setJButtonCocktailFermer(
			org.cocktail.application.palette.JButtonCocktail buttonCocktailFermer) {
		jButtonCocktailFermer = buttonCocktailFermer;
	}

	public org.cocktail.application.palette.JButtonCocktail getJButtonCocktailImpression() {
		return jButtonCocktailImpression;
	}

	public void setJButtonCocktailImpression(
			org.cocktail.application.palette.JButtonCocktail buttonCocktailImpression) {
		jButtonCocktailImpression = buttonCocktailImpression;
	}

	public org.cocktail.application.palette.JLabelCocktail getJLabelCocktailIcone() {
		return jLabelCocktailIcone;
	}

	public void setJLabelCocktailIcone(
			org.cocktail.application.palette.JLabelCocktail labelCocktailIcone) {
		jLabelCocktailIcone = labelCocktailIcone;
	}

	public org.cocktail.application.palette.JTextFieldCocktail getJTextFieldCocktailNumero() {
		return jTextFieldCocktailNumero;
	}

	public void setJTextFieldCocktailNumero(
			org.cocktail.application.palette.JTextFieldCocktail textFieldCocktailNumero) {
		jTextFieldCocktailNumero = textFieldCocktailNumero;
	}

	public org.cocktail.application.palette.JTextFieldCocktail getJTextFieldCocktailUfr() {
		return jTextFieldCocktailUfr;
	}

	public void setJTextFieldCocktailUfr(
			org.cocktail.application.palette.JTextFieldCocktail textFieldCocktailUfr) {
		jTextFieldCocktailUfr = textFieldCocktailUfr;
	}

	/** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonCocktailFermer = new org.cocktail.application.palette.JButtonCocktail();
        jLabelCocktailIcone = new org.cocktail.application.palette.JLabelCocktail();
        jButtonCocktailImpression = new org.cocktail.application.palette.JButtonCocktail();
        jPanel1 = new javax.swing.JPanel();
        jLabelCocktail3 = new org.cocktail.application.palette.JLabelCocktail();
        jLabelCocktail2 = new org.cocktail.application.palette.JLabelCocktail();
        jTextFieldCocktailNumero = new org.cocktail.application.palette.JTextFieldCocktail();
        jTextFieldCocktailUfr = new org.cocktail.application.palette.JTextFieldCocktail();
        jLabelPieceNum = new javax.swing.JLabel();
        jTextFieldCocktailNumeroPiece = new org.cocktail.application.palette.JTextFieldCocktail();
        jPanel2 = new javax.swing.JPanel();
        jCheckBoxBord = new javax.swing.JCheckBox();
        jCheckBoxMandats = new javax.swing.JCheckBox();
        jCheckBoxBordJourn = new javax.swing.JCheckBox();
        jCheckBoxRecapDepenses = new javax.swing.JCheckBox();
        jCheckBoxTitre = new javax.swing.JCheckBox();
        jCheckBoxSommesAPayer = new javax.swing.JCheckBox();
        jCheckBoxBulletinPerception = new javax.swing.JCheckBox();
        jCheckBoxBulletinLiquidation = new javax.swing.JCheckBox();
        jCheckBoxAnnexeTitreCo = new javax.swing.JCheckBox();

        jButtonCocktailFermer.setText("Fermer");

        jButtonCocktailImpression.setText("Imprimer PDF");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Références"));

        jLabelCocktail3.setLabelFor(jTextFieldCocktailNumero);
        jLabelCocktail3.setText("Num. Bordereau :");

        jLabelCocktail2.setLabelFor(jTextFieldCocktailUfr);
        jLabelCocktail2.setText("UFR  : ");

        jTextFieldCocktailNumero.setBackground(new java.awt.Color(255, 255, 255));
        jTextFieldCocktailNumero.setBorder(javax.swing.BorderFactory.createEmptyBorder(2, 4, 2, 4));
        jTextFieldCocktailNumero.setEditable(false);
        jTextFieldCocktailNumero.setFont(jTextFieldCocktailNumero.getFont().deriveFont(jTextFieldCocktailNumero.getFont().getStyle() | java.awt.Font.BOLD));
        jTextFieldCocktailNumero.setMargin(new java.awt.Insets(1, 4, 1, 1));
        jTextFieldCocktailNumero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCocktailNumeroActionPerformed(evt);
            }
        });

        jTextFieldCocktailUfr.setBackground(new java.awt.Color(255, 255, 255));
        jTextFieldCocktailUfr.setBorder(javax.swing.BorderFactory.createEmptyBorder(2, 4, 2, 4));
        jTextFieldCocktailUfr.setEditable(false);
        jTextFieldCocktailUfr.setFont(jTextFieldCocktailUfr.getFont().deriveFont(jTextFieldCocktailUfr.getFont().getStyle() | java.awt.Font.BOLD));
        jTextFieldCocktailUfr.setMargin(new java.awt.Insets(1, 4, 1, 1));
        jTextFieldCocktailUfr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCocktailUfrActionPerformed(evt);
            }
        });

        jLabelPieceNum.setLabelFor(jTextFieldCocktailNumeroPiece);
        jLabelPieceNum.setText("Num. Mandat / ORV :");

        jTextFieldCocktailNumeroPiece.setBackground(new java.awt.Color(255, 255, 255));
        jTextFieldCocktailNumeroPiece.setBorder(javax.swing.BorderFactory.createEmptyBorder(2, 4, 2, 4));
        jTextFieldCocktailNumeroPiece.setEditable(false);
        jTextFieldCocktailNumeroPiece.setFont(jTextFieldCocktailNumeroPiece.getFont().deriveFont(jTextFieldCocktailNumeroPiece.getFont().getStyle() | java.awt.Font.BOLD));
        jTextFieldCocktailNumeroPiece.setMargin(new java.awt.Insets(1, 4, 1, 1));
        jTextFieldCocktailNumeroPiece.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCocktailNumeroPieceActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabelCocktail2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabelPieceNum)
                    .add(jLabelCocktail3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 55, Short.MAX_VALUE)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jTextFieldCocktailUfr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 195, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextFieldCocktailNumero, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                    .add(jTextFieldCocktailNumeroPiece, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(new java.awt.Component[] {jTextFieldCocktailNumero, jTextFieldCocktailNumeroPiece, jTextFieldCocktailUfr}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(7, 7, 7)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextFieldCocktailUfr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabelCocktail2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelCocktail3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextFieldCocktailNumero, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(9, 9, 9)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextFieldCocktailNumeroPiece, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabelPieceNum))
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Documents à imprimer"));

        jCheckBoxBord.setText("Bordereau");
        jCheckBoxBord.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxBordActionPerformed(evt);
            }
        });

        jCheckBoxMandats.setText("Mandat / ORV");

        jCheckBoxBordJourn.setText("Bordereau journal");

        jCheckBoxRecapDepenses.setText("Récapitulatif des dépenses");

        jCheckBoxTitre.setText("Ordre de recette / AOR");

        jCheckBoxSommesAPayer.setText("Avis des sommes à payer");

        jCheckBoxBulletinPerception.setText("Bulletin de perception");

        jCheckBoxBulletinLiquidation.setText("Bulletin de liquidation");

        jCheckBoxAnnexeTitreCo.setText("Annexe Titre collectif");

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jCheckBoxBord)
                    .add(jCheckBoxBordJourn)
                    .add(jCheckBoxMandats)
                    .add(jCheckBoxRecapDepenses))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 48, Short.MAX_VALUE)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jCheckBoxAnnexeTitreCo)
                    .add(jCheckBoxBulletinPerception)
                    .add(jCheckBoxBulletinLiquidation)
                    .add(jCheckBoxSommesAPayer)
                    .add(jCheckBoxTitre))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jCheckBoxBord)
                    .add(jCheckBoxSommesAPayer))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jCheckBoxBordJourn)
                    .add(jCheckBoxTitre))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jCheckBoxMandats)
                    .add(jCheckBoxBulletinPerception))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jCheckBoxRecapDepenses)
                    .add(jCheckBoxBulletinLiquidation))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jCheckBoxAnnexeTitreCo)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(layout.createSequentialGroup()
                                .add(jLabelCocktailIcone, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 156, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 122, Short.MAX_VALUE)
                                .add(jButtonCocktailFermer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 114, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(jButtonCocktailImpression, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(21, 21, 21))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabelCocktailIcone, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 132, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButtonCocktailFermer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jButtonCocktailImpression, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(17, 17, 17))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldCocktailUfrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCocktailUfrActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_jTextFieldCocktailUfrActionPerformed

    private void jTextFieldCocktailNumeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCocktailNumeroActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_jTextFieldCocktailNumeroActionPerformed

    private void jCheckBoxBordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxBordActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBoxBordActionPerformed

    private void jTextFieldCocktailNumeroPieceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCocktailNumeroPieceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldCocktailNumeroPieceActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public org.cocktail.application.palette.JButtonCocktail jButtonCocktailFermer;
    public org.cocktail.application.palette.JButtonCocktail jButtonCocktailImpression;
    public javax.swing.JCheckBox jCheckBoxAnnexeTitreCo;
    public javax.swing.JCheckBox jCheckBoxBord;
    public javax.swing.JCheckBox jCheckBoxBordJourn;
    public javax.swing.JCheckBox jCheckBoxBulletinLiquidation;
    public javax.swing.JCheckBox jCheckBoxBulletinPerception;
    public javax.swing.JCheckBox jCheckBoxMandats;
    public javax.swing.JCheckBox jCheckBoxRecapDepenses;
    public javax.swing.JCheckBox jCheckBoxSommesAPayer;
    public javax.swing.JCheckBox jCheckBoxTitre;
    public org.cocktail.application.palette.JLabelCocktail jLabelCocktail2;
    public org.cocktail.application.palette.JLabelCocktail jLabelCocktail3;
    public org.cocktail.application.palette.JLabelCocktail jLabelCocktailIcone;
    public javax.swing.JLabel jLabelPieceNum;
    public javax.swing.JPanel jPanel1;
    public javax.swing.JPanel jPanel2;
    public org.cocktail.application.palette.JTextFieldCocktail jTextFieldCocktailNumero;
    public org.cocktail.application.palette.JTextFieldCocktail jTextFieldCocktailNumeroPiece;
    public org.cocktail.application.palette.JTextFieldCocktail jTextFieldCocktailUfr;
    // End of variables declaration//GEN-END:variables

    public JCheckBox getjCheckBoxAnnexeTitreCo() {
        return jCheckBoxAnnexeTitreCo;
    }

    public JLabel getjLabelPieceNum() {
        return jLabelPieceNum;
    }

    public JTextFieldCocktail getjTextFieldCocktailNumeroPiece() {
        return jTextFieldCocktailNumeroPiece;
    }

    public JCheckBox getjCheckBoxBord() {
        return jCheckBoxBord;
    }

    public JCheckBox getjCheckBoxBordJourn() {
        return jCheckBoxBordJourn;
    }

    public JCheckBox getjCheckBoxBulletinLiquidation() {
        return jCheckBoxBulletinLiquidation;
    }

    public JCheckBox getjCheckBoxBulletinPerception() {
        return jCheckBoxBulletinPerception;
    }

    public JCheckBox getjCheckBoxMandats() {
        return jCheckBoxMandats;
    }

    public JCheckBox getjCheckBoxRecapDepenses() {
        return jCheckBoxRecapDepenses;
    }

    public JCheckBox getjCheckBoxSommesAPayer() {
        return jCheckBoxSommesAPayer;
    }

    public JCheckBox getjCheckBoxTitre() {
        return jCheckBoxTitre;
    }
    
}
