/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.abricot.client.gui;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import org.cocktail.application.palette.JPanelCocktail;

public class MandatAlgoNib extends JPanelCocktail {

    /** Creates new form MandatAlgoNibnb */
    public MandatAlgoNib() {
        initComponents();
    }
    
    public org.cocktail.application.palette.JButtonCocktail getJButtonCocktailAnnuler() {
		return jButtonCocktailAnnuler;
	}

	public void setJButtonCocktailAnnuler(
			org.cocktail.application.palette.JButtonCocktail buttonCocktailAnnuler) {
		jButtonCocktailAnnuler = buttonCocktailAnnuler;
	}

	public org.cocktail.application.palette.JButtonCocktail getJButtonCocktailValider() {
		return jButtonCocktailValider;
	}

	public void setJButtonCocktailValider(
			org.cocktail.application.palette.JButtonCocktail buttonCocktailValider) {
		jButtonCocktailValider = buttonCocktailValider;
	}

public JRadioButton getjRadioButtonALGO_bordereau_1D1M() {
        return jRadioButtonALGO_bordereau_1D1M;
    }

    public ButtonGroup getButtonGroup1() {
        return buttonGroup1;
    }

    public JRadioButton getjRadioButtonALGO_ndep_mand_fou_rib_pco_mod() {
        return jRadioButtonALGO_ndep_mand_fou_rib_pco_mod;
    }

    public JRadioButton getjRadioButtonALGO_ndep_mand_org_fou_rib_pco_mod() {
        return jRadioButtonALGO_ndep_mand_org_fou_rib_pco_mod;
    }



	/** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jButtonCocktailAnnuler = new org.cocktail.application.palette.JButtonCocktail();
        jButtonCocktailValider = new org.cocktail.application.palette.JButtonCocktail();
        jTextPane1 = new javax.swing.JTextPane();
        jPanel1 = new javax.swing.JPanel();
        jRadioButtonALGO_bordereau_1D1M = new javax.swing.JRadioButton();
        jRadioButtonALGO_ndep_mand_org_fou_rib_pco_mod = new javax.swing.JRadioButton();
        jRadioButtonALGO_ndep_mand_fou_rib_pco_mod = new javax.swing.JRadioButton();

        setForeground(new java.awt.Color(255, 0, 51));

        jButtonCocktailAnnuler.setText("Annuler");
        jButtonCocktailAnnuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCocktailAnnulerActionPerformed(evt);
            }
        });

        jButtonCocktailValider.setText("Valider");

        jTextPane1.setFont(jTextPane1.getFont().deriveFont(jTextPane1.getFont().getStyle() | java.awt.Font.BOLD));
        jTextPane1.setText("Dans les choix suivants,\non crée un mandat par fournisseur, par imputation et par mode de paiement.\nLes dépenses sur Ressources Affectées sont traitées sur des mandats spécifiques.");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        buttonGroup1.add(jRadioButtonALGO_bordereau_1D1M);
        jRadioButtonALGO_bordereau_1D1M.setText("Un mandat par facture");

        buttonGroup1.add(jRadioButtonALGO_ndep_mand_org_fou_rib_pco_mod);
        jRadioButtonALGO_ndep_mand_org_fou_rib_pco_mod.setText("Un mandat pour plusieurs factures de meme  ligne budgetaire");

        buttonGroup1.add(jRadioButtonALGO_ndep_mand_fou_rib_pco_mod);
        jRadioButtonALGO_ndep_mand_fou_rib_pco_mod.setText("Un mandat pour plusieurs factures");

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jRadioButtonALGO_bordereau_1D1M)
                    .add(jRadioButtonALGO_ndep_mand_org_fou_rib_pco_mod)
                    .add(jRadioButtonALGO_ndep_mand_fou_rib_pco_mod))
                .addContainerGap(200, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jRadioButtonALGO_bordereau_1D1M)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jRadioButtonALGO_ndep_mand_org_fou_rib_pco_mod)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jRadioButtonALGO_ndep_mand_fou_rib_pco_mod)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jTextPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 525, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(jButtonCocktailAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 133, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 255, Short.MAX_VALUE)
                        .add(jButtonCocktailValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 141, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(jTextPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 67, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(11, 11, 11)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jButtonCocktailAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButtonCocktailValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCocktailAnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCocktailAnnulerActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_jButtonCocktailAnnulerActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.ButtonGroup buttonGroup1;
    public org.cocktail.application.palette.JButtonCocktail jButtonCocktailAnnuler;
    public org.cocktail.application.palette.JButtonCocktail jButtonCocktailValider;
    public javax.swing.JPanel jPanel1;
    public javax.swing.JRadioButton jRadioButtonALGO_bordereau_1D1M;
    public javax.swing.JRadioButton jRadioButtonALGO_ndep_mand_fou_rib_pco_mod;
    public javax.swing.JRadioButton jRadioButtonALGO_ndep_mand_org_fou_rib_pco_mod;
    public javax.swing.JTextPane jTextPane1;
    // End of variables declaration//GEN-END:variables
    
}
