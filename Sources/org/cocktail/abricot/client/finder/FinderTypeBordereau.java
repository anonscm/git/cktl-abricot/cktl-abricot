/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.abricot.client.finder;


import org.cocktail.abricot.client.ApplicationClient;
import org.cocktail.abricot.client.Privileges;
import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOTypeBordereau;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderTypeBordereau {


	static public NSMutableArrayDisplayGroup findTypesDeBordereaux(ApplicationCocktail app) {

		NSMutableArrayDisplayGroup tmpFetch = new NSMutableArrayDisplayGroup();
		NSMutableArrayDisplayGroup tmpResult = new NSMutableArrayDisplayGroup();

		ApplicationClient myApp = ((ApplicationClient) app);

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOTypeBordereau.TBO_TYPE_KEY, EOQualifier.QualifierOperatorEqual, "BTME"));
		quals.addObject(new EOKeyValueQualifier(EOTypeBordereau.TBO_TYPE_KEY, EOQualifier.QualifierOperatorEqual, "BTMEP"));
		quals.addObject(new EOKeyValueQualifier(EOTypeBordereau.TBO_TYPE_KEY, EOQualifier.QualifierOperatorEqual, "BTTE"));
		quals.addObject(new EOKeyValueQualifier(EOTypeBordereau.TBO_TYPE_KEY, EOQualifier.QualifierOperatorLike, "BTPI*"));
		quals.addObject(new EOKeyValueQualifier(EOTypeBordereau.TBO_TYPE_KEY, EOQualifier.QualifierOperatorLike, "BTMS"));

		tmpFetch = Finder.find(app, EOTypeBordereau.ENTITY_NAME, null, new EOOrQualifier(quals));

		boolean found = false;
		for (int i = 0; i < myApp.getMesPrivilegesImpressions().count() && !found; i++) {
			if (((Privileges) myApp.getMesPrivilegesImpressions().objectAtIndex(i)).getKey().equals(Privileges.IMPGEST)) {
				tmpResult.addObjectsFromArray(tmpFetch);
				found = true;
			}
		}

		if (!found) {
			String tmpKey;

			for (int i = 0; i < ((ApplicationClient) app).getMesPrivilegesImpressions().count(); i++) {
				tmpKey = ((Privileges) ((ApplicationClient) app).getMesPrivilegesImpressions().objectAtIndex(i)).getKey();
				for (int j = 0; j < tmpFetch.count(); j++) {
					if (tmpKey.equals(((EOTypeBordereau) tmpFetch.objectAtIndex(j)).tboTypeCreation()))
						tmpResult.addObject(tmpFetch.objectAtIndex(j));
				}
			}
		}

		return tmpResult;
	}

	
	static public NSMutableArrayDisplayGroup findTypesDeBordereauxDepense(ApplicationCocktail app){

		NSMutableArrayDisplayGroup tmpFetch = new NSMutableArrayDisplayGroup();
		NSMutableArrayDisplayGroup tmpResult = new NSMutableArrayDisplayGroup();

		tmpFetch=Finder.find(app, EOTypeBordereau.ENTITY_NAME, null, null);
		String tmpKey;

		for (int i = 0; i < ((ApplicationClient)app).getMesPrivilegesDepense().count(); i++) {
			tmpKey=((Privileges)((ApplicationClient)app).getMesPrivilegesDepense().objectAtIndex(i)).getKey();
			for (int j = 0; j < tmpFetch.count(); j++) {
				if (tmpKey.equals(((EOTypeBordereau)tmpFetch.objectAtIndex(j)).tboTypeCreation()))
					tmpResult.addObject(tmpFetch.objectAtIndex(j));
			}
		}

		return tmpResult;
	}


	static public NSMutableArrayDisplayGroup findTypesDeBordereauxRecette(ApplicationCocktail app) {

		NSMutableArrayDisplayGroup tmpFetch = new NSMutableArrayDisplayGroup();
		NSMutableArrayDisplayGroup tmpResult = new NSMutableArrayDisplayGroup();

		tmpFetch = Finder.find(app, EOTypeBordereau.ENTITY_NAME, null, null);
		String tmpKey;

		for (int i = 0; i < ((ApplicationClient) app).getMesPrivilegesRecette().count(); i++) {
			tmpKey = ((Privileges) ((ApplicationClient) app).getMesPrivilegesRecette().objectAtIndex(i)).getKey();
			for (int j = 0; j < tmpFetch.count(); j++) {
				if (tmpKey.equals(((EOTypeBordereau) tmpFetch.objectAtIndex(j)).tboTypeCreation())) {
					tmpResult.addObject(tmpFetch.objectAtIndex(j));
				}
			}
		}
		return tmpResult;
	}


	static public NSMutableArrayDisplayGroup findTypesDeBordereauxPaye(ApplicationCocktail app) {

		NSMutableArrayDisplayGroup tmpFetch = new NSMutableArrayDisplayGroup();
		NSMutableArrayDisplayGroup tmpResult = new NSMutableArrayDisplayGroup();

		tmpFetch = Finder.find(app, EOTypeBordereau.ENTITY_NAME, null, null);
		String tmpKey;

		for (int i = 0; i < ((ApplicationClient) app).getMesPrivilegesPaye().count(); i++) {
			tmpKey = ((Privileges) ((ApplicationClient) app).getMesPrivilegesPaye().objectAtIndex(i)).getKey();
			for (int j = 0; j < tmpFetch.count(); j++) {
				if (tmpKey.equals(((EOTypeBordereau) tmpFetch.objectAtIndex(j)).tboTypeCreation())) {
					tmpResult.addObject(tmpFetch.objectAtIndex(j));
				}
			}
		}

		return tmpResult;
	}
	
	static public NSMutableArrayDisplayGroup findTypesDeBordereauxVisa(ApplicationCocktail app)	{

		NSArray leSort = null;
		EOQualifier aQualifier = null;
		NSMutableArrayDisplayGroup tmpFetch = new NSMutableArrayDisplayGroup();
		NSMutableArrayDisplayGroup tmpResult = new NSMutableArrayDisplayGroup();
		// le sort 

		tmpFetch=Finder.find(app, EOTypeBordereau.ENTITY_NAME, leSort, aQualifier);
		String tmpKey;

		for (int i = 0; i < ((ApplicationClient)app).getMesPrivilegesVisa().count(); i++) {
			tmpKey=((Privileges)((ApplicationClient)app).getMesPrivilegesVisa().objectAtIndex(i)).getKey();
			for (int j = 0; j < tmpFetch.count(); j++) {
				if (tmpKey.equals(((EOTypeBordereau)tmpFetch.objectAtIndex(j)).tboTypeCreation()))
					tmpResult.addObject(tmpFetch.objectAtIndex(j));
			}
		}
		return tmpResult;
	}
	

}
