/*
 * Created on 27 sept. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.abricot.client.finder;

import org.cocktail.application.client.eof.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;


/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderExercice {
	

/**
 * 
 * Liste des exercices depuis 2004
 * 
 * @param ec
 * @return
 */
	public static NSArray findExercices(EOEditingContext ec)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(
				EOExercice.EXE_EXERCICE_KEY + " >= 2004 and (" + 
				EOExercice.EXE_STAT_KEY  + " = 'C' or " +
				EOExercice.EXE_STAT_KEY  + " = 'R' or " +
				EOExercice.EXE_STAT_KEY  + " = 'O' or " +
				EOExercice.EXE_STAT_KEY  + " = 'P')", null);
		
		NSArray mySort = new NSArray(new EOSortOrdering(EOExercice.EXE_EXERCICE_KEY, EOSortOrdering.CompareDescending));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOExercice.ENTITY_NAME, myQualifier, mySort);
		
		return ec.objectsWithFetchSpecification(fs);
		
	}
	
	/**
	 * 
	 * Exercices ouverts (Etat Restreint ou Ouvert)
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray findExercicesOuverts(EOEditingContext ec)	{
		
		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(
				EOExercice.EXE_STAT_KEY  + " = 'R' or " +
				EOExercice.EXE_STAT_KEY  + " = 'O' ", null);
		
		NSArray mySort = new NSArray(new EOSortOrdering(EOExercice.EXE_EXERCICE_KEY, EOSortOrdering.CompareAscending));
		
		EOFetchSpecification fs = new EOFetchSpecification(EOExercice.ENTITY_NAME, myQualifier, mySort);
		
		return ec.objectsWithFetchSpecification(fs);
		
	}
	
	
	/**
	 * 
	 * Exercice Courant (Etat = 'O'uvert)
	 * 
	 * @param ec
	 * @return
	 */
	public static EOExercice exerciceCourant(EOEditingContext ec)	{
		
		try {
			EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOExercice.EXE_STAT_KEY + " = 'O'", null);

			EOFetchSpecification fs = new EOFetchSpecification(EOExercice.ENTITY_NAME, myQualifier, null);

			return (EOExercice)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e)	{return null;}
		
	}


}
