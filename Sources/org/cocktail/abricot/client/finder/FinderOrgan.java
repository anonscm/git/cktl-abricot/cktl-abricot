/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.abricot.client.finder;


import java.util.TimeZone;

import org.cocktail.abricot.client.ApplicationClient;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.eof.EOUtilisateurOrgan;
import org.cocktail.client.common.utilities.DateCtrl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author cpinsard
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FinderOrgan {
	
	
	/**
	 * 
	 * @param ec
	 * @param niveau
	 * @return
	 */
		public static NSArray findOrgansForUtilisateur(EOEditingContext ec, EOUtilisateur utilisateur, EOExercice exercice)  {

			try {

				NSMutableArray mySort =new NSMutableArray();
				mySort.addObject(new EOSortOrdering(EOOrgan.ORG_ETAB_KEY, EOSortOrdering.CompareAscending));
				mySort.addObject(new EOSortOrdering(EOOrgan.ORG_UB_KEY, EOSortOrdering.CompareAscending));
				mySort.addObject(new EOSortOrdering(EOOrgan.ORG_CR_KEY, EOSortOrdering.CompareAscending));
				mySort.addObject(new EOSortOrdering(EOOrgan.ORG_SOUSCR_KEY, EOSortOrdering.CompareAscending));

				NSMutableArray mesQualifiers = new NSMutableArray();

				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
						EOOrgan.UTILISATEUR_ORGANS_KEY+"."+EOUtilisateurOrgan.UTILISATEUR_KEY + " = %@", new NSArray(utilisateur)));

				mesQualifiers.addObject(getQualifierForPeriodeAndExercice(EOOrgan.ORG_DATE_OUVERTURE_KEY, 
						EOOrgan.ORG_DATE_CLOTURE_KEY, exercice));
				
				EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
				fs.setRefreshesRefetchedObjects(true);

				return ec.objectsWithFetchSpecification(fs);
			}
			catch (Exception e)	{
				e.printStackTrace();
				return new NSArray();
			}
	    }
		
		
	    public static EOQualifier getQualifierForPeriodeAndExercice(String pathKeyOuverture, String pathKeyCloture, EOExercice exercice){
	        NSMutableArray ou = new NSMutableArray();
	        NSMutableArray et1 = new NSMutableArray();
	        et1.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyCloture+" = nil", null));
	        et1.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyOuverture+" < %@", new NSArray(dateFinForExercice(exercice))));
	        NSMutableArray et2 = new NSMutableArray();
	        et2.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyCloture+" >= %@", new NSArray(dateDebutForExercice(exercice))));
	        et2.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyOuverture+" < %@", new NSArray(dateFinForExercice(exercice))));
	        NSMutableArray et3 = new NSMutableArray();
	        et3.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyCloture+" >= %@", new NSArray(dateFinForExercice(exercice))));
	        et3.addObject(EOQualifier.qualifierWithQualifierFormat(pathKeyOuverture+" < %@", new NSArray(dateDebutForExercice(exercice))));
	        ou.addObject(new EOAndQualifier(et1));
	        ou.addObject(new EOAndQualifier(et2));
	        ou.addObject(new EOAndQualifier(et3));
	        return new EOOrQualifier(ou);
	    }

	    private static NSTimestamp dateDebutForExercice(EOExercice exercice){
	        String timeZone = ((ApplicationClient)ApplicationClient.sharedApplication()).getApplicationParametre("GRHUM_TIMEZONE");
	        if(timeZone==null)
	            timeZone = "Europe/Paris";
	        //le 01/01/de l'anne de l'exercice
	        return new NSTimestamp(exercice.exeExercice().intValue(),1,1,0,0,0,TimeZone.getTimeZone(timeZone));
	    }
	    
	    private static NSTimestamp dateFinForExercice(EOExercice exercice){
	        String timeZone = ((ApplicationClient)ApplicationClient.sharedApplication()).getApplicationParametre("GRHUM_TIMEZONE");
	        if(timeZone==null)
	            timeZone = "Europe/Paris";
	        //lle 01/01/de l'anne suivant celle de l'exercice
	        return new NSTimestamp(exercice.exeExercice().intValue()+1,1,1,0,0,0,TimeZone.getTimeZone(timeZone));
	    }
		
	/**
	 * 
	 * @param ec
	 * @param niveau
	 * @return
	 */
		public static NSArray rechercherLignesBudgetairesNiveau(EOEditingContext ec,EOExercice exercice, Number niveau)  {
	        NSMutableArray mySort=new NSMutableArray(EOSortOrdering.sortOrderingWithKey(EOOrgan.ORG_UB_KEY,EOSortOrdering.CompareAscending));
	        mySort.addObject(EOSortOrdering.sortOrderingWithKey(EOOrgan.ORG_CR_KEY,EOSortOrdering.CompareAscending));
	        mySort.addObject(EOSortOrdering.sortOrderingWithKey(EOOrgan.ORG_SOUSCR_KEY,EOSortOrdering.CompareAscending));

	        NSMutableArray mesQualifiers = new NSMutableArray();

	        if (exercice != null) {

	        	NSMutableArray mesOrQualifiers = new NSMutableArray();
		        
		        mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_DATE_CLOTURE_KEY + " = nil", new NSArray(exercice)));

		        mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_DATE_CLOTURE_KEY + " >=%@", new NSArray(DateCtrl.stringToDate("01/01/"+exercice.exeExercice().toString()))));

		        mesQualifiers.addObject(new EOOrQualifier(mesOrQualifiers));

		        
		        mesOrQualifiers = new NSMutableArray();
		        mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_DATE_OUVERTURE_KEY + " = nil", new NSArray(exercice)));

		        mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_DATE_OUVERTURE_KEY + " <=%@", new NSArray(DateCtrl.stringToDate("31/12/"+exercice.exeExercice().toString()))));

		        mesQualifiers.addObject(new EOOrQualifier(mesOrQualifiers));

	        }
	        
	        mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + " =%@", new NSArray(niveau)));
	        	       
	        System.out
					.println("FinderOrgan.rechercherLignesBudgetairesNiveau() " + new EOAndQualifier(mesQualifiers));
	        
	        EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
	        fs.setRefreshesRefetchedObjects(true);
	        return ec.objectsWithFetchSpecification(fs);
	    }

		public static EOOrgan findOrganForUb(EOEditingContext ec, String ub)  {

			try {
				NSMutableArray mesQualifiers = new NSMutableArray();

				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + " = %@", new NSArray(new Integer(2))));

				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_UB_KEY + " = %@", new NSArray(ub)));

				EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
				fs.setRefreshesRefetchedObjects(true);

				return (EOOrgan)ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
			}
			catch (Exception e) {
				return null;
			}
	    }
			
		public static EOOrgan findUb(EOEditingContext ec, String cr, String sousCr)  {

			try {
				NSMutableArray mesQualifiers = new NSMutableArray();

				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_CR_KEY + " = %@", new NSArray(cr)));

				if (sousCr != null) {
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + " = %@", new NSArray(new Integer(4))));
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_SOUSCR_KEY + " = %@", new NSArray(sousCr)));
				}
				else
					mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + " = %@", new NSArray(new Integer(3))));
					
				EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
				fs.setRefreshesRefetchedObjects(true);

				System.out.println("FinderOrgan.findUb() QUALIFIER : " + new EOAndQualifier(mesQualifiers));
								
				return findOrganForUb(ec, ((EOOrgan)ec.objectsWithFetchSpecification(fs).objectAtIndex(0)).orgUb());
			}
			catch (Exception e) {
				e.printStackTrace();
				return null;
			}
	    }

	/**
	 * @param ec
	 * @param exercice
	 * @param utilisateurOrgans
	 * @return
	 */
	public static NSArray findUbs(EOEditingContext ec, EOExercice exercice, NSArray organs) {

		NSArray composantes = FinderOrgan.rechercherLignesBudgetairesNiveau(ec, exercice, new Integer(2));
		NSArray privateOrgans = organs;

		NSMutableArray ubsAutorisees = new NSMutableArray();
		for (int i = 0; i < composantes.count(); i++) {
			EOOrgan organ = (EOOrgan) composantes.objectAtIndex(i);
			if (privateOrgans.containsObject(organ))
				ubsAutorisees.addObject(organ);
		}

		return ubsAutorisees;

	}
			
    
}
