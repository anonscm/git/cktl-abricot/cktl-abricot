// _EOVAbricotPrestationsInternes.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVAbricotPrestationsInternes.java instead.
package org.cocktail.abricot.client.eof.modele;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOVAbricotPrestationsInternes extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "VAbricotPrestationsInternes";
	public static final String ENTITY_TABLE_NAME = "MARACUJA.V_ABRICOT_PRESTATIONS_INTERNES";



	// Attributes


	public static final String DEP_ID_KEY = "depId";
	public static final String ENG_ID_KEY = "engId";
	public static final String FAC_ID_KEY = "facId";
	public static final String PDR_ID_KEY = "pdrId";
	public static final String PEF_ID_KEY = "pefId";
	public static final String PREST_ID_KEY = "prestId";
	public static final String REC_ID_KEY = "recId";
	public static final String TBO_ORDRE_KEY = "tboOrdre";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String FAC_ID_COLKEY = "FAC_ID";
	public static final String PDR_ID_COLKEY = "PDR_ID";
	public static final String PEF_ID_COLKEY = "PEF_ID";
	public static final String PREST_ID_COLKEY = "PREST_ID";
	public static final String REC_ID_COLKEY = "REC_ID";
	public static final String TBO_ORDRE_COLKEY = "TBO_ORDRE";



	// Relationships
	public static final String TYPE_BORDEREAU_KEY = "typeBordereau";
	public static final String V_ABRICOT_DEPENSE_PI_KEY = "vAbricotDepensePi";
	public static final String V_ABRICOT_RECETTE_PI_KEY = "vAbricotRecettePi";



	// Accessors methods
  public Integer depId() {
    return (Integer) storedValueForKey(DEP_ID_KEY);
  }

  public void setDepId(Integer value) {
    takeStoredValueForKey(value, DEP_ID_KEY);
  }

  public Integer engId() {
    return (Integer) storedValueForKey(ENG_ID_KEY);
  }

  public void setEngId(Integer value) {
    takeStoredValueForKey(value, ENG_ID_KEY);
  }

  public Integer facId() {
    return (Integer) storedValueForKey(FAC_ID_KEY);
  }

  public void setFacId(Integer value) {
    takeStoredValueForKey(value, FAC_ID_KEY);
  }

  public Integer pdrId() {
    return (Integer) storedValueForKey(PDR_ID_KEY);
  }

  public void setPdrId(Integer value) {
    takeStoredValueForKey(value, PDR_ID_KEY);
  }

  public Integer pefId() {
    return (Integer) storedValueForKey(PEF_ID_KEY);
  }

  public void setPefId(Integer value) {
    takeStoredValueForKey(value, PEF_ID_KEY);
  }

  public Integer prestId() {
    return (Integer) storedValueForKey(PREST_ID_KEY);
  }

  public void setPrestId(Integer value) {
    takeStoredValueForKey(value, PREST_ID_KEY);
  }

  public Integer recId() {
    return (Integer) storedValueForKey(REC_ID_KEY);
  }

  public void setRecId(Integer value) {
    takeStoredValueForKey(value, REC_ID_KEY);
  }

  public Integer tboOrdre() {
    return (Integer) storedValueForKey(TBO_ORDRE_KEY);
  }

  public void setTboOrdre(Integer value) {
    takeStoredValueForKey(value, TBO_ORDRE_KEY);
  }

  public org.cocktail.application.client.eof.EOTypeBordereau typeBordereau() {
    return (org.cocktail.application.client.eof.EOTypeBordereau)storedValueForKey(TYPE_BORDEREAU_KEY);
  }

  public void setTypeBordereauRelationship(org.cocktail.application.client.eof.EOTypeBordereau value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeBordereau oldValue = typeBordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_BORDEREAU_KEY);
    }
  }
  
  public org.cocktail.abricot.client.eof.modele.EOVAbricotDepensePi vAbricotDepensePi() {
    return (org.cocktail.abricot.client.eof.modele.EOVAbricotDepensePi)storedValueForKey(V_ABRICOT_DEPENSE_PI_KEY);
  }

  public void setVAbricotDepensePiRelationship(org.cocktail.abricot.client.eof.modele.EOVAbricotDepensePi value) {
    if (value == null) {
    	org.cocktail.abricot.client.eof.modele.EOVAbricotDepensePi oldValue = vAbricotDepensePi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, V_ABRICOT_DEPENSE_PI_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, V_ABRICOT_DEPENSE_PI_KEY);
    }
  }
  
  public org.cocktail.abricot.client.eof.modele.EOVAbricotRecettePi vAbricotRecettePi() {
    return (org.cocktail.abricot.client.eof.modele.EOVAbricotRecettePi)storedValueForKey(V_ABRICOT_RECETTE_PI_KEY);
  }

  public void setVAbricotRecettePiRelationship(org.cocktail.abricot.client.eof.modele.EOVAbricotRecettePi value) {
    if (value == null) {
    	org.cocktail.abricot.client.eof.modele.EOVAbricotRecettePi oldValue = vAbricotRecettePi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, V_ABRICOT_RECETTE_PI_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, V_ABRICOT_RECETTE_PI_KEY);
    }
  }
  

  public static EOVAbricotPrestationsInternes createVAbricotPrestationsInternes(EOEditingContext editingContext, Integer depId
, Integer engId
, Integer facId
, Integer pdrId
, Integer pefId
, Integer prestId
, Integer recId
) {
    EOVAbricotPrestationsInternes eo = (EOVAbricotPrestationsInternes) createAndInsertInstance(editingContext, _EOVAbricotPrestationsInternes.ENTITY_NAME);    
		eo.setDepId(depId);
		eo.setEngId(engId);
		eo.setFacId(facId);
		eo.setPdrId(pdrId);
		eo.setPefId(pefId);
		eo.setPrestId(prestId);
		eo.setRecId(recId);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOVAbricotPrestationsInternes.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOVAbricotPrestationsInternes.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOVAbricotPrestationsInternes localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVAbricotPrestationsInternes)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOVAbricotPrestationsInternes localInstanceIn(EOEditingContext editingContext, EOVAbricotPrestationsInternes eo) {
    EOVAbricotPrestationsInternes localInstance = (eo == null) ? null : (EOVAbricotPrestationsInternes)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOVAbricotPrestationsInternes#localInstanceIn a la place.
   */
	public static EOVAbricotPrestationsInternes localInstanceOf(EOEditingContext editingContext, EOVAbricotPrestationsInternes eo) {
		return EOVAbricotPrestationsInternes.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOVAbricotPrestationsInternes fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOVAbricotPrestationsInternes fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVAbricotPrestationsInternes eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVAbricotPrestationsInternes)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVAbricotPrestationsInternes fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVAbricotPrestationsInternes fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVAbricotPrestationsInternes eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVAbricotPrestationsInternes)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOVAbricotPrestationsInternes fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVAbricotPrestationsInternes eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVAbricotPrestationsInternes ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVAbricotPrestationsInternes fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
