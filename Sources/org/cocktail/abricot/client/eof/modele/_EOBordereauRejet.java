// _EOBordereauRejet.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBordereauRejet.java instead.
package org.cocktail.abricot.client.eof.modele;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOBordereauRejet extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "BordereauRejet";
	public static final String ENTITY_TABLE_NAME = "MARACUJA.BORDEREAU_REJET";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "brjOrdre";

	public static final String BRJ_ETAT_KEY = "brjEtat";
	public static final String BRJ_NUM_KEY = "brjNum";
	public static final String BRJ_ORDRE_BIS_KEY = "brjOrdreBis";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String TBO_ORDRE_KEY = "tboOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

// Attributs non visibles
	public static final String BRJ_ORDRE_KEY = "brjOrdre";

//Colonnes dans la base de donnees
	public static final String BRJ_ETAT_COLKEY = "BRJ_ETAT";
	public static final String BRJ_NUM_COLKEY = "BRJ_NUM";
	public static final String BRJ_ORDRE_BIS_COLKEY = "BRJ_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String TBO_ORDRE_COLKEY = "TBO_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";

	public static final String BRJ_ORDRE_COLKEY = "BRJ_ORDRE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String MANDATS_KEY = "mandats";
	public static final String TITRES_KEY = "titres";
	public static final String TYPE_BORDEREAU_KEY = "typeBordereau";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String brjEtat() {
    return (String) storedValueForKey(BRJ_ETAT_KEY);
  }

  public void setBrjEtat(String value) {
    takeStoredValueForKey(value, BRJ_ETAT_KEY);
  }

  public Integer brjNum() {
    return (Integer) storedValueForKey(BRJ_NUM_KEY);
  }

  public void setBrjNum(Integer value) {
    takeStoredValueForKey(value, BRJ_NUM_KEY);
  }

  public Integer brjOrdreBis() {
    return (Integer) storedValueForKey(BRJ_ORDRE_BIS_KEY);
  }

  public void setBrjOrdreBis(Integer value) {
    takeStoredValueForKey(value, BRJ_ORDRE_BIS_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public String gesCode() {
    return (String) storedValueForKey(GES_CODE_KEY);
  }

  public void setGesCode(String value) {
    takeStoredValueForKey(value, GES_CODE_KEY);
  }

  public Integer tboOrdre() {
    return (Integer) storedValueForKey(TBO_ORDRE_KEY);
  }

  public void setTboOrdre(Integer value) {
    takeStoredValueForKey(value, TBO_ORDRE_KEY);
  }

  public Integer utlOrdre() {
    return (Integer) storedValueForKey(UTL_ORDRE_KEY);
  }

  public void setUtlOrdre(Integer value) {
    takeStoredValueForKey(value, UTL_ORDRE_KEY);
  }

  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOTypeBordereau typeBordereau() {
    return (org.cocktail.application.client.eof.EOTypeBordereau)storedValueForKey(TYPE_BORDEREAU_KEY);
  }

  public void setTypeBordereauRelationship(org.cocktail.application.client.eof.EOTypeBordereau value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeBordereau oldValue = typeBordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_BORDEREAU_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOUtilisateur utilisateur() {
    return (org.cocktail.application.client.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.application.client.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray mandats() {
    return (NSArray)storedValueForKey(MANDATS_KEY);
  }

  public NSArray mandats(EOQualifier qualifier) {
    return mandats(qualifier, null);
  }

  public NSArray mandats(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = mandats();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToMandatsRelationship(org.cocktail.abricot.client.eof.modele.EOMandat object) {
    addObjectToBothSidesOfRelationshipWithKey(object, MANDATS_KEY);
  }

  public void removeFromMandatsRelationship(org.cocktail.abricot.client.eof.modele.EOMandat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, MANDATS_KEY);
  }

  public org.cocktail.abricot.client.eof.modele.EOMandat createMandatsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Mandat");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, MANDATS_KEY);
    return (org.cocktail.abricot.client.eof.modele.EOMandat) eo;
  }

  public void deleteMandatsRelationship(org.cocktail.abricot.client.eof.modele.EOMandat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, MANDATS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllMandatsRelationships() {
    Enumeration objects = mandats().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteMandatsRelationship((org.cocktail.abricot.client.eof.modele.EOMandat)objects.nextElement());
    }
  }

  public NSArray titres() {
    return (NSArray)storedValueForKey(TITRES_KEY);
  }

  public NSArray titres(EOQualifier qualifier) {
    return titres(qualifier, null);
  }

  public NSArray titres(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = titres();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTitresRelationship(org.cocktail.abricot.client.eof.modele.EOTitre object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TITRES_KEY);
  }

  public void removeFromTitresRelationship(org.cocktail.abricot.client.eof.modele.EOTitre object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TITRES_KEY);
  }

  public org.cocktail.abricot.client.eof.modele.EOTitre createTitresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Titre");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TITRES_KEY);
    return (org.cocktail.abricot.client.eof.modele.EOTitre) eo;
  }

  public void deleteTitresRelationship(org.cocktail.abricot.client.eof.modele.EOTitre object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TITRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTitresRelationships() {
    Enumeration objects = titres().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTitresRelationship((org.cocktail.abricot.client.eof.modele.EOTitre)objects.nextElement());
    }
  }


  public static EOBordereauRejet createBordereauRejet(EOEditingContext editingContext, Integer brjOrdreBis
, Integer exeOrdre
, String gesCode
, Integer tboOrdre
, Integer utlOrdre
) {
    EOBordereauRejet eo = (EOBordereauRejet) createAndInsertInstance(editingContext, _EOBordereauRejet.ENTITY_NAME);    
		eo.setBrjOrdreBis(brjOrdreBis);
		eo.setExeOrdre(exeOrdre);
		eo.setGesCode(gesCode);
		eo.setTboOrdre(tboOrdre);
		eo.setUtlOrdre(utlOrdre);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOBordereauRejet.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOBordereauRejet.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOBordereauRejet localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBordereauRejet)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOBordereauRejet localInstanceIn(EOEditingContext editingContext, EOBordereauRejet eo) {
    EOBordereauRejet localInstance = (eo == null) ? null : (EOBordereauRejet)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOBordereauRejet#localInstanceIn a la place.
   */
	public static EOBordereauRejet localInstanceOf(EOEditingContext editingContext, EOBordereauRejet eo) {
		return EOBordereauRejet.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOBordereauRejet fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOBordereauRejet fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBordereauRejet eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBordereauRejet)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBordereauRejet fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBordereauRejet fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBordereauRejet eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBordereauRejet)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOBordereauRejet fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBordereauRejet eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBordereauRejet ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBordereauRejet fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
