// _EOVAbricotRecetteATitrer.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVAbricotRecetteATitrer.java instead.
package org.cocktail.abricot.client.eof.modele;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOVAbricotRecetteATitrer extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "VAbricotRecetteATitrer";
	public static final String ENTITY_TABLE_NAME = "MARACUJA.V_ABRICOT_RECETTE_A_TITRER";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rpcoId";

	public static final String ADR_ADRESSE1_KEY = "adrAdresse1";
	public static final String ADR_ADRESSE2_KEY = "adrAdresse2";
	public static final String ADR_CIVILITE_KEY = "adrCivilite";
	public static final String ADR_CP_KEY = "adrCp";
	public static final String ADR_NOM_KEY = "adrNom";
	public static final String ADR_PRENOM_KEY = "adrPrenom";
	public static final String ADR_VILLE_KEY = "adrVille";
	public static final String BIC_KEY = "bic";
	public static final String C_BANQUE_KEY = "cBanque";
	public static final String C_GUICHET_KEY = "cGuichet";
	public static final String DOMICILIATION_KEY = "domiciliation";
	public static final String EXE_EXERCICE_KEY = "exeExercice";
	public static final String FAC_DATE_SAISIE_KEY = "facDateSaisie";
	public static final String FAC_NUMERO_KEY = "facNumero";
	public static final String FAP_NUMERO_KEY = "fapNumero";
	public static final String IBAN_KEY = "iban";
	public static final String MOD_CODE_KEY = "modCode";
	public static final String MOD_DOM_KEY = "modDom";
	public static final String MOD_LIBELLE_KEY = "modLibelle";
	public static final String NO_COMPTE_KEY = "noCompte";
	public static final String ORG_CR_KEY = "orgCr";
	public static final String ORG_ID_KEY = "orgId";
	public static final String ORG_SOUSCR_KEY = "orgSouscr";
	public static final String ORG_UB_KEY = "orgUb";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PERS_LC_KEY = "persLc";
	public static final String PERS_LIBELLE_KEY = "persLibelle";
	public static final String PERS_TYPE_KEY = "persType";
	public static final String REC_HT_SAISIE_KEY = "recHtSaisie";
	public static final String REC_ID_KEY = "recId";
	public static final String REC_NB_PIECE_KEY = "recNbPiece";
	public static final String REC_NUMERO_KEY = "recNumero";
	public static final String REC_TTC_SAISIE_KEY = "recTtcSaisie";
	public static final String REC_TVA_SAISIE_KEY = "recTvaSaisie";
	public static final String RPCO_HT_SAISIE_KEY = "rpcoHtSaisie";
	public static final String RPCO_ID_KEY = "rpcoId";
	public static final String TBO_ORDRE_KEY = "tboOrdre";
	public static final String TCD_CODE_KEY = "tcdCode";
	public static final String TCD_LIBELLE_KEY = "tcdLibelle";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TTC_KEY = "ttc";
	public static final String TVA_KEY = "tva";
	public static final String UTLNOMPRENOM_KEY = "utlnomprenom";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String ADR_ADRESSE1_COLKEY = "ADR_ADRESSE1";
	public static final String ADR_ADRESSE2_COLKEY = "ADR_ADRESSE2";
	public static final String ADR_CIVILITE_COLKEY = "ADR_CIVILITE";
	public static final String ADR_CP_COLKEY = "ADR_CP";
	public static final String ADR_NOM_COLKEY = "ADR_NOM";
	public static final String ADR_PRENOM_COLKEY = "ADR_PRENOM";
	public static final String ADR_VILLE_COLKEY = "ADR_VILLE";
	public static final String BIC_COLKEY = "BIC";
	public static final String C_BANQUE_COLKEY = "C_BANQUE";
	public static final String C_GUICHET_COLKEY = "C_GUICHET";
	public static final String DOMICILIATION_COLKEY = "DOMICILIATION";
	public static final String EXE_EXERCICE_COLKEY = "EXE_EXERCICE";
	public static final String FAC_DATE_SAISIE_COLKEY = "FAC_DATE_SAISIE";
	public static final String FAC_NUMERO_COLKEY = "FAC_NUMERO";
	public static final String FAP_NUMERO_COLKEY = "FAP_NUMERO";
	public static final String IBAN_COLKEY = "IBAN";
	public static final String MOD_CODE_COLKEY = "MOD_CODE";
	public static final String MOD_DOM_COLKEY = "MOD_DOM";
	public static final String MOD_LIBELLE_COLKEY = "MOD_LIBELLE";
	public static final String NO_COMPTE_COLKEY = "NO_COMPTE";
	public static final String ORG_CR_COLKEY = "ORG_CR";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String ORG_SOUSCR_COLKEY = "ORG_SOUSCR";
	public static final String ORG_UB_COLKEY = "ORG_UB";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PERS_LC_COLKEY = "PERS_LC";
	public static final String PERS_LIBELLE_COLKEY = "PERS_LIBELLE";
	public static final String PERS_TYPE_COLKEY = "PERS_TYPE";
	public static final String REC_HT_SAISIE_COLKEY = "REC_HT_SAISIE";
	public static final String REC_ID_COLKEY = "REC_ID";
	public static final String REC_NB_PIECE_COLKEY = "REC_NB_PIECE";
	public static final String REC_NUMERO_COLKEY = "REC_NUMERO";
	public static final String REC_TTC_SAISIE_COLKEY = "REC_TTC_SAISIE";
	public static final String REC_TVA_SAISIE_COLKEY = "REC_TVA_SAISIE";
	public static final String RPCO_HT_SAISIE_COLKEY = "RPCO_HT_SAISIE";
	public static final String RPCO_ID_COLKEY = "RPCO_ID";
	public static final String TBO_ORDRE_COLKEY = "TBO_ORDRE";
	public static final String TCD_CODE_COLKEY = "TCD_CODE";
	public static final String TCD_LIBELLE_COLKEY = "TCD_LIBELLE";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String TTC_COLKEY = "TTC";
	public static final String TVA_COLKEY = "TVA";
	public static final String UTLNOMPRENOM_COLKEY = "UTLNOMPRENOM";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";



	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String TYPE_BORDEREAU_KEY = "typeBordereau";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String adrAdresse1() {
    return (String) storedValueForKey(ADR_ADRESSE1_KEY);
  }

  public void setAdrAdresse1(String value) {
    takeStoredValueForKey(value, ADR_ADRESSE1_KEY);
  }

  public String adrAdresse2() {
    return (String) storedValueForKey(ADR_ADRESSE2_KEY);
  }

  public void setAdrAdresse2(String value) {
    takeStoredValueForKey(value, ADR_ADRESSE2_KEY);
  }

  public String adrCivilite() {
    return (String) storedValueForKey(ADR_CIVILITE_KEY);
  }

  public void setAdrCivilite(String value) {
    takeStoredValueForKey(value, ADR_CIVILITE_KEY);
  }

  public String adrCp() {
    return (String) storedValueForKey(ADR_CP_KEY);
  }

  public void setAdrCp(String value) {
    takeStoredValueForKey(value, ADR_CP_KEY);
  }

  public String adrNom() {
    return (String) storedValueForKey(ADR_NOM_KEY);
  }

  public void setAdrNom(String value) {
    takeStoredValueForKey(value, ADR_NOM_KEY);
  }

  public String adrPrenom() {
    return (String) storedValueForKey(ADR_PRENOM_KEY);
  }

  public void setAdrPrenom(String value) {
    takeStoredValueForKey(value, ADR_PRENOM_KEY);
  }

  public String adrVille() {
    return (String) storedValueForKey(ADR_VILLE_KEY);
  }

  public void setAdrVille(String value) {
    takeStoredValueForKey(value, ADR_VILLE_KEY);
  }

  public String bic() {
    return (String) storedValueForKey(BIC_KEY);
  }

  public void setBic(String value) {
    takeStoredValueForKey(value, BIC_KEY);
  }

  public String cBanque() {
    return (String) storedValueForKey(C_BANQUE_KEY);
  }

  public void setCBanque(String value) {
    takeStoredValueForKey(value, C_BANQUE_KEY);
  }

  public String cGuichet() {
    return (String) storedValueForKey(C_GUICHET_KEY);
  }

  public void setCGuichet(String value) {
    takeStoredValueForKey(value, C_GUICHET_KEY);
  }

  public String domiciliation() {
    return (String) storedValueForKey(DOMICILIATION_KEY);
  }

  public void setDomiciliation(String value) {
    takeStoredValueForKey(value, DOMICILIATION_KEY);
  }

  public Integer exeExercice() {
    return (Integer) storedValueForKey(EXE_EXERCICE_KEY);
  }

  public void setExeExercice(Integer value) {
    takeStoredValueForKey(value, EXE_EXERCICE_KEY);
  }

  public NSTimestamp facDateSaisie() {
    return (NSTimestamp) storedValueForKey(FAC_DATE_SAISIE_KEY);
  }

  public void setFacDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, FAC_DATE_SAISIE_KEY);
  }

  public Integer facNumero() {
    return (Integer) storedValueForKey(FAC_NUMERO_KEY);
  }

  public void setFacNumero(Integer value) {
    takeStoredValueForKey(value, FAC_NUMERO_KEY);
  }

  public Double fapNumero() {
    return (Double) storedValueForKey(FAP_NUMERO_KEY);
  }

  public void setFapNumero(Double value) {
    takeStoredValueForKey(value, FAP_NUMERO_KEY);
  }

  public String iban() {
    return (String) storedValueForKey(IBAN_KEY);
  }

  public void setIban(String value) {
    takeStoredValueForKey(value, IBAN_KEY);
  }

  public String modCode() {
    return (String) storedValueForKey(MOD_CODE_KEY);
  }

  public void setModCode(String value) {
    takeStoredValueForKey(value, MOD_CODE_KEY);
  }

  public String modDom() {
    return (String) storedValueForKey(MOD_DOM_KEY);
  }

  public void setModDom(String value) {
    takeStoredValueForKey(value, MOD_DOM_KEY);
  }

  public String modLibelle() {
    return (String) storedValueForKey(MOD_LIBELLE_KEY);
  }

  public void setModLibelle(String value) {
    takeStoredValueForKey(value, MOD_LIBELLE_KEY);
  }

  public String noCompte() {
    return (String) storedValueForKey(NO_COMPTE_KEY);
  }

  public void setNoCompte(String value) {
    takeStoredValueForKey(value, NO_COMPTE_KEY);
  }

  public String orgCr() {
    return (String) storedValueForKey(ORG_CR_KEY);
  }

  public void setOrgCr(String value) {
    takeStoredValueForKey(value, ORG_CR_KEY);
  }

  public Integer orgId() {
    return (Integer) storedValueForKey(ORG_ID_KEY);
  }

  public void setOrgId(Integer value) {
    takeStoredValueForKey(value, ORG_ID_KEY);
  }

  public String orgSouscr() {
    return (String) storedValueForKey(ORG_SOUSCR_KEY);
  }

  public void setOrgSouscr(String value) {
    takeStoredValueForKey(value, ORG_SOUSCR_KEY);
  }

  public String orgUb() {
    return (String) storedValueForKey(ORG_UB_KEY);
  }

  public void setOrgUb(String value) {
    takeStoredValueForKey(value, ORG_UB_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public String persLc() {
    return (String) storedValueForKey(PERS_LC_KEY);
  }

  public void setPersLc(String value) {
    takeStoredValueForKey(value, PERS_LC_KEY);
  }

  public String persLibelle() {
    return (String) storedValueForKey(PERS_LIBELLE_KEY);
  }

  public void setPersLibelle(String value) {
    takeStoredValueForKey(value, PERS_LIBELLE_KEY);
  }

  public String persType() {
    return (String) storedValueForKey(PERS_TYPE_KEY);
  }

  public void setPersType(String value) {
    takeStoredValueForKey(value, PERS_TYPE_KEY);
  }

  public java.math.BigDecimal recHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(REC_HT_SAISIE_KEY);
  }

  public void setRecHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_HT_SAISIE_KEY);
  }

  public Integer recId() {
    return (Integer) storedValueForKey(REC_ID_KEY);
  }

  public void setRecId(Integer value) {
    takeStoredValueForKey(value, REC_ID_KEY);
  }

  public Double recNbPiece() {
    return (Double) storedValueForKey(REC_NB_PIECE_KEY);
  }

  public void setRecNbPiece(Double value) {
    takeStoredValueForKey(value, REC_NB_PIECE_KEY);
  }

  public Integer recNumero() {
    return (Integer) storedValueForKey(REC_NUMERO_KEY);
  }

  public void setRecNumero(Integer value) {
    takeStoredValueForKey(value, REC_NUMERO_KEY);
  }

  public java.math.BigDecimal recTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(REC_TTC_SAISIE_KEY);
  }

  public void setRecTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal recTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(REC_TVA_SAISIE_KEY);
  }

  public void setRecTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_TVA_SAISIE_KEY);
  }

  public java.math.BigDecimal rpcoHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RPCO_HT_SAISIE_KEY);
  }

  public void setRpcoHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RPCO_HT_SAISIE_KEY);
  }

  public Integer rpcoId() {
    return (Integer) storedValueForKey(RPCO_ID_KEY);
  }

  public void setRpcoId(Integer value) {
    takeStoredValueForKey(value, RPCO_ID_KEY);
  }

  public Integer tboOrdre() {
    return (Integer) storedValueForKey(TBO_ORDRE_KEY);
  }

  public void setTboOrdre(Integer value) {
    takeStoredValueForKey(value, TBO_ORDRE_KEY);
  }

  public String tcdCode() {
    return (String) storedValueForKey(TCD_CODE_KEY);
  }

  public void setTcdCode(String value) {
    takeStoredValueForKey(value, TCD_CODE_KEY);
  }

  public String tcdLibelle() {
    return (String) storedValueForKey(TCD_LIBELLE_KEY);
  }

  public void setTcdLibelle(String value) {
    takeStoredValueForKey(value, TCD_LIBELLE_KEY);
  }

  public Integer tcdOrdre() {
    return (Integer) storedValueForKey(TCD_ORDRE_KEY);
  }

  public void setTcdOrdre(Integer value) {
    takeStoredValueForKey(value, TCD_ORDRE_KEY);
  }

  public java.math.BigDecimal ttc() {
    return (java.math.BigDecimal) storedValueForKey(TTC_KEY);
  }

  public void setTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TTC_KEY);
  }

  public java.math.BigDecimal tva() {
    return (java.math.BigDecimal) storedValueForKey(TVA_KEY);
  }

  public void setTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TVA_KEY);
  }

  public String utlnomprenom() {
    return (String) storedValueForKey(UTLNOMPRENOM_KEY);
  }

  public void setUtlnomprenom(String value) {
    takeStoredValueForKey(value, UTLNOMPRENOM_KEY);
  }

  public Integer utlOrdre() {
    return (Integer) storedValueForKey(UTL_ORDRE_KEY);
  }

  public void setUtlOrdre(Integer value) {
    takeStoredValueForKey(value, UTL_ORDRE_KEY);
  }

  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOOrgan organ() {
    return (org.cocktail.application.client.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.client.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOTypeBordereau typeBordereau() {
    return (org.cocktail.application.client.eof.EOTypeBordereau)storedValueForKey(TYPE_BORDEREAU_KEY);
  }

  public void setTypeBordereauRelationship(org.cocktail.application.client.eof.EOTypeBordereau value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeBordereau oldValue = typeBordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_BORDEREAU_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOUtilisateur utilisateur() {
    return (org.cocktail.application.client.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.application.client.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

  public static EOVAbricotRecetteATitrer createVAbricotRecetteATitrer(EOEditingContext editingContext, String adrCivilite
, String adrNom
, Integer exeExercice
, NSTimestamp facDateSaisie
, Integer facNumero
, Double fapNumero
, String modCode
, String modLibelle
, Integer orgId
, String persLibelle
, String persType
, java.math.BigDecimal recHtSaisie
, Integer recId
, Integer recNumero
, java.math.BigDecimal recTtcSaisie
, java.math.BigDecimal recTvaSaisie
, java.math.BigDecimal rpcoHtSaisie
, Integer rpcoId
, Integer tboOrdre
, String tcdCode
, String tcdLibelle
, Integer tcdOrdre
, java.math.BigDecimal ttc
, java.math.BigDecimal tva
, Integer utlOrdre
) {
    EOVAbricotRecetteATitrer eo = (EOVAbricotRecetteATitrer) createAndInsertInstance(editingContext, _EOVAbricotRecetteATitrer.ENTITY_NAME);    
		eo.setAdrCivilite(adrCivilite);
		eo.setAdrNom(adrNom);
		eo.setExeExercice(exeExercice);
		eo.setFacDateSaisie(facDateSaisie);
		eo.setFacNumero(facNumero);
		eo.setFapNumero(fapNumero);
		eo.setModCode(modCode);
		eo.setModLibelle(modLibelle);
		eo.setOrgId(orgId);
		eo.setPersLibelle(persLibelle);
		eo.setPersType(persType);
		eo.setRecHtSaisie(recHtSaisie);
		eo.setRecId(recId);
		eo.setRecNumero(recNumero);
		eo.setRecTtcSaisie(recTtcSaisie);
		eo.setRecTvaSaisie(recTvaSaisie);
		eo.setRpcoHtSaisie(rpcoHtSaisie);
		eo.setRpcoId(rpcoId);
		eo.setTboOrdre(tboOrdre);
		eo.setTcdCode(tcdCode);
		eo.setTcdLibelle(tcdLibelle);
		eo.setTcdOrdre(tcdOrdre);
		eo.setTtc(ttc);
		eo.setTva(tva);
		eo.setUtlOrdre(utlOrdre);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOVAbricotRecetteATitrer.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOVAbricotRecetteATitrer.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOVAbricotRecetteATitrer localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVAbricotRecetteATitrer)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOVAbricotRecetteATitrer localInstanceIn(EOEditingContext editingContext, EOVAbricotRecetteATitrer eo) {
    EOVAbricotRecetteATitrer localInstance = (eo == null) ? null : (EOVAbricotRecetteATitrer)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOVAbricotRecetteATitrer#localInstanceIn a la place.
   */
	public static EOVAbricotRecetteATitrer localInstanceOf(EOEditingContext editingContext, EOVAbricotRecetteATitrer eo) {
		return EOVAbricotRecetteATitrer.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOVAbricotRecetteATitrer fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOVAbricotRecetteATitrer fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVAbricotRecetteATitrer eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVAbricotRecetteATitrer)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVAbricotRecetteATitrer fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVAbricotRecetteATitrer fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVAbricotRecetteATitrer eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVAbricotRecetteATitrer)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOVAbricotRecetteATitrer fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVAbricotRecetteATitrer eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVAbricotRecetteATitrer ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVAbricotRecetteATitrer fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
