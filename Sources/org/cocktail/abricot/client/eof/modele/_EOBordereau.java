// _EOBordereau.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBordereau.java instead.
package org.cocktail.abricot.client.eof.modele;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOBordereau extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "Bordereau";
	public static final String ENTITY_TABLE_NAME = "MARACUJA.BORDEREAU";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "borId";

	public static final String BOR_DATE_CREATION_KEY = "borDateCreation";
	public static final String BOR_DATE_VISA_KEY = "borDateVisa";
	public static final String BOR_ETAT_KEY = "borEtat";
	public static final String BOR_ID_BIS_KEY = "borIdBis";
	public static final String BOR_NUM_KEY = "borNum";
	public static final String BOR_ORDRE_KEY = "borOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String TBO_ORDRE_KEY = "tboOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";
	public static final String UTL_ORDRE_VISA_KEY = "utlOrdreVisa";

// Attributs non visibles
	public static final String BOR_ID_KEY = "borId";

//Colonnes dans la base de donnees
	public static final String BOR_DATE_CREATION_COLKEY = "BOR_DATE_CREATION";
	public static final String BOR_DATE_VISA_COLKEY = "BOR_DATE_VISA";
	public static final String BOR_ETAT_COLKEY = "BOR_ETAT";
	public static final String BOR_ID_BIS_COLKEY = "BOR_ID";
	public static final String BOR_NUM_COLKEY = "BOR_NUM";
	public static final String BOR_ORDRE_COLKEY = "BOR_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String TBO_ORDRE_COLKEY = "TBO_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";
	public static final String UTL_ORDRE_VISA_COLKEY = "UTL_ORDRE_VISA";

	public static final String BOR_ID_COLKEY = "BOR_ID";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String TYPE_BORDEREAU_KEY = "typeBordereau";



	// Accessors methods
  public NSTimestamp borDateCreation() {
    return (NSTimestamp) storedValueForKey(BOR_DATE_CREATION_KEY);
  }

  public void setBorDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, BOR_DATE_CREATION_KEY);
  }

  public NSTimestamp borDateVisa() {
    return (NSTimestamp) storedValueForKey(BOR_DATE_VISA_KEY);
  }

  public void setBorDateVisa(NSTimestamp value) {
    takeStoredValueForKey(value, BOR_DATE_VISA_KEY);
  }

  public String borEtat() {
    return (String) storedValueForKey(BOR_ETAT_KEY);
  }

  public void setBorEtat(String value) {
    takeStoredValueForKey(value, BOR_ETAT_KEY);
  }

  public Double borIdBis() {
    return (Double) storedValueForKey(BOR_ID_BIS_KEY);
  }

  public void setBorIdBis(Double value) {
    takeStoredValueForKey(value, BOR_ID_BIS_KEY);
  }

  public Integer borNum() {
    return (Integer) storedValueForKey(BOR_NUM_KEY);
  }

  public void setBorNum(Integer value) {
    takeStoredValueForKey(value, BOR_NUM_KEY);
  }

  public Double borOrdre() {
    return (Double) storedValueForKey(BOR_ORDRE_KEY);
  }

  public void setBorOrdre(Double value) {
    takeStoredValueForKey(value, BOR_ORDRE_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public String gesCode() {
    return (String) storedValueForKey(GES_CODE_KEY);
  }

  public void setGesCode(String value) {
    takeStoredValueForKey(value, GES_CODE_KEY);
  }

  public Integer tboOrdre() {
    return (Integer) storedValueForKey(TBO_ORDRE_KEY);
  }

  public void setTboOrdre(Integer value) {
    takeStoredValueForKey(value, TBO_ORDRE_KEY);
  }

  public Double utlOrdre() {
    return (Double) storedValueForKey(UTL_ORDRE_KEY);
  }

  public void setUtlOrdre(Double value) {
    takeStoredValueForKey(value, UTL_ORDRE_KEY);
  }

  public Double utlOrdreVisa() {
    return (Double) storedValueForKey(UTL_ORDRE_VISA_KEY);
  }

  public void setUtlOrdreVisa(Double value) {
    takeStoredValueForKey(value, UTL_ORDRE_VISA_KEY);
  }

  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOTypeBordereau typeBordereau() {
    return (org.cocktail.application.client.eof.EOTypeBordereau)storedValueForKey(TYPE_BORDEREAU_KEY);
  }

  public void setTypeBordereauRelationship(org.cocktail.application.client.eof.EOTypeBordereau value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeBordereau oldValue = typeBordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_BORDEREAU_KEY);
    }
  }
  

  public static EOBordereau createBordereau(EOEditingContext editingContext, Double borOrdre
, Integer exeOrdre
, String gesCode
, Integer tboOrdre
, Double utlOrdre
, org.cocktail.application.client.eof.EOExercice exercice, org.cocktail.application.client.eof.EOTypeBordereau typeBordereau) {
    EOBordereau eo = (EOBordereau) createAndInsertInstance(editingContext, _EOBordereau.ENTITY_NAME);    
		eo.setBorOrdre(borOrdre);
		eo.setExeOrdre(exeOrdre);
		eo.setGesCode(gesCode);
		eo.setTboOrdre(tboOrdre);
		eo.setUtlOrdre(utlOrdre);
    eo.setExerciceRelationship(exercice);
    eo.setTypeBordereauRelationship(typeBordereau);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOBordereau.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOBordereau.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOBordereau localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBordereau)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOBordereau localInstanceIn(EOEditingContext editingContext, EOBordereau eo) {
    EOBordereau localInstance = (eo == null) ? null : (EOBordereau)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOBordereau#localInstanceIn a la place.
   */
	public static EOBordereau localInstanceOf(EOEditingContext editingContext, EOBordereau eo) {
		return EOBordereau.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOBordereau fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOBordereau fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBordereau eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBordereau)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBordereau fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBordereau fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBordereau eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBordereau)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOBordereau fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBordereau eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBordereau ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBordereau fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
