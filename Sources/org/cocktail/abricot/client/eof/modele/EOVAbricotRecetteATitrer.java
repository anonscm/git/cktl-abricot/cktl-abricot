/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.abricot.client.eof.modele;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOTypeBordereau;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOVAbricotRecetteATitrer extends _EOVAbricotRecetteATitrer {

	private static final long serialVersionUID = 1L;

    public EOVAbricotRecetteATitrer() {
        super();
    }

    
	static public NSMutableArrayDisplayGroup findTitres(ApplicationCocktail app, EOQualifier qualifier) {
		NSMutableArrayDisplayGroup tmpTitre = new NSMutableArrayDisplayGroup();

		tmpTitre = Finder.find(app, EOTitre.ENTITY_NAME, null, qualifier);
		return tmpTitre;
	}

	
	
	public static NSArray find(EOEditingContext ec, EOExercice exercice, EOTypeBordereau typeBordereau, EOUtilisateur utilisateur, NSArray lesOrgans)	{

		 try {
			 			 
			 NSMutableArray mesQualifiers = new NSMutableArray();

			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVAbricotRecetteATitrer.EXERCICE_KEY + " = %@",new NSArray(exercice)));

			 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVAbricotRecetteATitrer.TYPE_BORDEREAU_KEY +" = %@",new NSArray(typeBordereau)));

			 if (utilisateur != null)
				 mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVAbricotRecetteATitrer.UTILISATEUR_KEY + " = %@",new NSArray(utilisateur)));
			 
			 NSMutableArray mySort = new NSMutableArray(new EOSortOrdering(EOVAbricotRecetteATitrer.ORG_UB_KEY, EOSortOrdering.CompareDescending));
			 mySort.addObject(new EOSortOrdering(EOVAbricotRecetteATitrer.ORG_CR_KEY, EOSortOrdering.CompareDescending));
			 mySort.addObject(new EOSortOrdering(EOVAbricotRecetteATitrer.ORG_SOUSCR_KEY, EOSortOrdering.CompareDescending));
			 			 
			 EOFetchSpecification fs = new EOFetchSpecification(EOVAbricotRecetteATitrer.ENTITY_NAME,new EOAndQualifier(mesQualifiers), mySort);
			 fs.setRefreshesRefetchedObjects(true);
			 
			 NSArray recettes = ec.objectsWithFetchSpecification(fs);
			 NSMutableArray result = new NSMutableArray();

			 // Restriction aux lignes budgetaires de l'utilisateur
				EOOrgan tmpOrgan = null;

				for (int i = 0; i < recettes.count(); i++) {
					tmpOrgan = ((EOVAbricotRecetteATitrer)recettes.objectAtIndex(i)).organ();
					for (int j = 0; j < lesOrgans.count(); j++) {
						if (tmpOrgan.equals((EOOrgan)lesOrgans.objectAtIndex(j)))
							result.addObject((EOVAbricotRecetteATitrer)recettes.objectAtIndex(i));
					}
				}
			 
			 return result;
			 
		 }
		 catch (Exception ex)	{
			 ex.printStackTrace();
			 return new NSArray();
		 }
	}

	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
