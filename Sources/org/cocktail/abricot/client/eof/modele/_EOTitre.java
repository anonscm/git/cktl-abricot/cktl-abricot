// _EOTitre.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTitre.java instead.
package org.cocktail.abricot.client.eof.modele;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOTitre extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "Titre";
	public static final String ENTITY_TABLE_NAME = "MARACUJA.TITRE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "titId";

	public static final String BOR_ID_KEY = "borId";
	public static final String BOR_ORDRE_KEY = "borOrdre";
	public static final String BRJ_ORDRE_KEY = "brjOrdre";
	public static final String DETAIL_NUMERO_KEY = "detailNumero";
	public static final String DETAIL_TTC_KEY = "detailTtc";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String MOR_ORDRE_KEY = "morOrdre";
	public static final String MOTIF_REJET_KEY = "motifRejet";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String ORI_ORDRE_KEY = "oriOrdre";
	public static final String PAI_ORDRE_KEY = "paiOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PREST_ID_KEY = "prestId";
	public static final String RIB_ORDRE_COMPTABLE_KEY = "ribOrdreComptable";
	public static final String RIB_ORDRE_ORDONNATEUR_KEY = "ribOrdreOrdonnateur";
	public static final String TIT_DATE_REMISE_KEY = "titDateRemise";
	public static final String TIT_DATE_VISA_PRINC_KEY = "titDateVisaPrinc";
	public static final String TIT_ETAT_KEY = "titEtat";
	public static final String TIT_ETAT_REMISE_KEY = "titEtatRemise";
	public static final String TIT_HT_KEY = "titHt";
	public static final String TIT_ID_KEY = "titId";
	public static final String TIT_LIBELLE_KEY = "titLibelle";
	public static final String TIT_MOTIF_REJET_KEY = "titMotifRejet";
	public static final String TIT_NB_PIECE_KEY = "titNbPiece";
	public static final String TIT_NUMERO_KEY = "titNumero";
	public static final String TIT_NUMERO_REJET_KEY = "titNumeroRejet";
	public static final String TIT_ORDRE_KEY = "titOrdre";
	public static final String TIT_ORGINE_KEY_KEY = "titOrgineKey";
	public static final String TIT_ORIGINE_LIB_KEY = "titOrigineLib";
	public static final String TIT_TTC_KEY = "titTtc";
	public static final String TIT_TVA_KEY = "titTva";
	public static final String TOR_ORDRE_KEY = "torOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String BOR_ID_COLKEY = "BOR_ID";
	public static final String BOR_ORDRE_COLKEY = "BOR_ORDRE";
	public static final String BRJ_ORDRE_COLKEY = "BRJ_ORDRE";
	public static final String DETAIL_NUMERO_COLKEY = "TIT_NUMERO";
	public static final String DETAIL_TTC_COLKEY = "TIT_TTC";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String MOR_ORDRE_COLKEY = "MOR_ORDRE";
	public static final String MOTIF_REJET_COLKEY = "TIT_MOTIF_REJET";
	public static final String ORG_ORDRE_COLKEY = "ORG_ORDRE";
	public static final String ORI_ORDRE_COLKEY = "ORI_ORDRE";
	public static final String PAI_ORDRE_COLKEY = "PAI_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PREST_ID_COLKEY = "PREST_ID";
	public static final String RIB_ORDRE_COMPTABLE_COLKEY = "RIB_ORDRE_COMPTABLE";
	public static final String RIB_ORDRE_ORDONNATEUR_COLKEY = "RIB_ORDRE_ORDONNATEUR";
	public static final String TIT_DATE_REMISE_COLKEY = "TIT_DATE_REMISE";
	public static final String TIT_DATE_VISA_PRINC_COLKEY = "TIT_DATE_VISA_PRINC";
	public static final String TIT_ETAT_COLKEY = "TIT_ETAT";
	public static final String TIT_ETAT_REMISE_COLKEY = "TIT_ETAT_REMISE";
	public static final String TIT_HT_COLKEY = "TIT_HT";
	public static final String TIT_ID_COLKEY = "TIT_ID";
	public static final String TIT_LIBELLE_COLKEY = "TIT_LIBELLE";
	public static final String TIT_MOTIF_REJET_COLKEY = "TIT_MOTIF_REJET";
	public static final String TIT_NB_PIECE_COLKEY = "TIT_NB_PIECE";
	public static final String TIT_NUMERO_COLKEY = "TIT_NUMERO";
	public static final String TIT_NUMERO_REJET_COLKEY = "TIT_NUMERO_REJET";
	public static final String TIT_ORDRE_COLKEY = "TIT_ORDRE";
	public static final String TIT_ORGINE_KEY_COLKEY = "TIT_ORGINE_KEY";
	public static final String TIT_ORIGINE_LIB_COLKEY = "TIT_ORIGINE_LIB";
	public static final String TIT_TTC_COLKEY = "TIT_TTC";
	public static final String TIT_TVA_COLKEY = "TIT_TVA";
	public static final String TOR_ORDRE_COLKEY = "TOR_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";



	// Relationships
	public static final String BORDEREAU_KEY = "bordereau";
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String ORGAN_KEY = "organ";
	public static final String V_FOURNISSEUR_KEY = "vFournisseur";



	// Accessors methods
  public Integer borId() {
    return (Integer) storedValueForKey(BOR_ID_KEY);
  }

  public void setBorId(Integer value) {
    takeStoredValueForKey(value, BOR_ID_KEY);
  }

  public Integer borOrdre() {
    return (Integer) storedValueForKey(BOR_ORDRE_KEY);
  }

  public void setBorOrdre(Integer value) {
    takeStoredValueForKey(value, BOR_ORDRE_KEY);
  }

  public Integer brjOrdre() {
    return (Integer) storedValueForKey(BRJ_ORDRE_KEY);
  }

  public void setBrjOrdre(Integer value) {
    takeStoredValueForKey(value, BRJ_ORDRE_KEY);
  }

  public Integer detailNumero() {
    return (Integer) storedValueForKey(DETAIL_NUMERO_KEY);
  }

  public void setDetailNumero(Integer value) {
    takeStoredValueForKey(value, DETAIL_NUMERO_KEY);
  }

  public java.math.BigDecimal detailTtc() {
    return (java.math.BigDecimal) storedValueForKey(DETAIL_TTC_KEY);
  }

  public void setDetailTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DETAIL_TTC_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public Integer fouOrdre() {
    return (Integer) storedValueForKey(FOU_ORDRE_KEY);
  }

  public void setFouOrdre(Integer value) {
    takeStoredValueForKey(value, FOU_ORDRE_KEY);
  }

  public String gesCode() {
    return (String) storedValueForKey(GES_CODE_KEY);
  }

  public void setGesCode(String value) {
    takeStoredValueForKey(value, GES_CODE_KEY);
  }

  public Integer modOrdre() {
    return (Integer) storedValueForKey(MOD_ORDRE_KEY);
  }

  public void setModOrdre(Integer value) {
    takeStoredValueForKey(value, MOD_ORDRE_KEY);
  }

  public Integer morOrdre() {
    return (Integer) storedValueForKey(MOR_ORDRE_KEY);
  }

  public void setMorOrdre(Integer value) {
    takeStoredValueForKey(value, MOR_ORDRE_KEY);
  }

  public String motifRejet() {
    return (String) storedValueForKey(MOTIF_REJET_KEY);
  }

  public void setMotifRejet(String value) {
    takeStoredValueForKey(value, MOTIF_REJET_KEY);
  }

  public Integer orgOrdre() {
    return (Integer) storedValueForKey(ORG_ORDRE_KEY);
  }

  public void setOrgOrdre(Integer value) {
    takeStoredValueForKey(value, ORG_ORDRE_KEY);
  }

  public Double oriOrdre() {
    return (Double) storedValueForKey(ORI_ORDRE_KEY);
  }

  public void setOriOrdre(Double value) {
    takeStoredValueForKey(value, ORI_ORDRE_KEY);
  }

  public Double paiOrdre() {
    return (Double) storedValueForKey(PAI_ORDRE_KEY);
  }

  public void setPaiOrdre(Double value) {
    takeStoredValueForKey(value, PAI_ORDRE_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public Double prestId() {
    return (Double) storedValueForKey(PREST_ID_KEY);
  }

  public void setPrestId(Double value) {
    takeStoredValueForKey(value, PREST_ID_KEY);
  }

  public Integer ribOrdreComptable() {
    return (Integer) storedValueForKey(RIB_ORDRE_COMPTABLE_KEY);
  }

  public void setRibOrdreComptable(Integer value) {
    takeStoredValueForKey(value, RIB_ORDRE_COMPTABLE_KEY);
  }

  public Integer ribOrdreOrdonnateur() {
    return (Integer) storedValueForKey(RIB_ORDRE_ORDONNATEUR_KEY);
  }

  public void setRibOrdreOrdonnateur(Integer value) {
    takeStoredValueForKey(value, RIB_ORDRE_ORDONNATEUR_KEY);
  }

  public NSTimestamp titDateRemise() {
    return (NSTimestamp) storedValueForKey(TIT_DATE_REMISE_KEY);
  }

  public void setTitDateRemise(NSTimestamp value) {
    takeStoredValueForKey(value, TIT_DATE_REMISE_KEY);
  }

  public NSTimestamp titDateVisaPrinc() {
    return (NSTimestamp) storedValueForKey(TIT_DATE_VISA_PRINC_KEY);
  }

  public void setTitDateVisaPrinc(NSTimestamp value) {
    takeStoredValueForKey(value, TIT_DATE_VISA_PRINC_KEY);
  }

  public String titEtat() {
    return (String) storedValueForKey(TIT_ETAT_KEY);
  }

  public void setTitEtat(String value) {
    takeStoredValueForKey(value, TIT_ETAT_KEY);
  }

  public String titEtatRemise() {
    return (String) storedValueForKey(TIT_ETAT_REMISE_KEY);
  }

  public void setTitEtatRemise(String value) {
    takeStoredValueForKey(value, TIT_ETAT_REMISE_KEY);
  }

  public java.math.BigDecimal titHt() {
    return (java.math.BigDecimal) storedValueForKey(TIT_HT_KEY);
  }

  public void setTitHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TIT_HT_KEY);
  }

  public Integer titId() {
    return (Integer) storedValueForKey(TIT_ID_KEY);
  }

  public void setTitId(Integer value) {
    takeStoredValueForKey(value, TIT_ID_KEY);
  }

  public String titLibelle() {
    return (String) storedValueForKey(TIT_LIBELLE_KEY);
  }

  public void setTitLibelle(String value) {
    takeStoredValueForKey(value, TIT_LIBELLE_KEY);
  }

  public String titMotifRejet() {
    return (String) storedValueForKey(TIT_MOTIF_REJET_KEY);
  }

  public void setTitMotifRejet(String value) {
    takeStoredValueForKey(value, TIT_MOTIF_REJET_KEY);
  }

  public Double titNbPiece() {
    return (Double) storedValueForKey(TIT_NB_PIECE_KEY);
  }

  public void setTitNbPiece(Double value) {
    takeStoredValueForKey(value, TIT_NB_PIECE_KEY);
  }

  public Double titNumero() {
    return (Double) storedValueForKey(TIT_NUMERO_KEY);
  }

  public void setTitNumero(Double value) {
    takeStoredValueForKey(value, TIT_NUMERO_KEY);
  }

  public Double titNumeroRejet() {
    return (Double) storedValueForKey(TIT_NUMERO_REJET_KEY);
  }

  public void setTitNumeroRejet(Double value) {
    takeStoredValueForKey(value, TIT_NUMERO_REJET_KEY);
  }

  public Integer titOrdre() {
    return (Integer) storedValueForKey(TIT_ORDRE_KEY);
  }

  public void setTitOrdre(Integer value) {
    takeStoredValueForKey(value, TIT_ORDRE_KEY);
  }

  public Double titOrgineKey() {
    return (Double) storedValueForKey(TIT_ORGINE_KEY_KEY);
  }

  public void setTitOrgineKey(Double value) {
    takeStoredValueForKey(value, TIT_ORGINE_KEY_KEY);
  }

  public String titOrigineLib() {
    return (String) storedValueForKey(TIT_ORIGINE_LIB_KEY);
  }

  public void setTitOrigineLib(String value) {
    takeStoredValueForKey(value, TIT_ORIGINE_LIB_KEY);
  }

  public java.math.BigDecimal titTtc() {
    return (java.math.BigDecimal) storedValueForKey(TIT_TTC_KEY);
  }

  public void setTitTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TIT_TTC_KEY);
  }

  public java.math.BigDecimal titTva() {
    return (java.math.BigDecimal) storedValueForKey(TIT_TVA_KEY);
  }

  public void setTitTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TIT_TVA_KEY);
  }

  public Double torOrdre() {
    return (Double) storedValueForKey(TOR_ORDRE_KEY);
  }

  public void setTorOrdre(Double value) {
    takeStoredValueForKey(value, TOR_ORDRE_KEY);
  }

  public Double utlOrdre() {
    return (Double) storedValueForKey(UTL_ORDRE_KEY);
  }

  public void setUtlOrdre(Double value) {
    takeStoredValueForKey(value, UTL_ORDRE_KEY);
  }

  public org.cocktail.abricot.client.eof.modele.EOBordereau bordereau() {
    return (org.cocktail.abricot.client.eof.modele.EOBordereau)storedValueForKey(BORDEREAU_KEY);
  }

  public void setBordereauRelationship(org.cocktail.abricot.client.eof.modele.EOBordereau value) {
    if (value == null) {
    	org.cocktail.abricot.client.eof.modele.EOBordereau oldValue = bordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BORDEREAU_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOModePaiement modePaiement() {
    return (org.cocktail.application.client.eof.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
  }

  public void setModePaiementRelationship(org.cocktail.application.client.eof.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOModePaiement oldValue = modePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOOrgan organ() {
    return (org.cocktail.application.client.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.client.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.VFournisseur vFournisseur() {
    return (org.cocktail.application.client.eof.VFournisseur)storedValueForKey(V_FOURNISSEUR_KEY);
  }

  public void setVFournisseurRelationship(org.cocktail.application.client.eof.VFournisseur value) {
    if (value == null) {
    	org.cocktail.application.client.eof.VFournisseur oldValue = vFournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, V_FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, V_FOURNISSEUR_KEY);
    }
  }
  

  public static EOTitre createTitre(EOEditingContext editingContext, Integer borId
, Integer borOrdre
, Integer detailNumero
, java.math.BigDecimal detailTtc
, Integer exeOrdre
, String gesCode
, String pcoNum
, String titEtat
, java.math.BigDecimal titHt
, Integer titId
, Double titNumero
, Integer titOrdre
, java.math.BigDecimal titTtc
, java.math.BigDecimal titTva
, Double torOrdre
, Double utlOrdre
) {
    EOTitre eo = (EOTitre) createAndInsertInstance(editingContext, _EOTitre.ENTITY_NAME);    
		eo.setBorId(borId);
		eo.setBorOrdre(borOrdre);
		eo.setDetailNumero(detailNumero);
		eo.setDetailTtc(detailTtc);
		eo.setExeOrdre(exeOrdre);
		eo.setGesCode(gesCode);
		eo.setPcoNum(pcoNum);
		eo.setTitEtat(titEtat);
		eo.setTitHt(titHt);
		eo.setTitId(titId);
		eo.setTitNumero(titNumero);
		eo.setTitOrdre(titOrdre);
		eo.setTitTtc(titTtc);
		eo.setTitTva(titTva);
		eo.setTorOrdre(torOrdre);
		eo.setUtlOrdre(utlOrdre);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOTitre.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOTitre.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOTitre localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTitre)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOTitre localInstanceIn(EOEditingContext editingContext, EOTitre eo) {
    EOTitre localInstance = (eo == null) ? null : (EOTitre)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOTitre#localInstanceIn a la place.
   */
	public static EOTitre localInstanceOf(EOEditingContext editingContext, EOTitre eo) {
		return EOTitre.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOTitre fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOTitre fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTitre eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTitre)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTitre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTitre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTitre eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTitre)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOTitre fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTitre eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTitre ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTitre fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
