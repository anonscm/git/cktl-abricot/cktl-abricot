package org.cocktail.abricot.client;


import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;

/**
 * Rassemble tous les appels aux methodes definies sur le serveur.
 */

public class ServerProxy  {	
	
	public static String clientSideRequestAppVersion(EOEditingContext ec) {
		return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				"session", 
				"clientSideRequestAppVersion", 
				null,  
				null, 
				false);
	}

	/**
	 * Verification du login/mot de passe cote serveur
	 */
	public static Boolean clientSideRequestSetLoginParametres(EOEditingContext ec, String login, String ip) {
		return (Boolean)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestSetLoginParametres", new Class[] {String.class, String.class}, new Object[] {login, ip}, false);		
	}

	public static NSArray clientSideRequestGetConnectedUsers(EOEditingContext ec) {
		return (NSArray) ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, 
				"session", 
				"clientSideRequestGetConnectedUsers", 
				new Class[] {}, 
				new Object[] {}, 
				false);
	}

}
