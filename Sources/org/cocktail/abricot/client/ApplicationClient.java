/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.abricot.client;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.net.UnknownHostException;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOFonction;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.eof.EOUtilisateurFonction;
import org.cocktail.application.client.finder.FinderGrhum;
import org.cocktail.client.common.utilities.DateCtrl;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Ceci est le controleur global de l'application.<br>
 * Il pilote les privileges et initialise l'application
 */
public class ApplicationClient extends ApplicationCocktail {

	public static final String NOM_APPLICATION = "ABRICOT - Gestion des Bordereaux";

	private MyByteArrayOutputStream	redirectedOutStream, redirectedErrStream; // Nouvelles sorties

	/** Temoin des privileges pour les bordereaux de depenses */
	private NSMutableArray mesPrivilegesDepense = null;
	/** Temoin des privileges pour les bordereaux de titres */
	private NSMutableArray mesPrivilegesRecette = null;
	/** Temoin des privileges pour les bordereaux des prestations internes */
	private NSMutableArray mesPrivilegesPI = null;
	/** Temoin des privileges pour le visa des bordereaux de rejet */
	private NSMutableArray mesPrivilegesVisa = null;
	/** Temoin des privileges pour les bordereaux Paye */
	private NSMutableArray mesPrivilegesPaye = null;
	/** Temoin des privileges pour l'acces a l interface d'impression */
	private NSMutableArray mesPrivilegesImpressions = null;

	/**
	 * Constructeur de l'application et liaison avec TYPE_APPLICATION
	 */
	public ApplicationClient() {

		super();
		setWithLogs(false);
		setNAME_APP("ABRICOT");
		setTYAPSTRID("ABRICOT");
		AliveThread thr = new AliveThread(this);
		thr.start();

	}
	
	
	private void redirectLogs() {
		redirectedOutStream = new MyByteArrayOutputStream(System.out, 102400); // limite
		// le
		// log
		// en
		// memoire
		// a
		// 100Ko
		redirectedErrStream = new MyByteArrayOutputStream(System.err, 102400); // limite
		// le log en memoire a 100Ko
		System.setOut(new PrintStream(redirectedOutStream));
		System.setErr(new PrintStream(redirectedErrStream));
		// on force la sortie du NSLog (qui etait initialise par defaut sur
		// System.xxx AVANT le changement)
		((NSLog.PrintStreamLogger) NSLog.out).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.debug).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.err).setPrintStream(System.err);
	}

	
	
	/**
	 * Methode qui traite les privileges accordes a cet utilisateur <br>
	 * en fonction des attributions dans JefyAdmin (voir Temoins)
	 */
	public void creationDesPrivileges() {
		super.creationDesPrivileges();
		//NSArray lesUtilisateurFonctions = new NSArray(getMesUtilisateurFonction());
		setMesPrivilegesDepense(new NSMutableArray());
		setMesPrivilegesRecette(new NSMutableArray());
		setMesPrivilegesPI(new NSMutableArray());
		setMesPrivilegesVisa(new NSMutableArray());
		setMesPrivilegesPaye(new NSMutableArray());
		setMesPrivilegesImpressions(new NSMutableArray());

		for (java.util.Enumeration<EOUtilisateurFonction> e = getMesUtilisateurFonction().objectEnumerator(); e.hasMoreElements();) {
			EOFonction maFonction = e.nextElement().fonction();
			if (	maFonction.fonIdInterne().equals(Privileges.DEP_1) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_2) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_4) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_5) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_8) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_18) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_19) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_REGUL_PAF) ||	// 22
					maFonction.fonIdInterne().equals(Privileges.ORV_PAF) ||
					maFonction.fonIdInterne().equals(Privileges.CAP_PAF)) {			// 23
				addPrivileges(new Privileges(Privileges.DEPENSE));
				getMesPrivilegesDepense().addObject(new Privileges(maFonction.fonIdInterne()));
			}

			if (maFonction.fonIdInterne().equals(Privileges.DEP_3) || maFonction.fonIdInterne().equals(Privileges.DEP_PAF)) {
				addPrivileges(new Privileges(Privileges.PAYE));
				getMesPrivilegesPaye().addObject(new Privileges(maFonction.fonIdInterne()));
			}

			if (maFonction.fonIdInterne().equals(Privileges.REC_12) ||
					maFonction.fonIdInterne().equals(Privileges.REC_13) ||
					maFonction.fonIdInterne().equals(Privileges.REC_6) ||
					maFonction.fonIdInterne().equals(Privileges.REC_7) ||
					maFonction.fonIdInterne().equals(Privileges.REC_9)
			// || maFonction.fonIdInterne().equals(Privileges.REC_PAF) 

			) {
				addPrivileges(new Privileges(Privileges.RECETTE));
				getMesPrivilegesRecette().addObject(new Privileges(maFonction.fonIdInterne()));
			}

			if (maFonction.fonIdInterne().equals(Privileges.PID_11) ||
					maFonction.fonIdInterne().equals(Privileges.PIR_200) ||
					maFonction.fonIdInterne().equals(Privileges.PID_201)) {
				addPrivileges(new Privileges(Privileges.PI));
				getMesPrivilegesPI().addObject(new Privileges(maFonction.fonIdInterne()));
			}

			//			if (maFonction.fonIdInterne().equals(Privileges.REI)) {
			//				addPrivileges(new Privileges(Privileges.REIMPUT));
			//			}

			if (maFonction.fonIdInterne().equals(Privileges.VIS_101) || maFonction.fonIdInterne().equals(Privileges.VIS_102)) {
				addPrivileges(new Privileges(Privileges.VISA));
				getMesPrivilegesVisa().addObject(new Privileges(maFonction.fonIdInterne()));
			}

			if (maFonction.fonIdInterne().equals(Privileges.IMP) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_1) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_2) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_3) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_4) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_5) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_8) ||
					maFonction.fonIdInterne().equals(Privileges.PID_201) ||
					maFonction.fonIdInterne().equals(Privileges.REC_12) ||
					maFonction.fonIdInterne().equals(Privileges.REC_13) ||
					maFonction.fonIdInterne().equals(Privileges.REC_6) ||
					maFonction.fonIdInterne().equals(Privileges.REC_7) ||
					maFonction.fonIdInterne().equals(Privileges.REC_9) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_18) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_19) ||
					maFonction.fonIdInterne().equals(Privileges.PIR_200) ||
					//maFonction.fonIdInterne().equals(Privileges.REC_PAF) ||
					maFonction.fonIdInterne().equals(Privileges.ORV_PAF) ||
					maFonction.fonIdInterne().equals(Privileges.IMPGEST) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_PAF) ||
					maFonction.fonIdInterne().equals(Privileges.DEP_REGUL_PAF) ||
					maFonction.fonIdInterne().equals(Privileges.CAP_PAF)

			) {
				addPrivileges(new Privileges(Privileges.IMPRESSION));
				getMesPrivilegesImpressions().addObject(new Privileges(maFonction.fonIdInterne()));
			}
		}
	}

	/**
	 * Recuperation des UtilisateurFonction et TypeApplicationFonctions pour cet utilisateur
	 */
	public void initFindersAccesEtDroits() {
		super.initFindersAccesEtDroits();
		setMesUtilisateurFonction(FinderGrhum.findUtilisateurFonctions(this, getCurrentUtilisateur(), getCurrentTypeApplication()));
		setMesTypeApplicationFonctions(FinderGrhum.findTypeApplicationfonctions(this, getCurrentTypeApplication()));
	}

	//	Initialisation du panel principal de l'application
	/**
	 * Initialisation de l'interface et controleur principal
	 */
	public void initMonApplication() {
		
		super.initMonApplication();

		redirectLogs();
		
		ServerProxy.clientSideRequestSetLoginParametres(getAppEditingContext(), getUserInfos().login() ,getIpAdress());
		
		Superviseur.sharedInstance(getAppEditingContext()).init();

	}
	
	public void finishInitialization() {
		try {
			super.finishInitialization();

			try {
				compareJarVersionsClientAndServer();
			} catch (Exception e) {
				e.printStackTrace();
				this.fenetreDeDialogueInformation(e.getMessage());
				quit();
			}

		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	private final String getIpAdress() {
	       try {
	           final String s = java.net.InetAddress.getLocalHost().getHostAddress();
	           final String s2 = java.net.InetAddress.getLocalHost().getHostName() ;
	           return s+" / " + s2;
	       } catch (UnknownHostException e) {
	          return "Machine inconnue";

	       }
	   }

	//	MODIF POUR  GERER DES PROBLEMES VIA DES EXCEPTIONS
	/**
	 * Methode outils qui pemet d'executer des procedures stockees avec log
	 */
	public void executeStoredProcedure(String proc, NSDictionary dico) throws Exception {
		boolean retour = getToolsCocktailEOF().executeStoredProcedure(proc, dico);
		if (!retour) {
			throw new Exception("PROBLEME LORS DE L'EXECUTION DE LA PROCEDURE :" + proc);
		}
	}

	/**
	 * Permet d'executer une procedure stockee avec valeur de retour
	 * 
	 * @return dico de retour de l'execution
	 */
	public NSDictionary returnValuesForLastStoredProcedureInvocation() {
		return getToolsCocktailEOF().returnValuesForLastStoredProcedureInvocation();
	}

	protected void setExercice(EOExercice exer) {
		setCurrentExercice(exer);
	}

	protected EOUtilisateur getUtilisateur() {
		return getCurrentUtilisateur();
	}

	protected void setMesPrivilegesVisa(NSMutableArray mesPrivilegesVisa) {
		this.mesPrivilegesVisa = mesPrivilegesVisa;
	}

	public NSMutableArray getMesPrivilegesVisa() {
		return mesPrivilegesVisa;
	}

	protected void setMesPrivilegesPI(NSMutableArray mesPrivilegesPI) {
		this.mesPrivilegesPI = mesPrivilegesPI;
	}

	public NSMutableArray getMesPrivilegesPI() {
		return mesPrivilegesPI;
	}

	protected void setMesPrivilegesRecette(NSMutableArray mesPrivilegesRcette) {
		this.mesPrivilegesRecette = mesPrivilegesRcette;
	}

	public NSMutableArray getMesPrivilegesRecette() {
		return mesPrivilegesRecette;
	}

	protected void setMesPrivilegesDepense(NSMutableArray mesPrivilegesDepense) {
		this.mesPrivilegesDepense = mesPrivilegesDepense;
	}

	public NSMutableArray getMesPrivilegesDepense() {
		return mesPrivilegesDepense;
	}

	public NSMutableArray getMesPrivilegesPaye() {
		return mesPrivilegesPaye;
	}

	protected void setMesPrivilegesPaye(NSMutableArray mesPrivilegesPaye) {
		this.mesPrivilegesPaye = mesPrivilegesPaye;
	}

	public NSMutableArray getMesPrivilegesImpressions() {
		return mesPrivilegesImpressions;
	}

	protected void setMesPrivilegesImpressions(NSMutableArray mesPrivilegesImpressions) {
		this.mesPrivilegesImpressions = mesPrivilegesImpressions;
	}

	/**
	 * Methode outils permettant de forcer le refetch d'un objet
	 */
	public void invalidateObject(EOEditingContext ed, EOGenericRecord eo) {
		ed.invalidateObjectsWithGlobalIDs(
				new NSArray(ed.globalIDForObject(eo)));
	}

	/**
	 * Object (Thread) qui permet de conserver un client connecte a l'application cote serveur<br>
	 */
	public class AliveThread extends Thread {
		/** Un attribut propre a chaque thread */
		private final ApplicationCocktail appCock;

		/** Creation et demarrage automatique du thread */
		public AliveThread(ApplicationCocktail appCock) {
			this.appCock = appCock;
		}

		public void run() {
			try {
				while (true) {

					synchronized (this) {
						try {
							appCock.getUserInfos();
							System.out.println("Client <-> Serveur ok");
						} catch (Exception e) {
							appCock.fenetreDeDialogueInformation("Probleme de connection !\n Hors service");
							System.out.println("Client <-> Serveur ko");
							appCock.quit();
						}
						;
					}
					Thread.sleep(60000);
				}

			} catch (InterruptedException exc) {
				exc.printStackTrace();
			}
		}

	}
	
	
	public String outLogs()	{
		return redirectedOutStream.toString();
	}

	public String errLogs()	{
		return redirectedErrStream.toString();
	}
	public void cleanLogs(String type)	{

		if (type.equals("CLIENT"))	{
			redirectedOutStream.reset();
			redirectedErrStream.reset();
		}

		if (type.equals("SERVER"))	{
			((EODistributedObjectStore)getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getAppEditingContext(), "session", "clientSideRequestCleanLogs", new Class[] {}, new Object[] {}, false);;	        
		}

	}
	public void sendLog(String typeLog) {
		String 	destinataire = "cpinsard@univ-lr.fr";
		String emetteur = "cpinsard@univ-lr.fr";

		try {
			
			if (getUserInfos().email() != null)
				emetteur = getUserInfos().email();
			
			String	sujet = "ABRICOT LOGS - ";//etablissement().llStructure();
			sujet  = sujet + DateCtrl.dateToString(new NSTimestamp(),"%d/%m/%Y %H:%M")+")";
//
			String	message = "LOGS CLIENT ET SERVEUR.";
			message = message + "\nINDIVIDU CONNECTE : " + getUserInfos().nom()+ " " +getUserInfos().prenom();

			message = message + "\n\n************* LOGS CLIENT *****************";
			message = message + "\nOUTPUT log :\n\n" + redirectedOutStream.toString() + "\n\nERROR log :\n\n" + redirectedErrStream.toString();

			message = message + "\n\n************* LOGS SERVER *****************";
			message = message + "\n*****************************************";

			String outLog = (String)((EODistributedObjectStore)getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getAppEditingContext(), "session", "clientSideRequestOutLog", new Class[] {}, new Object[] {}, false);
			String errLog = (String)((EODistributedObjectStore)getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getAppEditingContext(), "session", "clientSideRequestErrLog", new Class[] {}, new Object[] {}, false);
			message = message + "\nOUTPUT log SERVER :\n\n" + outLog + "\n\nERROR log SERVER :\n\n" + errLog;

			StringBuffer mail = new StringBuffer();
			mail.append(message);
		
			((EODistributedObjectStore)getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(
					getAppEditingContext(), 
					"session", 
					"clientSideRequestSendMail", 
					new Class[] {String.class,String.class,String.class,String.class,String.class}, 
					new Object[] { emetteur, destinataire, null, sujet, mail.toString() }, 
					false);

			EODialogs.runInformationDialog("ENVOI MAIL","Le mail a bien été envoyé.");
		}
		catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
	}

	// Classe pour rediriger les logs vers la sortie initiale tout en les
	// conservant dans un ByteArray...
	// Garde en memoire un log de taille entre maxCount/2 et maxCount octets
	private class MyByteArrayOutputStream extends ByteArrayOutputStream {
		protected PrintStream out;

		protected int maxCount;

		public MyByteArrayOutputStream() {
			this(System.out, 0);
		}

		public MyByteArrayOutputStream(PrintStream out) {
			this(out, 0);
		}

		public MyByteArrayOutputStream(PrintStream out, int maxCount) {
			super(maxCount);
			this.out = out;
			this.maxCount = maxCount;
		}

		public synchronized void write(int b) {
			if (maxCount > 0 && count + 1 > maxCount) {
				shift(Math.min(maxCount >> 1, count));
			}
			super.write(b);
			out.write(b);
		}

		public synchronized void write(byte[] b, int off, int len) {
			if (maxCount > 0 && count + len > maxCount) {
				shift(Math.min(maxCount >> 1, count));
			}
			super.write(b, off, len);
			out.write(b, off, len);
		}

		private void shift(int shift) {
			for (int i = shift; i < count; i++) {
				buf[i - shift] = buf[i];
			}
			count = count - shift;
		}
	}


}
