/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.abricot.client;

import org.cocktail.abricot.client.ctrl.ExerciceSelectCtrl;
import org.cocktail.abricot.client.ctrl.ImpressionCtrl;
import org.cocktail.abricot.client.ctrl.MandatCtrl;
import org.cocktail.abricot.client.ctrl.PayeCtrl;
import org.cocktail.abricot.client.ctrl.PrestationInterneCtrl;
import org.cocktail.abricot.client.ctrl.TitreCtrl;
import org.cocktail.abricot.client.ctrl.VisaRejetCtrl;
import org.cocktail.abricot.client.finder.FinderExercice;
import org.cocktail.abricot.client.gui.SuperviseurView;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.client.common.swing.BusyGlassPane;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.CocktailIcones;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;

public class Superviseur {

	private static final long serialVersionUID = 4504464890259714143L;

	private static Superviseur sharedInstance;
	
	private ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();
	private EOEditingContext ec;
	
	private SuperviseurView  myView = null;
	
	private EOExercice currentExercice;
	
    private	BusyGlassPane	myBusyGlassPane;

    public Superviseur(EOEditingContext editingContext) 	{

    	super();
    	
        ec = editingContext;
 
        myView = new SuperviseurView();
        
        myBusyGlassPane = new BusyGlassPane();
        myView.setGlassPane(myBusyGlassPane);
                
        myView.getBtnGetExercice().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				getExercice();
			}
		});

        myView.getBtnDepenses().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				CRICursor.setWaitCursor(myView);
				MandatCtrl.sharedInstance(ec).open();
				CRICursor.setDefaultCursor(myView);
			}
		});

        myView.getBtnTitres().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				CRICursor.setWaitCursor(myView);
				TitreCtrl.sharedInstance(ec).open();
				CRICursor.setDefaultCursor(myView);
			}
		});

        myView.getBtnPrestations().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				CRICursor.setWaitCursor(myView);
				PrestationInterneCtrl.sharedInstance(ec).open();
				CRICursor.setDefaultCursor(myView);

			}
		});

        myView.getBtnPayes().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				CRICursor.setWaitCursor(myView);
				PayeCtrl.sharedInstance(ec).open();
				CRICursor.setDefaultCursor(myView);
			}
		});

        myView.getBtnRejets().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				CRICursor.setWaitCursor(myView);
				VisaRejetCtrl.sharedInstance(ec).open();
				CRICursor.setDefaultCursor(myView);

			}
		});

        myView.getBtnImpressions().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				CRICursor.setWaitCursor(myView);
				ImpressionCtrl.sharedInstance(ec).open();
				CRICursor.setDefaultCursor(myView);

//				if (monImpressionNibCtrl == null)
//					monImpressionNibCtrl = new ImpressionNibCtrl(NSApp, 0,0,380,420);
//
//				monImpressionNibCtrl.afficherFenetre();

			}
		});

        myView.getBtnQuitter().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				NSApp.quit();
			}
		});

        myView.getBtnDepenses().setEnabled(NSApp.getMesPrivilegesDepense().count() > 0);
        myView.getBtnTitres().setEnabled(NSApp.getMesPrivilegesRecette().count() > 0);
        myView.getBtnPrestations().setEnabled(NSApp.getMesPrivilegesPI().count() > 0);
        myView.getBtnPayes().setEnabled(NSApp.getMesPrivilegesPaye().count() > 0);
        myView.getBtnRejets().setEnabled(NSApp.getMesPrivilegesVisa().count() > 0);
        myView.getBtnImpressions().setEnabled(NSApp.getMesPrivilegesImpressions().count() > 0);
		
        myView.setIconImage(CocktailIcones.ICON_APP_LOGO.getImage()); 
        
        currentExercice = FinderExercice.exerciceCourant(ec);
        myView.getTfExercice().setText(currentExercice.exeExercice().toString());
        
        myView.getRootPane().setJMenuBar(MainMenu.sharedInstance(ec));
    }

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static Superviseur sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new Superviseur(editingContext);
		return sharedInstance;
	}
    
    public void init ()	{
 
		String bdConnexionName = (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestBdConnexionName", new Class[] {}, new Object[] {}, false);
		
		myView.getLblVersion().setText(ServerProxy.clientSideRequestAppVersion(ec) + " - "  + NSArray.componentsSeparatedByString(bdConnexionName, "@").objectAtIndex(1)  + " (JRE " + System.getProperty("java.version") +")");
		
		myView.setTitle(ApplicationClient.NOM_APPLICATION);
		
		myView.setVisible(true);
    	    	
    }
    
    private void getExercice() {
    	
    	setGlassPane(true);
    	
    	EOExercice exer = ExerciceSelectCtrl.sharedInstance(ec).getExercice();
    	if (exer != null) {

    		currentExercice = exer;
    	
			NSApp.setCurrentExercice(currentExercice);
			myView.getTfExercice().setText(currentExercice.exeExercice().toString());
			
			NSApp.getObserveurExerciceSelection().sendMessageToObserver(this, "changementExercice");
			
			MandatCtrl.sharedInstance(ec).setCurrentExercice(currentExercice);
			TitreCtrl.sharedInstance(ec).setCurrentExercice(currentExercice);
			PrestationInterneCtrl.sharedInstance(ec).setCurrentExercice(currentExercice);
			PayeCtrl.sharedInstance(ec).setCurrentExercice(currentExercice);
			VisaRejetCtrl.sharedInstance(ec).setCurrentExercice(currentExercice);
			ImpressionCtrl.sharedInstance(ec).setCurrentExercice(currentExercice);

    	}

    	setGlassPane(false);

    }

    
    public void setGlassPane(boolean bool) {
    	myBusyGlassPane.setVisible(bool);
    }
        
}
