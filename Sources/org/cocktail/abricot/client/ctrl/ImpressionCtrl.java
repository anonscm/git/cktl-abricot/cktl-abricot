package org.cocktail.abricot.client.ctrl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.abricot.client.ApplicationClient;
import org.cocktail.abricot.client.Privileges;
import org.cocktail.abricot.client.eof.modele.EOBordereau;
import org.cocktail.abricot.client.eof.modele.EOMandat;
import org.cocktail.abricot.client.eof.modele.EOTitre;
import org.cocktail.abricot.client.finder.FinderImpression;
import org.cocktail.abricot.client.finder.FinderOrgan;
import org.cocktail.abricot.client.finder.FinderTypeBordereau;
import org.cocktail.abricot.client.gui.ImpressionView;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOFonction;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOTypeBordereau;
import org.cocktail.application.client.eof.EOUtilisateurFonction;
import org.cocktail.application.client.eof.EOUtilisateurFonctionGestion;
import org.cocktail.client.common.swing.ZEOTable.ZEOTableListener;
import org.cocktail.client.common.utilities.CRICursor;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class ImpressionCtrl {

	public static ImpressionCtrl sharedInstance;

	private	ApplicationClient	NSApp = (ApplicationClient)ApplicationClient.sharedApplication();	
	private EOEditingContext 	ec;

	private ImpressionView myView;

	private EODisplayGroup eodTypeBordereau = new EODisplayGroup(), eodUbs = new EODisplayGroup(), eodBordereaux = new EODisplayGroup(), eodDetail = new EODisplayGroup();

	private ListenerUb listenerUb = new ListenerUb();
	private ListenerTypeBordereau listenerTypeBordereau = new ListenerTypeBordereau();
	private ListenerBordereaux listenerBordereaux = new ListenerBordereaux();

	private ImpressionPanelNibCtrl monImpressionPanelNibCtrl;

	private EOExercice currentExercice;
	private EOTypeBordereau currentTypeBordereau;
	private EOBordereau currentBordereau;
	private EOOrgan currentUb;

	public ImpressionCtrl(EOEditingContext globalEc) {

		ec = globalEc;

		myView = new ImpressionView(new JFrame(), false, eodUbs, eodTypeBordereau, eodBordereaux, eodDetail);


		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				myView.setVisible(false);
			}
		});

		myView.getBtnRefresh().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				filter();
			}
		});

		myView.getBtnPrint().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimer();
			}
		});

		myView.getBtnPrintDetail().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimerDetail();
			}
		});

		currentExercice = NSApp.getCurrentExercice();

		myView.getMyEOTableUb().addListener(listenerUb);
		myView.getMyEOTableTypeBordereau().addListener(listenerTypeBordereau);
		myView.getMyEOTableBordereau().addListener(listenerBordereaux);

		setTypesBordereaux();
		setUbs(currentExercice);

	}


	public static ImpressionCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new ImpressionCtrl(editingContext);
		return sharedInstance;
	}

	private void setTypesBordereaux() {

		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOTypeBordereau.TBO_LIBELLE_KEY, EOSortOrdering.CompareAscending));
		eodTypeBordereau.setSortOrderings(mySort);

		eodTypeBordereau.setObjectArray(FinderTypeBordereau.findTypesDeBordereaux(NSApp));
		myView.getMyEOTableTypeBordereau().updateData();
		myView.getMyTableModelTypeBordereau().fireTableDataChanged();

	}

	public void setUbs(EOExercice exercice) {
		NSMutableArray tmp = new NSMutableArray(FinderOrgan.findUbs(ec, exercice, NSApp.getMesOrgans()));

		//On ajoute celles qui sont affectees comme code gestion pour la fonction IMPGEST
		EOQualifier qual = new EOKeyValueQualifier(EOUtilisateurFonction.FONCTION_KEY+"."+EOFonction.FON_ID_INTERNE_KEY, EOQualifier.QualifierOperatorEqual, Privileges.IMPGEST);
		NSArray utilFonctions = EOQualifier.filteredArrayWithQualifier(NSApp.getMesUtilisateurFonction(), qual);
		for (int i = 0; i < utilFonctions.count(); i++) {
			EOUtilisateurFonction utilFonction = (EOUtilisateurFonction) utilFonctions.objectAtIndex(i);
			NSArray ufgestions = utilFonction.utilisateurFonctionGestions();
			NSArray composantes = FinderImpression.findLesComposantes(NSApp);
			for (int j = 0; j < ufgestions.count(); j++) {
				EOUtilisateurFonctionGestion ufg = (EOUtilisateurFonctionGestion) ufgestions.objectAtIndex(j);
				NSArray compsFiltrees = EOQualifier.filteredArrayWithQualifier(composantes, new EOKeyValueQualifier(EOOrgan.ORG_UB_KEY, EOQualifier.QualifierOperatorEqual, ufg.gestion().gesCode()));
				if (compsFiltrees.count() > 0) {
					if (tmp.indexOfObject(compsFiltrees.objectAtIndex(0)) == NSArray.NotFound) {
						tmp.addObject(compsFiltrees.objectAtIndex(0));
					}
				}
			}
		}

		eodUbs.setObjectArray(tmp);
		myView.getMyEOTableUb().updateData();
		myView.getMyTableModelUb().fireTableDataChanged();

	}



	private void imprimer() {

		if (monImpressionPanelNibCtrl == null)
			monImpressionPanelNibCtrl = new ImpressionPanelNibCtrl(NSApp, 0, 0, 360, 360);

		try {

			if (eodBordereaux.selectedObjects().count() == 1) {
				monImpressionPanelNibCtrl.afficherFenetrePourBordereau((EOBordereau) eodBordereaux.selectedObjects().objectAtIndex(0));
			}
			else {
				monImpressionPanelNibCtrl.afficherFenetrePourBordereau(null);
			}
		} catch (Exception e) {
			System.out.println("actionImprimer - ERREUR :\n" + e);
		}

	}


	private void imprimerDetail() {

		try {

			if (monImpressionPanelNibCtrl == null)
				monImpressionPanelNibCtrl = new ImpressionPanelNibCtrl(NSApp, 0, 0, 360, 360);

			Object selected = eodDetail.selectedObjects().objectAtIndex(0);
			if (selected instanceof EOMandat)
				monImpressionPanelNibCtrl.afficherFenetrePourMandat((EOMandat) selected);
			else 
				if (selected instanceof EOTitre)
					monImpressionPanelNibCtrl.afficherFenetrePourTitre((EOTitre) selected);

		} catch (Exception e) {

			System.out.println("actionImprimer " + e);

		}

	}


	private EOQualifier filterQualifier() {

		NSMutableArray mesQualifiers = new NSMutableArray();
		return new EOAndQualifier(mesQualifiers);
		
	}


	public Integer getNbDetail() {
		return new Integer(eodDetail.displayedObjects().count());
	}


	private void filter() {

		eodBordereaux.setObjectArray(new NSArray());

		eodUbs.setQualifier(filterQualifier());
		eodUbs.updateDisplayedObjects();

		if (currentUb != null && currentTypeBordereau != null)
			eodBordereaux.setObjectArray(EOBordereau.rechercherBordereaux(ec, currentUb.orgUb(), currentExercice, currentTypeBordereau));
		
		myView.getMyEOTableBordereau().updateData();

		updateUI();

	}


	public void open() {

		myView.getTfTitre().setText("EDITION DES BORDEREAUX - Exercice " + currentExercice.exeExercice());
		myView.setVisible(true);

		updateUI();
	}


	private class ListenerUb implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			CRICursor.setWaitCursor(myView);

			eodBordereaux.setObjectArray(new NSArray());
			eodDetail.setObjectArray(new NSArray());

			currentUb = (EOOrgan)eodUbs.selectedObject();

			if (currentUb != null && currentTypeBordereau != null); 
				filter();

			myView.getMyEOTableBordereau().updateData();
			myView.getMyEOTableDetail().updateData();

			updateUI();

			CRICursor.setDefaultCursor(myView);

		}
	}


	private class ListenerTypeBordereau implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			CRICursor.setWaitCursor(myView);

			eodBordereaux.setObjectArray(new NSArray());
			eodDetail.setObjectArray(new NSArray());

			currentTypeBordereau = (EOTypeBordereau)eodTypeBordereau.selectedObject();

			if (currentTypeBordereau != null && currentUb != null) 
				filter();

			myView.getMyEOTableBordereau().updateData();
			myView.getMyEOTableDetail().updateData();

			updateUI();

			CRICursor.setDefaultCursor(myView);

		}
	}

	public void setCurrentExercice(EOExercice exercice) {

		if (currentExercice.exeExercice().intValue() > exercice.exeExercice().intValue() ||
				currentExercice.exeExercice().intValue() < exercice.exeExercice().intValue()) {
			setUbs(exercice);
		}

		currentExercice = exercice;

		myView.getTfTitre().setText("EDITION DES BORDEREAUX  - Exercice " + currentExercice.exeExercice());

		filter();

	}

	private class ListenerBordereaux implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			CRICursor.setWaitCursor(myView);
			
			currentBordereau = (EOBordereau)eodBordereaux.selectedObject();
			eodDetail.setObjectArray(new NSArray());

			if (currentBordereau != null) {

				NSMutableArray detail = new NSMutableArray();
				
				detail.addObjectsFromArray(EOMandat.findForBordereau(ec, currentBordereau));

				detail.addObjectsFromArray(EOTitre.findForBordereau(ec, currentBordereau));

				eodDetail.setObjectArray(detail.immutableClone());

			}

			myView.getMyEOTableDetail().updateData();

			updateUI();

			CRICursor.setDefaultCursor(myView);

		}
	}


	private void updateUI() {

		myView.getBtnRefresh().setEnabled(currentExercice != null && currentTypeBordereau != null && currentUb != null);

		myView.getBtnPrint().setEnabled(currentExercice != null && currentBordereau != null);

		myView.getBtnPrintDetail().setEnabled(currentExercice != null && currentBordereau != null && eodDetail.selectedObjects().count() > 0);

	}

	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

	private class CheckBoxEtatListener implements ActionListener	{
		public CheckBoxEtatListener ()	{
			super();
		}

		public void actionPerformed(ActionEvent anAction)	{

			filter();

		}

	}

}
