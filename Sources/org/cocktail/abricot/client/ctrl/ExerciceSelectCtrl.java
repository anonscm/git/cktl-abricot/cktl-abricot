package org.cocktail.abricot.client.ctrl;

import javax.swing.JFrame;

import org.cocktail.abricot.client.finder.FinderExercice;
import org.cocktail.abricot.client.gui.ExerciceSelectView;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.client.common.swing.ZEOTable.ZEOTableListener;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class ExerciceSelectCtrl  {


	private static ExerciceSelectCtrl sharedInstance;

	private EOEditingContext ec;

	private ExerciceSelectView myView;

	private EODisplayGroup eod;

	private EOExercice currentExercice;

	public ExerciceSelectCtrl(EOEditingContext editingContext)	{

		super();

		ec = editingContext;

		eod = new EODisplayGroup();
		myView = new ExerciceSelectView(new JFrame(), true, eod);

		myView.getButtonAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				annuler();
			}
		});

		myView.getMyEOTable().addListener(new ListenerExercice());

	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static ExerciceSelectCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)	
			sharedInstance = new ExerciceSelectCtrl(editingContext);
		return sharedInstance;
	}

	/**
	 * 
	 * @return
	 */
	public EOExercice getExercice()	{

		if (eod.displayedObjects().count() == 0) {

			eod.setObjectArray(FinderExercice.findExercices(ec));
			myView.getMyEOTable().updateData();

		}

		myView.setVisible(true);

		if (currentExercice == null)	
			return null;

		return currentExercice;

	}

	public EOExercice getExerciceOuvert()	{

		if (eod.displayedObjects().count() == 0) {

			eod.setObjectArray(FinderExercice.findExercicesOuverts(ec));
			myView.getMyEOTable().updateData();

		}

		myView.show();

		if (currentExercice == null)	
			return null;

		return currentExercice;

	}

	public void annuler() {
		eod.setSelectionIndexes(new NSArray());
		currentExercice = null;
		myView.dispose();
	}  	


	private class ListenerExercice implements ZEOTableListener {

		public void onDbClick() {
			myView.dispose();
		}

		public void onSelectionChanged() {
			currentExercice = (EOExercice)eod.selectedObject();
		}
	}

}
