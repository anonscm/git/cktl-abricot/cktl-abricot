package org.cocktail.abricot.client.ctrl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import org.cocktail.abricot.client.ApplicationClient;
import org.cocktail.abricot.client.eof.modele.EOBordereau;
import org.cocktail.abricot.client.eof.modele.EOVAbricotDepenseAMandater;
import org.cocktail.abricot.client.eof.modele.EOVAbricotPapayeAttente;
import org.cocktail.abricot.client.gui.PayeView;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.client.common.swing.ZEOTable.ZEOTableListener;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.CocktailFormats;
import org.cocktail.client.common.utilities.CocktailUtilities;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class PayeCtrl {

	public static PayeCtrl sharedInstance;

	private static final String VOUS_ALLEZ_MANDATER_ETES_VOUS_SUR = "Vous allez creer un bordereau de mandat de payes ! \n Etes vous sûr ?";

	private ApplicationClient NSApp = (ApplicationClient) ApplicationClient
			.sharedApplication();
	private EOEditingContext ec;

	private PayeView myView;

	private EODisplayGroup eodUb = new EODisplayGroup(),
			eodDepenses = new EODisplayGroup();

	private ListenerDepenses listenerDepenses = new ListenerDepenses();
	private ListenerUb listenerUb = new ListenerUb();
	private CheckBoxTypePayeListener listenerTypePaye = new CheckBoxTypePayeListener();

	private EOExercice currentExercice;
	private EOVAbricotPapayeAttente currentUb;

	private String algo = "";

	public PayeCtrl(EOEditingContext globalEc) {

		myView = new PayeView(new JFrame(), false, eodUb, eodDepenses);

		ec = globalEc;

		myView.getBtnFermer().addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						myView.setVisible(false);
					}
				});

		myView.getBtnRefresh().addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						filter();
					}
				});

		myView.getBtnPrint().addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						imprimer();
					}
				});

		myView.getBtnCreerBordereau().addActionListener(
				new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						mandater();
					}
				});

		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(
				EOVAbricotDepenseAMandater.DPP_NUMERO_FACTURE_KEY,
				EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOVAbricotPapayeAttente.ORG_UB_KEY,
				EOSortOrdering.CompareAscending));
		// eodDepenses.setSortOrderings(mySort);

		mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOVAbricotPapayeAttente.ORG_UB_KEY,
				EOSortOrdering.CompareAscending));
		eodUb.setSortOrderings(mySort);

		currentExercice = NSApp.getCurrentExercice();

		myView.getMyEOTableUb().addListener(listenerUb);
		myView.getMyEOTableDepenses().addListener(listenerDepenses);
		myView.getCheckPapaye().addActionListener(listenerTypePaye);
		myView.getCheckPaf().addActionListener(listenerTypePaye);

		myView.getCheckPaf().setSelected(true);

		filter();

		updateUI();
	}

	private class CheckBoxTypePayeListener implements ActionListener {
		public CheckBoxTypePayeListener() {
			super();
		}

		public void actionPerformed(ActionEvent anAction) {
			filter();
		}

	}

	public static PayeCtrl sharedInstance(EOEditingContext editingContext) {
		if (sharedInstance == null)
			sharedInstance = new PayeCtrl(editingContext);
		return sharedInstance;
	}

	private void filter() {

		CRICursor.setWaitCursor(myView);

		if (myView.getCheckPaf().isSelected())
			eodUb.setObjectArray(EOVAbricotPapayeAttente.findLesPayesAttente(
					ec, currentExercice, "PAF"));
		else
			eodUb.setObjectArray(EOVAbricotPapayeAttente.findLesPayesAttente(
					ec, currentExercice, "PAPAYE"));

		myView.getMyEOTableUb().updateData();

		updateUI();

		CRICursor.setDefaultCursor(myView);

	}

	public void open() {

		myView.getTfTitre().setText(
				"BORDEREAUX DE PAYE - Exercice "
						+ currentExercice.exeExercice());
		myView.setVisible(true);

	}

	/**
	 * 
	 * Mandatement des depenses selectionnees
	 * 
	 */
	private void mandater() {

		if (!EODialogs.runConfirmOperationDialog("Mandatement ...",
				VOUS_ALLEZ_MANDATER_ETES_VOUS_SUR, "OUI", "NON"))
			return;

		NSDictionary dicoRetour = null;
		try {
			NSApp.ceerTransactionLog();

			NSMutableDictionary params = new NSMutableDictionary();
			params.takeValueForKey("vide", "info");

			// recup un id de selection ...
			((ApplicationClient) NSApp).executeStoredProcedure(
					"getSelectionId", params);
			dicoRetour = ((ApplicationClient) NSApp)
					.returnValuesForLastStoredProcedureInvocation();
			Integer abrid = (Integer) dicoRetour.valueForKey("returnValue");

			NSApp.afficherUnLogDansTransactionLog(
					"Recuperation d'un numero de selection ...OK", 20);

			// on recupere la chaine des id des depenses a mandater
			String chaine = "";
			for (int i = 0; i < eodDepenses.displayedObjects().count(); i++)
				chaine = chaine
						+ ((EOVAbricotDepenseAMandater) eodDepenses
								.displayedObjects().objectAtIndex(i)).dpcoId()
						+ "$";

			chaine = chaine + "$";

			// on positionne la selection dans la base de donnee
			// abrid integer,lesdepid varchar,lesrecid varchar ,utlordre
			// integer,exeordre integer ,tboordre integer,abrgroupby
			// varchar,gescode varchar
			params = new NSMutableDictionary();
			params.takeValueForKey(((EOVAbricotDepenseAMandater) eodDepenses
					.displayedObjects().objectAtIndex(0)).orgUb(), "a08gescode");
			params.takeValueForKey("ndep_mand_fou_rib_pco_mod", "a07abrgroupby");
			params.takeValueForKey(new Integer(
					((EOVAbricotDepenseAMandater) eodDepenses
							.displayedObjects().objectAtIndex(0))
							.typeBordereau().tboOrdre().intValue()),
					"a06tboordre");
			params.takeValueForKey(NSApp.getCurrentExercice().exeOrdre(),
					"a05exeordre");
			params.takeValueForKey(NSApp.getCurrentUtilisateur().utlOrdre(),
					"a04utlordre");
			params.takeValueForKey("", "a03lesrecid");
			params.takeValueForKey(chaine, "a02lesdepid");
			params.takeValueForKey(dicoRetour.valueForKey("returnValue"),
					"a01abrid");

			((ApplicationClient) NSApp).executeStoredProcedure(
					"setSelectionId", params);
			NSApp.afficherUnLogDansTransactionLog(
					"Enregistrement de votre selection ...OK", 30);

			NSApp.afficherUnLogDansTransactionLog(
					"Generation du bordereau ...OK", 40);

			dicoRetour = null;
			params = new NSMutableDictionary();
			params.takeValueForKey(abrid, "abrid");

			// recup du biorid ...
			((ApplicationClient) NSApp).executeStoredProcedure(
					"getSelectionBorid", params);
			dicoRetour = ((ApplicationClient) NSApp)
					.returnValuesForLastStoredProcedureInvocation();
			Integer borid = (Integer) dicoRetour.valueForKey("returnValue");

			NSApp.afficherUnLogDansTransactionLog(
					"Verification du traitement ...OK", 60);

			NSApp.afficherUnLogDansTransactionLog("FIN...", 100);

			NSApp.finirTransactionLog();
			NSApp.fermerTransactionLog();
			try {

				ImpressionPanelNibCtrl monImpressionPanelNibCtrl = new ImpressionPanelNibCtrl(
						NSApp, 0, 0, 360, 360);

				if (borid != null)
					monImpressionPanelNibCtrl
							.afficherFenetrePourBordereau(EOBordereau
									.findForBorid(ec, borid));
				else
					monImpressionPanelNibCtrl
							.afficherFenetrePourBordereau(null);

			} catch (Exception e) {
				System.out.println("actionImprimer " + e);
			}

		} catch (Exception e) {
			dicoRetour = ((ApplicationClient) NSApp)
					.returnValuesForLastStoredProcedureInvocation();
			NSApp.afficherUnLogDansTransactionLog(
					"probleme !!" + dicoRetour.toString(), 0);
			NSApp.finirTransactionLog();
		}

		filter();
	}

	protected String getAlgo() {
		return algo;
	}

	protected void setAlgo(String setAlgo) {
		this.algo = setAlgo;
	}

	/**
	 * Impression des depenses selectionnees
	 */
	private void imprimer() {

		try {
			java.util.Calendar currentTime = java.util.Calendar.getInstance();
			String lastModif = "-"
					+ currentTime.get(java.util.Calendar.DAY_OF_MONTH) + "."
					+ currentTime.get(java.util.Calendar.MONTH) + "."
					+ currentTime.get(java.util.Calendar.YEAR) + "-"
					+ currentTime.get(java.util.Calendar.HOUR_OF_DAY) + "h"
					+ currentTime.get(java.util.Calendar.MINUTE) + "m"
					+ currentTime.get(java.util.Calendar.SECOND);

			String chaine = "";
			for (int i = 0; i < eodDepenses.displayedObjects().count(); i++) {
				chaine = chaine
						+ ((EOVAbricotDepenseAMandater) eodDepenses
								.displayedObjects().objectAtIndex(i)).dpcoId()
						+ ",";
			}
			chaine = chaine + "-1";

			NSMutableDictionary parameters = new NSMutableDictionary();
			parameters.takeValueForKey(chaine, "DPCOIDS");

			NSApp.getToolsCocktailReports().imprimerReportParametres(
					"depense.jasper", parameters, "depense" + lastModif);

		} catch (Exception e) {
			System.out.println("actionImprimer " + e);
		}

	}

	public void setCurrentExercice(EOExercice exercice) {

		currentExercice = exercice;
		myView.getTfTitre().setText(
				"DEPENSES - Exercice " + currentExercice.exeExercice());

		filter();

	}

	private class ListenerDepenses implements ZEOTableListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			updateUI();

		}
	}

	private class ListenerUb implements ZEOTableListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			eodDepenses.setObjectArray(new NSArray());
			currentUb = (EOVAbricotPapayeAttente) eodUb.selectedObject();

			if (currentUb != null) {

				if (myView.getCheckPaf().isSelected())
					eodDepenses.setObjectArray(EOVAbricotDepenseAMandater.find(
							ec, currentExercice, currentUb.orgUb(), currentUb.mois() , currentUb.typeBordereau(), null,
							(NSArray) NSApp.getMesOrgans()
							));
				else
					eodDepenses.setObjectArray(EOVAbricotDepenseAMandater.find(
							ec, currentExercice, currentUb.orgUb(), currentUb.mois() , currentUb.typeBordereau(), null,
							(NSArray) NSApp.getMesOrgans()
							));
//					eodDepenses.setObjectArray(EOVAbricotPapayeAttente
//							.findLesPayesDeLaComposante(ec, currentExercice,
//									currentUb.orgUb(), currentUb.mois(),
//									"PAPAYE"));
			}

			myView.getMyEOTableDepenses().updateData();

			updateUI();

		}
	}

	private void updateUI() {

		myView.getBtnRefresh().setEnabled(currentExercice != null);

		myView.getBtnCreerBordereau().setEnabled(currentUb != null);

		myView.getBtnPrint()
				.setEnabled(
						currentUb != null
								&& eodDepenses.displayedObjects().count() > 0);

		myView.getLblDepenses()
				.setText(
						eodDepenses.displayedObjects().count()
								+ " / "
								+ CocktailFormats.FORMAT_DECIMAL.format(CocktailUtilities.computeSumForKey(
										eodDepenses.displayedObjects(),
										EOVAbricotDepenseAMandater.DPP_TTC_SAISIE_KEY))
								+ " " + CocktailFormats.STRING_EURO);

	}
}
