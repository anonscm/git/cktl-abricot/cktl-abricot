package org.cocktail.abricot.client.ctrl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.abricot.client.ApplicationClient;
import org.cocktail.abricot.client.Privileges;
import org.cocktail.abricot.client.eof.modele.EOBordereau;
import org.cocktail.abricot.client.eof.modele.EOVAbricotRecetteATitrer;
import org.cocktail.abricot.client.finder.FinderTypeBordereau;
import org.cocktail.abricot.client.gui.TitreView;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeBordereau;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.client.common.swing.ZEOTable.ZEOTableListener;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.CocktailFormats;
import org.cocktail.client.common.utilities.CocktailUtilities;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class TitreCtrl {

	public static TitreCtrl sharedInstance;

	private static final String ALGO_PROBLEME = "PROBLEME";

	private static final String VOUS_ALLEZ_TITRER_ETES_VOUS_SUR = "Vous allez creer un bordereau de titre !! \n Etes vous sûr ?";
	private static final String VOUS_ALLEZ_TITRER_IMPOSSIBLE_MULTI_COMOPSANTE = "Vous allez creer un bordereau de titre !! \n Impossible votre selection n Impossible votre selection doit concerner une seule UFR !";
	private static final String VOUS_ALLEZ_TITRER_IMPOSSIBLE_MAUVAIS_ALGO = "Vous allez creer un bordereau de titre !! \n Impossible mauvais type de construction des titres !";

	private	ApplicationClient	NSApp = (ApplicationClient)ApplicationClient.sharedApplication();	
	private EOEditingContext 	ec;

	private TitreView myView;

	private EODisplayGroup eodTypeBordereau = new EODisplayGroup(), eodDepenses = new EODisplayGroup(), eodSelection = new EODisplayGroup();

	private ListenerTypeBordereau listenerTypeBordereau = new ListenerTypeBordereau();
	private ListenerDepenses listenerDepenses = new ListenerDepenses();
	private ListenerSelection listenerSelection = new ListenerSelection();

	private EOExercice currentExercice;
	private EOTypeBordereau currentTypeBordereau;

	private String algo = "";

	public TitreCtrl(EOEditingContext globalEc) {

		ec = globalEc;

		myView = new TitreView(new JFrame(), false, eodTypeBordereau, eodDepenses, eodSelection);


		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				myView.setVisible(false);
			}
		});

		myView.getBtnRefresh().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				filter();
			}
		});

		myView.getBtnSelectDepense().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterSelection();
			}
		});


		myView.getBtnDeselectDepense().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerSelection();
			}
		});

		myView.getBtnPrint().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimer();
			}
		});

		myView.getBtnCreerBordereau().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				titrer();
			}
		});

		myView.getBtnCleanSelection().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				filter();
			}
		});

		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOVAbricotRecetteATitrer.ORG_UB_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOVAbricotRecetteATitrer.ORG_CR_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOVAbricotRecetteATitrer.ORG_SOUSCR_KEY, EOSortOrdering.CompareAscending));

		eodSelection.setSortOrderings(mySort);

		mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOTypeBordereau.TBO_LIBELLE_KEY, EOSortOrdering.CompareAscending));

		eodTypeBordereau.setSortOrderings(mySort);

		currentExercice = NSApp.getCurrentExercice();

		myView.getMyEOTableTypeBordereau().addListener(listenerTypeBordereau);
		eodTypeBordereau.setObjectArray(FinderTypeBordereau.findTypesDeBordereauxRecette(NSApp));
		myView.getMyEOTableTypeBordereau().updateData();

		myView.getMyEOTableDepenses().addListener(listenerDepenses);
		myView.getMyEOTableSelection().addListener(listenerSelection);

		myView.getTfFiltreUb().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreCr().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreLibelleFacture().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreFournisseur().getDocument().addDocumentListener(new ADocumentListener());

		myView.getCheckMesRecettes().addActionListener(new CheckBoxMesDepensesListener());

		updateUI();
	}


	public static TitreCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new TitreCtrl(editingContext);
		return sharedInstance;
	}



	private EOQualifier filterQualifier() {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (myView.getCheckMesRecettes().isSelected()) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVAbricotRecetteATitrer.UTILISATEUR_KEY + " = %@", new NSArray(NSApp.getCurrentUtilisateur())));

		if (myView.getTfFiltreUb().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVAbricotRecetteATitrer.ORG_UB_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreUb().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltreCr().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVAbricotRecetteATitrer.ORG_CR_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreCr().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltreLibelleFacture().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVAbricotRecetteATitrer.FAC_NUMERO_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreLibelleFacture().getText() + "*'",null));

		if (myView.getTfFiltreFournisseur().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVAbricotRecetteATitrer.ADR_NOM_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreFournisseur().getText() + "*'",null));

		return new EOAndQualifier(mesQualifiers);
	}





	private void filter() {

		eodSelection.setObjectArray(new NSArray());
		eodDepenses.setObjectArray(EOVAbricotRecetteATitrer.find(ec, currentExercice, currentTypeBordereau, null, (NSArray) NSApp.getMesOrgans()));

		eodDepenses.setQualifier(filterQualifier());
		eodDepenses.updateDisplayedObjects();

		myView.getMyEOTableDepenses().updateData();
		myView.getMyTableModelDepenses().fireTableDataChanged();

		myView.getMyEOTableSelection().updateData();

		updateUI();

	}


	public void open() {

		myView.getTfTitre().setText("BORDEREAUX DE TITRES DE RECETTES - Exercice " + currentExercice.exeExercice());
		myView.setVisible(true);

	}



	private void ajouterSelection() {


		NSMutableArray maSelection = new NSMutableArray(eodSelection.displayedObjects());
		maSelection.addObjectsFromArray(eodDepenses.selectedObjects());

		NSMutableArray mesDepenses = new NSMutableArray(eodDepenses.displayedObjects());
		mesDepenses.removeObjectsInArray(eodDepenses.selectedObjects());

		int row = myView.getMyEOTableDepenses().getSelectedRow();

		eodDepenses.setObjectArray(new NSArray());
		eodSelection.setObjectArray(new NSArray());
		myView.getMyEOTableDepenses().updateData();
		myView.getMyEOTableSelection().updateData();

		eodDepenses.setObjectArray(mesDepenses);
		eodSelection.setObjectArray(maSelection);

		myView.getMyEOTableDepenses().updateData();
		myView.getMyEOTableSelection().updateData();

		if (row < eodDepenses.displayedObjects().count())
			myView.getMyEOTableDepenses().forceNewSelection(new NSArray(new Integer(row)));
		else
			myView.getMyEOTableDepenses().forceNewSelection(new NSArray(new Integer((eodDepenses.displayedObjects().count()-1))));

		updateUI();

	}


	private void supprimerSelection() {

		NSMutableArray maSelection = new NSMutableArray(eodSelection.displayedObjects());
		maSelection.removeObjectsInArray(eodSelection.selectedObjects());

		NSMutableArray mesDepenses = new NSMutableArray(eodDepenses.displayedObjects());
		mesDepenses.addObjectsFromArray(eodSelection.selectedObjects());

		eodSelection.deleteSelection();

		eodDepenses.setObjectArray(mesDepenses);
		eodSelection.setObjectArray(maSelection);

		myView.getMyEOTableDepenses().updateData();
		myView.getMyEOTableSelection().updateData();

		updateUI();

	}


	private void titrer() {

		EOVAbricotRecetteATitrer firstSelection = (EOVAbricotRecetteATitrer) eodSelection.displayedObjects().objectAtIndex(0);

		for (int i = 1; i < eodSelection.displayedObjects().count(); i++) {
			if (!firstSelection.organ().orgUb().
					equals(((EOVAbricotRecetteATitrer) eodSelection.displayedObjects().objectAtIndex(i)).organ().orgUb())) {
				EODialogs.runInformationDialog("ATTENTION", VOUS_ALLEZ_TITRER_IMPOSSIBLE_MULTI_COMOPSANTE);
				return;
			}
		}

		// AVANT ALGO fenetreDeDialogueYESCancel(ToolsSwing.formaterStringU(VOUS_ALLEZ_MANDATER_ETES_VOUS_SUR), this, METHODE_MANDATER);
		if (currentTypeBordereau.tboTypeCreation().equals(Privileges.DEP_8)) {
			titrerSelection();
		}
		else
			TitreAlgoNibCtrl.sharedInstance(NSApp, 0, 0, 380, 420, this).afficherFenetre();
	}



	/**
	 * 
	 * Mandatement des depenses selectionnees
	 * 
	 */
	private void titrerSelection() {
		NSDictionary dicoRetour = null;

		try {
			NSApp.ceerTransactionLog();
			NSApp.afficherUnLogDansTransactionLog("DEBUT....", 10);

			NSMutableDictionary params = new NSMutableDictionary();
			params.takeValueForKey("vide", "info");

			// recup un id de selection ...
			((ApplicationClient) NSApp).executeStoredProcedure("getSelectionId", params);
			dicoRetour = ((ApplicationClient) NSApp).returnValuesForLastStoredProcedureInvocation();
			Integer abrid = (Integer) dicoRetour.valueForKey("returnValue");

			NSApp.afficherUnLogDansTransactionLog("Recuperation d'un numero de selection ...OK", 20);

			// on recupere la chaine des id 
			String chaine = "";
			for (int i = 0; i < eodSelection.displayedObjects().count(); i++)				
				 chaine = chaine + ((EOVAbricotRecetteATitrer) eodSelection.displayedObjects().objectAtIndex(i)).rpcoId() + "$";

			chaine = chaine + "$";

			// on positionne la selection dans la base de donnee
			//abrid integer,lesdepid varchar,lesrecid varchar ,utlordre integer,exeordre integer ,tboordre integer,abrgroupby varchar,gescode varchar
			params = new NSMutableDictionary();
			params.takeValueForKey(((EOVAbricotRecetteATitrer) eodSelection.displayedObjects().objectAtIndex(0)).orgUb(), "a08gescode");

			//            if (isTitreCollectif()) {
			//                params.takeValueForKey("bordereau_NR1T", "a07abrgroupby");
			//            } else {
			//                params.takeValueForKey("bordereau_1R1T", "a07abrgroupby");
			//            }

			params.takeValueForKey(getAlgo(), "a07abrgroupby");

			params.takeValueForKey(currentTypeBordereau.tboOrdre(), "a06tboordre");
			params.takeValueForKey(NSApp.getCurrentExercice().exeOrdre(), "a05exeordre");
			params.takeValueForKey(NSApp.getCurrentUtilisateur().utlOrdre(), "a04utlordre");
			params.takeValueForKey(chaine, "a03lesrecid");
			params.takeValueForKey("", "a02lesdepid");
			params.takeValueForKey(dicoRetour.valueForKey("returnValue"), "a01abrid");

			((ApplicationClient) NSApp).executeStoredProcedure("setSelectionId", params);
			NSApp.afficherUnLogDansTransactionLog("Enregistrement de votre selection ...OK", 30);

			NSApp.afficherUnLogDansTransactionLog("Generation du bordereau ...OK", 40);

			dicoRetour = null;
			params = new NSMutableDictionary();
			params.takeValueForKey(abrid, "abrid");

			System.out.println("MandatCtrl.mandaterSelection() params  " + params);

			((ApplicationClient) NSApp).executeStoredProcedure("getSelectionBorid", params);
			dicoRetour = ((ApplicationClient) NSApp).returnValuesForLastStoredProcedureInvocation();
			Integer borid = (Integer) dicoRetour.valueForKey("returnValue");

			NSApp.finirTransactionLog();
			NSApp.finirTransactionLog();
			NSApp.fermerTransactionLog();
			
			try {

				ImpressionPanelNibCtrl monImpressionPanelNibCtrl = new ImpressionPanelNibCtrl(NSApp, 0, 0, 360, 360);

				if (borid != null)
					monImpressionPanelNibCtrl.afficherFenetrePourBordereau(EOBordereau.findForBorid(ec, borid));
				else
					monImpressionPanelNibCtrl.afficherFenetrePourBordereau(null);

			} catch (Exception e) {
				System.out.println("actionImprimer ERREUR" + e);
			}

			filter();

		} catch (Exception e) {
			dicoRetour = ((ApplicationClient)NSApp).returnValuesForLastStoredProcedureInvocation();
			NSApp.afficherUnLogDansTransactionLog("probleme !!" + dicoRetour.toString(), 0);
			NSApp.finirTransactionLog();
		}
	}

	protected String getAlgo() {
		return algo;
	}
	protected void setAlgo(String setAlgo) {
		this.algo = setAlgo;
	}


	public void inspecteurAnnuler(NibCtrl object) {

	}

	public void inspecteurValider(NibCtrl object) {

		if (object == TitreAlgoNibCtrl.sharedInstance(NSApp, 0, 0, 380, 420, this)) {
			setAlgo(ALGO_PROBLEME);
			setAlgo(TitreAlgoNibCtrl.sharedInstance(NSApp, 0, 0, 380, 420, this).getCurrentAlgo());
			if (getAlgo().equals(ALGO_PROBLEME)) {
				EODialogs.runInformationDialog("ERREUR", VOUS_ALLEZ_TITRER_IMPOSSIBLE_MAUVAIS_ALGO);
			}
			else {				
				if (EODialogs.runConfirmOperationDialog("Attention",
						VOUS_ALLEZ_TITRER_ETES_VOUS_SUR,
						"OUI", "NON"))
					titrerSelection();
			}
		}
	}


	/**
	 * Impression des depenses selectionnees
	 */
	private void imprimer() {

		try {
			java.util.Calendar currentTime = java.util.Calendar.getInstance();
			String lastModif = "-" + currentTime.get(java.util.Calendar.DAY_OF_MONTH) + "." + currentTime.get(java.util.Calendar.MONTH) + "." + currentTime.get(java.util.Calendar.YEAR) + "-" + currentTime.get(java.util.Calendar.HOUR_OF_DAY) + "h" + currentTime.get(java.util.Calendar.MINUTE) + "m"
			+ currentTime.get(java.util.Calendar.SECOND);

			String chaine = "";
			for (int i = 0; i < eodSelection.displayedObjects().count(); i++) {
				chaine = chaine + ((EOVAbricotRecetteATitrer) eodSelection.displayedObjects().objectAtIndex(i)).rpcoId() + ",";
			}
			chaine = chaine + "-1";

			NSMutableDictionary parameters = new NSMutableDictionary();
			parameters.takeValueForKey(chaine, "RPCOIDS");

			System.out.println("TitreCtrl.imprimer() " + parameters);
			
			NSApp.getToolsCocktailReports().imprimerReportParametres("recetteAttente.jasper", parameters, "recetteAttente" + lastModif);

		} catch (Exception e) {
			System.out.println("actionImprimer " + e);
		}

	}

	private class ListenerTypeBordereau implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			CRICursor.setWaitCursor(myView);

			eodDepenses.setObjectArray(new NSArray());
			eodSelection.setObjectArray(new NSArray());

			currentTypeBordereau = (EOTypeBordereau)eodTypeBordereau.selectedObject();

			if (currentTypeBordereau != null)			 
				filter();

			myView.getMyEOTableDepenses().updateData();
			myView.getMyEOTableSelection().updateData();

			updateUI();

			CRICursor.setDefaultCursor(myView);

		}
	}

	public void setCurrentExercice(EOExercice exercice) {

		currentExercice = exercice;
		myView.getTfTitre().setText("TITRES DE RECETTE - Exercice " + currentExercice.exeExercice());

		if (currentTypeBordereau != null)
			filter();

	}

	private class ListenerDepenses implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

			ajouterSelection();

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			updateUI();

		}
	}


	private class ListenerSelection implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

			supprimerSelection();

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			updateUI();

		}
	}

	private void updateUI() {

		myView.getCheckMesRecettes().setEnabled(currentTypeBordereau != null);

		myView.getBtnRefresh().setEnabled(currentExercice != null && currentTypeBordereau != null);

		myView.getBtnSelectDepense().setEnabled(currentTypeBordereau != null && eodDepenses.selectedObjects().count() > 0);

		myView.getBtnDeselectDepense().setEnabled(currentTypeBordereau != null && eodSelection.selectedObjects().count() > 0);

		myView.getBtnCreerBordereau().setEnabled(currentTypeBordereau != null && eodSelection.displayedObjects().count() > 0);

		myView.getBtnPrint().setEnabled(currentTypeBordereau != null && eodSelection.displayedObjects().count() > 0);

		myView.getBtnCleanSelection().setEnabled(currentTypeBordereau != null && eodSelection.displayedObjects().count() > 0);

		myView.getLblDepenses().setText(eodDepenses.displayedObjects().count()+" / " + CocktailFormats.FORMAT_DECIMAL.format(CocktailUtilities.computeSumForKey(eodDepenses.displayedObjects(), EOVAbricotRecetteATitrer.REC_TTC_SAISIE_KEY)) + " " + CocktailFormats.STRING_EURO);
		myView.getLblSelection().setText(eodSelection.displayedObjects().count()+" / " + CocktailFormats.FORMAT_DECIMAL.format(CocktailUtilities.computeSumForKey(eodSelection.displayedObjects(), EOVAbricotRecetteATitrer.REC_TTC_SAISIE_KEY)) + " " + CocktailFormats.STRING_EURO);

	}

	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

	private class CheckBoxMesDepensesListener implements ActionListener	{
		public CheckBoxMesDepensesListener ()	{
			super();
		}

		public void actionPerformed(ActionEvent anAction)	{

			filter();

		}

	}

}
