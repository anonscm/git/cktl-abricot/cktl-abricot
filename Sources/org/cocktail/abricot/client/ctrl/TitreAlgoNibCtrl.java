/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.abricot.client.ctrl;

import org.cocktail.abricot.client.ApplicationClient;
import org.cocktail.abricot.client.gui.TitreAlgoNib;
import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.ToolsSwing;

/**
 *Ce controleur permet de gerer l'interface de selection de l'algorithme de
 * titrage.<br>
 *L'interface a ete developpee avec NetBeans.<br>
 *Le controleur est initilisee par TitreNibCtrl<br>
 *Le controleur pilote son interface MainNib
 */
public class TitreAlgoNibCtrl extends NibCtrl {

	public static TitreAlgoNibCtrl sharedInstance;

	// methodes
	/** action du controleur */
	private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
	/** action du controleur */
	private static final String METHODE_ACTION_VALIDER = "actionValider";

	// algos
	/** Algorythme pris en charge par le controleur, voir plsq abricot_bordereau */
	public static final String ALGO_bordereau_1R1T = "bordereau_1R1T";
	public static final String ALGO_bordereau_NR1T = "bordereau_NR1T";

	/** Algorythme Valeur lors d'une selection d'algorythme non valide */
	public static final String ALGO_PROBLEME = "PROBLEME";

	// le nib
	/** Interface du controleur */
	private TitreAlgoNib monTitreAlgoNib = null;

	/** Controleur parent */
	private TitreCtrl parentControleur = null;

	public TitreAlgoNibCtrl(ApplicationCocktail ctrl, int alocation_x,
			int alocation_y, int ataille_x, int ataille_y, TitreCtrl parentControleur) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);

		setMonTitreAlgoNib(new TitreAlgoNib());
		creationFenetre(getMonTitreAlgoNib(), ToolsSwing.formaterStringU("Choisir un type de création de vos mandats."), parentControleur);
		//getMonTitreAlgoNib().setPreferredSize(new java.awt.Dimension(590, 230));
		//getMonTitreAlgoNib().setSize(590, 230);
		setWithLogs(false);
	}

    public static TitreAlgoNibCtrl sharedInstance(ApplicationCocktail ctrl, int alocation_x,
			int alocation_y, int ataille_x, int ataille_y, TitreCtrl parentControleur)	{
     	if (sharedInstance == null)
     		sharedInstance = new TitreAlgoNibCtrl(ctrl, alocation_x, alocation_y, ataille_x, ataille_y, parentControleur);
     	return sharedInstance;
     }

	public void creationFenetre(TitreAlgoNib leTitreAlgoNib, String title, TitreCtrl parentControleur) {
		super.creationFenetre(leTitreAlgoNib, title);
		setMonTitreAlgoNib(leTitreAlgoNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
		this.parentControleur = parentControleur;
		initCheckBox();
	}

	public void afficherFenetre() {
		super.afficherFenetre();
		initCheckBox();
		((ApplicationClient) app).removeFromLesPanelsModal(getMonTitreAlgoNib());
		((ApplicationClient) app).activerLesGlassPane();
	}

	private void bindingAndCustomization() {

		try {

			// le cablage des  objets
			getMonTitreAlgoNib()
					.getJButtonCocktailAnnuler()
					.addDelegateActionListener(this, METHODE_ACTION_ANNULER);

			getMonTitreAlgoNib()
					.getJButtonCocktailValider()
					.addDelegateActionListener(this, METHODE_ACTION_VALIDER);

			// les images des objets
			getMonTitreAlgoNib().getJButtonCocktailAnnuler().setIcone(IconeCocktail.FERMER);//"close_view");
			getMonTitreAlgoNib().getJButtonCocktailValider().setIcone(IconeCocktail.VALIDER);//"valider16");

			//sera modal lors d une transaction 
			app.addLesPanelsModal(getMonTitreAlgoNib());

			// obseration des changements de selection de lexercice :
			((ApplicationClient) app).getObserveurExerciceSelection()
					.addObserverForMessage(this, "changementExercice");

		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this,
					"Exception", e.getMessage(), true);
		}

	}

	/**
	 * Initialisation des checkBox de l'interface
	 */
	private void initCheckBox() {
		getMonTitreAlgoNib().getjRadioButtonTitreIndividuel().setSelected(true);

		//		getMonTitreAlgoNib().getJCheckBox1().setSelected(false);
		//		getMonTitreAlgoNib().getJCheckBox2().setSelected(false);
		//		getMonTitreAlgoNib().getJCheckBox3().setSelected(false);
	}

	/**
	 * Permet de determiner l'algorythme en fonction de la checkbox coche
	 * 
	 * @return un algo (public static final String ALGO...)
	 */

	protected String getCurrentAlgo() {
		String algo = ALGO_PROBLEME;

		if (getMonTitreAlgoNib().getjRadioButtonTitreCollectif().isSelected()) {
			algo = ALGO_bordereau_NR1T;
		}
		else {
			algo = ALGO_bordereau_1R1T;
		}

		return algo;
	}

	// Actions
	/**
	 * Annulation de la selection d'un algo on informe le parentControleur
	 * getParentControleur().inspecteurAnnuler(this);
	 **/
	public void actionAnnuler() {

		initCheckBox();
		app.addLesPanelsModal(getMonTitreAlgoNib());
		((ApplicationClient) app).retirerLesGlassPane();

		masquerFenetre();
		getParentControleur().inspecteurAnnuler(this);
	}

	/**
	 * Validation de la selection de l'algo<br>
	 * on informe le parentControleur
	 * getParentControleur().inspecteurValider(this);
	 */
	public void actionValider() {
		//		int cpt = 0;
		//
		//		if (getMonTitreAlgoNib().getJCheckBox1().isSelected()) {
		//			cpt = cpt + 1;
		//		}
		//		if (getMonTitreAlgoNib().getJCheckBox2().isSelected()) {
		//			cpt = cpt + 1;
		//		}
		//		if (getMonTitreAlgoNib().getJCheckBox3().isSelected()) {
		//			cpt = cpt + 1;
		//		}
		//
		//		if (cpt == 1) {

		app.addLesPanelsModal(getMonTitreAlgoNib());
		((ApplicationClient) app).retirerLesGlassPane();

		masquerFenetre();
		getParentControleur().inspecteurValider(this);
		//		}
		//		else {
		//			fenetreDeDialogueInformation(ToolsSwing.formaterStringU("Votre selection n est pas valide !"));
		//		}
	}

	// accesseurs
	protected TitreAlgoNib getMonTitreAlgoNib() {
		return monTitreAlgoNib;
	}

	protected void setMonTitreAlgoNib(TitreAlgoNib monTitreAlgoNib) {
		this.monTitreAlgoNib = monTitreAlgoNib;
	}

	protected TitreCtrl getParentControleur() {
		return parentControleur;
	}

	protected void setParentControleur(TitreCtrl parentControleur) {
		this.parentControleur = parentControleur;
	}

}
