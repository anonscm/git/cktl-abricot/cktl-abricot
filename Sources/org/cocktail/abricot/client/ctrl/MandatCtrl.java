package org.cocktail.abricot.client.ctrl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.abricot.client.ApplicationClient;
import org.cocktail.abricot.client.Privileges;
import org.cocktail.abricot.client.eof.modele.EOBordereau;
import org.cocktail.abricot.client.eof.modele.EOVAbricotDepenseAMandater;
import org.cocktail.abricot.client.finder.FinderTypeBordereau;
import org.cocktail.abricot.client.gui.MandatView;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeBordereau;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.client.common.swing.ZEOTable.ZEOTableListener;
import org.cocktail.client.common.utilities.CRICursor;
import org.cocktail.client.common.utilities.CocktailFormats;
import org.cocktail.client.common.utilities.CocktailUtilities;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class MandatCtrl {

	public static MandatCtrl sharedInstance;

	private static final String ALGO_PROBLEME = "PROBLEME";

	private static final String VOUS_ALLEZ_MANDATER_ETES_VOUS_SUR = "Vous allez creer un bordereau de mandat !! \n Etes vous sûr ?";
	private static final String VOUS_ALLEZ_MANDATER_IMPOSSIBLE_MULTI_COMPOSANTE = "Vous allez creer un bordereau de mandat !! \n Impossible votre selection doit concerner une seule UB !";
	private static final String VOUS_ALLEZ_MANDATER_IMPOSSIBLE_MULTI_MP_EXTOURNE = "Vous allez creer un bordereau de mandat !! \n Impossible, les dépenses à extourner doivent être séparées des autres dépenses !";
	private static final String VOUS_ALLEZ_MANDATER_IMPOSSIBLE_MAUVAIS_ALGO = "Vous allez creer un bordereau de mandat !! \n Impossible mauvais type de construction des mandats !";

	private ApplicationClient NSApp = (ApplicationClient) ApplicationClient.sharedApplication();
	private EOEditingContext ec;

	private MandatView myView;

	private EODisplayGroup eodTypeBordereau = new EODisplayGroup(), eodDepenses = new EODisplayGroup(), eodSelection = new EODisplayGroup();

	private ListenerTypeBordereau listenerTypeBordereau = new ListenerTypeBordereau();
	private ListenerDepenses listenerDepenses = new ListenerDepenses();
	private ListenerSelection listenerSelection = new ListenerSelection();

	private EOExercice currentExercice;
	private EOTypeBordereau currentTypeBordereau;

	private String algo = "";

	public MandatCtrl(EOEditingContext globalEc) {

		ec = globalEc;

		myView = new MandatView(new JFrame(), false, eodTypeBordereau, eodDepenses, eodSelection);

		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				myView.setVisible(false);
			}
		});

		myView.getBtnRefresh().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				filter();
			}
		});

		myView.getBtnSelectDepense().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterSelection();
			}
		});

		myView.getBtnDeselectDepense().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerSelection();
			}
		});

		myView.getBtnPrint().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimer();
			}
		});

		myView.getBtnCreerBordereau().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				mandater();
			}
		});

		myView.getBtnCleanSelection().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				filter();
			}
		});

		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOVAbricotDepenseAMandater.ORG_UB_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOVAbricotDepenseAMandater.DEP_ID_KEY, EOSortOrdering.CompareDescending));

		eodDepenses.setSortOrderings(mySort);

		mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOVAbricotDepenseAMandater.ORG_UB_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOVAbricotDepenseAMandater.DEP_ID_KEY, EOSortOrdering.CompareDescending));

		eodSelection.setSortOrderings(mySort);

		mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOTypeBordereau.TBO_LIBELLE_KEY, EOSortOrdering.CompareAscending));

		eodTypeBordereau.setSortOrderings(mySort);

		currentExercice = NSApp.getCurrentExercice();

		myView.getMyEOTableTypeBordereau().addListener(listenerTypeBordereau);
		eodTypeBordereau.setObjectArray(FinderTypeBordereau.findTypesDeBordereauxDepense(NSApp));
		myView.getMyEOTableTypeBordereau().updateData();

		myView.getMyEOTableDepenses().addListener(listenerDepenses);
		myView.getMyEOTableSelection().addListener(listenerSelection);

		myView.getTfFiltreUb().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreCr().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreMode().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreLibelleFacture().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreFournisseur().getDocument().addDocumentListener(new ADocumentListener());

		myView.getCheckMesDepenses().addActionListener(new CheckBoxMesDepensesListener());

		updateUI();
	}

	public static MandatCtrl sharedInstance(EOEditingContext editingContext) {
		if (sharedInstance == null)
			sharedInstance = new MandatCtrl(editingContext);
		return sharedInstance;
	}

	private EOQualifier filterQualifier() {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (myView.getCheckMesDepenses().isSelected())
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVAbricotDepenseAMandater.UTILISATEUR_KEY + " = %@", new NSArray(NSApp.getCurrentUtilisateur())));

		if (myView.getTfFiltreUb().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVAbricotDepenseAMandater.ORG_UB_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreUb().getText().toUpperCase() + "*'", null));

		if (myView.getTfFiltreCr().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVAbricotDepenseAMandater.ORG_CR_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreCr().getText().toUpperCase() + "*'", null));

		if (myView.getTfFiltreLibelleFacture().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVAbricotDepenseAMandater.DPP_NUMERO_FACTURE_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreLibelleFacture().getText() + "*'", null));

		if (myView.getTfFiltreFournisseur().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVAbricotDepenseAMandater.ADR_NOM_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreFournisseur().getText() + "*'", null));

		if (myView.getTfFiltreMode().getText().length() > 0)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOVAbricotDepenseAMandater.MOD_CODE_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreMode().getText() + "*'", null));

		return new EOAndQualifier(mesQualifiers);
	}

	private void filter() {

		eodSelection.setObjectArray(new NSArray());
		eodDepenses.setObjectArray(EOVAbricotDepenseAMandater.find(ec, currentExercice, currentTypeBordereau, null, (NSArray) NSApp.getMesOrgans()));

		CocktailUtilities.invalidateObjects(ec, eodDepenses.displayedObjects());

		eodDepenses.setObjectArray(EOVAbricotDepenseAMandater.find(ec, currentExercice, currentTypeBordereau, null, (NSArray) NSApp.getMesOrgans()));

		eodDepenses.setQualifier(filterQualifier());
		eodDepenses.updateDisplayedObjects();

		myView.getMyEOTableDepenses().updateData();
		myView.getMyTableModelDepenses().fireTableDataChanged();

		myView.getMyEOTableSelection().updateData();

		updateUI();

	}

	public void open() {

		myView.getTfTitre().setText("BORDEREAUX DE DEPENSES - Exercice " + currentExercice.exeExercice());
		myView.setVisible(true);

	}

	private void ajouterSelection() {

		NSMutableArray maSelection = new NSMutableArray(eodSelection.displayedObjects());
		maSelection.addObjectsFromArray(eodDepenses.selectedObjects());

		int row = myView.getMyEOTableDepenses().getSelectedRow();

		NSMutableArray mesDepenses = new NSMutableArray(eodDepenses.displayedObjects());
		mesDepenses.removeObjectsInArray(eodDepenses.selectedObjects());

		eodDepenses.setObjectArray(new NSArray());
		eodSelection.setObjectArray(new NSArray());
		myView.getMyEOTableDepenses().updateData();
		myView.getMyEOTableSelection().updateData();

		eodDepenses.setObjectArray(mesDepenses);
		eodSelection.setObjectArray(maSelection);

		myView.getMyEOTableDepenses().updateData();
		myView.getMyEOTableSelection().updateData();

		if (row < eodDepenses.displayedObjects().count())
			myView.getMyEOTableDepenses().forceNewSelection(new NSArray(new Integer(row)));
		else
			myView.getMyEOTableDepenses().forceNewSelection(new NSArray(new Integer((eodDepenses.displayedObjects().count() - 1))));

		updateUI();

	}

	private void supprimerSelection() {

		NSMutableArray maSelection = new NSMutableArray(eodSelection.displayedObjects());
		maSelection.removeObjectsInArray(eodSelection.selectedObjects());

		NSMutableArray mesDepenses = new NSMutableArray(eodDepenses.displayedObjects());
		mesDepenses.addObjectsFromArray(eodSelection.selectedObjects());

		eodSelection.deleteSelection();

		eodDepenses.setObjectArray(mesDepenses);
		eodSelection.setObjectArray(maSelection);

		myView.getMyEOTableDepenses().updateData();
		myView.getMyEOTableSelection().updateData();

		updateUI();

	}

	/**
	 * Mandatement des depenses selectionnees
	 */
	private void mandater() {

		EOQualifier qualExtourne = new EOKeyValueQualifier(EOVAbricotDepenseAMandater.MOD_DOM_KEY, EOQualifier.QualifierOperatorEqual, "EXTOURNE");
		NSArray res = EOQualifier.filteredArrayWithQualifier(eodSelection.displayedObjects(), qualExtourne);
		if (res.count() > 0) {
			EOQualifier qualNotExtourne = new EOKeyValueQualifier(EOVAbricotDepenseAMandater.MOD_DOM_KEY, EOQualifier.QualifierOperatorNotEqual, "EXTOURNE");
			NSArray res2 = EOQualifier.filteredArrayWithQualifier(eodSelection.displayedObjects(), qualNotExtourne);
			if (res2.count() > 0) {
				EODialogs.runInformationDialog("ATTENTION", VOUS_ALLEZ_MANDATER_IMPOSSIBLE_MULTI_MP_EXTOURNE);
				return;
			}
		}

		EOVAbricotDepenseAMandater firstSelection = (EOVAbricotDepenseAMandater) eodSelection.displayedObjects().objectAtIndex(0);

		for (int i = 1; i < eodSelection.displayedObjects().count(); i++) {
			if (!firstSelection.organ().orgUb().
					equals(((EOVAbricotDepenseAMandater) eodSelection.displayedObjects().objectAtIndex(i)).organ().orgUb())) {
				EODialogs.runInformationDialog("ATTENTION", VOUS_ALLEZ_MANDATER_IMPOSSIBLE_MULTI_COMPOSANTE);
				return;
			}
		}

		// AVANT ALGO fenetreDeDialogueYESCancel(ToolsSwing.formaterStringU(VOUS_ALLEZ_MANDATER_ETES_VOUS_SUR), this, METHODE_MANDATER);
		if (currentTypeBordereau.tboTypeCreation().equals(Privileges.DEP_8)) {
			mandaterSelection();
		}
		else
			MandatAlgoNibCtrl.sharedInstance(NSApp, 0, 0, 380, 420, this).afficherFenetre();
	}

	private void mandaterSelection() {
		NSDictionary dicoRetour = null;
		try {
			NSApp.ceerTransactionLog();
			NSApp.afficherUnLogDansTransactionLog("DEBUT....", 10);

			NSMutableDictionary params = new NSMutableDictionary();
			params.takeValueForKey("vide", "info");

			// recup un id de selection ...
			NSApp.executeStoredProcedure("getSelectionId", params);
			dicoRetour = NSApp.returnValuesForLastStoredProcedureInvocation();
			Integer abrid = (Integer) dicoRetour.valueForKey("returnValue");

			NSApp.afficherUnLogDansTransactionLog("Recuperation d'un numero de selection ...OK", 20);

			// on recupere la chaine des id des depenses a mandater
			String chaine = "";
			for (int i = 0; i < eodSelection.displayedObjects().count(); i++)
				chaine = chaine + ((EOVAbricotDepenseAMandater) eodSelection.displayedObjects().objectAtIndex(i)).dpcoId() + "$";
			chaine = chaine + "$";

			// on positionne la selection dans la base de donnee
			//abrid integer,lesdepid varchar,lesrecid varchar ,utlordre integer,exeordre integer ,tboordre integer,abrgroupby varchar,gescode varchar
			params = new NSMutableDictionary();
			params.takeValueForKey(((EOVAbricotDepenseAMandater) eodSelection.displayedObjects().objectAtIndex(0)).orgUb(), "a08gescode");
			//params.takeValueForKey("ndep_mand_org_fou_rib_pco_mod","a07abrgroupby");

			// si bordereau de reversement DEP_8 alors ALGO_bordereau_1D1M
			if (currentTypeBordereau.tboTypeCreation().equals(Privileges.DEP_8))
				params.takeValueForKey(MandatAlgoNibCtrl.ALGO_bordereau_1D1M, "a07abrgroupby");
			else
				params.takeValueForKey(getAlgo(), "a07abrgroupby");

			params.takeValueForKey(currentTypeBordereau.tboOrdre(), "a06tboordre");
			params.takeValueForKey(NSApp.getCurrentExercice().exeOrdre(), "a05exeordre");
			params.takeValueForKey(NSApp.getCurrentUtilisateur().utlOrdre(), "a04utlordre");
			params.takeValueForKey("", "a03lesrecid");
			params.takeValueForKey(chaine, "a02lesdepid");
			params.takeValueForKey(dicoRetour.valueForKey("returnValue"), "a01abrid");

			System.out.println("MandatCtrl.mandaterSelection() params  " + params);

			NSApp.executeStoredProcedure("setSelectionId", params);
			NSApp.afficherUnLogDansTransactionLog("Enregistrement de votre selection ...OK", 30);

			NSApp.afficherUnLogDansTransactionLog("Generation du bordereau ...OK", 40);

			dicoRetour = null;
			params = new NSMutableDictionary();
			params.takeValueForKey(abrid, "abrid");

			NSApp.executeStoredProcedure("getSelectionBorid", params);
			dicoRetour = NSApp.returnValuesForLastStoredProcedureInvocation();
			Integer borid = (Integer) dicoRetour.valueForKey("returnValue");

			filter();

			NSApp.finirTransactionLog();
			NSApp.fermerTransactionLog();
			try {

				ImpressionPanelNibCtrl monImpressionPanelNibCtrl = new ImpressionPanelNibCtrl(NSApp, 0, 0, 360, 360);

				if (borid != null)
					monImpressionPanelNibCtrl.afficherFenetrePourBordereau(EOBordereau.findForBorid(ec, borid));
				else
					monImpressionPanelNibCtrl.afficherFenetrePourBordereau(null);

			} catch (Exception e) {
				System.out.println("actionImprimer OUPS" + e);
			}

		} catch (Exception e) {
			dicoRetour = NSApp.returnValuesForLastStoredProcedureInvocation();
			NSApp.afficherUnLogDansTransactionLog("probleme !!" + dicoRetour.toString(), 0);
			NSApp.finirTransactionLog();
		}
	}

	protected String getAlgo() {
		return algo;
	}

	protected void setAlgo(String setAlgo) {
		this.algo = setAlgo;
	}

	public void inspecteurAnnuler(NibCtrl object) {

	}

	public void inspecteurValider(NibCtrl object) {

		if (object == MandatAlgoNibCtrl.sharedInstance(NSApp, 0, 0, 380, 420, this)) {
			setAlgo(ALGO_PROBLEME);
			setAlgo(MandatAlgoNibCtrl.sharedInstance(NSApp, 0, 0, 380, 420, this).getCurrentAlgo());
			if (getAlgo().equals(ALGO_PROBLEME)) {
				EODialogs.runInformationDialog("ERREUR", VOUS_ALLEZ_MANDATER_IMPOSSIBLE_MAUVAIS_ALGO);
			}
			else {
				if (EODialogs.runConfirmOperationDialog("Attention",
						VOUS_ALLEZ_MANDATER_ETES_VOUS_SUR,
						"OUI", "NON"))
					mandaterSelection();
			}
		}
	}

	/**
	 * Impression des depenses selectionnees
	 */
	private void imprimer() {

		try {
			java.util.Calendar currentTime = java.util.Calendar.getInstance();
			String lastModif = "-" + currentTime.get(java.util.Calendar.DAY_OF_MONTH) + "." + currentTime.get(java.util.Calendar.MONTH) + "." + currentTime.get(java.util.Calendar.YEAR) + "-"
					+ currentTime.get(java.util.Calendar.HOUR_OF_DAY) + "h" + currentTime.get(java.util.Calendar.MINUTE) + "m" + currentTime.get(java.util.Calendar.SECOND);

			// on recupere la chaine des id des depenses a mandater
			String chaine = "";
			for (int i = 0; i < eodSelection.displayedObjects().count(); i++) {
				chaine = chaine + ((EOVAbricotDepenseAMandater) eodSelection.displayedObjects().objectAtIndex(i)).dpcoId() + ",";
			}
			chaine = chaine + "-1";

			NSMutableDictionary parameters = new NSMutableDictionary();
			parameters.takeValueForKey(chaine, "DPCOIDS");
			//System.out.println("\n chaine \n"+chaine);

			NSApp.getToolsCocktailReports().imprimerReportParametres("depense.jasper", parameters, "depense" + lastModif);

		} catch (Exception e) {
			System.out.println("actionImprimer OUPS" + e);
		}

	}

	private class ListenerTypeBordereau implements ZEOTableListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			CRICursor.setWaitCursor(myView);

			eodDepenses.setObjectArray(new NSArray());
			eodSelection.setObjectArray(new NSArray());

			currentTypeBordereau = (EOTypeBordereau) eodTypeBordereau.selectedObject();

			if (currentTypeBordereau != null)
				filter();

			myView.getMyEOTableDepenses().updateData();
			myView.getMyEOTableSelection().updateData();

			updateUI();

			CRICursor.setDefaultCursor(myView);

		}
	}

	public void setCurrentExercice(EOExercice exercice) {

		currentExercice = exercice;
		myView.getTfTitre().setText("DEPENSES - Exercice " + currentExercice.exeExercice());

		filter();

	}

	private class ListenerDepenses implements ZEOTableListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

			ajouterSelection();

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			updateUI();

		}
	}

	private class ListenerSelection implements ZEOTableListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

			supprimerSelection();

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			updateUI();

		}
	}

	private void updateUI() {

		myView.getCheckMesDepenses().setEnabled(currentTypeBordereau != null);

		myView.getBtnRefresh().setEnabled(currentExercice != null && currentTypeBordereau != null);

		myView.getBtnSelectDepense().setEnabled(currentTypeBordereau != null && eodDepenses.selectedObjects().count() > 0);

		myView.getBtnDeselectDepense().setEnabled(currentTypeBordereau != null && eodSelection.selectedObjects().count() > 0);

		myView.getBtnCreerBordereau().setEnabled(currentTypeBordereau != null && eodSelection.displayedObjects().count() > 0);

		myView.getBtnPrint().setEnabled(currentTypeBordereau != null && eodSelection.displayedObjects().count() > 0);

		myView.getBtnCleanSelection().setEnabled(currentTypeBordereau != null && eodSelection.displayedObjects().count() > 0);

		myView.getLblDepenses().setText(eodDepenses.displayedObjects().count() + " / " + CocktailFormats.FORMAT_DECIMAL.format(CocktailUtilities.computeSumForKey(eodDepenses.displayedObjects(), EOVAbricotDepenseAMandater.DPCO_TTC_SAISIE_KEY)) + " " + CocktailFormats.STRING_EURO);
		myView.getLblSelection().setText(eodSelection.displayedObjects().count() + " / " + CocktailFormats.FORMAT_DECIMAL.format(CocktailUtilities.computeSumForKey(eodSelection.displayedObjects(), EOVAbricotDepenseAMandater.DPCO_TTC_SAISIE_KEY)) + " " + CocktailFormats.STRING_EURO);

	}

	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();
		}

		public void removeUpdate(DocumentEvent e) {
			filter();
		}
	}

	private class CheckBoxMesDepensesListener implements ActionListener {
		public CheckBoxMesDepensesListener() {
			super();
		}

		public void actionPerformed(ActionEvent anAction) {

			filter();

		}

	}

}
