package org.cocktail.abricot.client.ctrl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.abricot.client.ApplicationClient;
import org.cocktail.abricot.client.eof.modele.EOBordereauRejet;
import org.cocktail.abricot.client.finder.FinderTypeBordereau;
import org.cocktail.abricot.client.gui.VisaRejetView;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeBordereau;
import org.cocktail.client.common.swing.ZEOTable.ZEOTableListener;
import org.cocktail.client.common.utilities.CRICursor;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class VisaRejetCtrl {

	public static VisaRejetCtrl sharedInstance;

	private static final String VOUS_ALLEZ_VISER_ETES_VOUS_SUR = "Confirmez-vous le visa du bordereau de rejet sélectionné ?";

	private	ApplicationClient	NSApp = (ApplicationClient)ApplicationClient.sharedApplication();	
	private EOEditingContext 	ec;

	private VisaRejetView myView;

	private EODisplayGroup eodTypeBordereau = new EODisplayGroup(), eodDepenses = new EODisplayGroup(), eodSelection = new EODisplayGroup();

	private ListenerTypeBordereau listenerTypeBordereau = new ListenerTypeBordereau();
	private ListenerDepenses listenerDepenses = new ListenerDepenses();

	private EOExercice currentExercice;
	private EOTypeBordereau currentTypeBordereau;
	private EOBordereauRejet currentBordereauRejet;

	private String algo = "";

	public VisaRejetCtrl(EOEditingContext globalEc) {

		ec = globalEc;

		myView = new VisaRejetView(new JFrame(), false, eodTypeBordereau, eodDepenses, eodSelection);


		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				myView.setVisible(false);
			}
		});

		myView.getBtnRefresh().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				filter();
			}
		});

		myView.getBtnViser().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				viser();
			}
		});

//		NSMutableArray mySort = new NSMutableArray();
//		mySort.addObject(new EOSortOrdering(EOVAbricotDepenseAMandater.ORG_UB_KEY, EOSortOrdering.CompareAscending));
//		mySort.addObject(new EOSortOrdering(EOVAbricotDepenseAMandater.ORG_CR_KEY, EOSortOrdering.CompareAscending));
//		mySort.addObject(new EOSortOrdering(EOVAbricotDepenseAMandater.ORG_SOUSCR_KEY, EOSortOrdering.CompareAscending));
//		mySort.addObject(new EOSortOrdering(EOVAbricotDepenseAMandater.DPP_NUMERO_FACTURE_KEY, EOSortOrdering.CompareAscending));
//
//		eodSelection.setSortOrderings(mySort);


		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOTypeBordereau.TBO_LIBELLE_KEY, EOSortOrdering.CompareAscending));

		eodTypeBordereau.setSortOrderings(mySort);

		currentExercice = NSApp.getCurrentExercice();

		myView.getTfFiltreUb().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreNoRejet().getDocument().addDocumentListener(new ADocumentListener());

		myView.getCheckBdxValides().setSelected(true);
		myView.getCheckBdxValides().addActionListener(new CheckBoxEtatListener());
		myView.getCheckBdxVises().addActionListener(new CheckBoxEtatListener());

		myView.getMyEOTableDepenses().addListener(listenerDepenses);
		myView.getMyEOTableTypeBordereau().addListener(listenerTypeBordereau);

		eodTypeBordereau.setObjectArray(FinderTypeBordereau.findTypesDeBordereauxVisa(NSApp));
		myView.getMyEOTableTypeBordereau().updateData();
		
	}


	public static VisaRejetCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new VisaRejetCtrl(editingContext);
		return sharedInstance;
	}



	private EOQualifier filterQualifier() {

		NSMutableArray mesQualifiers = new NSMutableArray();

//		if (myView.getCheckBdxValides().isSelected()) 
//			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBordereauRejet.BRJ_ETAT_KEY + " = %@", new NSArray("VALIDE")));
//		else
//			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBordereauRejet.BRJ_ETAT_KEY + " = %@", new NSArray("VISE")));
			
		if (myView.getTfFiltreUb().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBordereauRejet.GES_CODE_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreUb().getText() + "*'",null));

		if (myView.getTfFiltreNoRejet().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBordereauRejet.BRJ_NUM_KEY + " = " + myView.getTfFiltreNoRejet().getText(),null));

		return new EOAndQualifier(mesQualifiers);
	}





	private void filter() {

		eodSelection.setObjectArray(new NSArray());
		
		eodDepenses.setObjectArray(EOBordereauRejet.find(ec, currentExercice , currentTypeBordereau, (myView.getCheckBdxValides().isSelected())?"VALIDE":"VISE"));

		eodDepenses.setQualifier(filterQualifier());
		eodDepenses.updateDisplayedObjects();

		myView.getMyEOTableDepenses().updateData();
		myView.getMyTableModelDepenses().fireTableDataChanged();

		myView.getMyEOTableSelection().updateData();

		updateUI();

	}


	public void open() {

		myView.getTfTitre().setText("VISA DES BORDEREAUX DE REJETS - Exercice " + currentExercice.exeExercice());
		myView.setVisible(true);

		updateUI();
	}


	/**
	 * 
	 * Mandatement des depenses selectionnees
	 * 
	 */
	private void viser() {
		
		NSDictionary dicoRetour= null;

		 if (!EODialogs.runConfirmOperationDialog("Attention",
				 VOUS_ALLEZ_VISER_ETES_VOUS_SUR,
				 "OUI", "NON"))
			 return;

		try{
			NSApp.ceerTransactionLog();
			NSApp.afficherUnLogDansTransactionLog("DEBUT....",10);
			NSMutableDictionary params=  new NSMutableDictionary();
			NSApp.afficherUnLogDansTransactionLog("Préparation de la transaction ...OK",40);
			NSApp.afficherUnLogDansTransactionLog("Visa du bordereau ...",60);

			params=  new NSMutableDictionary();
			params.takeValueForKey((Integer)((EOBordereauRejet)eodDepenses.selectedObjects().objectAtIndex(0)).brjOrdreBis(), "brjordre");
			
			NSApp.executeStoredProcedure("viserBordereauRejet",params);
			NSApp.afficherUnLogDansTransactionLog("Visa du bordereau ...OK",80);
			NSApp.afficherUnLogDansTransactionLog("FIN...",100);
			NSApp.finirTransactionLog();
			NSApp.invalidateObject(ec, ((EOBordereauRejet)eodDepenses.selectedObjects().objectAtIndex(0)));
			
			EODialogs.runInformationDialog("VISA", "Le bordereau de rejet " + currentBordereauRejet.brjNum() + " a été visé.");
			
		} catch (Exception e) {
			dicoRetour=((ApplicationClient) NSApp).returnValuesForLastStoredProcedureInvocation();
			NSApp.afficherUnLogDansTransactionLog("probleme !!"+dicoRetour.toString(),0);
			NSApp.finirTransactionLog();
		}
		filter();
	}



	protected String getAlgo() {
		return algo;
	}
	protected void setAlgo(String setAlgo) {
		this.algo = setAlgo;
	}



	private class ListenerTypeBordereau implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			CRICursor.setWaitCursor(myView);

			eodDepenses.setObjectArray(new NSArray());
			eodSelection.setObjectArray(new NSArray());

			currentTypeBordereau = (EOTypeBordereau)eodTypeBordereau.selectedObject();

			if (currentTypeBordereau != null) 
				filter();

			myView.getMyEOTableDepenses().updateData();
			myView.getMyEOTableSelection().updateData();

			updateUI();

			CRICursor.setDefaultCursor(myView);

		}
	}

	public void setCurrentExercice(EOExercice exercice) {

		currentExercice = exercice;
		myView.getTfTitre().setText("DEPENSES - Exercice " + currentExercice.exeExercice());

		if (currentTypeBordereau != null)
			filter();

	}

	private class ListenerDepenses implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			currentBordereauRejet = (EOBordereauRejet)eodDepenses.selectedObject();
			
			NSMutableArray detail = new NSMutableArray();
			
			if (currentBordereauRejet != null) {

				detail .addObjectsFromArray(currentBordereauRejet.mandats());
				
				detail .addObjectsFromArray(currentBordereauRejet.titres());

			}

			eodSelection.setObjectArray(detail.immutableClone());
			myView.getMyEOTableSelection().updateData();
			
			updateUI();

		}
	}


	private void updateUI() {

		myView.getBtnRefresh().setEnabled(currentExercice != null && currentTypeBordereau != null);

		myView.getBtnViser().setEnabled(currentTypeBordereau != null && currentBordereauRejet != null && currentBordereauRejet.brjEtat().equals("VALIDE"));

	}

	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

	private class CheckBoxEtatListener implements ActionListener	{
		public CheckBoxEtatListener ()	{
			super();
		}

		public void actionPerformed(ActionEvent anAction)	{

			filter();

		}

	}

}
