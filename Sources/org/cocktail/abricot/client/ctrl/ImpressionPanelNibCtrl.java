/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.abricot.client.ctrl;

import javax.swing.ImageIcon;

import org.cocktail.abricot.client.Privileges;
import org.cocktail.abricot.client.eof.modele.EOBordereau;
import org.cocktail.abricot.client.eof.modele.EOMandat;
import org.cocktail.abricot.client.eof.modele.EOTitre;
import org.cocktail.abricot.client.eof.modele.EOVAbricotRecetteATitrer;
import org.cocktail.abricot.client.gui.ImpressionPanelNib;
import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.nibctrl.NibCtrl;

import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Ce controleur permet de gerer le panel d'impression.<br>
 * L'interface a ete developpee avec NetBeans.<br>
 * Le controleur est initilisee par le MainNibCtrl<br>
 * Le controleur pilote son interface ImpressionPanelNib
 * 
 * @author Rivalland Frederic (frederic.rivalland@univ-paris5.fr)
 * @author Rodolphe Prin
 */
public class ImpressionPanelNibCtrl extends NibCtrl {
	/** Bordereau courant du panel d'impression */
	private EOBordereau currentBordereau = null;
	/** Interface du controleur */
	private ImpressionPanelNib monImpressionPanelNib = null;
	private EOTitre currentTitre;
	private EOMandat currentMandat;

	/** action du controleur */
	private static final String METHODE_ACTION_FERMER = "actionFermer";
	/** action du controleur */
	private static final String METHODE_ACTION_IMPRIMER = "actionImprimer";

	public ImpressionPanelNibCtrl(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		setWithLogs(true);
		monImpressionPanelNib = new ImpressionPanelNib();
		creationFenetre(monImpressionPanelNib, "IMPRESSION DES BORDEREAUX");
	}

	public void creationFenetre(ImpressionPanelNib monImprissionPanelNib, String title) {
		super.creationFenetre(monImprissionPanelNib, title);
		setMonImpressionPanelNib(monImprissionPanelNib);
		bindingAndCustomization();
		setNibCtrlLocation(LOCATION_MIDDLE);
	}

	private void bindingAndCustomization() {
		try {
			EOClientResourceBundle leBundle = new EOClientResourceBundle();
			// les listeners
			getMonImpressionPanelNib().getJButtonCocktailImpression().addDelegateActionListener(this, METHODE_ACTION_IMPRIMER);
			getMonImpressionPanelNib().getJButtonCocktailFermer().addDelegateActionListener(this, METHODE_ACTION_FERMER);

			// icones
			getMonImpressionPanelNib().getJButtonCocktailImpression().setIcone(IconeCocktail.IMPRIMER);//"page_obj");
			getMonImpressionPanelNib().getJLabelCocktailIcone().setIcon((ImageIcon) leBundle.getObject("imprimante"));
			getMonImpressionPanelNib().getJButtonCocktailFermer().setIcone(IconeCocktail.FERMER);//"close_view");

			//sera modal lors d une transaction 
			app.addLesPanelsModal(getMonImpressionPanelNib());
		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(), true);
		}
	}

	private void enableCheckBoxes() {
		getMonImpressionPanelNib().getjCheckBoxBord().setEnabled(false);
		getMonImpressionPanelNib().getjCheckBoxBordJourn().setEnabled(false);
		getMonImpressionPanelNib().getjCheckBoxMandats().setEnabled(false);
		getMonImpressionPanelNib().getjCheckBoxRecapDepenses().setEnabled(false);
		getMonImpressionPanelNib().getjCheckBoxBulletinLiquidation().setEnabled(false);
		getMonImpressionPanelNib().getjCheckBoxBulletinPerception().setEnabled(false);
		getMonImpressionPanelNib().getjCheckBoxSommesAPayer().setEnabled(false);
		getMonImpressionPanelNib().getjCheckBoxTitre().setEnabled(false);
		getMonImpressionPanelNib().getjCheckBoxAnnexeTitreCo().setEnabled(false);

		getMonImpressionPanelNib().getjCheckBoxBord().setEnabled(true);
		getMonImpressionPanelNib().getjCheckBoxBordJourn().setEnabled(true);
		getMonImpressionPanelNib().getjCheckBoxMandats().setEnabled(isBorMandats() || isBorORVs());
		getMonImpressionPanelNib().getjCheckBoxRecapDepenses().setEnabled(isBorMandats());
		getMonImpressionPanelNib().getjCheckBoxBulletinLiquidation().setEnabled(isBorTitres());
		getMonImpressionPanelNib().getjCheckBoxBulletinPerception().setEnabled(isBorTitres());
		getMonImpressionPanelNib().getjCheckBoxSommesAPayer().setEnabled(isBorTitres());
		getMonImpressionPanelNib().getjCheckBoxTitre().setEnabled(isBorTitres() || isBorAor());
		getMonImpressionPanelNib().getjCheckBoxAnnexeTitreCo().setEnabled((isBorTitres() || isBorAor()) && isBorTitreCo());

		getMonImpressionPanelNib().getjCheckBoxBord().setSelected(true);
		getMonImpressionPanelNib().getjCheckBoxBordJourn().setSelected(true);
		getMonImpressionPanelNib().getjCheckBoxMandats().setSelected(isBorMandats() || isBorORVs());
		getMonImpressionPanelNib().getjCheckBoxRecapDepenses().setSelected(isBorMandats());
		getMonImpressionPanelNib().getjCheckBoxBulletinLiquidation().setSelected(isBorTitres());
		getMonImpressionPanelNib().getjCheckBoxBulletinPerception().setSelected(isBorTitres());
		getMonImpressionPanelNib().getjCheckBoxSommesAPayer().setSelected(isBorTitres());

		getMonImpressionPanelNib().getjCheckBoxTitre().setSelected(isBorTitres() || isBorAor());
		getMonImpressionPanelNib().getjCheckBoxAnnexeTitreCo().setSelected((isBorTitres() || isBorAor()) && isBorTitreCo());

	}

	private void enableCheckBoxesForTitre() {
		enableCheckBoxes();
		getMonImpressionPanelNib().getjCheckBoxBord().setEnabled(false);
		getMonImpressionPanelNib().getjCheckBoxBordJourn().setEnabled(false);

		getMonImpressionPanelNib().getjCheckBoxBord().setSelected(false);
		getMonImpressionPanelNib().getjCheckBoxBordJourn().setSelected(false);
	}

	private void enableCheckBoxesForMandat() {
		getMonImpressionPanelNib().getjCheckBoxBord().setEnabled(false);
		getMonImpressionPanelNib().getjCheckBoxBordJourn().setEnabled(false);
		getMonImpressionPanelNib().getjCheckBoxRecapDepenses().setEnabled(false);

		getMonImpressionPanelNib().getjCheckBoxBord().setSelected(false);
		getMonImpressionPanelNib().getjCheckBoxBordJourn().setSelected(false);
		getMonImpressionPanelNib().getjCheckBoxRecapDepenses().setSelected(false);
	}

	public boolean isBorMandats() {
		return (currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.DEP_2) ||
				currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.DEP_1) ||
				currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.DEP_3) ||
				currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.DEP_4) ||
				currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.DEP_5) ||
				currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.DEP_19) ||
				currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.DEP_PAF) ||
				currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.CAP_PAF) ||
				currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.DEP_REGUL_PAF) || 
				currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.PID_201));

	}

	public boolean isBorORVs() {
		return (currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.DEP_8) ||
				currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.DEP_18) || 
				currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.ORV_PAF));
	}

	public boolean isBorTitres() {
		return (currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.REC_12) ||
				currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.REC_13) ||
				currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.REC_6) ||
				currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.REC_7) || 
				currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.PIR_200));
	}

	public boolean isBorAor() {
		return (currentBordereau.typeBordereau().tboTypeCreation().equals(Privileges.REC_9));
	}

	public boolean isBorTitreCo() {

		EOQualifier qual = new EOKeyValueQualifier(EOTitre.TIT_LIBELLE_KEY, EOQualifier.QualifierOperatorEqual, "TITRE COLLECTIF");
		EOQualifier qual2 = new EOKeyValueQualifier(EOTitre.BORDEREAU_KEY, EOQualifier.QualifierOperatorEqual, currentBordereau);
		NSArray res = EOVAbricotRecetteATitrer.findTitres(app, new EOAndQualifier(new NSArray(new Object[] {qual, qual2})));
		return (res.count() > 0);

	}

	//	actions
	/**
	 * Permet de declencher la generation des pdfs du bordereau
	 */
	public void actionImprimer() {

		try {
			java.util.Calendar currentTime = java.util.Calendar.getInstance();
			String lastModif = "-" + currentTime.get(java.util.Calendar.DAY_OF_MONTH) + "." + (currentTime.get(java.util.Calendar.MONTH) + 1) + "." + currentTime.get(java.util.Calendar.YEAR) + "-"
			+ currentTime.get(java.util.Calendar.HOUR_OF_DAY) + "h" + currentTime.get(java.util.Calendar.MINUTE) + "m" + currentTime.get(java.util.Calendar.SECOND);

			NSMutableDictionary parameters = new NSMutableDictionary();
			parameters.takeValueForKey(new Integer(currentBordereau.borIdBis().intValue()), "BORDEREAU");
						
			// Calcul du nombre de mandats
			NSMutableArray detail = new NSMutableArray();
			
			detail.addObjectsFromArray(EOMandat.findForBordereau(app.getAppEditingContext(), currentBordereau));
			detail.addObjectsFromArray(EOTitre.findForBordereau(app.getAppEditingContext(), currentBordereau));

			Integer nbMandats = new Integer(detail.count());
			
//			parameters.takeValueForKey(ImpressionCtrl.sharedInstance(app.getAppEditingContext()).getNbDetail(), "NB_MANDATS");
			parameters.takeValueForKey(nbMandats, "NB_MANDATS");
			parameters.takeValueForKey(app.getApplicationParametre("REP_BASE_JASPER_PATH"), "REP_BASE_PATH");
			parameters.takeValueForKey("", "TIT_INTITULE");
			parameters.takeValueForKey(new Integer(-1000), "TIT_ID");
			parameters.takeValueForKey(new Integer(-1000), "MAN_ID");
			
			if (currentTitre != null) {
				parameters.takeValueForKey(new Integer(-1000), "BORDEREAU");
				parameters.takeValueForKey(new Integer(((Number) currentTitre.valueForKey("titId")).intValue()), "TIT_ID");
			}

			if (currentMandat != null) {
				parameters.takeValueForKey(new Integer(-1000), "BORDEREAU");
				parameters.takeValueForKey(new Integer(((Number) currentMandat.valueForKey("manId")).intValue()), "MAN_ID");
			}

			if (isBorMandats()) {
				if (getMonImpressionPanelNib().getjCheckBoxBordJourn().isSelected()) {
					app.getToolsCocktailReports().imprimerReportParametres("Bord_journalM.jasper", parameters, "Bord_journalM" + lastModif);
				}
				if (getMonImpressionPanelNib().getjCheckBoxBord().isSelected()) {
					app.getToolsCocktailReports().imprimerReportParametres("BORD_MAND_4.jasper", parameters, "BORD_MAND_4" + lastModif);
				}
				if (getMonImpressionPanelNib().getjCheckBoxMandats().isSelected()) {
					app.getToolsCocktailReports().imprimerReportParametres("MANDAT_1.jasper", parameters, "MANDAT_1" + lastModif);
				}
				if (getMonImpressionPanelNib().getjCheckBoxRecapDepenses().isSelected()) {
					app.getToolsCocktailReports().imprimerReportParametres("reglement_recap.jasper", parameters, "reglement_recap" + lastModif);
				}
			}

			else if (isBorORVs()) {
				if (getMonImpressionPanelNib().getjCheckBoxBordJourn().isSelected()) {
					app.getToolsCocktailReports().imprimerReportParametres("Bord_journalM_or.jasper", parameters, "Bord_journalM_or" + lastModif);
				}
				if (getMonImpressionPanelNib().getjCheckBoxBord().isSelected()) {
					app.getToolsCocktailReports().imprimerReportParametres("BORD_MAND_4_or.jasper", parameters, "BORD_MAND_4_or" + lastModif);
				}
				if (getMonImpressionPanelNib().getjCheckBoxMandats().isSelected()) {
					app.getToolsCocktailReports().imprimerReportParametres("reversement.jasper", parameters, "reversement" + lastModif);
				}
				//app.getToolsCocktailReports().imprimerReportParametres("reglement_recap.jasper", parameters, "reglement_recap"+lastModif);
			}

			else if (isBorTitres() || isBorAor()) {

				if (getMonImpressionPanelNib().getjCheckBoxBordJourn().isSelected()) {
					app.getToolsCocktailReports().imprimerReportParametres("Bord_journal.jasper", parameters, "Bord_journal" + lastModif);
				}
				if (getMonImpressionPanelNib().getjCheckBoxBord().isSelected()) {
					app.getToolsCocktailReports().imprimerReportParametres("BTTE.jasper", parameters, "BTTE" + lastModif);
				}

				if (isBorAor()) {
					if (getMonImpressionPanelNib().getjCheckBoxTitre().isSelected()) {
						app.getToolsCocktailReports().imprimerReportParametres("reduction.jasper", parameters, "reduction" + lastModif);
					}
					if (isBorTitreCo()) {
						if (getMonImpressionPanelNib().getjCheckBoxAnnexeTitreCo().isSelected()) {
							app.getToolsCocktailReports().imprimerReportParametres("titreco_annexe.jasper", parameters, "titreCoAnnexe" + lastModif);
						}
					}
				}
				else {

					if (isBorTitres()) {
						if (getMonImpressionPanelNib().getjCheckBoxSommesAPayer().isSelected()) {
							app.getToolsCocktailReports().imprimerReportParametres("Somme_apayer.jasper", parameters, "Somme_apayer" + lastModif);
						}

						if (getMonImpressionPanelNib().getjCheckBoxTitre().isSelected()) {
							parameters.setObjectForKey("Ordre de recette", "TIT_INTITULE");
							app.getToolsCocktailReports().imprimerReportParametres("recette.jasper", parameters, "recette1" + lastModif);
						}

						if (getMonImpressionPanelNib().getjCheckBoxBulletinPerception().isSelected()) {
							parameters.setObjectForKey("bulletin de perception", "TIT_INTITULE");
							app.getToolsCocktailReports().imprimerReportParametres("recette.jasper", parameters, "recette2" + lastModif);
						}

						if (getMonImpressionPanelNib().getjCheckBoxBulletinLiquidation().isSelected()) {
							parameters.setObjectForKey("bulletin de liquidation", "TIT_INTITULE");
							app.getToolsCocktailReports().imprimerReportParametres("recette.jasper", parameters, "recette3" + lastModif);
						}
					}
					//ne pas imprimer l'annexe si pas titre co
					if (isBorTitreCo()) {
						if (getMonImpressionPanelNib().getjCheckBoxAnnexeTitreCo().isSelected()) {
							app.getToolsCocktailReports().imprimerReportParametres("titreco_annexe.jasper", parameters, "titreCoAnnexe" + lastModif);
						}
					}
				}
			}
		}

		catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(), true);
		}
	}

	/**
	 * Permet de fermer la fenetre
	 */
	public void actionFermer() {
		trace(METHODE_ACTION_FERMER);
		masquerFenetre();
	}

	/**
	 * Permet de demander l'ouverture du panel d'impression
	 * 
	 * @param bord bordereau a imprimer
	 */
	public void afficherFenetrePourBordereau(EOBordereau bord) {
		super.afficherFenetre();
		resetVariables();
		setBordereau(bord);
		setUpdateInteface();
	}

	public void afficherFenetrePourTitre(EOTitre titre) {
		super.afficherFenetre();
		resetVariables();
		setBordereau(titre.bordereau());
		setTitre(titre);
		setUpdateInteface();
		enableCheckBoxesForTitre();
		getMonImpressionPanelNib().getjLabelPieceNum().setVisible(true);
		getMonImpressionPanelNib().getjTextFieldCocktailNumeroPiece().setVisible(true);
		getMonImpressionPanelNib().getjLabelPieceNum().setText("Num. Titre / AOR :");
		getMonImpressionPanelNib().getjTextFieldCocktailNumeroPiece().setText("" + titre.titNumero().intValue());
	}

	public void afficherFenetrePourMandat(EOMandat mandat) {
		super.afficherFenetre();
		resetVariables();
		setBordereau(mandat.bordereau());
		setMandat(mandat);
		setUpdateInteface();
		enableCheckBoxesForMandat();
		getMonImpressionPanelNib().getjLabelPieceNum().setVisible(true);
		getMonImpressionPanelNib().getjTextFieldCocktailNumeroPiece().setVisible(true);
		getMonImpressionPanelNib().getjLabelPieceNum().setText("Num. Mandat / ORV :");
		getMonImpressionPanelNib().getjTextFieldCocktailNumeroPiece().setText("" + mandat.manNumero().intValue());
	}

	private void resetVariables() {
		setBordereau(null);
		setMandat(null);
		setTitre(null);
	}

	/**
	 * Permet de mettre a jour l'interface
	 */
	private void setUpdateInteface() {
		enableCheckBoxes();

		getMonImpressionPanelNib().getjLabelPieceNum().setVisible(false);
		getMonImpressionPanelNib().getjTextFieldCocktailNumeroPiece().setVisible(false);

		getMonImpressionPanelNib().getJTextFieldCocktailUfr().setText(currentBordereau.gesCode());
		getMonImpressionPanelNib().getJTextFieldCocktailNumero().setText(currentBordereau.borNum().toString());
		initNbExemplaires();
	}

	private void initNbExemplaires() {
		//		getMonImpressionPanelNib().getJTextFieldCocktailNbMandats().setText("3");
		//		getMonImpressionPanelNib().getJTextFieldCocktailNbRecap().setText("3");
	}

	/**
	 * Permet de positionner le bordereau a imprimer
	 * 
	 * @param monBord
	 */

	protected void setBordereau(EOBordereau monBord) {
		currentBordereau = monBord;
	}

	protected void setTitre(EOTitre monTitre) {
		currentTitre = monTitre;
	}

	protected void setMandat(EOMandat monMandat) {
		currentMandat = monMandat;
	}

	protected void setMonImpressionPanelNib(ImpressionPanelNib monImpressionPanelNib) {
		this.monImpressionPanelNib = monImpressionPanelNib;
	}

	protected ImpressionPanelNib getMonImpressionPanelNib() {
		return monImpressionPanelNib;
	}

}
