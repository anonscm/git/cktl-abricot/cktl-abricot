package org.cocktail.abricot.client.ctrl;

import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.abricot.client.ApplicationClient;
import org.cocktail.abricot.client.eof.modele.EOBordereau;
import org.cocktail.abricot.client.eof.modele.EOVAbricotDepensePi;
import org.cocktail.abricot.client.eof.modele.EOVAbricotPrestationsInternes;
import org.cocktail.abricot.client.eof.modele.EOVAbricotRecettePi;
import org.cocktail.abricot.client.gui.PrestationInterneView;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.client.common.swing.ZEOTable.ZEOTableListener;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class PrestationInterneCtrl {

	public static PrestationInterneCtrl sharedInstance;

	private static final String BORDEREAU_1D1M1R1T = "bordereau_1D1M1R1T";
	
	private static final String VOUS_ALLEZ_MANDATER_TITRER_ETES_VOUS_SUR = "Vous allez creer deux bordereaux de prestation !! \n Etes vous sûr ?";

	private static final String VOUS_ALLEZ_MANDATER_IMPOSSIBLE_MULTI_COMPOSANTE_DEPENSE = "Vous allez creer un bordereau de prestations internes !! \n Impossible votre selection doit concerner une seule UB en DEPENSE!";
	private static final String VOUS_ALLEZ_MANDATER_IMPOSSIBLE_MULTI_COMPOSANTE_RECETTE = "Vous allez creer un bordereau de prestations internes !! \n Impossible votre selection doit concerner une seule UB en RECETTE!";

	private	ApplicationClient	NSApp = (ApplicationClient)ApplicationClient.sharedApplication();	
	private EOEditingContext 	ec;

	private PrestationInterneView myView;

	private EODisplayGroup eodDepenses = new EODisplayGroup(), eodSelection = new EODisplayGroup();

	private ListenerDepenses listenerDepenses = new ListenerDepenses();
	private ListenerSelection listenerSelection = new ListenerSelection();

	private EOExercice currentExercice;

	private String algo = "";

	public PrestationInterneCtrl(EOEditingContext globalEc) {

		ec = globalEc;

		myView = new PrestationInterneView(new JFrame(), false, eodDepenses, eodSelection);


		myView.getBtnFermer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				myView.setVisible(false);
			}
		});

		myView.getBtnRefresh().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				filter();
			}
		});

		myView.getBtnSelectDepense().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ajouterSelection();
			}
		});


		myView.getBtnDeselectDepense().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprimerSelection();
			}
		});

		myView.getBtnPrint().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				imprimer();
			}
		});

		myView.getBtnCreerBordereau().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				creerBordereaux();
			}
		});

		myView.getBtnCleanSelection().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				filter();
			}
		});

		NSMutableArray mySort = new NSMutableArray();
		mySort.addObject(new EOSortOrdering(EOVAbricotPrestationsInternes.V_ABRICOT_DEPENSE_PI_KEY+"."+EOVAbricotDepensePi.ORG_UB_KEY, EOSortOrdering.CompareAscending));
		mySort.addObject(new EOSortOrdering(EOVAbricotPrestationsInternes.V_ABRICOT_DEPENSE_PI_KEY+"."+EOVAbricotDepensePi.ADR_NOM_KEY, EOSortOrdering.CompareAscending));

		eodSelection.setSortOrderings(mySort);

		currentExercice = NSApp.getCurrentExercice();

		myView.getMyEOTableDepenses().addListener(listenerDepenses);
		myView.getMyEOTableSelection().addListener(listenerSelection);

		myView.getTfFiltreClient().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreFournisseur().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreRecette().getDocument().addDocumentListener(new ADocumentListener());
		myView.getTfFiltreDepense().getDocument().addDocumentListener(new ADocumentListener());

		filter();

	}


	public static PrestationInterneCtrl sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new PrestationInterneCtrl(editingContext);
		return sharedInstance;
	}



	private EOQualifier filterQualifier() {

		NSMutableArray mesQualifiers = new NSMutableArray();

		if (myView.getTfFiltreClient().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOVAbricotPrestationsInternes.V_ABRICOT_RECETTE_PI_KEY + "." + EOVAbricotRecettePi.ADR_NOM_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreClient().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltreFournisseur().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOVAbricotPrestationsInternes.V_ABRICOT_DEPENSE_PI_KEY + "." + EOVAbricotDepensePi.ADR_NOM_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreFournisseur().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltreDepense().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOVAbricotPrestationsInternes.V_ABRICOT_DEPENSE_PI_KEY + "." + EOVAbricotDepensePi.ORG_UB_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreDepense().getText().toUpperCase() + "*'",null));

		if (myView.getTfFiltreRecette().getText().length() > 0 ) 
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(
					EOVAbricotPrestationsInternes.V_ABRICOT_RECETTE_PI_KEY + "." + EOVAbricotRecettePi.ORG_UB_KEY + " caseInsensitiveLike '*" + myView.getTfFiltreRecette().getText().toUpperCase() + "*'",null));

		return new EOAndQualifier(mesQualifiers);
	}





	private void filter() {

		eodSelection.setObjectArray(new NSArray());
		eodDepenses.setObjectArray(EOVAbricotPrestationsInternes.findLesPrestationsAViser(NSApp));

		eodDepenses.setQualifier(filterQualifier());
		eodDepenses.updateDisplayedObjects();

		myView.getMyEOTableDepenses().updateData();
		myView.getMyTableModelDepenses().fireTableDataChanged();

		myView.getMyEOTableSelection().updateData();

		updateUI();

	}


	public void open() {

		myView.getTfTitre().setText("BORDEREAUX DE PRESTATIONS INTERNES - Exercice " + currentExercice.exeExercice());
		myView.setVisible(true);

	}



	private void ajouterSelection() {

		try {
			
			NSMutableArray maSelection = new NSMutableArray(eodSelection.displayedObjects());
			maSelection.addObjectsFromArray(eodDepenses.selectedObjects());

			NSMutableArray mesDepenses = new NSMutableArray(eodDepenses.displayedObjects());
			mesDepenses.removeObjectsInArray(eodDepenses.selectedObjects());

			int row = myView.getMyEOTableDepenses().getSelectedRow();

			eodDepenses.setObjectArray(new NSArray());
			eodSelection.setObjectArray(new NSArray());
			myView.getMyEOTableDepenses().updateData();
			myView.getMyEOTableSelection().updateData();

			eodDepenses.setObjectArray(mesDepenses);
			eodSelection.setObjectArray(maSelection);

			myView.getMyEOTableDepenses().updateData();
			myView.getMyEOTableSelection().updateData();
			
			if (row < eodDepenses.displayedObjects().count())
				myView.getMyEOTableDepenses().forceNewSelection(new NSArray(new Integer(row)));
			else
				myView.getMyEOTableDepenses().forceNewSelection(new NSArray(new Integer((eodDepenses.displayedObjects().count()-1))));

		}
		catch (Exception e) {
			EODialogs.runErrorDialog("ERREUR",e.getMessage());
		}

		updateUI();

	}


	private void supprimerSelection() {

		NSMutableArray maSelection = new NSMutableArray(eodSelection.displayedObjects());
		maSelection.removeObjectsInArray(eodSelection.selectedObjects());

		NSMutableArray mesDepenses = new NSMutableArray(eodDepenses.displayedObjects());
		mesDepenses.addObjectsFromArray(eodSelection.selectedObjects());

		eodSelection.deleteSelection();

		eodDepenses.setObjectArray(mesDepenses);
		eodSelection.setObjectArray(maSelection);

		myView.getMyEOTableDepenses().updateData();
		myView.getMyEOTableSelection().updateData();

		updateUI();

	}

	private void creerBordereaux() {


		// TEST Multi composante 

		EOVAbricotDepensePi firstDepense = ((EOVAbricotPrestationsInternes) eodSelection.displayedObjects().objectAtIndex(0)).vAbricotDepensePi();
		EOVAbricotRecettePi firstRecette = ((EOVAbricotPrestationsInternes) eodSelection.displayedObjects().objectAtIndex(0)).vAbricotRecettePi();

		for (int i = 1; i < eodSelection.displayedObjects().count(); i++) {

			EOVAbricotDepensePi maPrestationDepense = ((EOVAbricotPrestationsInternes) eodSelection.displayedObjects().objectAtIndex(i)).vAbricotDepensePi();
			EOVAbricotRecettePi maPrestationRecette = ((EOVAbricotPrestationsInternes) eodSelection.displayedObjects().objectAtIndex(i)).vAbricotRecettePi();

			if (!firstDepense.organ().orgUb().
					equals(maPrestationDepense.organ().orgUb())) {
				EODialogs.runInformationDialog("ATTENTION", VOUS_ALLEZ_MANDATER_IMPOSSIBLE_MULTI_COMPOSANTE_DEPENSE);
				return;
			}
			
			if (!firstRecette.organ().orgUb().
					equals(maPrestationRecette.organ().orgUb())) {
				EODialogs.runInformationDialog("ATTENTION", VOUS_ALLEZ_MANDATER_IMPOSSIBLE_MULTI_COMPOSANTE_RECETTE);
				return;
			}
		}

		if (!EODialogs.runConfirmOperationDialog("Mandatement ...",VOUS_ALLEZ_MANDATER_TITRER_ETES_VOUS_SUR,"OUI","NON"))
			return;

		NSDictionary dicoRetour = null;
		try {

			NSApp.ceerTransactionLog();
			NSApp.afficherUnLogDansTransactionLog("DEBUT....", 10);

			NSMutableDictionary params = new NSMutableDictionary();
			params.takeValueForKey("vide", "info");

			// recup un id de selection ...
			NSApp.executeStoredProcedure("getSelectionId", params);
			dicoRetour = NSApp.returnValuesForLastStoredProcedureInvocation();
			Integer abrid = (Integer) dicoRetour.valueForKey("returnValue");

			NSApp.afficherUnLogDansTransactionLog("Recuperation d'un numero de selection ...OK", 20);

			// on recupere la chaine des id des depenses
			String chaineDep = "";
			for (int i = 0; i < eodSelection.displayedObjects().count(); i++) {
				chaineDep = chaineDep + ((EOGenericRecord) eodSelection.displayedObjects().objectAtIndex(i)).valueForKeyPath("vAbricotDepensePi.dpcoId") + "$";
			}
			chaineDep = chaineDep + "$";

			// on recupere la chaine des id des recette
			String chaineRec = "";
			for (int i = 0; i < eodSelection.displayedObjects().count(); i++) {
				chaineRec = chaineRec + ((EOGenericRecord) eodSelection.displayedObjects().objectAtIndex(i)).valueForKeyPath("vAbricotRecettePi.rpcoId") + "$";
			}
			chaineRec = chaineRec + "$";

			// on positionne la selection dans la base de donnee
			//abrid integer,lesdepid varchar,lesrecid varchar ,utlordre integer,exeordre integer ,tboordre integer,abrgroupby varchar,gescode varchar
			params = new NSMutableDictionary();
			params.takeValueForKey(((EOVAbricotPrestationsInternes) eodSelection.displayedObjects().objectAtIndex(0)).vAbricotDepensePi().organ().orgUb(), "a08gescodemandat");
			params.takeValueForKey(((EOVAbricotPrestationsInternes) eodSelection.displayedObjects().objectAtIndex(0)).vAbricotRecettePi().organ().orgUb(), "a09gescodetitre");
			params.takeValueForKey(BORDEREAU_1D1M1R1T, "a07abrgroupby");
			params.takeValueForKey(NSApp.getCurrentUtilisateur().utlOrdre(), "a04utlordre");
			params.takeValueForKey(NSApp.getCurrentExercice().exeOrdre(), "a05exeordre");
			params.takeValueForKey(chaineRec, "a03lesrecid");
			params.takeValueForKey(chaineDep, "a02lesdepid");
			params.takeValueForKey(abrid, "a01abrid");

			NSApp.executeStoredProcedure("setSelectionIntern", params);

			NSApp.afficherUnLogDansTransactionLog("Enregistrement de votre selection ...OK", 30);
			// re fetch
			NSApp.afficherUnLogDansTransactionLog("Generation du bordereau de mandat...OK", 40);

			dicoRetour = null;
			params = new NSMutableDictionary();
			params.takeValueForKey(new Integer(abrid.intValue() * -1), "abrid");

			// recup du biorid ...
			NSApp.executeStoredProcedure("getSelectionBorid", params);
			dicoRetour = NSApp.returnValuesForLastStoredProcedureInvocation();
			Integer borid = (Integer) dicoRetour.valueForKey("returnValue");

			NSApp.afficherUnLogDansTransactionLog("Generation du bordereau de titre...OK", 60);
			filter();
			NSApp.afficherUnLogDansTransactionLog("Verification du traitement ...OK", 80);

			NSApp.afficherUnLogDansTransactionLog("FIN...", 100);
			NSApp.finirTransactionLog();
			NSApp.fermerTransactionLog();

			System.out.println("monImpressionPanelNibT borid" + borid);
			try {

				ImpressionPanelNibCtrl monImpressionPanelNibCtrlT = new ImpressionPanelNibCtrl(NSApp, 0, 0, 360, 360);

				if (borid != null) {
					monImpressionPanelNibCtrlT.afficherFenetrePourBordereau(EOBordereau.findForBorid(ec, borid));
				}
				else {
					monImpressionPanelNibCtrlT.afficherFenetrePourBordereau(null);
				}
			} catch (Exception e) {
				System.out.println("actionImprimer OUPS" + e);
			}

			// recup du biorid ...
			dicoRetour = null;
			params = new NSMutableDictionary();
			params.takeValueForKey(abrid, "abrid");
			NSApp.executeStoredProcedure("getSelectionBorid", params);
			dicoRetour = NSApp.returnValuesForLastStoredProcedureInvocation();
			borid = (Integer) dicoRetour.valueForKey("returnValue");
			System.out.println("monImpressionPanelNibM borid" + borid);
			try {

				ImpressionPanelNibCtrl monImpressionPanelNibCtrlM = new ImpressionPanelNibCtrl(NSApp, 0, 0, 360, 360);

				if (borid != null) {
					monImpressionPanelNibCtrlM.afficherFenetrePourBordereau(EOBordereau.findForBorid(ec, borid));
				}
				else {
					monImpressionPanelNibCtrlM.afficherFenetrePourBordereau(null);
				}
			} catch (Exception e) {
				System.out.println("actionImprimer OUPS" + e);
			}

		} catch (Exception e) {
			dicoRetour = NSApp.returnValuesForLastStoredProcedureInvocation();
			NSApp.afficherUnLogDansTransactionLog("probleme !!" + dicoRetour.toString(), 0);
			NSApp.finirTransactionLog();
		}

	}

	protected String getAlgo() {
		return algo;
	}
	protected void setAlgo(String setAlgo) {
		this.algo = setAlgo;
	}

	/**
	 * Impression des depenses selectionnees
	 */
	private void imprimer() {

		try {
			java.util.Calendar currentTime = java.util.Calendar.getInstance();
			String lastModif = "-" + currentTime.get(java.util.Calendar.DAY_OF_MONTH) + "." + currentTime.get(java.util.Calendar.MONTH) + "." + currentTime.get(java.util.Calendar.YEAR) + "-"
			+ currentTime.get(java.util.Calendar.HOUR_OF_DAY) + "h" + currentTime.get(java.util.Calendar.MINUTE) + "m" + currentTime.get(java.util.Calendar.SECOND);

			// on recupere la chaine des id des depenses a mandater
			String chaine = "";
			for (int i = 0; i < eodSelection.displayedObjects().count(); i++) {
				chaine = chaine + ((EOGenericRecord) eodSelection.displayedObjects().objectAtIndex(i)).valueForKeyPath("vAbricotRecettePi.rpcoId") + ",";
			}
			chaine = chaine + "-1";

			NSMutableDictionary parameters = new NSMutableDictionary();
			parameters.takeValueForKey(chaine, "RPCOIDS");
			NSApp.getToolsCocktailReports().imprimerReportParametres("recettepi.jasper", parameters, "recette" + lastModif);

			// on recupere la chaine des id des depenses a mandater
			chaine = "";
			for (int i = 0; i < eodSelection.displayedObjects().count(); i++) {
				chaine = chaine + ((EOGenericRecord) eodSelection.displayedObjects().objectAtIndex(i)).valueForKeyPath("vAbricotDepensePi.dpcoId") + ",";
			}
			chaine = chaine + "-1";

			parameters = new NSMutableDictionary();
			parameters.takeValueForKey(chaine, "DPCOIDS");
			NSApp.getToolsCocktailReports().imprimerReportParametres("depensepi.jasper", parameters, "depensepi" + lastModif);

		} catch (Exception e) {
			System.out.println("actionImprimer OUPS" + e);
		}

	}


	public void setCurrentExercice(EOExercice exercice) {

		currentExercice = exercice;
		myView.getTfTitre().setText("DEPENSES - Exercice " + currentExercice.exeExercice());

		filter();

	}

	private class ListenerDepenses implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

			ajouterSelection();

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			updateUI();

		}
	}


	private class ListenerSelection implements ZEOTableListener {

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {

			supprimerSelection();

		}

		/* (non-Javadoc)
		 * @see kiwi.client.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

			updateUI();

		}
	}

	private void updateUI() {


		myView.getBtnRefresh().setEnabled(currentExercice != null);

		myView.getBtnSelectDepense().setEnabled(eodDepenses.selectedObjects().count() > 0);

		myView.getBtnDeselectDepense().setEnabled(eodSelection.selectedObjects().count() > 0);

		myView.getBtnCreerBordereau().setEnabled(eodSelection.displayedObjects().count() > 0);

		myView.getBtnPrint().setEnabled(eodSelection.displayedObjects().count() > 0);

		myView.getBtnCleanSelection().setEnabled(eodSelection.displayedObjects().count() > 0);

		myView.getLblDepenses().setText(eodDepenses.displayedObjects().count()+"");//+" / " + CocktailUtilities.FORMAT_DECIMAL.format(CocktailUtilities.computeSumForKey(eodDepenses.displayedObjects(), EOVAbricotPrestationsInternes.V_ABRICOT_RECETTE_PI_KEY+"."+EOVAbricotRecettePi.RPCO_HT_SAISIE_KEY)) + " " + CocktailConstantes.STRING_EURO);
		myView.getLblSelection().setText(eodSelection.displayedObjects().count()+"");//+" / " + CocktailUtilities.FORMAT_DECIMAL.format(CocktailUtilities.computeSumForKey(eodSelection.displayedObjects(),  EOVAbricotPrestationsInternes.V_ABRICOT_DEPENSE_PI_KEY+"."+EOVAbricotDepensePi.DPCO_HT_SAISIE_KEY)) + " " + CocktailConstantes.STRING_EURO);


	}

	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();		
		}

		public void removeUpdate(DocumentEvent e) {
			filter();			
		}
	}

}
