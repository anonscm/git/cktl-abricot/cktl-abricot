/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.abricot.client.ctrl;

import org.cocktail.abricot.client.ApplicationClient;
import org.cocktail.abricot.client.gui.MandatAlgoNib;
import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.ToolsSwing;

/**
 *Ce controleur permet de gerer l'interface de selection de l'algorythme de
 * mandatement.<br>
 *L'interface a ete developpee avec NetBeans.<br>
 *Le controleur est initilisee par MandatNibCtrl<br>
 *Le controleur pilote son interface MainNib
 * 
 * @author Rivalland Frederic (frederic.rivalland@univ-paris5.fr)
 * @author Rodolphe Prin
 */
public class MandatAlgoNibCtrl extends NibCtrl {

	public static MandatAlgoNibCtrl sharedInstance;

	// methodes
	/** action du controleur */
	private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
	/** action du controleur */
	private static final String METHODE_ACTION_VALIDER = "actionValider";

	// algos
	/** Algorythme pris en charge par le controleur, voir plsq abricot_bordereau */
	public static final String ALGO_bordereau_1D1M = "bordereau_1D1M";
	/** Algorythme pris en charge par le controleur,voir plsq abricot_bordereau */
	public static final String ALGO_ndep_mand_fou_rib_pco_mod = "ndep_mand_fou_rib_pco_mod";
	/** Algorythme pris en charge par le controleur, voir plsq abricot_bordereau */
	public static final String ALGO_ndep_mand_org_fou_rib_pco_mod = "ndep_mand_org_fou_rib_pco_mod";
	/** Algorythme Valeur lors d'une selection d'algorythme non valide */
	public static final String ALGO_PROBLEME = "PROBLEME";

	// le nib
	/** Interface du controleur */
	private MandatAlgoNib monMandatAlgoNib = null;

	/** Controleur parent */
	private MandatCtrl parentControleur = null;

	public MandatAlgoNibCtrl(ApplicationCocktail ctrl, int alocation_x,
			int alocation_y, int ataille_x, int ataille_y, MandatCtrl parentControleur) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);

		setMonMandatAlgoNib(new MandatAlgoNib());
		creationFenetre(getMonMandatAlgoNib(), ToolsSwing.formaterStringU("Choisir un type de création de vos mandats."), parentControleur);
		//		getMonMandatAlgoNib().setPreferredSize(new java.awt.Dimension(590, 230));
		//		getMonMandatAlgoNib().setSize(590, 230);
		setWithLogs(false);
	}

    public static MandatAlgoNibCtrl sharedInstance(ApplicationCocktail ctrl, int alocation_x,
			int alocation_y, int ataille_x, int ataille_y, MandatCtrl parentControleur)	{
     	if (sharedInstance == null)
     		sharedInstance = new MandatAlgoNibCtrl(ctrl, alocation_x, alocation_y, ataille_x, ataille_y, parentControleur);
     	return sharedInstance;
     }

    
	public void creationFenetre(MandatAlgoNib leMandatAlgoNib, String title, MandatCtrl parentControleur) {
		super.creationFenetre(leMandatAlgoNib, title);
		setMonMandatAlgoNib(leMandatAlgoNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
		this.parentControleur = parentControleur;
		initCheckBox();
	}

	public void afficherFenetre() {
		super.afficherFenetre();
		initCheckBox();
		((ApplicationClient) app).removeFromLesPanelsModal(getMonMandatAlgoNib());
		((ApplicationClient) app).activerLesGlassPane();
	}

	private void bindingAndCustomization() {

		try {

			// le cablage des  objets
			getMonMandatAlgoNib()
					.getJButtonCocktailAnnuler()
					.addDelegateActionListener(this, METHODE_ACTION_ANNULER);

			getMonMandatAlgoNib()
					.getJButtonCocktailValider()
					.addDelegateActionListener(this, METHODE_ACTION_VALIDER);

			// les images des objets
			getMonMandatAlgoNib().getJButtonCocktailAnnuler().setIcone(IconeCocktail.FERMER);//"close_view");
			getMonMandatAlgoNib().getJButtonCocktailValider().setIcone(IconeCocktail.VALIDER);//"valider16");

			//sera modal lors d une transaction 
			app.addLesPanelsModal(getMonMandatAlgoNib());

			// obseration des changements de selection de lexercice :
			((ApplicationClient) app).getObserveurExerciceSelection()
					.addObserverForMessage(this, "changementExercice");

		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this,
					"Exception", e.getMessage(), true);
		}

	}

	/**
	 * Initialisation des checkBox de l'interface
	 */
	private void initCheckBox() {
		//		getMonMandatAlgoNib().getJCheckBox1().setSelected(false);
		//		getMonMandatAlgoNib().getJCheckBox2().setSelected(false);
		//		getMonMandatAlgoNib().getJCheckBox3().setSelected(false);
	}

	/**
	 * Permet de determiner l'algorythme en fonction de la checkbox coche
	 * 
	 * @return un algo (public static final String ALGO...)
	 */

	protected String getCurrentAlgo() {
		String algo = ALGO_PROBLEME;

		if (getMonMandatAlgoNib().getjRadioButtonALGO_bordereau_1D1M().isSelected()) {
			algo = ALGO_bordereau_1D1M;
		}

		if (getMonMandatAlgoNib().getjRadioButtonALGO_ndep_mand_org_fou_rib_pco_mod().isSelected()) {
			algo = ALGO_ndep_mand_org_fou_rib_pco_mod;
		}

		if (getMonMandatAlgoNib().getjRadioButtonALGO_ndep_mand_fou_rib_pco_mod().isSelected()) {
			algo = ALGO_ndep_mand_fou_rib_pco_mod;
		}

		return algo;
	}

	// Actions
	/**
	 * Annulation de la selection d'un algo on informe le parentControleur
	 * getParentControleur().inspecteurAnnuler(this);
	 **/
	public void actionAnnuler() {

		initCheckBox();
		app.addLesPanelsModal(getMonMandatAlgoNib());
		((ApplicationClient) app).retirerLesGlassPane();

		masquerFenetre();
		getParentControleur().inspecteurAnnuler(this);
	}

	/**
	 * Validation de la selection de l'algo<br>
	 * on informe le parentControleur
	 * getParentControleur().inspecteurValider(this);
	 */
	public void actionValider() {
		//		int cpt = 0;

		//		if (	getMonMandatAlgoNib().getJCheckBox1().isSelected())
		//			cpt = cpt +1;
		//		if (	getMonMandatAlgoNib().getJCheckBox2().isSelected())
		//			cpt = cpt +1;
		//		if (	getMonMandatAlgoNib().getJCheckBox3().isSelected())
		//			cpt = cpt +1;
		//
		//		if (cpt == 1) {
		if (getMonMandatAlgoNib().getButtonGroup1().getSelection() == null) {
			fenetreDeDialogueInformation(ToolsSwing.formaterStringU("Veillez choisir un type de mandat !"));
		}
		else {
			app.addLesPanelsModal(getMonMandatAlgoNib());
			((ApplicationClient) app).retirerLesGlassPane();

			masquerFenetre();
			getParentControleur().inspecteurValider(this);
		}

		//		}
		//		else
		//		{
		//			fenetreDeDialogueInformation( ToolsSwing.formaterStringU("Votre selection n est pas valide !"));
		//		}
	}

	// accesseurs
	protected MandatAlgoNib getMonMandatAlgoNib() {
		return monMandatAlgoNib;
	}

	protected void setMonMandatAlgoNib(MandatAlgoNib monMandatAlgoNib) {
		this.monMandatAlgoNib = monMandatAlgoNib;
	}

	protected MandatCtrl getParentControleur() {
		return parentControleur;
	}

	protected void setParentControleur(MandatCtrl parentControleur) {
		this.parentControleur = parentControleur;
	}
}
