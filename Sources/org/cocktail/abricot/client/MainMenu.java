package org.cocktail.abricot.client;
/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/


import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import org.cocktail.client.common.utilities.CocktailIcones;
import org.cocktail.client.common.utilities.ConnectedUsersCtrl;

import com.webobjects.eocontrol.EOEditingContext;

public class MainMenu extends JMenuBar
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7194100670810870934L;

	public static MainMenu sharedInstance;

	ApplicationClient NSApp = (ApplicationClient)ApplicationClient.sharedApplication();;

	JMenu	menuApplication;

	JMenuItem		itemUtilisateursConnectes;

	protected EOEditingContext ec;

	/** Constructeur */
	public MainMenu(EOEditingContext globalEc)	{

		ec = globalEc;

		menuApplication = new JMenu("Application");

		// Application
		itemUtilisateursConnectes = new JMenuItem(new ActionConnectedUsers());
		menuApplication.add(itemUtilisateursConnectes);

		menuApplication.add(new JMenuItem(new ActionQuitter("Quitter")));

		//menuApplication.setIcon(CocktailConstantes.ICON_APP_LOGO);

		// Ajout des menus
		add(menuApplication);
		add(new MenuAide(NSApp, "Aide"));

	}
	
	/**
	 * 
	 * @param editingContext
	 * @return
	 */    
	public static MainMenu sharedInstance(EOEditingContext editingContext)	{
		if (sharedInstance == null)
			sharedInstance = new MainMenu(editingContext);
		return sharedInstance;
	}
	
	/** Quitte l'application */
	private class ActionQuitter extends AbstractAction	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 7609687677689215285L;

		public ActionQuitter (String name)		{
			putValue(Action.NAME,name);
			putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_EXIT_16);
		}

		public void actionPerformed(ActionEvent event)		{
			NSApp.quit();
		}
	}

	private final class ActionConnectedUsers extends AbstractAction {

		private static final long serialVersionUID = 5125101540436981811L;

		public ActionConnectedUsers() {
			super("Utilisateurs connectés");
		}

		public void actionPerformed(final ActionEvent e) {
			ConnectedUsersCtrl.sharedInstance(ec).open();
		}
	} 


}
