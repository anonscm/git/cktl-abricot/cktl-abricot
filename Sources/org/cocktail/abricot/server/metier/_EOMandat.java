// _EOMandat.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMandat.java instead.
package org.cocktail.abricot.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;

public abstract class _EOMandat extends  ERXGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "Mandat";
	public static final String ENTITY_TABLE_NAME = "MARACUJA.MANDAT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "manId";

	public static final String BOR_ID_KEY = "borId";
	public static final String BRJ_ORDRE_KEY = "brjOrdre";
	public static final String DETAIL_NUMERO_KEY = "detailNumero";
	public static final String DETAIL_TTC_KEY = "detailTtc";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MAN_DATE_REMISE_KEY = "manDateRemise";
	public static final String MAN_DATE_VISA_PRINC_KEY = "manDateVisaPrinc";
	public static final String MAN_ETAT_KEY = "manEtat";
	public static final String MAN_ETAT_REMISE_KEY = "manEtatRemise";
	public static final String MAN_HT_KEY = "manHt";
	public static final String MAN_ID_KEY = "manId";
	public static final String MAN_MOTIF_REJET_KEY = "manMotifRejet";
	public static final String MAN_NB_PIECE_KEY = "manNbPiece";
	public static final String MAN_NUMERO_KEY = "manNumero";
	public static final String MAN_NUMERO_REJET_KEY = "manNumeroRejet";
	public static final String MAN_ORDRE_KEY = "manOrdre";
	public static final String MAN_ORGINE_KEY_KEY = "manOrgineKey";
	public static final String MAN_ORIGINE_LIB_KEY = "manOrigineLib";
	public static final String MAN_TTC_KEY = "manTtc";
	public static final String MAN_TVA_KEY = "manTva";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String MOTIF_REJET_KEY = "motifRejet";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String ORI_ORDRE_KEY = "oriOrdre";
	public static final String PAI_ORDRE_KEY = "paiOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PREST_ID_KEY = "prestId";
	public static final String RIB_ORDRE_COMPTABLE_KEY = "ribOrdreComptable";
	public static final String RIB_ORDRE_ORDONNATEUR_KEY = "ribOrdreOrdonnateur";
	public static final String TOR_ORDRE_KEY = "torOrdre";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String BOR_ID_COLKEY = "BOR_ID";
	public static final String BRJ_ORDRE_COLKEY = "BRJ_ORDRE";
	public static final String DETAIL_NUMERO_COLKEY = "MAN_NUMERO";
	public static final String DETAIL_TTC_COLKEY = "MAN_TTC";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String MAN_DATE_REMISE_COLKEY = "MAN_DATE_REMISE";
	public static final String MAN_DATE_VISA_PRINC_COLKEY = "MAN_DATE_VISA_PRINC";
	public static final String MAN_ETAT_COLKEY = "MAN_ETAT";
	public static final String MAN_ETAT_REMISE_COLKEY = "MAN_ETAT_REMISE";
	public static final String MAN_HT_COLKEY = "MAN_HT";
	public static final String MAN_ID_COLKEY = "MAN_ID";
	public static final String MAN_MOTIF_REJET_COLKEY = "MAN_MOTIF_REJET";
	public static final String MAN_NB_PIECE_COLKEY = "MAN_NB_PIECE";
	public static final String MAN_NUMERO_COLKEY = "MAN_NUMERO";
	public static final String MAN_NUMERO_REJET_COLKEY = "MAN_NUMERO_REJET";
	public static final String MAN_ORDRE_COLKEY = "MAN_ORDRE";
	public static final String MAN_ORGINE_KEY_COLKEY = "MAN_ORGINE_KEY";
	public static final String MAN_ORIGINE_LIB_COLKEY = "MAN_ORIGINE_LIB";
	public static final String MAN_TTC_COLKEY = "MAN_TTC";
	public static final String MAN_TVA_COLKEY = "MAN_TVA";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String MOTIF_REJET_COLKEY = "MAN_MOTIF_REJET";
	public static final String ORG_ORDRE_COLKEY = "ORG_ORDRE";
	public static final String ORI_ORDRE_COLKEY = "ORI_ORDRE";
	public static final String PAI_ORDRE_COLKEY = "PAI_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PREST_ID_COLKEY = "PREST_ID";
	public static final String RIB_ORDRE_COMPTABLE_COLKEY = "RIB_ORDRE_COMPTABLE";
	public static final String RIB_ORDRE_ORDONNATEUR_COLKEY = "RIB_ORDRE_ORDONNATEUR";
	public static final String TOR_ORDRE_COLKEY = "TOR_ORDRE";



	// Relationships
	public static final String BORDEREAU_KEY = "bordereau";
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String ORGAN_KEY = "organ";
	public static final String V_FOURNISSEUR_KEY = "vFournisseur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer borId() {
    return (Integer) storedValueForKey(BOR_ID_KEY);
  }

  public void setBorId(Integer value) {
    takeStoredValueForKey(value, BOR_ID_KEY);
  }

  public Integer brjOrdre() {
    return (Integer) storedValueForKey(BRJ_ORDRE_KEY);
  }

  public void setBrjOrdre(Integer value) {
    takeStoredValueForKey(value, BRJ_ORDRE_KEY);
  }

  public Integer detailNumero() {
    return (Integer) storedValueForKey(DETAIL_NUMERO_KEY);
  }

  public void setDetailNumero(Integer value) {
    takeStoredValueForKey(value, DETAIL_NUMERO_KEY);
  }

  public java.math.BigDecimal detailTtc() {
    return (java.math.BigDecimal) storedValueForKey(DETAIL_TTC_KEY);
  }

  public void setDetailTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DETAIL_TTC_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public Integer fouOrdre() {
    return (Integer) storedValueForKey(FOU_ORDRE_KEY);
  }

  public void setFouOrdre(Integer value) {
    takeStoredValueForKey(value, FOU_ORDRE_KEY);
  }

  public String gesCode() {
    return (String) storedValueForKey(GES_CODE_KEY);
  }

  public void setGesCode(String value) {
    takeStoredValueForKey(value, GES_CODE_KEY);
  }

  public NSTimestamp manDateRemise() {
    return (NSTimestamp) storedValueForKey(MAN_DATE_REMISE_KEY);
  }

  public void setManDateRemise(NSTimestamp value) {
    takeStoredValueForKey(value, MAN_DATE_REMISE_KEY);
  }

  public NSTimestamp manDateVisaPrinc() {
    return (NSTimestamp) storedValueForKey(MAN_DATE_VISA_PRINC_KEY);
  }

  public void setManDateVisaPrinc(NSTimestamp value) {
    takeStoredValueForKey(value, MAN_DATE_VISA_PRINC_KEY);
  }

  public String manEtat() {
    return (String) storedValueForKey(MAN_ETAT_KEY);
  }

  public void setManEtat(String value) {
    takeStoredValueForKey(value, MAN_ETAT_KEY);
  }

  public String manEtatRemise() {
    return (String) storedValueForKey(MAN_ETAT_REMISE_KEY);
  }

  public void setManEtatRemise(String value) {
    takeStoredValueForKey(value, MAN_ETAT_REMISE_KEY);
  }

  public java.math.BigDecimal manHt() {
    return (java.math.BigDecimal) storedValueForKey(MAN_HT_KEY);
  }

  public void setManHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MAN_HT_KEY);
  }

  public Integer manId() {
    return (Integer) storedValueForKey(MAN_ID_KEY);
  }

  public void setManId(Integer value) {
    takeStoredValueForKey(value, MAN_ID_KEY);
  }

  public String manMotifRejet() {
    return (String) storedValueForKey(MAN_MOTIF_REJET_KEY);
  }

  public void setManMotifRejet(String value) {
    takeStoredValueForKey(value, MAN_MOTIF_REJET_KEY);
  }

  public Double manNbPiece() {
    return (Double) storedValueForKey(MAN_NB_PIECE_KEY);
  }

  public void setManNbPiece(Double value) {
    takeStoredValueForKey(value, MAN_NB_PIECE_KEY);
  }

  public Double manNumero() {
    return (Double) storedValueForKey(MAN_NUMERO_KEY);
  }

  public void setManNumero(Double value) {
    takeStoredValueForKey(value, MAN_NUMERO_KEY);
  }

  public Double manNumeroRejet() {
    return (Double) storedValueForKey(MAN_NUMERO_REJET_KEY);
  }

  public void setManNumeroRejet(Double value) {
    takeStoredValueForKey(value, MAN_NUMERO_REJET_KEY);
  }

  public Integer manOrdre() {
    return (Integer) storedValueForKey(MAN_ORDRE_KEY);
  }

  public void setManOrdre(Integer value) {
    takeStoredValueForKey(value, MAN_ORDRE_KEY);
  }

  public Double manOrgineKey() {
    return (Double) storedValueForKey(MAN_ORGINE_KEY_KEY);
  }

  public void setManOrgineKey(Double value) {
    takeStoredValueForKey(value, MAN_ORGINE_KEY_KEY);
  }

  public String manOrigineLib() {
    return (String) storedValueForKey(MAN_ORIGINE_LIB_KEY);
  }

  public void setManOrigineLib(String value) {
    takeStoredValueForKey(value, MAN_ORIGINE_LIB_KEY);
  }

  public java.math.BigDecimal manTtc() {
    return (java.math.BigDecimal) storedValueForKey(MAN_TTC_KEY);
  }

  public void setManTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MAN_TTC_KEY);
  }

  public java.math.BigDecimal manTva() {
    return (java.math.BigDecimal) storedValueForKey(MAN_TVA_KEY);
  }

  public void setManTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MAN_TVA_KEY);
  }

  public Integer modOrdre() {
    return (Integer) storedValueForKey(MOD_ORDRE_KEY);
  }

  public void setModOrdre(Integer value) {
    takeStoredValueForKey(value, MOD_ORDRE_KEY);
  }

  public String motifRejet() {
    return (String) storedValueForKey(MOTIF_REJET_KEY);
  }

  public void setMotifRejet(String value) {
    takeStoredValueForKey(value, MOTIF_REJET_KEY);
  }

  public Integer orgOrdre() {
    return (Integer) storedValueForKey(ORG_ORDRE_KEY);
  }

  public void setOrgOrdre(Integer value) {
    takeStoredValueForKey(value, ORG_ORDRE_KEY);
  }

  public Integer oriOrdre() {
    return (Integer) storedValueForKey(ORI_ORDRE_KEY);
  }

  public void setOriOrdre(Integer value) {
    takeStoredValueForKey(value, ORI_ORDRE_KEY);
  }

  public Integer paiOrdre() {
    return (Integer) storedValueForKey(PAI_ORDRE_KEY);
  }

  public void setPaiOrdre(Integer value) {
    takeStoredValueForKey(value, PAI_ORDRE_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public Long prestId() {
    return (Long) storedValueForKey(PREST_ID_KEY);
  }

  public void setPrestId(Long value) {
    takeStoredValueForKey(value, PREST_ID_KEY);
  }

  public Integer ribOrdreComptable() {
    return (Integer) storedValueForKey(RIB_ORDRE_COMPTABLE_KEY);
  }

  public void setRibOrdreComptable(Integer value) {
    takeStoredValueForKey(value, RIB_ORDRE_COMPTABLE_KEY);
  }

  public Double ribOrdreOrdonnateur() {
    return (Double) storedValueForKey(RIB_ORDRE_ORDONNATEUR_KEY);
  }

  public void setRibOrdreOrdonnateur(Double value) {
    takeStoredValueForKey(value, RIB_ORDRE_ORDONNATEUR_KEY);
  }

  public Double torOrdre() {
    return (Double) storedValueForKey(TOR_ORDRE_KEY);
  }

  public void setTorOrdre(Double value) {
    takeStoredValueForKey(value, TOR_ORDRE_KEY);
  }

  public org.cocktail.abricot.server.metier.EOBordereau bordereau() {
    return (org.cocktail.abricot.server.metier.EOBordereau)storedValueForKey(BORDEREAU_KEY);
  }

  public void setBordereauRelationship(org.cocktail.abricot.server.metier.EOBordereau value) {
    if (value == null) {
    	org.cocktail.abricot.server.metier.EOBordereau oldValue = bordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BORDEREAU_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOModePaiement modePaiement() {
    return (org.cocktail.application.serveur.eof.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
  }

  public void setModePaiementRelationship(org.cocktail.application.serveur.eof.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOModePaiement oldValue = modePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOOrgan organ() {
    return (org.cocktail.application.serveur.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.serveur.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.VFournisseur vFournisseur() {
    return (org.cocktail.application.serveur.eof.VFournisseur)storedValueForKey(V_FOURNISSEUR_KEY);
  }

  public void setVFournisseurRelationship(org.cocktail.application.serveur.eof.VFournisseur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.VFournisseur oldValue = vFournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, V_FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, V_FOURNISSEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EOMandat avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOMandat createEOMandat(EOEditingContext editingContext, Integer borId
, Integer detailNumero
, java.math.BigDecimal detailTtc
, Integer exeOrdre
, String gesCode
, String manEtat
, java.math.BigDecimal manHt
, Integer manId
, Double manNumero
, Integer manOrdre
, java.math.BigDecimal manTtc
, java.math.BigDecimal manTva
, Integer orgOrdre
, String pcoNum
, Double torOrdre
, org.cocktail.abricot.server.metier.EOBordereau bordereau, org.cocktail.application.serveur.eof.EOOrgan organ			) {
    EOMandat eo = (EOMandat) createAndInsertInstance(editingContext, _EOMandat.ENTITY_NAME);    
		eo.setBorId(borId);
		eo.setDetailNumero(detailNumero);
		eo.setDetailTtc(detailTtc);
		eo.setExeOrdre(exeOrdre);
		eo.setGesCode(gesCode);
		eo.setManEtat(manEtat);
		eo.setManHt(manHt);
		eo.setManId(manId);
		eo.setManNumero(manNumero);
		eo.setManOrdre(manOrdre);
		eo.setManTtc(manTtc);
		eo.setManTva(manTva);
		eo.setOrgOrdre(orgOrdre);
		eo.setPcoNum(pcoNum);
		eo.setTorOrdre(torOrdre);
    eo.setBordereauRelationship(bordereau);
    eo.setOrganRelationship(organ);
    return eo;
  }

  
	  public EOMandat localInstanceIn(EOEditingContext editingContext) {
	  		return (EOMandat)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMandat creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMandat creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOMandat object = (EOMandat)createAndInsertInstance(editingContext, _EOMandat.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOMandat localInstanceIn(EOEditingContext editingContext, EOMandat eo) {
    EOMandat localInstance = (eo == null) ? null : (EOMandat)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOMandat#localInstanceIn a la place.
   */
	public static EOMandat localInstanceOf(EOEditingContext editingContext, EOMandat eo) {
		return EOMandat.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOMandat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOMandat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMandat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMandat)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMandat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMandat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMandat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMandat)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOMandat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMandat eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMandat ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMandat fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
