// _EOVAbricotDepensePi.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVAbricotDepensePi.java instead.
package org.cocktail.abricot.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;

public abstract class _EOVAbricotDepensePi extends  ERXGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "VAbricotDepensePi";
	public static final String ENTITY_TABLE_NAME = "MARACUJA.V_ABRICOT_DEPENSE_PI";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "depId";

	public static final String ADR_ADRESSE1_KEY = "adrAdresse1";
	public static final String ADR_ADRESSE2_KEY = "adrAdresse2";
	public static final String ADR_CIVILITE_KEY = "adrCivilite";
	public static final String ADR_CP_KEY = "adrCp";
	public static final String ADR_NOM_KEY = "adrNom";
	public static final String ADR_PRENOM_KEY = "adrPrenom";
	public static final String ADR_VILLE_KEY = "adrVille";
	public static final String BIC_KEY = "bic";
	public static final String C_BANQUE_KEY = "cBanque";
	public static final String C_GUICHET_KEY = "cGuichet";
	public static final String DEP_ID_KEY = "depId";
	public static final String DOMICILIATION_KEY = "domiciliation";
	public static final String DPCO_HT_SAISIE_KEY = "dpcoHtSaisie";
	public static final String DPCO_ID_KEY = "dpcoId";
	public static final String DPCO_TTC_SAISIE_KEY = "dpcoTtcSaisie";
	public static final String DPCO_TVA_SAISIE_KEY = "dpcoTvaSaisie";
	public static final String DPP_DATE_FACTURE_KEY = "dppDateFacture";
	public static final String DPP_DATE_RECEPTION_KEY = "dppDateReception";
	public static final String DPP_DATE_SAISIE_KEY = "dppDateSaisie";
	public static final String DPP_DATE_SERVICE_FAIT_KEY = "dppDateServiceFait";
	public static final String DPP_HT_SAISIE_KEY = "dppHtSaisie";
	public static final String DPP_ID_KEY = "dppId";
	public static final String DPP_NB_PIECE_KEY = "dppNbPiece";
	public static final String DPP_NUMERO_FACTURE_KEY = "dppNumeroFacture";
	public static final String DPP_TTC_SAISIE_KEY = "dppTtcSaisie";
	public static final String DPP_TVA_SAISIE_KEY = "dppTvaSaisie";
	public static final String EXE_EXERCICE_KEY = "exeExercice";
	public static final String IBAN_KEY = "iban";
	public static final String MOD_CODE_KEY = "modCode";
	public static final String MOD_DOM_KEY = "modDom";
	public static final String MOD_LIBELLE_KEY = "modLibelle";
	public static final String NO_COMPTE_KEY = "noCompte";
	public static final String ORG_CR_KEY = "orgCr";
	public static final String ORG_ID_KEY = "orgId";
	public static final String ORG_SOUSCR_KEY = "orgSouscr";
	public static final String ORG_UB_KEY = "orgUb";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PERS_LC_KEY = "persLc";
	public static final String PERS_LIBELLE_KEY = "persLibelle";
	public static final String PERS_TYPE_KEY = "persType";
	public static final String TBO_ORDRE_KEY = "tboOrdre";
	public static final String TCD_CODE_KEY = "tcdCode";
	public static final String TCD_LIBELLE_KEY = "tcdLibelle";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String UTLNOMPRENOM_KEY = "utlnomprenom";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String ADR_ADRESSE1_COLKEY = "ADR_ADRESSE1";
	public static final String ADR_ADRESSE2_COLKEY = "ADR_ADRESSE2";
	public static final String ADR_CIVILITE_COLKEY = "ADR_CIVILITE";
	public static final String ADR_CP_COLKEY = "ADR_CP";
	public static final String ADR_NOM_COLKEY = "ADR_NOM";
	public static final String ADR_PRENOM_COLKEY = "ADR_PRENOM";
	public static final String ADR_VILLE_COLKEY = "ADR_VILLE";
	public static final String BIC_COLKEY = "BIC";
	public static final String C_BANQUE_COLKEY = "C_BANQUE";
	public static final String C_GUICHET_COLKEY = "C_GUICHET";
	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String DOMICILIATION_COLKEY = "DOMICILIATION";
	public static final String DPCO_HT_SAISIE_COLKEY = "DPCO_HT_SAISIE";
	public static final String DPCO_ID_COLKEY = "DPCO_ID";
	public static final String DPCO_TTC_SAISIE_COLKEY = "DPCO_TTC_SAISIE";
	public static final String DPCO_TVA_SAISIE_COLKEY = "DPCO_TVA_SAISIE";
	public static final String DPP_DATE_FACTURE_COLKEY = "DPP_DATE_FACTURE";
	public static final String DPP_DATE_RECEPTION_COLKEY = "DPP_DATE_RECEPTION";
	public static final String DPP_DATE_SAISIE_COLKEY = "DPP_DATE_SAISIE";
	public static final String DPP_DATE_SERVICE_FAIT_COLKEY = "DPP_DATE_SERVICE_FAIT";
	public static final String DPP_HT_SAISIE_COLKEY = "DPP_HT_SAISIE";
	public static final String DPP_ID_COLKEY = "DPP_ID";
	public static final String DPP_NB_PIECE_COLKEY = "DPP_NB_PIECE";
	public static final String DPP_NUMERO_FACTURE_COLKEY = "DPP_NUMERO_FACTURE";
	public static final String DPP_TTC_SAISIE_COLKEY = "DPP_TTC_SAISIE";
	public static final String DPP_TVA_SAISIE_COLKEY = "DPP_TVA_SAISIE";
	public static final String EXE_EXERCICE_COLKEY = "EXE_EXERCICE";
	public static final String IBAN_COLKEY = "IBAN";
	public static final String MOD_CODE_COLKEY = "MOD_CODE";
	public static final String MOD_DOM_COLKEY = "MOD_DOM";
	public static final String MOD_LIBELLE_COLKEY = "MOD_LIBELLE";
	public static final String NO_COMPTE_COLKEY = "NO_COMPTE";
	public static final String ORG_CR_COLKEY = "ORG_CR";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String ORG_SOUSCR_COLKEY = "ORG_SOUSCR";
	public static final String ORG_UB_COLKEY = "ORG_UB";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PERS_LC_COLKEY = "PERS_LC";
	public static final String PERS_LIBELLE_COLKEY = "PERS_LIBELLE";
	public static final String PERS_TYPE_COLKEY = "PERS_TYPE";
	public static final String TBO_ORDRE_COLKEY = "TBO_ORDRE";
	public static final String TCD_CODE_COLKEY = "TCD_CODE";
	public static final String TCD_LIBELLE_COLKEY = "TCD_LIBELLE";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String UTLNOMPRENOM_COLKEY = "UTLNOMPRENOM";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";



	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String adrAdresse1() {
    return (String) storedValueForKey(ADR_ADRESSE1_KEY);
  }

  public void setAdrAdresse1(String value) {
    takeStoredValueForKey(value, ADR_ADRESSE1_KEY);
  }

  public String adrAdresse2() {
    return (String) storedValueForKey(ADR_ADRESSE2_KEY);
  }

  public void setAdrAdresse2(String value) {
    takeStoredValueForKey(value, ADR_ADRESSE2_KEY);
  }

  public String adrCivilite() {
    return (String) storedValueForKey(ADR_CIVILITE_KEY);
  }

  public void setAdrCivilite(String value) {
    takeStoredValueForKey(value, ADR_CIVILITE_KEY);
  }

  public String adrCp() {
    return (String) storedValueForKey(ADR_CP_KEY);
  }

  public void setAdrCp(String value) {
    takeStoredValueForKey(value, ADR_CP_KEY);
  }

  public String adrNom() {
    return (String) storedValueForKey(ADR_NOM_KEY);
  }

  public void setAdrNom(String value) {
    takeStoredValueForKey(value, ADR_NOM_KEY);
  }

  public String adrPrenom() {
    return (String) storedValueForKey(ADR_PRENOM_KEY);
  }

  public void setAdrPrenom(String value) {
    takeStoredValueForKey(value, ADR_PRENOM_KEY);
  }

  public String adrVille() {
    return (String) storedValueForKey(ADR_VILLE_KEY);
  }

  public void setAdrVille(String value) {
    takeStoredValueForKey(value, ADR_VILLE_KEY);
  }

  public String bic() {
    return (String) storedValueForKey(BIC_KEY);
  }

  public void setBic(String value) {
    takeStoredValueForKey(value, BIC_KEY);
  }

  public String cBanque() {
    return (String) storedValueForKey(C_BANQUE_KEY);
  }

  public void setCBanque(String value) {
    takeStoredValueForKey(value, C_BANQUE_KEY);
  }

  public String cGuichet() {
    return (String) storedValueForKey(C_GUICHET_KEY);
  }

  public void setCGuichet(String value) {
    takeStoredValueForKey(value, C_GUICHET_KEY);
  }

  public Integer depId() {
    return (Integer) storedValueForKey(DEP_ID_KEY);
  }

  public void setDepId(Integer value) {
    takeStoredValueForKey(value, DEP_ID_KEY);
  }

  public String domiciliation() {
    return (String) storedValueForKey(DOMICILIATION_KEY);
  }

  public void setDomiciliation(String value) {
    takeStoredValueForKey(value, DOMICILIATION_KEY);
  }

  public java.math.BigDecimal dpcoHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPCO_HT_SAISIE_KEY);
  }

  public void setDpcoHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPCO_HT_SAISIE_KEY);
  }

  public Integer dpcoId() {
    return (Integer) storedValueForKey(DPCO_ID_KEY);
  }

  public void setDpcoId(Integer value) {
    takeStoredValueForKey(value, DPCO_ID_KEY);
  }

  public java.math.BigDecimal dpcoTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPCO_TTC_SAISIE_KEY);
  }

  public void setDpcoTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPCO_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal dpcoTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPCO_TVA_SAISIE_KEY);
  }

  public void setDpcoTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPCO_TVA_SAISIE_KEY);
  }

  public NSTimestamp dppDateFacture() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_FACTURE_KEY);
  }

  public void setDppDateFacture(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_FACTURE_KEY);
  }

  public NSTimestamp dppDateReception() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_RECEPTION_KEY);
  }

  public void setDppDateReception(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_RECEPTION_KEY);
  }

  public NSTimestamp dppDateSaisie() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_SAISIE_KEY);
  }

  public void setDppDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_SAISIE_KEY);
  }

  public NSTimestamp dppDateServiceFait() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_SERVICE_FAIT_KEY);
  }

  public void setDppDateServiceFait(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_SERVICE_FAIT_KEY);
  }

  public java.math.BigDecimal dppHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPP_HT_SAISIE_KEY);
  }

  public void setDppHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPP_HT_SAISIE_KEY);
  }

  public Integer dppId() {
    return (Integer) storedValueForKey(DPP_ID_KEY);
  }

  public void setDppId(Integer value) {
    takeStoredValueForKey(value, DPP_ID_KEY);
  }

  public Double dppNbPiece() {
    return (Double) storedValueForKey(DPP_NB_PIECE_KEY);
  }

  public void setDppNbPiece(Double value) {
    takeStoredValueForKey(value, DPP_NB_PIECE_KEY);
  }

  public String dppNumeroFacture() {
    return (String) storedValueForKey(DPP_NUMERO_FACTURE_KEY);
  }

  public void setDppNumeroFacture(String value) {
    takeStoredValueForKey(value, DPP_NUMERO_FACTURE_KEY);
  }

  public java.math.BigDecimal dppTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPP_TTC_SAISIE_KEY);
  }

  public void setDppTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPP_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal dppTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPP_TVA_SAISIE_KEY);
  }

  public void setDppTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPP_TVA_SAISIE_KEY);
  }

  public Integer exeExercice() {
    return (Integer) storedValueForKey(EXE_EXERCICE_KEY);
  }

  public void setExeExercice(Integer value) {
    takeStoredValueForKey(value, EXE_EXERCICE_KEY);
  }

  public String iban() {
    return (String) storedValueForKey(IBAN_KEY);
  }

  public void setIban(String value) {
    takeStoredValueForKey(value, IBAN_KEY);
  }

  public String modCode() {
    return (String) storedValueForKey(MOD_CODE_KEY);
  }

  public void setModCode(String value) {
    takeStoredValueForKey(value, MOD_CODE_KEY);
  }

  public String modDom() {
    return (String) storedValueForKey(MOD_DOM_KEY);
  }

  public void setModDom(String value) {
    takeStoredValueForKey(value, MOD_DOM_KEY);
  }

  public String modLibelle() {
    return (String) storedValueForKey(MOD_LIBELLE_KEY);
  }

  public void setModLibelle(String value) {
    takeStoredValueForKey(value, MOD_LIBELLE_KEY);
  }

  public String noCompte() {
    return (String) storedValueForKey(NO_COMPTE_KEY);
  }

  public void setNoCompte(String value) {
    takeStoredValueForKey(value, NO_COMPTE_KEY);
  }

  public String orgCr() {
    return (String) storedValueForKey(ORG_CR_KEY);
  }

  public void setOrgCr(String value) {
    takeStoredValueForKey(value, ORG_CR_KEY);
  }

  public Integer orgId() {
    return (Integer) storedValueForKey(ORG_ID_KEY);
  }

  public void setOrgId(Integer value) {
    takeStoredValueForKey(value, ORG_ID_KEY);
  }

  public String orgSouscr() {
    return (String) storedValueForKey(ORG_SOUSCR_KEY);
  }

  public void setOrgSouscr(String value) {
    takeStoredValueForKey(value, ORG_SOUSCR_KEY);
  }

  public String orgUb() {
    return (String) storedValueForKey(ORG_UB_KEY);
  }

  public void setOrgUb(String value) {
    takeStoredValueForKey(value, ORG_UB_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public String persLc() {
    return (String) storedValueForKey(PERS_LC_KEY);
  }

  public void setPersLc(String value) {
    takeStoredValueForKey(value, PERS_LC_KEY);
  }

  public String persLibelle() {
    return (String) storedValueForKey(PERS_LIBELLE_KEY);
  }

  public void setPersLibelle(String value) {
    takeStoredValueForKey(value, PERS_LIBELLE_KEY);
  }

  public String persType() {
    return (String) storedValueForKey(PERS_TYPE_KEY);
  }

  public void setPersType(String value) {
    takeStoredValueForKey(value, PERS_TYPE_KEY);
  }

  public Integer tboOrdre() {
    return (Integer) storedValueForKey(TBO_ORDRE_KEY);
  }

  public void setTboOrdre(Integer value) {
    takeStoredValueForKey(value, TBO_ORDRE_KEY);
  }

  public String tcdCode() {
    return (String) storedValueForKey(TCD_CODE_KEY);
  }

  public void setTcdCode(String value) {
    takeStoredValueForKey(value, TCD_CODE_KEY);
  }

  public String tcdLibelle() {
    return (String) storedValueForKey(TCD_LIBELLE_KEY);
  }

  public void setTcdLibelle(String value) {
    takeStoredValueForKey(value, TCD_LIBELLE_KEY);
  }

  public Integer tcdOrdre() {
    return (Integer) storedValueForKey(TCD_ORDRE_KEY);
  }

  public void setTcdOrdre(Integer value) {
    takeStoredValueForKey(value, TCD_ORDRE_KEY);
  }

  public String utlnomprenom() {
    return (String) storedValueForKey(UTLNOMPRENOM_KEY);
  }

  public void setUtlnomprenom(String value) {
    takeStoredValueForKey(value, UTLNOMPRENOM_KEY);
  }

  public Integer utlOrdre() {
    return (Integer) storedValueForKey(UTL_ORDRE_KEY);
  }

  public void setUtlOrdre(Integer value) {
    takeStoredValueForKey(value, UTL_ORDRE_KEY);
  }

  public org.cocktail.application.serveur.eof.EOExercice exercice() {
    return (org.cocktail.application.serveur.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.serveur.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOOrgan organ() {
    return (org.cocktail.application.serveur.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.serveur.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOUtilisateur utilisateur() {
    return (org.cocktail.application.serveur.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.application.serveur.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EOVAbricotDepensePi avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOVAbricotDepensePi createEOVAbricotDepensePi(EOEditingContext editingContext, String adrCivilite
, String adrNom
, Integer depId
, java.math.BigDecimal dpcoHtSaisie
, Integer dpcoId
, java.math.BigDecimal dpcoTtcSaisie
, java.math.BigDecimal dpcoTvaSaisie
, NSTimestamp dppDateFacture
, NSTimestamp dppDateReception
, NSTimestamp dppDateSaisie
, NSTimestamp dppDateServiceFait
, java.math.BigDecimal dppHtSaisie
, Integer dppId
, Double dppNbPiece
, String dppNumeroFacture
, java.math.BigDecimal dppTtcSaisie
, java.math.BigDecimal dppTvaSaisie
, Integer exeExercice
, String modCode
, String modDom
, String modLibelle
, Integer orgId
, String pcoNum
, String persLibelle
, String persType
, Integer tboOrdre
, String tcdCode
, String tcdLibelle
, Integer tcdOrdre
			) {
    EOVAbricotDepensePi eo = (EOVAbricotDepensePi) createAndInsertInstance(editingContext, _EOVAbricotDepensePi.ENTITY_NAME);    
		eo.setAdrCivilite(adrCivilite);
		eo.setAdrNom(adrNom);
		eo.setDepId(depId);
		eo.setDpcoHtSaisie(dpcoHtSaisie);
		eo.setDpcoId(dpcoId);
		eo.setDpcoTtcSaisie(dpcoTtcSaisie);
		eo.setDpcoTvaSaisie(dpcoTvaSaisie);
		eo.setDppDateFacture(dppDateFacture);
		eo.setDppDateReception(dppDateReception);
		eo.setDppDateSaisie(dppDateSaisie);
		eo.setDppDateServiceFait(dppDateServiceFait);
		eo.setDppHtSaisie(dppHtSaisie);
		eo.setDppId(dppId);
		eo.setDppNbPiece(dppNbPiece);
		eo.setDppNumeroFacture(dppNumeroFacture);
		eo.setDppTtcSaisie(dppTtcSaisie);
		eo.setDppTvaSaisie(dppTvaSaisie);
		eo.setExeExercice(exeExercice);
		eo.setModCode(modCode);
		eo.setModDom(modDom);
		eo.setModLibelle(modLibelle);
		eo.setOrgId(orgId);
		eo.setPcoNum(pcoNum);
		eo.setPersLibelle(persLibelle);
		eo.setPersType(persType);
		eo.setTboOrdre(tboOrdre);
		eo.setTcdCode(tcdCode);
		eo.setTcdLibelle(tcdLibelle);
		eo.setTcdOrdre(tcdOrdre);
    return eo;
  }

  
	  public EOVAbricotDepensePi localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVAbricotDepensePi)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVAbricotDepensePi creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVAbricotDepensePi creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOVAbricotDepensePi object = (EOVAbricotDepensePi)createAndInsertInstance(editingContext, _EOVAbricotDepensePi.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOVAbricotDepensePi localInstanceIn(EOEditingContext editingContext, EOVAbricotDepensePi eo) {
    EOVAbricotDepensePi localInstance = (eo == null) ? null : (EOVAbricotDepensePi)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOVAbricotDepensePi#localInstanceIn a la place.
   */
	public static EOVAbricotDepensePi localInstanceOf(EOEditingContext editingContext, EOVAbricotDepensePi eo) {
		return EOVAbricotDepensePi.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVAbricotDepensePi fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVAbricotDepensePi fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVAbricotDepensePi eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVAbricotDepensePi)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVAbricotDepensePi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVAbricotDepensePi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVAbricotDepensePi eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVAbricotDepensePi)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVAbricotDepensePi fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVAbricotDepensePi eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVAbricotDepensePi ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVAbricotDepensePi fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
