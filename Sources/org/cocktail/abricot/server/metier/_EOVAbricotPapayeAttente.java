// _EOVAbricotPapayeAttente.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVAbricotPapayeAttente.java instead.
package org.cocktail.abricot.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;

public abstract class _EOVAbricotPapayeAttente extends  ERXGenericRecord {

	private static final long serialVersionUID = -3258666968396815005L;

	
	public static final String ENTITY_NAME = "VAbricotPapayeAttente";
	public static final String ENTITY_TABLE_NAME = "MARACUJA.V_ABRICOT_PAPAYE_ATTENTE";



	// Attributes


	public static final String EXERCICE_KEY = "exercice";
	public static final String MOIS_KEY = "mois";
	public static final String MONTANT_KEY = "montant";
	public static final String NB_ATTENTE_KEY = "nbAttente";
	public static final String ORG_UB_KEY = "orgUb";
	public static final String TBO_ORDRE_KEY = "tboOrdre";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String EXERCICE_COLKEY = "EXERCICE";
	public static final String MOIS_COLKEY = "MOIS";
	public static final String MONTANT_COLKEY = "MONTANT";
	public static final String NB_ATTENTE_COLKEY = "NB_ATTENTE";
	public static final String ORG_UB_COLKEY = "ORG_UB";
	public static final String TBO_ORDRE_COLKEY = "TBO_ORDRE";



	// Relationships
	public static final String TYPE_BORDEREAU_KEY = "typeBordereau";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer exercice() {
    return (Integer) storedValueForKey(EXERCICE_KEY);
  }

  public void setExercice(Integer value) {
    takeStoredValueForKey(value, EXERCICE_KEY);
  }

  public String mois() {
    return (String) storedValueForKey(MOIS_KEY);
  }

  public void setMois(String value) {
    takeStoredValueForKey(value, MOIS_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_KEY);
  }

  public Integer nbAttente() {
    return (Integer) storedValueForKey(NB_ATTENTE_KEY);
  }

  public void setNbAttente(Integer value) {
    takeStoredValueForKey(value, NB_ATTENTE_KEY);
  }

  public String orgUb() {
    return (String) storedValueForKey(ORG_UB_KEY);
  }

  public void setOrgUb(String value) {
    takeStoredValueForKey(value, ORG_UB_KEY);
  }

  public Integer tboOrdre() {
    return (Integer) storedValueForKey(TBO_ORDRE_KEY);
  }

  public void setTboOrdre(Integer value) {
    takeStoredValueForKey(value, TBO_ORDRE_KEY);
  }

  public org.cocktail.application.serveur.eof.EOTypeBordereau typeBordereau() {
    return (org.cocktail.application.serveur.eof.EOTypeBordereau)storedValueForKey(TYPE_BORDEREAU_KEY);
  }

  public void setTypeBordereauRelationship(org.cocktail.application.serveur.eof.EOTypeBordereau value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeBordereau oldValue = typeBordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_BORDEREAU_KEY);
    }
  }
  

/**
 * Créer une instance de EOVAbricotPapayeAttente avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOVAbricotPapayeAttente createEOVAbricotPapayeAttente(EOEditingContext editingContext, Integer exercice
, String mois
, java.math.BigDecimal montant
, Integer nbAttente
, String orgUb
, Integer tboOrdre
, org.cocktail.application.serveur.eof.EOTypeBordereau typeBordereau			) {
    EOVAbricotPapayeAttente eo = (EOVAbricotPapayeAttente) createAndInsertInstance(editingContext, _EOVAbricotPapayeAttente.ENTITY_NAME);    
		eo.setExercice(exercice);
		eo.setMois(mois);
		eo.setMontant(montant);
		eo.setNbAttente(nbAttente);
		eo.setOrgUb(orgUb);
		eo.setTboOrdre(tboOrdre);
    eo.setTypeBordereauRelationship(typeBordereau);
    return eo;
  }

  
	  public EOVAbricotPapayeAttente localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVAbricotPapayeAttente)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVAbricotPapayeAttente creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVAbricotPapayeAttente creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOVAbricotPapayeAttente object = (EOVAbricotPapayeAttente)createAndInsertInstance(editingContext, _EOVAbricotPapayeAttente.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOVAbricotPapayeAttente localInstanceIn(EOEditingContext editingContext, EOVAbricotPapayeAttente eo) {
    EOVAbricotPapayeAttente localInstance = (eo == null) ? null : (EOVAbricotPapayeAttente)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOVAbricotPapayeAttente#localInstanceIn a la place.
   */
	public static EOVAbricotPapayeAttente localInstanceOf(EOEditingContext editingContext, EOVAbricotPapayeAttente eo) {
		return EOVAbricotPapayeAttente.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVAbricotPapayeAttente fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVAbricotPapayeAttente fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVAbricotPapayeAttente eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVAbricotPapayeAttente)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVAbricotPapayeAttente fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVAbricotPapayeAttente fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVAbricotPapayeAttente eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVAbricotPapayeAttente)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVAbricotPapayeAttente fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVAbricotPapayeAttente eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVAbricotPapayeAttente ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVAbricotPapayeAttente fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
