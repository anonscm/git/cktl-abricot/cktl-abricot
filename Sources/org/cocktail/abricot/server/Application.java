package org.cocktail.abricot.server;
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Properties;
import java.util.TimeZone;

import org.cocktail.abricot.common.Constantes;
import org.cocktail.application.serveur.CocktailApplication;
import org.cocktail.application.serveur.eof.EOGrhumParametres;
import org.cocktail.fwkcktlwebapp.server.CktlERXStaticResourceRequestHandler;
import org.cocktail.fwkcktlwebapp.server.init.NSLegacyBundleMonkeyPatch;
import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimeZone;
import com.woinject.WOInject;

public class Application extends CocktailApplication {

	private	NSMutableArray utilisateursConnectes = new NSMutableArray();
	private MyByteArrayOutputStream	redirectedOutStream, redirectedErrStream; // Nouvelles sorties
	private	static final int LOG_OUT_TAILLE_MAXI = 100000;
	private NSMutableDictionary appParametres;

	public static void main(String argv[]) {
		NSLegacyBundleMonkeyPatch.apply();
        WOInject.init("org.cocktail.abricot.server.Application", argv);
	}

	/**
	 * 
	 */
	public Application() {

		super();

		Properties prop = System.getProperties();
		addLogMessage("INFORMATION", "WOOutputPath = "
				+ prop.getProperty("WOOutputPath"));
		addLogMessage("INFORMATION", "WOPort = " + prop.getProperty("WOPort"));
		checkExistenceFichiersImpression();

		// INIT TIME ZONE
		String timeZone = defaultTimeZone();
		TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
		NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName(timeZone,false));

		System.out.println(">>>>>  DEFAULT TIME ZONE : " + TimeZone.getDefault());

		System.setProperty("java.awt.headless","true");

		// redirection des logs
		redirectedOutStream = new MyByteArrayOutputStream(System.out);
		redirectedErrStream = new MyByteArrayOutputStream(System.err);
		System.setOut(new PrintStream(redirectedOutStream));
		System.setErr(new PrintStream(redirectedErrStream));

		((NSLog.PrintStreamLogger)NSLog.out).setPrintStream(System.out);
		((NSLog.PrintStreamLogger)NSLog.debug).setPrintStream(System.out);
		((NSLog.PrintStreamLogger)NSLog.err).setPrintStream(System.err);

		// fix pour que les WebServerResources marchent en javaclient
		if (isDirectConnectEnabled()) {
			registerRequestHandler(new CktlERXStaticResourceRequestHandler(), "wr");
		}
	}


	/**
	 * 
	 * @param editingContext
	 * @param cle
	 * @return
	 */
	public static String parametrePourCle(EOEditingContext editingContext,String cle) {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("paramKey = %@",new NSArray(cle));
		EOFetchSpecification fs = new EOFetchSpecification(EOGrhumParametres.ENTITY_NAME,qualifier,null);
		NSArray results = editingContext.objectsWithFetchSpecification(fs);
		try {
			return ((EOGrhumParametres)results.objectAtIndex(0)).paramValue();
		} catch(Exception e) {
			return null;
		}
	}


	/**
	 * 
	 * @return
	 */
	String defaultTimeZone() {
		String valeur = getParam("DEFAULT_TIME_ZONE");

		if (valeur == null) {
			EOEditingContext editingContext = new EOEditingContext();
			valeur = parametrePourCle(editingContext, "DEFAULT_TIME_ZONE");
			if (valeur == null) {
				if (valeur == null) {
					valeur = "GMT";
				}
			}
		}
		return valeur; 

	}

	/**
	 * 
	 * @param paramKey
	 * @return
	 */
	public String getParam(String paramKey) {
		NSArray a = (NSArray) appParametres().valueForKey(paramKey);
		String res = null;
		if (a == null || a.count() == 0) {
			// recherche dans le configFileName()/configTableName()
			res = config().stringForKey(paramKey);
		}
		else {
			res = (String) a.objectAtIndex(0);
		}
		return res;
	}

	/**
	 * 
	 * @return
	 */
	private NSMutableDictionary appParametres() {
		if (appParametres == null) {
			if (EOSharedEditingContext.defaultSharedEditingContext() == null || parametresTableName() == null) {
				return new NSMutableDictionary();
			}
			appParametres = new NSMutableDictionary();
			NSArray vParam = dataBus().fetchArray(parametresTableName(), null, null);
			EOGenericRecord vTmpRec;
			String previousParamKey = null;
			NSMutableArray a = null;
			for (java.util.Enumeration<EOGenericRecord> enumerator = vParam.objectEnumerator();enumerator.hasMoreElements();) {
				vTmpRec = enumerator.nextElement();
				if (vTmpRec.valueForKey("paramKey") == null || ((String) vTmpRec.valueForKey("paramKey")).equals("")
						|| vTmpRec.valueForKey("paramValue") == null) {
					continue;
				}
				if (!((String) vTmpRec.valueForKey("paramKey")).equalsIgnoreCase(previousParamKey)) {
					if (a != null && a.count() > 0) {
						appParametres.setObjectForKey(a, previousParamKey);
					}
					previousParamKey = (String) vTmpRec.valueForKey("paramKey");
					a = new NSMutableArray();
				}
				if (vTmpRec.valueForKey("paramValue") != null) {
					a.addObject((String) vTmpRec.valueForKey("paramValue"));
				}
			}
			if (a != null && a.count() > 0) {
				appParametres.setObjectForKey(a, previousParamKey);
			}
			EOSharedEditingContext.defaultSharedEditingContext().invalidateAllObjects();
		}
		return appParametres;
	}


	private String parametresTableName() {
		return null;
	}


	public NSArray utilisateursConnectes()	{
		return utilisateursConnectes;
	}

	private HashMap<?, Session> mySessions = new HashMap();
	private org.cocktail.abricot.server.Version _appVersion;

	public HashMap getMySessions() {
		return mySessions;
	}

	public void setMySessions(final HashMap mySessions) {
		this.mySessions = mySessions;
	}

	public String version() {
		return VersionMe.txtAppliVersion();
	}
	public String getMailHost() {
		return getMandatoryParameterFoKeyString("HOST_MAIL");
	}

	public void cleanLogs()	{
		redirectedErrStream.reset();
		redirectedOutStream.reset();		
	}
	public String outLogs()	{
		System.out.println("Application.outLogs() REDIRECT : " + redirectedOutStream);
		return redirectedOutStream.toString();
	}

	public String errLogs()	{
		return redirectedErrStream.toString();
	}

	public String configFileName() {
		return "Abricot.config";
	}



	public String configTableName() {
		return "FwkCktlWebApp_GrhumParametres";
	}

	public String mainModelName() {
		return "abricot";
	}

	public File getFichierReport(String fileName) {
		File f = new File(config().stringForKey("REP_BASE_JASPER_PATH_LOCAL") + fileName);
		if (!f.exists()) {
			f = new File(config().stringForKey("REP_BASE_JASPER_PATH") + fileName);
		}
		return f;
	}

	public boolean sendMail( String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody) {
		return mailBus().sendMail(mailFrom, mailTo, "", mailSubject, mailBody);
	}

	public void checkExistenceFichiersImpression() {
		System.out.println("");
		System.out.println("VERIFICATION DE LA PRESENCE DES FICHIERS D'IMPRESSION");
		System.out.println("-----------------------------------------------------");
		System.out.println("Verification dans " + config().stringForKey("REP_BASE_JASPER_PATH_LOCAL"));
		System.out.println("et " + config().stringForKey("REP_BASE_JASPER_PATH"));
		System.out.println("(emplacements definis dans le fichier de config)");
		boolean erreur = false;
		for (int i = 0; i < Constantes.LISTE_REQUIRED_REPORTS.length; i++) {
			String fichier = Constantes.LISTE_REQUIRED_REPORTS[i];
			System.out.print(fichier);
			System.out.print("......................");
			File f = getFichierReport(fichier);
			if (f.exists()) {
				System.out.println("OK     (" + f.getParent() + ")");
			}
			else {
				System.out.println("ATTENTION NON TROUVE");
				erreur = true;
			}
		}
		System.out.println();
		if (erreur) {
			System.err.println("CERTAINS FICHIER DE REPORT N'ONT PAS ETE TROUVES");
		}

	}

	// controle de versions
	public A_CktlVersion appCktlVersion() {
		if (_appVersion == null) {
			_appVersion = new Version();
		}
		return _appVersion;
	}


	private class MyByteArrayOutputStream extends ByteArrayOutputStream {
		protected PrintStream out;
		public MyByteArrayOutputStream(PrintStream out) {
			this.out = out;
		}
		public synchronized void write(int b) {
			super.write(b);
			out.write(b);
		}
		public synchronized void write(byte[] b, int off, int len) {
			super.write(b, off, len);	    			    		
			out.write(b, off, len);

			if (count > LOG_OUT_TAILLE_MAXI)	{
				String logServerOut = redirectedOutStream.toString();
				logServerOut = logServerOut.substring((LOG_OUT_TAILLE_MAXI / 2), logServerOut.length());

				// On reaffecte cette valeur a la variable redirectOutStream
				redirectedOutStream.reset();
				try {
					redirectedOutStream.write(logServerOut.getBytes());
				}
				catch (Exception e)	{}
			}

		}
	}



}
