
package org.cocktail.abricot.server;

import org.cocktail.fwkcktlwebapp.common.version.app.VersionApp;


// Versions du serveur, mini du framework, mini de la base
// nom de l'application

public class VersionMe {
	
	// Nom de l'appli
	
	public static final String APPLICATIONFINALNAME="Abricot - Gestion des mandatements";
	public static final String APPLICATIONINTERNALNAME="Abricot";
	public static final String APPLICATIONSTRID = "ABRICOT";

	// Version minimum de la base de donnees (USER MARACUJA) necessaire pour fonctionner avec cette version
	public static final String MINDBVERSION = "1.4.7.0";

	public static final int VERSIONNUMMAJ;
	public static final int VERSIONNUMMIN;
	public static final int VERSIONNUMPATCH;
	public static final int VERSIONNUMBUILD;

	public static final String VERSIONDATE;
	public static final String COMMENT = null;

	static {
		VersionApp versionApp = Application.application().injector().getInstance(VersionApp.class);
		VERSIONNUMMAJ = versionApp.majVersion();
		VERSIONNUMMIN = versionApp.minVersion();
		VERSIONNUMPATCH = versionApp.patchVersion();
		VERSIONNUMBUILD = versionApp.buildVersion();
		VERSIONDATE = versionApp.dateVersion();
	}

	public static final String VERSIONNUM = VERSIONNUMMAJ+"."+VERSIONNUMMIN+"."+VERSIONNUMPATCH+"."+VERSIONNUMBUILD;



	public static String appliVersion() {
		String appliVersion = VERSIONNUMMAJ + "." + VERSIONNUMMIN + "." + VERSIONNUMPATCH + "." + VERSIONNUMBUILD;
		return appliVersion;
	}

	public static String htmlAppliVersion() {
		String htmlAppliVersion = "<b>Version " + appliVersion();
		htmlAppliVersion += " du " + VERSIONDATE + "</b>";
		return htmlAppliVersion;
	}

	public static String txtAppliVersion() {
		return "Version " + appliVersion() + " du " + VERSIONDATE;
	}

	public static String rawVersion() {
		return appliVersion();
	}

	
}
