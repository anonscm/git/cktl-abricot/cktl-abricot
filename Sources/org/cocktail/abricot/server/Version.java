package org.cocktail.abricot.server;

import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionJava;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionOracleServer;

/**
 * @author Rodolphe PRIN
 */
public final class Version extends A_CktlVersion {

	// Version appli

	public static final int VERSIONNUMMAJ = VersionMe.VERSIONNUMMAJ;
	public static final int VERSIONNUMMIN = VersionMe.VERSIONNUMMIN;
	public static final int VERSIONNUMPATCH = VersionMe.VERSIONNUMPATCH;
	public static final int VERSIONNUMBUILD = VersionMe.VERSIONNUMBUILD;

	private static final String VERSIONDATE = VersionMe.VERSIONDATE;
	private static final String COMMENT = null;

	/** Nom qui caracterise la version (ex. RC1) ou vide */
	public static final String VERSIONLABEL = "";

	/** Version de la base de données requise */
	private static final String BD_VERSION_MIN = "1.9.3.4";
	private static final String BD_VERSION_MAX = null;


	/** Version du JRE */
	private static final String JRE_VERSION_MIN = "1.5.0.0";
	private static final String JRE_VERSION_MAX = null;

	/** Version d'ORACLE */
	private static final String ORACLE_VERSION_MIN = "9.0";
	private static final String ORACLE_VERSION_MAX = null;



	// Pour affichage en ligne de commande...
	public static void main(String argv[]) {
		System.out.println(VERSIONNUMMAJ + "." + VERSIONNUMMIN + "." + VERSIONNUMPATCH + "." + VERSIONNUMBUILD);

	}

	public String name() {
		return VersionMe.APPLICATIONFINALNAME;
	}

	public String internalName() {
		return VersionMe.APPLICATIONINTERNALNAME;
	}

	public int versionNumBuild() {
		return VERSIONNUMBUILD;
	}

	public int versionNumMaj() {
		return VERSIONNUMMAJ;
	}

	public int versionNumMin() {
		return VERSIONNUMMIN;
	}

	public int versionNumPatch() {
		return VERSIONNUMPATCH;
	}

	public String date() {
		return VERSIONDATE;
	}

	public String comment() {
		return COMMENT;
	}

	// liste des dependances
	public CktlVersionRequirements[] dependencies() {
		return new CktlVersionRequirements[] {
				new CktlVersionRequirements(new CktlVersionJava(), JRE_VERSION_MIN, JRE_VERSION_MAX, true),
				new CktlVersionRequirements(new CktlVersionOracleServer(), ORACLE_VERSION_MIN, ORACLE_VERSION_MAX, false),
				new CktlVersionRequirements(new VersionDatabase(), BD_VERSION_MIN, BD_VERSION_MAX, true),
		};
	}

}
