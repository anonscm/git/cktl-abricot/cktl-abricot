/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.abricot.common;


/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class Constantes {
	public static final String BORD_MAND_4 = "BORD_MAND_4.jasper";
	public static final String BORD_MAND_4_OR = "BORD_MAND_4_or.jasper";
	public static final String BTTE = "BTTE.jasper";
	public static final String BORD_JOURNAL = "Bord_journal.jasper";
	public static final String BORD_JOURNALM = "Bord_journalM.jasper";
	public static final String BORD_JOURNALM_OR = "Bord_journalM_or.jasper";
	public static final String DEPENSESMANDAT = "DepensesMandat.jasper";
	public static final String MANDAT_1 = "MANDAT_1.jasper";
	public static final String SOMME_APAYER = "Somme_apayer.jasper";
	public static final String BTTE_LIQUIDATION = "btte_liquidation.jasper";
	public static final String BTTE_LIQUIDATION_BROUILLARD = "btte_liquidation_Brouillard.jasper";
	public static final String DEPENSE = "depense.jasper";
	public static final String DEPENSEPI = "depensepi.jasper";
	public static final String REC_DECOMPTE = "rec_decompte.jasper";
	public static final String REC_DECOMPTE_ASP = "rec_decompte_asp.jasper";
	public static final String REC_DECOMPTE_OR = "rec_decompte_or.jasper";
	public static final String REC_LIQUIDATION = "rec_liquidation.jasper";
	public static final String REC_LIQUIDATION_OR = "rec_liquidation_or.jasper";
	public static final String RECETTE = "recette.jasper";
	public static final String RECETTEATTENTE = "recetteAttente.jasper";
	public static final String RECETTEPI = "recettepi.jasper";
	public static final String REDUCTION = "reduction.jasper";
	public static final String REGLEMENT_RECAP = "reglement_recap.jasper";
	public static final String REVERSEMENT = "reversement.jasper";
	public static final String TITRECO_ANNEXE = "titreco_annexe.jasper";
	public static final String TITRECO_ANNEXE_DETAIL = "titreco_annexe_detail.jasper";

	public static String[] LISTE_REQUIRED_REPORTS = new String[] {
			BORD_MAND_4,
			BORD_MAND_4_OR,
			BTTE,
			BORD_JOURNAL,
			BORD_JOURNALM,
			BORD_JOURNALM_OR,
			DEPENSESMANDAT,
			MANDAT_1,
			SOMME_APAYER,
			BTTE_LIQUIDATION,
			BTTE_LIQUIDATION_BROUILLARD,
			DEPENSE,
			DEPENSEPI,
			REC_DECOMPTE,
			REC_DECOMPTE_ASP,
			REC_DECOMPTE_OR,
			REC_LIQUIDATION,
			REC_LIQUIDATION_OR,
			RECETTE,
			RECETTEATTENTE,
			RECETTEPI,
			REDUCTION,
			REGLEMENT_RECAP,
			REVERSEMENT,
			TITRECO_ANNEXE,
			TITRECO_ANNEXE_DETAIL

	};

}
