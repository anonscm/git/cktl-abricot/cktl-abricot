/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.client.common.utilities;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoundedRangeModel;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import org.cocktail.client.common.swing.ZDatePickerPanel;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class CocktailUtilities 
{

	//public static final String WINDOWS_OS_NAME = "Windows";
	public static final String WINDOWS_OS_NAME = "Win"; // A utiliser avec startsWith
	public static final String WINDOWS2000_OS_NAME = "Windows 2000";

	public static final String WINDOWS_FILE_PATH_SEPARATOR = "\\"+"\\";
	public static final String APPLICATION_FILE_PATH_SEPARATOR = "\\";

	public static final String MAC_OS_X_OPEN = "open ";
	public static final String MAC_OS_X_OS_NAME = "Mac OS X";
	public static final String MAC_OS_X_EXEC = "open ";

	public static final String WINDOWS_EXEC = "launch.bat ";

	public static final String [] LETTRES_ALPHABET = new String[] {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};

	public static final String [] LISTE_MOIS = new String[] {"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet","Aout", "Septembre", "Octobre", "Novembre", "Décembre"};

	public static final String [] LISTE_MOIS_MAJ = new String[] {"JANVIER", "FEVRIER", "MARS", "AVRIL", "MAI", "JUIN", "JUILLET","AOUT", "SEPTEMBRE", "OCTOBRE", "NOVEMBRE", "DECEMBRE"};

	private static final	Color COULEUR_FOND_ACTIF = Color.white;
	private static final	Color COULEUR_FOND_INACTIF = new Color(222,222,222);
	//	private static final	Color COULEUR_FOND_INACTIF = new Color(255,0,0);

	private static final	Color COULEUR_TEXTE_ACTIF = Color.black;
	private static final	Color COULEUR_TEXTE_INACTIF = Color.black;


	private static JTextField myTextField;

	/**
	 * Methode qui <b>tente</b> de contourner le bug EOF qui se produit lors
	 * d'un saveChanges avec l'erreur "reentered responseToMessage()".<br>
	 * <b>Il faut appeler cette methode avant de creer un descendant d'EOCustomObject, donc bien avant le saveChanges()</b><br>
	 *
	 * Le principe est d'appeler la methode EOClassDescription.classDescriptionForEntityName("A") pour chaque relation de l'objet qu'on va creer.
	 * Il faut appeler cette methode avant de creer un objet, au lancement de l'application par exemple, sur toutes les entites du modele.
	 * Par exemple dans le cas d'un objet Facture qui a des objets Ligne, appeler EOClassDescription.classDescriptionForEntityName("Facture")
	 * avant de creer un objet Ligne.
	 * Repeter l'operation pour toutes les relations de l'objet.
	 *
	 * @param list Liste de String identifiant une entite du modele.
	 * @see http://www.omnigroup.com/mailman/archive/webobjects-dev/2002-May/023698.html
	 */
	public static final void fixWoBug_responseToMessage(final String[] list) {
		for (int i = 0; i < list.length; i++) {
			EOClassDescription.classDescriptionForEntityName(list[i]);
		}
	} 
	
	
	public static void invalidateObjects(EOEditingContext ec, NSArray objects) {
		
		for (Enumeration<EOEnterpriseObject> e = objects.objectEnumerator();e.hasMoreElements();) {
			ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(e.nextElement())));	
		}
				
	}
	
	public static String getErrorDialog(Exception e) {

		e.printStackTrace();

		String text = e.getMessage();

		if ((text == null) || (text.trim().length() == 0)) {

			e.printStackTrace();

			text = "Une erreur est survenue. Impossible de récupérer le message, il doit etre accessible dans la console..." +
			"\n\n"+e.getMessage();
		}
		else {
			String[] msgs = text.split("ORA-20001:");
			if (msgs.length > 1) {
				text = msgs[1].split("\n")[0];
			}
		}
		return text;
	}


	/** Affecte du texte a un JTextField. Aligne le texte a gauche et ajoute un tooltip si tout le texte est trop long pour etre completement affiche . */
	public static void setTextTextField(JTextField textfield, String texte) 
	{
		textfield.setText(texte);
		textfield.moveCaretPosition(0);
		BoundedRangeModel tmpBoundedRangeModel = textfield.getHorizontalVisibility();       
		if (tmpBoundedRangeModel.getMaximum()>textfield.getWidth() ) {
			textfield.setToolTipText(texte);
		}
		else {
			textfield.setToolTipText(null);
		}
	} 

    

	/** Initialisation d'un champ de saisie. Permet d'initialiser le texte, la couleur du texte, la couleur du fond , et de le rendre editable ou non*/
	public static void initTextField(JTextField leChamp, boolean clean, boolean actif)
	{
		if (actif)
		{
			leChamp.setForeground(COULEUR_TEXTE_ACTIF);
			leChamp.setBackground(COULEUR_FOND_ACTIF);
		}
		else
		{
			leChamp.setForeground(COULEUR_TEXTE_INACTIF);
			leChamp.setBackground(COULEUR_FOND_INACTIF);
		}

		if (clean)
			leChamp.setText(null);

		leChamp.setEditable(actif);
	}

	/**
	 * 
	 * @param eo
	 * @param key
	 * @return
	 */
	public static BigDecimal computeSumForKey(EODisplayGroup eo, String key) {

		BigDecimal total = new BigDecimal(0);

		int i = 0;
		while (i < eo.displayedObjects().count()) {
			try {
				if (NSDictionary.class.getName().equals(eo.allObjects().objectAtIndex(i).getClass().getName())
						|| NSMutableDictionary.class.getName().equals(eo.displayedObjects().objectAtIndex(i).getClass().getName())
				)	{
					NSDictionary dico = (NSDictionary) eo.displayedObjects().objectAtIndex(i);
					total = total.add((BigDecimal)dico.objectForKey(key));
				}
				else	{
					total = total.add(new BigDecimal(((EOEnterpriseObject) eo.displayedObjects().objectAtIndex(i)).valueForKey(key).toString()));
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				total = new BigDecimal(0);
				break;
			}
			i = i + 1;
		}

		return total.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * 
	 * @param eo
	 * @param key
	 * @return
	 */
	public static BigDecimal computeSumForKey(NSArray myArray, String key) {

		BigDecimal total = new BigDecimal(0);

		int i = 0;
		while (i < myArray.count()) {
			try {
				if (NSDictionary.class.getName().equals(myArray.objectAtIndex(i).getClass().getName())
						|| NSMutableDictionary.class.getName().equals(myArray.objectAtIndex(i).getClass().getName())
				)	{
					NSDictionary dico = (NSDictionary) myArray.objectAtIndex(i);
					total = total.add((new BigDecimal(dico.objectForKey(key).toString())));
				}
				else	{
					total = total.add((BigDecimal) ((EOEnterpriseObject) myArray.objectAtIndex(i)).valueForKey(key));
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				total = new BigDecimal(0);
				break;
			}
			i = i + 1;
		}

		return total.setScale(2, BigDecimal.ROUND_HALF_UP);
		
	}

	
	
	public static BigDecimal prixTTC(BigDecimal prixHT, BigDecimal taux) {
		
		BigDecimal ttc = prixHT.add( (prixHT.multiply(taux)).divide(new BigDecimal(100))  );
		
		return ttc.setScale(2);
		
	}
	
	

	public static String 	returnTempStringName(){

		java.util.Calendar currentTime = java.util.Calendar.getInstance();

		String result = "-"+currentTime.get(java.util.Calendar.DAY_OF_MONTH)+"."+
		currentTime.get(java.util.Calendar.MONTH)+"."+currentTime.get(java.util.Calendar.YEAR)+"-" + 
		currentTime.get(java.util.Calendar.HOUR_OF_DAY)+"h"+
		currentTime.get(java.util.Calendar.MINUTE)+"m"+currentTime.get(java.util.Calendar.SECOND);

		return result;
		
	}


	public static String nsdataToCsvString (NSData data){

		String tmp = new String(data.bytes());
		tmp = "\""+tmp;
		tmp= StringCtrl.replace(tmp, "##","\"\n\"");
		return 	StringCtrl.replace(tmp, "||","\";\"");
	}

	/** 
	 *
	 */
	public static void openFile(String filePath)	{

		File	aFile = new File(filePath);

		String osName = System.getProperties().getProperty("os.name");

		if(osName.equals(MAC_OS_X_OS_NAME)) 	{
			try { 
				System.out.println("ApplicationClient.openFile() EXEC MAC : " + MAC_OS_X_OPEN+aFile);
				Runtime.getRuntime().exec(MAC_OS_X_OPEN+aFile);
			}
			catch(Exception exception) {
				EODialogs.runErrorDialog("ERREUR", "Impossible de lancer l'application de visualisation du fichier.\nVous pouvez ouvrir manuellement le fichier : "+aFile.getPath()+"\nMESSAGE : " + exception.getMessage());				exception.printStackTrace();
			}
		}
		else	{
			try {
				Runtime runTime = Runtime.getRuntime();//.exec(WINDOWS_EXEC+filePath);
				runTime.exec(new String[] { "rundll32", "url.dll,FileProtocolHandler", "\"" + aFile + "\""} ); 
			}
			catch(Exception exception) {
				EODialogs.runErrorDialog("ERREUR", "Impossible de lancer l'application de visualisation du fichier.\nVous pouvez ouvrir manuellement le fichier : "+aFile.getPath()+"\nMESSAGE : " + exception.getMessage());				exception.printStackTrace();
			}
		}
	}
	
	public static void setMyTextField(JTextField textField)	{
		myTextField = textField;
	}
	public static JTextField getMyTextField()	{
		return myTextField;
	}
	
	public static void showDatePickerPanel(Dialog parentWindow) {
		Date ladate=null;
		
		final JDialog datePickerDialog = new JDialog(parentWindow);
		
		//        ((DateFormat)getFormat()).setTimeZone(myTimeZone);
		if ((getMyTextField().getText()!=null) && ((getMyTextField().getText().length()>0)) ) {
			//On travaille en GMT
			//			((DateFormat)getFormat()).setTimeZone(myTimeZone);
			//			SimpleDateFormat gmtDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			//			gmtDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
			try {
				//				ladate = gmtDateFormat.parse(getMyTexfield().getText());
				ladate = ((DateFormat)new SimpleDateFormat("dd/MM/yyyy")).parse(getMyTextField().getText());
			} catch (ParseException e) {
				e.printStackTrace();
				return;
			}
		}
		
		ZDatePickerPanel datePickerPanel = new ZDatePickerPanel();
		
		ComponentAdapter myComponentAdapter = new ComponentAdapter() {
			public void componentHidden(final ComponentEvent evt) {
				final Date dt = ((ZDatePickerPanel) evt.getSource()).getDate();
				if (null != dt) {
					getMyTextField().setText( new SimpleDateFormat("dd/MM/yyyy").format(dt) );
				}
				datePickerDialog.dispose();
			}
		};
		datePickerPanel.addComponentListener(myComponentAdapter);
		
		datePickerPanel.open(ladate);
		final Point p = getMyTextField().getLocationOnScreen();
		p.setLocation(p.getX(), p.getY() - 1 + myTextField.getSize().getHeight());
		
		//Ajouter la gestion de la touche echap pour fermer la fenetre		
		KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);    
		Action escapeAction = new AbstractAction(){
			public void actionPerformed(ActionEvent e){
				datePickerDialog.hide(); 
			}
		};      
		datePickerDialog.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escape, "ESCAPE");    
		datePickerDialog.getRootPane().getActionMap().put("ESCAPE", escapeAction);	
		datePickerDialog.getRootPane().setFocusable(true);
		
		
		datePickerDialog.setResizable(false);
		datePickerDialog.setUndecorated(true);
		datePickerDialog.getContentPane().add(datePickerPanel);
		datePickerDialog.setLocation(p);
		datePickerDialog.pack();
		datePickerDialog.show();    		
	}

	
	public static void openURL (String URL)	{
		try {
			Runtime runTime = Runtime.getRuntime();//.exec(WINDOWS_EXEC+filePath);
			runTime.exec(new String[] { "rundll32", "url.dll,FileProtocolHandler", URL} ); 
		}
		catch(Exception exception) {
			EODialogs.runErrorDialog("ERREUR", "Impossible de lancer l'application de visualisation du fichier.\nVous pouvez ouvrir manuellement le fichier : "+URL+"\nMESSAGE : " + exception.getMessage());				exception.printStackTrace();
		}	
	}

}
