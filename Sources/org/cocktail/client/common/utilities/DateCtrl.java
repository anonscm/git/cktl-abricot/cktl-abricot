/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.client.common.utilities;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

public class DateCtrl {
	
    /**
     * Retourne la date/time representant le moment en cours dans le fuseau
     * horaire par defaut.
     * 
     * @see NSTimeZone#defaultTimeZone()
     */
    public static NSTimestamp now() {
        // return toLocalDate(new NSTimestamp());
        return new NSTimestamp();
    }
    
	// DATE COMPLETION : un string en entree est transforme en date; Si retour "" -> ERREUR , sinon renvoi de la date formattee avec des /
	public static String dateCompletion(String dateString)
	{
		GregorianCalendar calendar = new GregorianCalendar();
		NSTimestamp dateJour = new NSTimestamp();
		calendar.setTime(dateJour);
		
		int i;
		String chiffres = "", resultat = "";
		String jour="",mois="",annee="";		
		
		for (i=0;i<dateString.length();i++)
		{
			if (NumberCtrl.estUnChiffre("" + dateString.charAt(i)))
				chiffres = chiffres + dateString.charAt(i);
		}
		
		// chiffres : String contenant tous les chiffres de la chaine entree
		// On regarde maintenant la taille de cette chaine
		// Si 1 ou 2 : on complete par le mois et l'annee en cours; Si 3 : on regarde si le mois ou le jour sur 1 caractere;
		
		// Cas de 1 chiffre : 1 journee, completee par le mois en cours et l'annee en cours
		if ( (chiffres.length() == 1) )
		{
			jour = chiffres;
			mois = String.valueOf(calendar.get(Calendar.MONTH) + 1);
			annee = String.valueOf(calendar.get(Calendar.YEAR));
		}
		
		// Cas de 2 chiffres : 1 journee, completee par le mois en cours et l'annee en cours
		if ( (chiffres.length() == 2) )
		{
			jour = chiffres;
			//System.out.println("MOIS  : " + mois);
			mois = String.valueOf(calendar.get(Calendar.MONTH)  + 1);
			annee = String.valueOf(calendar.get(Calendar.YEAR));
			resultat = formatteNo(jour) + "/" +formatteNo(mois) + "/" + formatterAnnee(annee); 
			
			if (!isValid(resultat))		// On teste si la date est valide avec le mois sur 1 caractere
			{
				jour = chiffres.substring(0,1);
				mois = chiffres.substring(1).substring(0,1);
			}
		}
		
		// Cas de 3 chiffres : 1 jour et 1 mois , reste a determiner
		if ( (chiffres.length() == 3) )
		{
			// L'annee est l'annee en cours
			annee = String.valueOf(calendar.get(Calendar.YEAR));
			
			mois = chiffres.substring(2).substring(0,1);	// mois sur 1 caractere
			jour = chiffres.substring(0,2);
			resultat = formatteNo (jour) + "/" + formatteNo (mois) + "/" + formatterAnnee(annee); 
			
			if (!isValid(resultat))		// On teste si la date est valide avec le mois sur 1 caractere
			{
				jour = chiffres.substring(0,1);
				mois = chiffres.substring(1).substring(0,2);
			}
		}
		
		// Cas de 4 chiffres : 1 jour et 1 mois, ou 1 jour, 1 mois et une annee
		if ( (chiffres.length() == 4) )
		{
			jour = chiffres.substring(0,2);
			mois = chiffres.substring(2).substring(0,2);
			annee = String.valueOf(calendar.get(Calendar.YEAR));
			
			resultat = formatteNo (jour) + "/" + formatteNo (mois) + "/" + formatterAnnee(annee); 
			
			if ( !isValid (resultat))
			{
				jour = chiffres.substring(0,1);
				mois = chiffres.substring(1).substring(0,1);
				annee = chiffres.substring(2).substring(0,2);			
			}
		}
		
		// Cas de 5 chiffres : 1 annee sur 2 caracteres
		if ( (chiffres.length() == 5) )
		{
			// On regarde si les 4 derniers chiffres correspondent a une annee
			annee = chiffres.substring(3).substring(0,2);
			
			// Reste 3 caracteres pour le mois et le jour
			mois = chiffres.substring(2).substring(0,1);	// mois sur 1 caractere
			jour = chiffres.substring(0,2);
			resultat = formatteNo (jour) + "/" + formatteNo (mois) + "/" + formatterAnnee(annee); 
			
			if (!isValid(resultat))		// On teste si la date est valide avec le mois sur 1 caractere
			{
				jour = chiffres.substring(0,1);
				mois = chiffres.substring(1).substring(0,2);
			}
		}
		
		// Cas de 6 chiffres : 2 pour le jour, le mois et 2 pour l'annee, ou 1 pour le jour, 1 pour le mois et 4 pour l'annee
		if ( (chiffres.length() == 6) )
		{
			// On regarde si les 4 derniers chiffres correspondent a une annee
			String localAnnee = chiffres.substring(2).substring(0,4);
			
			if (((Number)new Integer(localAnnee)).intValue() > 1900)	// L'annee est sur 4 chiffres
			{
				jour = chiffres.substring(0,1);
				mois = chiffres.substring(1).substring(0,1);
				annee = chiffres.substring(2).substring(0,4);
			}
			else		// L'annee est sur 2 chiffres
			{
				jour = chiffres.substring(0,2);
				mois = chiffres.substring(2).substring(0,2);
				annee = chiffres.substring(4).substring(0,2);
			}
		}
		
		// Cas de 7 chiffres : 4 chiffres pour l'annee - A choisir 2 pour le jour ou 2 pour le mois selon la saisie ...
		if ( (chiffres.length() == 7) )
		{
			annee = chiffres.substring(3).substring(0,4);
			
			// On teste si le mois est valide sur 1 ou 2 caracteres
			mois = chiffres.substring(1).substring(0,2);	// mois sur 2 caracteres
			if (((Number)new Integer(mois)).intValue() <= 12)	// mois valide
				jour = chiffres.substring(0,1);
			else
			{
				jour = chiffres.substring(0,2);
				mois = chiffres.substring(2).substring(0,1);
			}
		}
		
		// Cas de 8 chiffres : toute la date est donnee, on verifie si le mois, le jour et l'annee sont correts
		if ( (chiffres.length() == 8) )	{
			jour = chiffres.substring(0,2);
			mois = chiffres.substring(2).substring(0,2);
			annee = chiffres.substring(4).substring(0,4);;
		}
		
		resultat =formatteNo(jour) + "/" + formatteNo (mois) + "/" + formatterAnnee(annee); 
		
		if ( !isValid (resultat) )
			return "";
		
		return resultat;
	}
	
	/**
	 * 
	 * @param annee
	 * @return
	 */
	public static String formatterAnnee(String annee)	{
		if (annee.length() == 2)
		{
			if (((Number)new Integer(annee)).intValue() < 10)
				return "20" + annee;
			else
				return "19" + annee;
		}
		
		return annee;
	}
	
	
	/**
	 * Teste la validite de la date
	 * pour verifier "bon format de date"
	 * la date en string est convertie en date
	 * puis la date est convertie en string
	 * et les 2 strings sont comparees
	 * si elles sont egales, la date est valide
	 */
	public static boolean isValid(String dateString ) {

		try {
			
			NSTimestamp aDate = new NSTimestamp();
			String newDateString = new String();
			
			aDate = stringToDate ( dateString );
			newDateString = (String)dateToString ( aDate );
			if ( ! newDateString.equals(dateString) ) {
				return false;
			}
			return true;
			
		}
		catch (Exception e)	{
			return false;
		}
		
	}
	
	/**
	 * Teste si deux dates donnees correspondent au meme jour.
	 */
	public static boolean isSameDay(NSTimestamp date1, NSTimestamp date2) {
		return dateToString(date1).equals(dateToString(date2));
	}
	
	/**
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean isBeforeEq(NSTimestamp date1, NSTimestamp date2) {
		return (date1.getTime() <= date2.getTime());
	}
	
	/**
	 * Teste si la date <i>date1</i> precede strictement la date <i>date2</i>.
	 */
	public static boolean isBefore(NSTimestamp date1, NSTimestamp date2) {
		// WO4.5.x != WO5.x 
		return (date1.getTime() < date2.getTime());
	}
	
	/**
	 * Teste si la date <i>date1</i> succede ou est egale a la date
	 * la <i>date2</i>.
	 */
	public static boolean isAfterEq(NSTimestamp date1, NSTimestamp date2) {
		return isBeforeEq(date2, date1);
	}
	
	/**
	 * Teste si la date <i>date1</i> succede strictement la date <i>date2</i>.
	 */
	public static boolean isAfter(NSTimestamp date1, NSTimestamp date2) {
		return isBefore(date2, date1);
	}
	
	/*public NSTimestamp formatteDate(NSTimestampe myDate, String dateFormat)
	 {
	 NSTimestampFormatter formatter = new NSTimestampFormatter(dateFormat);
	 
	 }*/
	
	/**
	 * 
	 */
	public static String dateToString(NSTimestamp gregorianDate, String dateFormat) {
		String dateString = "";
		
		NSTimestampFormatter formatter = new NSTimestampFormatter(dateFormat);
		try {
			dateString = formatter.format(gregorianDate);
		} catch(Exception ex) { }
		return dateString;
	}
	
	public static String dateToString(NSTimestamp gregorianDate) {
		return dateToString(gregorianDate, "%d/%m/%Y");
	}
	
	/*
	 * Convertit la string en date
	 */
	public static NSTimestamp stringToDate(String dateString, String dateFormat) {
		NSTimestamp date = null;
		NSTimestampFormatter formatter;
		if ((dateFormat == null) || (dateFormat.trim().length() == 0))
			return null;
		try {
			formatter = new NSTimestampFormatter(dateFormat);
			date = (NSTimestamp)formatter.parseObject(dateString);
			if (!dateString.equals(dateToString(date, dateFormat)))
				return null;
		} catch(Exception ex) { }
		
		//return date;
		return date.timestampByAddingGregorianUnits(0,0,0,13,0,0);
	}
	
	public static NSTimestamp stringToDate (String dateString ) {
		return stringToDate(dateString, "%d/%m/%Y");
	}
	
	/**
	 * Convertie le numero du jour de la semaine de la representation
	 * anglaise vers celle francaise. 
	 */
	public static int getDayOfWeek(int dayOfWeek) {
		return (dayOfWeek == 0)?(dayOfWeek+6):(dayOfWeek-1);
	}
	
	/**
	 * Retourne le numero de jour de la semaine correspondant 
	 * a la date indiquee. Lundi est le premier jour de la semaine. 
	 */
	public static int getDayOfWeek(NSTimestamp date) {
		// WO4.5.x != WO5.x 
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		return calendar.get(GregorianCalendar.DAY_OF_WEEK);
	}
	
	
	/*public static String getCurrentDateTime() {
	 Calendar cal = new GregorianCalendar();
	 StringBuffer sb = new StringBuffer("");
	 sb.append(StringCtrl.get0Int(cal.get(Calendar.DAY_OF_MONTH), 2)).append("/");
	 sb.append(StringCtrl.get0Int(cal.get(Calendar.MONTH), 2)).append("/");
	 sb.append(cal.get(Calendar.YEAR)).append(" ");
	 sb.append(StringCtrl.get0Int(cal.get(Calendar.HOUR_OF_DAY), 2)).append(":");
	 sb.append(StringCtrl.get0Int(cal.get(Calendar.MINUTE), 2)).append(":");
	 sb.append(StringCtrl.get0Int(cal.get(Calendar.SECOND), 2));
	 return sb.toString();
	 }*/
	
	/**
	 * 
	 */
	public static boolean isJourFerie(NSTimestamp leJour) {	  
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(leJour);
		
		if (joursFeries(calendar.get(Calendar.YEAR)).containsObject(leJour))
			return true;
		
		return false;
	}
	
	// JOURS FERIES : Array de tous les jours feries de l'annee en cours
	public static NSArray joursFeries(int annee)
	{
		int jour, mois, nCycleLunaire, nBissextile, nLettDimanche, nC1, nC2, nC3;
		NSTimestamp    leJourTemp;
		NSMutableArray resultat = new NSMutableArray();
		
		resultat.addObject(DateCtrl.stringToDate("01/01/"+annee,"%d/%m/%Y"));
		resultat.addObject(DateCtrl.stringToDate("01/05/"+annee,"%d/%m/%Y"));
		resultat.addObject(DateCtrl.stringToDate("08/05/"+annee,"%d/%m/%Y"));
		resultat.addObject(DateCtrl.stringToDate("14/07/"+annee,"%d/%m/%Y"));
		resultat.addObject(DateCtrl.stringToDate("15/08/"+annee,"%d/%m/%Y"));
		resultat.addObject(DateCtrl.stringToDate("01/11/"+annee,"%d/%m/%Y"));
		resultat.addObject(DateCtrl.stringToDate("11/11/"+annee,"%d/%m/%Y"));
		resultat.addObject(DateCtrl.stringToDate("25/12/"+annee,"%d/%m/%Y"));
		
		if ((annee==1954)||(annee==2049))    { jour=18; mois=4; }
		else
		{
			if ((annee==1981)||(annee==2076))    { jour=19; mois=4; }
			else
			{
				nCycleLunaire=annee%19;
				nBissextile=annee%4;
				nLettDimanche=annee%7;
				
				nC1=((nCycleLunaire*19)+24)%30;
				nC2=(nBissextile*2 + nLettDimanche*4 + nC1*6 + 5)%7;
				nC3=nC1+nC2;
				
				if (nC3<=9)    { jour=22+nC3; mois=3; } else { jour=nC3-9; mois=4; }
			}
		}
		
		leJourTemp=new NSTimestamp(annee, mois, jour, 0, 0, 0,NSTimeZone.defaultTimeZone());
		
		// Lundi de Paques
		leJourTemp=leJourTemp.timestampByAddingGregorianUnits(0,0,1,0,0,0);
		resultat.addObject(leJourTemp); 
		
		// Jeudi de l'ascension : 38 jours apres le lundi de paques
		leJourTemp=leJourTemp. timestampByAddingGregorianUnits (0,0,38,0,0,0);
		resultat.addObject(leJourTemp); 
		
		// Lundi de Pentecote : 11 jours apres l'ascension
		leJourTemp=leJourTemp. timestampByAddingGregorianUnits (0,0,11,0,0,0);
		resultat.addObject(leJourTemp); 
		
		return (NSArray) resultat;
	}
	
	/** GET MOIS : renvoie le numero du mois formatte sur 2 caracteres */
	public static String formatteNoMois(int noMois)
	{
		if (noMois < 10)
			return "0" + noMois;
		
		return "" + noMois;
	}
	
	/**
	 * 
	 * @param date
	 * @return
	 */
	public static int getYear(NSTimestamp date)	{
		
		java.util.Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return (cal.get(GregorianCalendar.YEAR));
	}
	
	/**
	 * 
	 * @param date
	 * @return
	 */
	public static int getDAyOfMonth(NSTimestamp date)	{
		
		java.util.Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return (cal.get(GregorianCalendar.DAY_OF_MONTH));
	}
	
	/**
	 * 
	 * @param date
	 * @return
	 */
	public static int getMonth(NSTimestamp date)	{
		
		java.util.Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return (cal.get(GregorianCalendar.MONTH));
	}
	
	/** GET MOIS : renvoie le numero formatte sur 2 caracteres */
	public static String formatteNo(String numero)
	{
		if (numero.length() == 1)
			return "0" + numero;
		
		return "" + numero;
	}
	
	/** Renvoie l'age texte de l'enfant selectionne selon la date de naissance */
	public static String getAge(NSTimestamp dNaissance)	{
		
		DateCtrl.IntRef    gregorianYears=new DateCtrl.IntRef();
		DateCtrl.IntRef    gregorianMonths=new DateCtrl.IntRef();
		DateCtrl.IntRef    gregorianDays=new DateCtrl.IntRef();
		float nbAnnees = (float)0.0, nbMois = (float)0.0, nbJours = (float)0.0;
		
		//		dateJour.gregorianUnitsSinceTimestamp ( gregorianYears, gregorianMonths, gregorianDays,null, null, null, dNaissance);
		joursMoisAnneesEntre(dNaissance, new NSTimestamp(), gregorianYears, gregorianMonths, gregorianDays);
		
		nbAnnees =gregorianYears.value;
		nbMois =gregorianMonths.value;
		nbJours =gregorianDays.value;
		
		return "" + nbAnnees + " ans " + nbMois + " mois " + nbJours + " jours";
	}
	
	/**
	 * 
	 * @return
	 */
	public static String heureActuelle()	{
		
		String dateString ="";
		
		NSTimestampFormatter formatter = new NSTimestampFormatter("%d/%m/%Y %F");
		try {
			dateString = formatter.format(new NSTimestamp());
		} catch(Exception ex) { }
		return dateString;
		
	}
	
	public static String ecartMinutes(NSTimestamp date1, NSTimestamp date2)	{
		
		GregorianCalendar calDebut = new GregorianCalendar();
		GregorianCalendar calFin = new GregorianCalendar();
		
		calDebut.setTime(date1);
		calFin.setTime(date2);
		
		long secDebut = calDebut.getTimeInMillis();
		long secFin = calFin.getTimeInMillis();
		
		long ecart = secFin - secDebut;
		
		long secondes = ecart / (1000);
		long milliSecondes = ecart % (1000);
		
		return String.valueOf(secondes) + " secondes " + String.valueOf(milliSecondes) + " milliemes";
	}
	
	/** Calcule la dur&eacute;e &eacute;coul&eacute;e entre deux dates
	 * @param dateDebut 
	 * @param dateFin
	 * @param anneeRef	peut &ecirc;tre nul => calcul dans les autres unit&eacute;s
	 * @param moisRef	peut &ecirc;tre nul => calcul dans les autres unit&eacute;s
	 * @param jourRef	peut &ecirc;tre nul => calcul dans les autres unit&eacute;s
	 */
	public static void joursMoisAnneesEntre(NSTimestamp dateDebut,NSTimestamp dateFin,IntRef anneeRef,IntRef moisRef,IntRef jourRef) {
//		GregorianCalendar date1 = new GregorianCalendar(TimeZone.getTimeZone("Europe/Paris" )),date2 = new GregorianCalendar(TimeZone.getTimeZone("Europe/Paris" ));
		GregorianCalendar date1 = new GregorianCalendar(),date2 = new GregorianCalendar();
		date1.setTime(dateDebut);
		date2.setTime(dateFin);
		int annee1 = date1.get(Calendar.YEAR), annee2 = date2.get(Calendar.YEAR);
		int mois1 = date1.get(Calendar.MONTH), mois2 = date2.get(Calendar.MONTH);
		int jour1 = date1.get(Calendar.DAY_OF_MONTH), jour2 = date2.get(Calendar.DAY_OF_MONTH);
				
		if (anneeRef == null && moisRef == null) {
			if (jourRef != null) {
				// on veut calculer un nombre de jours
				jourRef.value = nbJoursEntre(dateDebut,dateFin,true);
			}
		} else {
			int differenceAnnee = annee2 - annee1;
			int differenceMois, differenceJours;
			if (mois1 <= mois2) {
				differenceMois = mois2 - mois1;
			} else {
				differenceAnnee = differenceAnnee - 1;
				differenceMois = 12 + (mois2 - mois1);
			}
			if (jour1 <= jour2) {
				differenceJours = jour2 - jour1 + 1;	// on inclut les bornes
			} else {
				differenceMois = differenceMois - 1;
				NSTimestamp nouvelleDateDebut = new NSTimestamp(annee2,mois2,jour1,0,0,0,NSTimeZone.defaultTimeZone()); // on prend le mois pr?c?dent mais = mois2 car java num?rote les mois a partir de zero
				differenceJours = nbJoursEntre(nouvelleDateDebut,dateFin,true);
			}
			if (differenceAnnee < 0) {
				anneeRef.value = 0;
			}
			if (differenceMois < 0) {
				differenceMois = 0;
			}
			// pr?parer les r?sultats selon la demande
			if (anneeRef == null) {
				// on veut un calcul en mois
				differenceMois += differenceAnnee * 12;
			} else {
				anneeRef.value = differenceAnnee;
			}
			if (moisRef == null) {
				if (jourRef != null) {
					// on veut une difference en annees et jours (le cas anneeRef == null && moisRef == null est deja traite)
					// on se cale n annees plus tard pour calculer le nombre de jours
					NSTimestamp nouvelleDateDebut = new NSTimestamp(annee1 + differenceAnnee,mois1+1,jour1,0,0,0,NSTimeZone.defaultTimeZone()); // les mois commencent a zero en java
					differenceJours = nbJoursEntre(nouvelleDateDebut,dateFin,true);
					jourRef.value = differenceJours;
				}
			} else {
				moisRef.value = differenceMois;
				if (jourRef != null) {
					jourRef.value = differenceJours;
				}
			}
		}
	}
	
	public static class IntRef extends Object {
		public int value;
	}
	
	/** retourne le nombre de jours &eacute;coul&eacute;s entre deux dates 
	 * le r&acu;tesulat est n&eacute;gatif si la deuxi&egrave;me date est ant&eacute;rieure */
	/** retourne le nombre de jours &eacute;coul&eacute;s entre deux dates 
	 * le r&acu;tesulat est n&eacute;gatif si la deuxi&egrave;me date est ant&eacute;rieure */
	public static int nbJoursEntre(NSTimestamp timestamp1,NSTimestamp timestamp2,boolean inclureBornes) {
		int nbJours = 0;
		boolean estPositif = true;
		GregorianCalendar date1 = new GregorianCalendar(), date2 = new GregorianCalendar();
				
		if (isBeforeEq(timestamp1,timestamp2)) {
			date2.setTime(timestamp2);
			date1.setTime(timestamp1);
		}
		else {
			date2.setTime(timestamp1);
			date1.setTime(timestamp2);
			estPositif = false;
		}
		
		date1.clear(Calendar.MILLISECOND);
		date1.clear(Calendar.SECOND);
		date1.clear(Calendar.MINUTE);
		date1.clear(Calendar.HOUR_OF_DAY);
		
		date2.clear(Calendar.MILLISECOND);
		date2.clear(Calendar.SECOND);
		date2.clear(Calendar.MINUTE);
		date2.clear(Calendar.HOUR_OF_DAY);
		
		while (date1.get(Calendar.YEAR) != date2.get(Calendar.YEAR) ||
				date1.get(Calendar.MONTH) != date2.get(Calendar.MONTH) ||
				date1.get(Calendar.DAY_OF_MONTH) < date2.get(Calendar.DAY_OF_MONTH)) {
			date1.add(Calendar.DATE, 1);
			nbJours++;
		}
		if (inclureBornes) {
			nbJours++;
		}
		if (estPositif) {
			return nbJours;
		} else {
			return -nbJours;
		}
	}
	public static NSTimestamp jourSuivant(NSTimestamp aDate) {
		if (aDate != null) {
			return aDate.timestampByAddingGregorianUnits(0,0,1,0,0,0);
		} else {
			return null;
		}
	}
	public static NSTimestamp jourPrecedent(NSTimestamp aDate) {
		if (aDate != null) {
			return aDate.timestampByAddingGregorianUnits(0,0,-1,0,0,0);
		} else {
			return null;
		}
	}
}