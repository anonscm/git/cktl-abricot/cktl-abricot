/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.client.common.utilities;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.cocktail.abricot.client.ApplicationClient;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;

/**
Classe destinee a afficher de facon simple des messages d'attente pour les utilisateurs.
Merci a Titou pour avoir trouve la bonne solution pour l'affichage.
 */
public class CocktailLogsClientServer extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 89836698816664198L;
	private	ApplicationClient	myApp;
	private	JComponent 	myContentPane;

	private	JTextArea	logs;
	private 	JLabel		labelTailleLogs;
	private	JButton		btnMail,btnClean, btnRafraichir, btnFermer;

	private	ButtonGroup		types;
	private	JRadioButton	typeClient, typeServeur;

	String title;   

	/**
	 * 
	 * @param aTitle
	 * @param clientErrLog
	 * @param clientOutLog
	 * @param serverErrLog
	 * @param serverOutLog
	 */
	public CocktailLogsClientServer(String aTitle) {

		super(aTitle);
		//		super(((ApplicationClient)EOApplication.sharedApplication()).superviseur().mainWindow, aTitle, false);

		myApp = (ApplicationClient)ApplicationClient.sharedApplication();

		title = aTitle;

		myContentPane = createUI();
		myContentPane.setOpaque(true);

		this.setContentPane(myContentPane);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		rafraichir();

		this.pack();
		centerWindow();
		this.setVisible(true);
	}

	/**
	 * 
	 * @return
	 */
	private JPanel createUI() {

		JPanel container=new JPanel(new BorderLayout());

		JPanel panelRadio=new JPanel(new GridLayout(1,1,5,5));
		JPanel containerRadio=new JPanel(new BorderLayout());

		JPanel panelMessage=new JPanel(new GridLayout(1,1,5,5));       
		JPanel panelLabel=new JPanel(new GridLayout(1,1,5,5));

		JPanel containerCenter=new JPanel(new BorderLayout());

		JPanel panelBouton=new JPanel(new GridLayout(1,2,5,5));

		//Types de logs : Radio boutons Client et Serveur
		types = new ButtonGroup();
		typeClient = new JRadioButton(new ActionListenerClient());
		typeServeur = new JRadioButton(new ActionListenerServeur());

		types.add(typeClient);
		types.add(typeServeur);
		types.setSelected(typeClient.getModel(), true);

		panelRadio.add(typeClient);
		panelRadio.add(typeServeur);

		containerRadio.add(panelRadio, BorderLayout.WEST);

		// Text Area
		logs = new JTextArea();
		logs.setBackground(new Color(220,220,220));
		panelMessage.add(new JScrollPane(logs));

		// Label pour la taille des logs
		labelTailleLogs = new JLabel("");
		labelTailleLogs.setForeground(new Color(0,0,255));
		panelLabel.add(labelTailleLogs);

		containerCenter.add(panelMessage,BorderLayout.CENTER);
		containerCenter.add(panelLabel,BorderLayout.SOUTH);

		// Boutons
		btnMail = new JButton(new ActionListenerMail());
		btnClean = new JButton(new ActionListenerClean());
		btnRafraichir = new JButton(new ActionListenerRafraichir());
		btnFermer = new JButton(new ActionListenerFermer());

		panelBouton.add(btnRafraichir);
		panelBouton.add(btnClean);
		panelBouton.add(btnMail);
		panelBouton.add(btnFermer);

		// Panel principal
		container.add(containerRadio, BorderLayout.NORTH);
		container.add(containerCenter, BorderLayout.CENTER);
		container.add(panelBouton, BorderLayout.SOUTH);

		container.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		container.setPreferredSize(new Dimension(600,400));

		return container;
	}

	public final void centerWindow() {
		int screenWidth = (int)this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int)this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth/2)-((int)this.getSize().getWidth()/2), ((screenHeight/2)-((int)this.getSize().getHeight()/2)));
	} 
	/**
	 * 
	 */
	public void setTitle(String aTitle) {
		title = aTitle;
		this.setTitle(title);
		this.paintAll(this.getGraphics());
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private final class ActionListenerClient extends AbstractAction	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 8096637817759660619L;

		public ActionListenerClient() {
			super("Messages Client");
			putValue(AbstractAction.SHORT_DESCRIPTION , "Afficher les logs client");
		}

		public void actionPerformed(ActionEvent e)	{
			rafraichir();
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private final class ActionListenerServeur extends AbstractAction	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 2362566961469106276L;

		public ActionListenerServeur() {
			super("Messages Serveur");
			putValue(AbstractAction.SHORT_DESCRIPTION , "Afficher les logs serveur");
		}

		public void actionPerformed(ActionEvent e)	{
			rafraichir();
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	private final class ActionListenerMail extends AbstractAction	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -5986732656801518546L;

		public ActionListenerMail() {
			super("Mail");
			putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_MAIL_32);
			putValue(AbstractAction.SHORT_DESCRIPTION , "Envoyer le log Client ou Serveur");
		}

		public void actionPerformed(ActionEvent e)	{
			mail();
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	public class ActionListenerClean extends AbstractAction	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -8334132009053194054L;
		public ActionListenerClean() {
			super("Nettoyer");
			putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_CORBEILLE_VIDE_32);
			putValue(AbstractAction.SHORT_DESCRIPTION , "Nettoyer");
		}
		public void actionPerformed(ActionEvent e)	{
			clean();
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	public class ActionListenerRafraichir extends AbstractAction	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1767234464390937896L;
		public ActionListenerRafraichir() {
			super("Rafraichir");
			putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_REFRESH_32);
			putValue(AbstractAction.SHORT_DESCRIPTION , "Rafraichir");
		}
		public void actionPerformed(ActionEvent e)	{
			rafraichir();
		}
	}

	/**
	 * 
	 * @author cpinsard
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	public class ActionListenerFermer extends AbstractAction	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 4484484520078804333L;
		public ActionListenerFermer() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, CocktailIcones.ICON_CLOSE);
			putValue(AbstractAction.SHORT_DESCRIPTION , "Fermer");
		}
		public void actionPerformed(ActionEvent e)	{
			setVisible(false);
		}
	}

	/**
	 * 
	 *
	 */
	public void mail()	{
		if (typeClient.getModel() == types.getSelection())
			((ApplicationClient)ApplicationClient.sharedApplication()).sendLog("CLIENT");       
		else
			((ApplicationClient)ApplicationClient.sharedApplication()).sendLog("SERVEUR");       
	}

	/**
	 * 
	 *
	 */
	public void clean()	{
		if (typeClient.getModel() == types.getSelection())
			((ApplicationClient)ApplicationClient.sharedApplication()).cleanLogs("CLIENT");       
		else
			((ApplicationClient)ApplicationClient.sharedApplication()).cleanLogs("SERVER");      

		labelTailleLogs.setText("TAILLE DES LOGS : OUT (0) , ERREUR (0)");

		logs.setText("");
	}

	public void rafraichir()	{
		String message = "";

		if (typeClient.getModel() == types.getSelection())	{
			message = ">>>> OUPUT LOGS :\n";
			message = message.concat(myApp.outLogs());       
			message = message.concat("\n\n>>>> ERROR LOGS :\n");
			message = message.concat(myApp.errLogs());   
			labelTailleLogs.setText("TAILLE DES LOGS : OUT ("+String.valueOf(myApp.outLogs().length()) 
					+ " octets) , ERREUR ("+myApp.errLogs().length()+" octets)");
		}
		else	{
			EOEditingContext ec = myApp.getAppEditingContext();
			String outLog = (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestOutLog", new Class[] {}, new Object[] {}, false);
			String errLog = (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestErrLog", new Class[] {}, new Object[] {}, false);
			message = message.concat(">>>> OUTPUT LOGS :\n");
			message = message.concat(outLog);       
			message = message.concat("\n\n>>>> ERROR LOGS :\n");
			message = message.concat(errLog);          		

			labelTailleLogs.setText("TAILLE DES LOGS : OUT ("+String.valueOf(outLog.length()) 
					+ " octets) , ERREUR ("+errLog.length()+" octets)");
		}

		logs.setText(message);
	}


}
