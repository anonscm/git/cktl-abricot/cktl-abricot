Abricot (gestion du mandatement et du titrage)
==============================================

1.4.8.0
-------
- Ajout du n° de SIRET dans l'édition du titre de recette (nécessite patch sql maracuja 1.9.3.6)


1.4.6.0
-------
Améliorations
Visa des bordereaux de rejet : Message d'erreur lors d'une tentative de visa d'un bordereau de rejet déjà visé
Impression : Suppression des types de bordereaux inutiles dans la fenêtre d'impression lorsque l'utilisateur a un droit d'impression limité par code gestion
Création des bordereaux de titre : remplacement de la colonne N° facture par le n° de facture "Papier"
Edition du Mandat : lorsque la dépense est passée sur un marché, la présentation du n° de marché est désormais identique à celle du bon de commande (année / n° marché / N° lot/ Libellé lot)



1.4.5.6
--------
Améliorations
Le droit d'impression par code gestion permet d'imprimer tous les types de bordereaux indépendamment du droit de création par type.
Création des bordereaux de Prestations internes : les colonnes client et fournisseur affichaient l'utilisateur à la place du client et du fournisseur.
Suppression de la fonctionnalité "Réimputations ordonnateur". Cette fonctionnalité est intégrée dans Carambole.

1.4.5.4
-------

Améliorations
Ajout des actions lolfs dans la liste des depenses en attente.
Corrections
L'impression des ORV de Paye à Facon n'était pas possible
Ajout du pays dans les adresses sur les editions



1.4.5.1
--------
Améliorations
Création des bordereau de paye et PAF : Le filtre mensuel est désormais actif. 	


1.4.5.0
-------
Améliorations
- Dans la liste des recettes en attente, ajout d'une colonne "numero de recette".
- Creation d'un bordereau de titre : remplacement de la question concernant les titres collectifs par un panneau d'options.
- Divers : modifications légères de l'interface (améliorations).
- Génération des titres : les titres sur un bordereau sont désormais numéroté dans le même ordre que les recettes.
- Lors de l'impression d'un bordereau de titres non collectif, l'annexe au bordereau de titre collectif n'est plus impriméee.  
- Lors de la création des bordereaux de titre ou de mandat, a procedure bloque si une des lignes budgétaires est fermée.
- Liste des recettes en attente : ajout de la contrepartie
- Impression du Bulletin de liquidation : Ajout des actions LOLF
- Impression des dépenses en attente : les dépenses sont désormais triées par date de facture
- Impressions : ajout d'un droit (Impressions limitees par code gestion) dans JefyAdmin. Ceci permet de spécifier quels sont les codes gestions (composantes) sur lesquelles l'utilisateur peut imprimer. Ca évite de donner le droit sur l'UB de l'organigramme budgétaire.
- Impressions : Il est désormais possible de choisir quels sont les documents à imprimer

   
Corrections
- Bordereaux de titres non admis : dorenavant la suppresion des recettes est effectuee lors du visa du bordereau de rejet si elles ont été marquees comme "a supprimer" dans Maracuja.
- Impression des ORV : Impression de la date de création de l'ORV (au lieu de la date de prise en charge comptable de l'ORV)
- Bordereaux de titre, liste des recettes en attente : le sous-cr n'était pas affiché.
- Corrections de certaines éditions de recettes reliées à des conventions (la convention n'était pas systématiquement récupérée)
- Lors de la création d'une prestation interne sur un SACD, les contreparties (181) étaient passées sur l'agence comptable au lieu du SACD.
- Numérotation des bordereaux, mandats, titres, AOR, ORV, BTMNA, BTTNA : lorsque la numérotation est paramétrée au niveau établissement, si ces pièces sont émises sur un SACD, la numérotation est effectuée actuellement sur l'établissement. A partir de 2010 elle se fera sur le SACD.

