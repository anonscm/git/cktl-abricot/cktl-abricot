CREATE OR REPLACE PACKAGE MARACUJA."BORDEREAU_ABRICOT" AS
/*
 * Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */-- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic

   /*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
 bordereau_1R1T
 bordereau_NR1T
 bordereau_1D1M
 bordereau_1D1M1R1T
 ndep_mand_org_fou_rib_pco (bordereau_ND1M)
 ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
 ndep_mand_fou_rib_pco (bordereau_ND1M)
 ndep_mand_fou_rib_pco_mod (bordereau_ND1M)

abr_etat='ATTENTE' qd la selection n est pas sur un bordereau
abr_etat='TRAITE' qd la selection est sur le bordereau
*/

   -- version du 02/03/2007
   PROCEDURE creer_bordereau (abrid INTEGER);

   PROCEDURE viser_bordereau_rejet (brjordre INTEGER);

   FUNCTION get_selection_id (info VARCHAR)
      RETURN INTEGER;

   FUNCTION get_selection_borid (abrid INTEGER)
      RETURN INTEGER;

   PROCEDURE set_selection_id (
      a01abrid        INTEGER,
      a02lesdepid     VARCHAR,
      a03lesrecid     VARCHAR,
      a04utlordre     INTEGER,
      a05exeordre     INTEGER,
      a06tboordre     INTEGER,
      a07abrgroupby   VARCHAR,
      a08gescode      VARCHAR
   );

   PROCEDURE set_selection_intern (
      a01abrid           INTEGER,
      a02lesdepid        VARCHAR,
      a03lesrecid        VARCHAR,
      a04utlordre        INTEGER,
      a05exeordre        INTEGER,
      a07abrgroupby      VARCHAR,
      a08gescodemandat   VARCHAR,
      a09gescodetitre    VARCHAR
   );

   PROCEDURE set_selection_paye (
      a01abrid           INTEGER,
      a02lesdepid        VARCHAR,
      a03lesrecid        VARCHAR,
      a04utlordre        INTEGER,
      a05exeordre        INTEGER,
      a07abrgroupby      VARCHAR,
      a08gescodemandat   VARCHAR,
      a09gescodetitre    VARCHAR
   );

-- creer bordereau (tbo_ordre) + numerotation
   FUNCTION get_num_borid (tboordre INTEGER, exeordre INTEGER, gescode VARCHAR, utlordre INTEGER)
      RETURN INTEGER;

-- les algo de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
   PROCEDURE bordereau_1r1t (abrid INTEGER, monborid INTEGER);

   PROCEDURE bordereau_nr1t (abrid INTEGER, monborid INTEGER);

   PROCEDURE bordereau_nd1m (abrid INTEGER, monborid INTEGER);

   PROCEDURE bordereau_1d1m (abrid INTEGER, monborid INTEGER);

   PROCEDURE bordereau_1d1m1r1t (abrid INTEGER, boridep INTEGER, boridrec INTEGER);

-- les mandats et titres
   FUNCTION set_mandat_depense (dpcoid INTEGER, borid INTEGER)
      RETURN INTEGER;

   FUNCTION set_mandat_depenses (lesdpcoid VARCHAR, borid INTEGER)
      RETURN INTEGER;

   FUNCTION set_titre_recette (rpcoid INTEGER, borid INTEGER)
      RETURN INTEGER;

   FUNCTION set_titre_recettes (lesrpcoid VARCHAR, borid INTEGER)
      RETURN INTEGER;

--function ndep_mand_org_fou_rib_pco (abrid integer,borid integer) return integer;
   FUNCTION ndep_mand_org_fou_rib_pco_mod (abrid INTEGER, borid INTEGER)
      RETURN INTEGER;

--function ndep_mand_fou_rib_pco  (abrid integer,borid integer) return integer;
   FUNCTION ndep_mand_fou_rib_pco_mod (abrid INTEGER, borid INTEGER)
      RETURN INTEGER;

-- procedures de verifications des etats
   FUNCTION selection_valide (abrid INTEGER)
      RETURN INTEGER;

   FUNCTION recette_valide (recid INTEGER)
      RETURN INTEGER;

   FUNCTION depense_valide (depid INTEGER)
      RETURN INTEGER;

   FUNCTION verif_bordereau_selection (borid INTEGER, abrid INTEGER)
      RETURN INTEGER;

-- procedures de locks de transaction
   PROCEDURE lock_mandats;

   PROCEDURE lock_titres;

-- procedure de recuperation des donn?e ordonnateur
--PROCEDURE get_depense_jefy_depense (manid INTEGER,utlordre INTEGER);
   PROCEDURE get_depense_jefy_depense (manid INTEGER);

   PROCEDURE get_recette_jefy_recette (titid INTEGER);

   PROCEDURE get_recette_prelevements (titid INTEGER);

-- procedures du brouillard
   PROCEDURE set_mandat_brouillard (manid INTEGER);

   PROCEDURE set_mandat_brouillard_intern (manid INTEGER);

--PROCEDURE maj_plancomptable_mandat (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);
   PROCEDURE set_titre_brouillard (titid INTEGER);

   PROCEDURE set_titre_brouillard_intern (titid INTEGER);

--PROCEDURE maj_plancomptable_titre (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

   -- outils
   FUNCTION inverser_sens_orv (tboordre INTEGER, sens VARCHAR)
      RETURN VARCHAR;

   FUNCTION recup_gescode (abrid INTEGER)
      RETURN VARCHAR;

   FUNCTION recup_utlordre (abrid INTEGER)
      RETURN INTEGER;

   FUNCTION recup_exeordre (abrid INTEGER)
      RETURN INTEGER;

   FUNCTION recup_tboordre (abrid INTEGER)
      RETURN INTEGER;

   FUNCTION recup_groupby (abrid INTEGER)
      RETURN VARCHAR;

   FUNCTION traiter_orgid (orgid INTEGER, exeordre INTEGER)
      RETURN INTEGER;

   FUNCTION inverser_sens (sens VARCHAR)
      RETURN VARCHAR;

-- apres creation des bordereaux
   PROCEDURE numeroter_bordereau (borid INTEGER);

   PROCEDURE controle_bordereau (borid INTEGER);

   PROCEDURE ctrl_date_exercice (borid INTEGER);
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA."BORDEREAU_ABRICOT" AS
   PROCEDURE creer_bordereau (abrid INTEGER) IS
      cpt            INTEGER;
      abrgroupby     abricot_bord_selection.abr_group_by%TYPE;
      monborid_dep   INTEGER;
      monborid_rec   INTEGER;

      CURSOR lesmandats IS
         SELECT man_id
           FROM mandat
          WHERE bor_id = monborid_dep;

      CURSOR lestitres IS
         SELECT tit_id
           FROM titre
          WHERE bor_id = monborid_rec;

      tmpmandid      INTEGER;
      tmptitid       INTEGER;
      tboordre       INTEGER;
   BEGIN
-- est ce une selection vide ???
      SELECT COUNT (*)
        INTO cpt
        FROM abricot_bord_selection
       WHERE abr_id = abrid;

      IF cpt != 0 THEN
/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
 bordereau_1R1T
 bordereau_1D1M
 bordereau_1D1M1R1T
 ndep_mand_org_fou_rib_pco (bordereau_ND1M)
 ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
 ndep_mand_fou_rib_pco (bordereau_ND1M)
 ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

         -- recup du group by pour traiter les cursors
         abrgroupby := recup_groupby (abrid);

         IF (abrgroupby = 'bordereau_1R1T') THEN
            monborid_rec :=
               get_num_borid (recup_tboordre (abrid),
                              recup_exeordre (abrid),
                              recup_gescode (abrid),
                              recup_utlordre (abrid)
                             );
            bordereau_1r1t (abrid, monborid_rec);
         END IF;

         IF (abrgroupby = 'bordereau_NR1T') THEN
            monborid_rec :=
               get_num_borid (recup_tboordre (abrid),
                              recup_exeordre (abrid),
                              recup_gescode (abrid),
                              recup_utlordre (abrid)
                             );
            bordereau_nr1t (abrid, monborid_rec);

-- controle RA
            SELECT COUNT (*)
              INTO cpt
              FROM titre
             WHERE ori_ordre IS NOT NULL AND bor_id = monborid_rec;

            IF cpt != 0 THEN
               raise_application_error
                     (-20001,
                      'Impossiblde traiter une recette sur convention affectee dans un bordereau collectif !'
                     );
            END IF;
         END IF;

         IF (abrgroupby = 'bordereau_1D1M') THEN
            monborid_dep :=
               get_num_borid (recup_tboordre (abrid),
                              recup_exeordre (abrid),
                              recup_gescode (abrid),
                              recup_utlordre (abrid)
                             );
            bordereau_1d1m (abrid, monborid_dep);
         END IF;

/*
IF (abrgroupby = 'bordereau_1D1M1R1T') THEN
 monborid_dep := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 monborid_rec := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 bordereau_1D1M(abrid,monborid_dep);
 bordereau_1R1T(abrid,monborid_rec);


END IF;
*/
         IF (abrgroupby NOT IN ('bordereau_1R1T', 'bordereau_NR1T', 'bordereau_1D1M', 'bordereau_1D1M1R1T')) THEN
            monborid_dep :=
               get_num_borid (recup_tboordre (abrid),
                              recup_exeordre (abrid),
                              recup_gescode (abrid),
                              recup_utlordre (abrid)
                             );
            bordereau_nd1m (abrid, monborid_dep);
         END IF;

         IF (monborid_dep IS NOT NULL) THEN
            bordereau_abricot.numeroter_bordereau (monborid_dep);

            OPEN lesmandats;

            LOOP
               FETCH lesmandats
                INTO tmpmandid;

               EXIT WHEN lesmandats%NOTFOUND;
               get_depense_jefy_depense (tmpmandid);
            END LOOP;

            CLOSE lesmandats;

            controle_bordereau (monborid_dep);
         END IF;

         IF (monborid_rec IS NOT NULL) THEN
            bordereau_abricot.numeroter_bordereau (monborid_rec);

            OPEN lestitres;

            LOOP
               FETCH lestitres
                INTO tmptitid;

               EXIT WHEN lestitres%NOTFOUND;
               -- recup du brouillard
               get_recette_jefy_recette (tmptitid);
               set_titre_brouillard (tmptitid);
               get_recette_prelevements (tmptitid);
            END LOOP;

            CLOSE lestitres;

            controle_bordereau (monborid_rec);
         END IF;

-- maj de l etat dans la selection
         IF (monborid_dep IS NOT NULL OR monborid_rec IS NOT NULL) THEN
            IF monborid_rec IS NOT NULL THEN
               UPDATE abricot_bord_selection
                  SET abr_etat = 'TRAITE',
                      bor_id = monborid_rec
                WHERE abr_id = abrid;
            END IF;

            IF monborid_dep IS NOT NULL THEN
               UPDATE abricot_bord_selection
                  SET abr_etat = 'TRAITE',
                      bor_id = monborid_dep
                WHERE abr_id = abrid;

               SELECT tbo_ordre
                 INTO tboordre
                 FROM bordereau
                WHERE bor_id = monborid_dep;

-- pour les bordereaux de papaye on retravaille le brouillard
               IF tboordre = 3 THEN
                  bordereau_abricot_paye.basculer_bouillard_paye (monborid_dep);
               END IF;

-- pour les bordereaux d'orv de papaye on retravaille le brouillard
               IF tboordre = 18 THEN
                  bordereau_abricot_paye.basculer_bouillard_paye_orv (monborid_dep);
               END IF;

-- pour les bordereaux de regul de papaye on retravaille le brouillard
               IF tboordre = 19 THEN
                  bordereau_abricot_paye.basculer_bouillard_paye_regul (monborid_dep);
               END IF;

-- pour les bordereaux de PAF on retravaille le brouillard
               IF tboordre = 20 THEN
                  bordereau_abricot_paf.basculer_bouillard_paye (monborid_dep);
               END IF;
-- pour les bordereaux d'orv de PAF on retravaille le brouillard
-- if tboordre = 21  then
--  bordereau_abricot_paf.basculer_bouillard_paye_orv(monborid_dep);
--  end if;

            -- pour les bordereaux de recette de PAF on retravaille le brouillard
--  if tboordre = -999  then
--   bordereau_abricot_paf.basculer_bouillard_paye_recettte(monborid_rec);
--  end if;
            END IF;
         END IF;
      END IF;
   END;

   PROCEDURE viser_bordereau_rejet (brjordre INTEGER) IS
      cpt              INTEGER;
      manid            maracuja.mandat.man_id%TYPE;
      titid            maracuja.titre.tit_id%TYPE;
      tboordre         INTEGER;
      reduction        INTEGER;
      utlordre         INTEGER;
      dpcoid           INTEGER;
      recid            INTEGER;
      depsuppression   VARCHAR2 (20);
      rpcoid           INTEGER;
      recsuppression   VARCHAR2 (20);
      exeordre         INTEGER;
      flag             INTEGER;
      borid            bordereau.bor_id%TYPE;

      CURSOR mandats IS
         SELECT man_id
           FROM maracuja.mandat
          WHERE brj_ordre = brjordre;

      CURSOR depenses IS
         SELECT dep_ordre, dep_suppression
           FROM maracuja.depense
          WHERE man_id = manid;

      CURSOR titres IS
         SELECT tit_id
           FROM maracuja.titre
          WHERE brj_ordre = brjordre;

      CURSOR recettes IS
         SELECT rec_ordre, rec_suppression
           FROM maracuja.recette
          WHERE tit_id = titid;

      deliq            INTEGER;
   BEGIN
      OPEN mandats;

      LOOP
         FETCH mandats
          INTO manid;

         EXIT WHEN mandats%NOTFOUND;

         OPEN depenses;

         LOOP
            FETCH depenses
             INTO dpcoid, depsuppression;

            EXIT WHEN depenses%NOTFOUND;
            -- casser le liens des mand_id dans depense_ctrl_planco
              -- supprimer le liens compteble <-> depense dans l inventaire
            jefy_depense.abricot.upd_depense_ctrl_planco (dpcoid, NULL);

            SELECT tbo_ordre, exe_ordre
              INTO tboordre, exeordre
              FROM jefy_depense.depense_ctrl_planco
             WHERE dpco_id = dpcoid;

            -- suppression de la depense demand?e par la personne qui a vis? et pas un bordereau de prestation interne recette 201
            IF depsuppression = 'OUI' AND tboordre != 201 THEN
               --  select max(utl_ordre) into utlordre from jefy_depense.depense_budget jdb,jefy_depense.depense_ctrl_planco jpbp
               --  where jpbp.dep_id = jdb.dep_id
               --  and dpco_id = dpcoid;
               SELECT utl_ordre
                 INTO utlordre
                 FROM jefy_depense.depense_budget
                WHERE dep_id IN (SELECT dep_id
                                   FROM jefy_depense.depense_ctrl_planco
                                  WHERE dpco_id = dpcoid);

               jefy_depense.abricot.del_depense_ctrl_planco (dpcoid, utlordre);
            END IF;
         END LOOP;

         CLOSE depenses;
      END LOOP;

      CLOSE mandats;

      OPEN titres;

      LOOP
         FETCH titres
          INTO titid;

         EXIT WHEN titres%NOTFOUND;

         OPEN recettes;

         LOOP
            FETCH recettes
             INTO rpcoid, recsuppression;

            EXIT WHEN recettes%NOTFOUND;

            -- casser le liens des tit_id dans recette_ctrl_planco
            SELECT r.rec_id_reduction
              INTO reduction
              FROM jefy_recette.recette r, jefy_recette.recette_ctrl_planco rpco
             WHERE rpco.rpco_id = rpcoid AND rpco.rec_id = r.rec_id;

            IF reduction IS NOT NULL THEN
               jefy_recette.api.upd_reduction_ctrl_planco (rpcoid, NULL);
            ELSE
               jefy_recette.api.upd_recette_ctrl_planco (rpcoid, NULL);
            END IF;

            SELECT tbo_ordre, exe_ordre
              INTO tboordre, exeordre
              FROM jefy_recette.recette_ctrl_planco
             WHERE rpco_id = rpcoid;

-- GESTION DES SUPPRESSIONS
-- suppression de la recette demand?e par la personne qui a vis? et pas un bordereau de prestation interne recette 200
            IF recsuppression = 'OUI' AND tboordre != 200 THEN
               SELECT utl_ordre
                 INTO utlordre
                 FROM jefy_recette.recette_budget
                WHERE rec_id IN (SELECT rec_id
                                   FROM jefy_recette.recette_ctrl_planco
                                  WHERE rpco_id = rpcoid);

               SELECT rec_id
                 INTO recid
                 FROM jefy_recette.recette_ctrl_planco
                WHERE rpco_id = rpcoid;

               jefy_recette.api.del_recette (recid, utlordre);
            END IF;
         END LOOP;

         CLOSE recettes;
      END LOOP;

      CLOSE titres;

      -- prise en compte des rejets des bordereaux de paye
      -- dans le cas du rejet d'un bordereau de paye, on met a jour des infos
      -- dans jefy_paye.jefy_paye_compta
      -- attention on considere que toutes les liquidations sont supprimees ou aucune.
      SELECT COUNT (*)
        INTO flag
        FROM mandat m, bordereau b, type_bordereau tb
       WHERE m.bor_id = b.bor_id
         AND b.tbo_ordre = tb.tbo_ordre
         AND m.brj_ordre = brjordre
         AND tb.tbo_type = 'BTMS'
         AND tb.tbo_sous_type = 'SALAIRES';

      IF (flag > 0) THEN
         SELECT COUNT (*)
           INTO flag
           FROM depense d, mandat m, bordereau b, type_bordereau tb
          WHERE m.bor_id = b.bor_id
            AND b.tbo_ordre = tb.tbo_ordre
            AND m.brj_ordre = brjordre
            AND tb.tbo_type = 'BTMS'
            AND tb.tbo_sous_type = 'SALAIRES'
            AND d.man_id = m.man_id
            AND d.dep_suppression = 'OUI';

         SELECT DISTINCT bor_id
                    INTO borid
                    FROM mandat
                   WHERE brj_ordre = brjordre;

         IF (flag = 0) THEN
            -- pas de suppression
            UPDATE jefy_paye.jefy_paye_compta
               SET jpc_etat = 'LIQUIDEE'
             WHERE bor_id = borid;
         ELSE
            DELETE FROM jefy_paye.jefy_paye_compta
                  WHERE bor_id = borid;
         END IF;
      END IF;

-- on passe le brjordre a VISE
      UPDATE bordereau_rejet
         SET brj_etat = 'VISE'
       WHERE brj_ordre = brjordre;
   END;

   FUNCTION get_selection_id (info VARCHAR)
      RETURN INTEGER IS
      selection   INTEGER;
   BEGIN
      SELECT maracuja.abricot_bord_selection_seq.NEXTVAL
        INTO selection
        FROM DUAL;

      RETURN selection;
   END;

   FUNCTION get_selection_borid (abrid INTEGER)
      RETURN INTEGER IS
      borid   INTEGER;
   BEGIN
      SELECT DISTINCT bor_id
                 INTO borid
                 FROM maracuja.abricot_bord_selection
                WHERE abr_id = abrid;

      RETURN borid;
   END;

   PROCEDURE set_selection_id (
      a01abrid        INTEGER,
      a02lesdepid     VARCHAR,
      a03lesrecid     VARCHAR,
      a04utlordre     INTEGER,
      a05exeordre     INTEGER,
      a06tboordre     INTEGER,
      a07abrgroupby   VARCHAR,
      a08gescode      VARCHAR
   ) IS
      chaine     VARCHAR (32000);
      premier    INTEGER;
      tmpdepid   INTEGER;
      tmprecid   INTEGER;
      cpt        INTEGER;
   BEGIN
/*
 bordereau_1R1T
 bordereau_1D1M
 bordereau_1D1M1R1T
 ndep_mand_org_fou_rib_pco (bordereau_ND1M)
 ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
 ndep_mand_fou_rib_pco (bordereau_ND1M)
 ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

      -- traitement de la chaine des depid
      IF a02lesdepid IS NOT NULL OR LENGTH (a02lesdepid) > 0 THEN
         chaine := a02lesdepid;

         LOOP
            premier := 1;

            -- On recupere le depordre
            LOOP
               IF SUBSTR (chaine, premier, 1) = '$' THEN
                  tmpdepid := en_nombre (SUBSTR (chaine, 1, premier - 1));
                  --   IF premier=1 THEN depordre := NULL; END IF;
                  EXIT;
               ELSE
                  premier := premier + 1;
               END IF;
            END LOOP;

            INSERT INTO maracuja.abricot_bord_selection
                        (abr_id,
                         utl_ordre,
                         dep_id,
                         rec_id,
                         exe_ordre,
                         tbo_ordre,
                         abr_etat,
                         abr_group_by,
                         ges_code
                        )
                 VALUES (a01abrid,                                                                    --ABR_ID
                         a04utlordre,                                                              --ult_ordre
                         tmpdepid,                                                                    --DEP_ID
                         NULL,                                                                        --REC_ID
                         a05exeordre,                                                              --EXE_ORDRE
                         a06tboordre,                                                             --TBO_ORDRE,
                         'ATTENTE',                                                                --ABR_ETAT,
                         a07abrgroupby,                                               --,ABR_GROUP_BY,GES_CODE
                         a08gescode                                                                 --ges_code
                        );

--RECHERCHE DU CARACTERE SENTINELLE
            IF SUBSTR (chaine, premier + 1, 1) = '$' THEN
               EXIT;
            END IF;

            chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
         END LOOP;
      END IF;

      -- traitement de la chaine des recid
      IF a03lesrecid IS NOT NULL OR LENGTH (a03lesrecid) > 0 THEN
         chaine := a03lesrecid;

         LOOP
            premier := 1;

            -- On recupere le depordre
            LOOP
               IF SUBSTR (chaine, premier, 1) = '$' THEN
                  tmprecid := en_nombre (SUBSTR (chaine, 1, premier - 1));
                  --   IF premier=1 THEN depordre := NULL; END IF;
                  EXIT;
               ELSE
                  premier := premier + 1;
               END IF;
            END LOOP;

            INSERT INTO maracuja.abricot_bord_selection
                        (abr_id,
                         utl_ordre,
                         dep_id,
                         rec_id,
                         exe_ordre,
                         tbo_ordre,
                         abr_etat,
                         abr_group_by,
                         ges_code
                        )
                 VALUES (a01abrid,                                                                    --ABR_ID
                         a04utlordre,                                                              --ult_ordre
                         NULL,                                                                        --DEP_ID
                         tmprecid,                                                                    --REC_ID
                         a05exeordre,                                                              --EXE_ORDRE
                         a06tboordre,                                                             --TBO_ORDRE,
                         'ATTENTE',                                                                --ABR_ETAT,
                         a07abrgroupby,                                               --,ABR_GROUP_BY,GES_CODE
                         a08gescode                                                                 --ges_code
                        );

--RECHERCHE DU CARACTERE SENTINELLE
            IF SUBSTR (chaine, premier + 1, 1) = '$' THEN
               EXIT;
            END IF;

            chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
         END LOOP;
      END IF;

      SELECT COUNT (*)
        INTO cpt
        FROM jefy_depense.depense_ctrl_planco
       WHERE dpco_id IN (SELECT dep_id
                           FROM abricot_bord_selection
                          WHERE abr_id = a01abrid) AND man_id IS NOT NULL;

      IF cpt > 0 THEN
         raise_application_error (-20001, 'VOTRE SELECTION CONTIENT UNE FACTURE DEJA SUR BORDEREAU  !');
      END IF;

      SELECT COUNT (*)
        INTO cpt
        FROM jefy_recette.recette_ctrl_planco
       WHERE rpco_id IN (SELECT rec_id
                           FROM abricot_bord_selection
                          WHERE abr_id = a01abrid) AND tit_id IS NOT NULL;

      IF cpt > 0 THEN
         raise_application_error (-20001, 'VOTRE SELECTION CONTIENT UNE RECETTE DEJA SUR BORDEREAU !');
      END IF;

      bordereau_abricot.creer_bordereau (a01abrid);
   END;

   PROCEDURE set_selection_intern (
      a01abrid           INTEGER,
      a02lesdepid        VARCHAR,
      a03lesrecid        VARCHAR,
      a04utlordre        INTEGER,
      a05exeordre        INTEGER,
      a07abrgroupby      VARCHAR,
      a08gescodemandat   VARCHAR,
      a09gescodetitre    VARCHAR
   ) IS
   BEGIN
-- ATENTION
-- tboordre : 200 recettes internes
-- tboordre : 201 mandats internes

      -- les mandats
      set_selection_id (a01abrid,
                        a02lesdepid,
                        NULL,
                        a04utlordre,
                        a05exeordre,
                        201,
                        'bordereau_1D1M',
                        a08gescodemandat
                       );
-- les titres
      set_selection_id (-a01abrid,
                        NULL,
                        a03lesrecid,
                        a04utlordre,
                        a05exeordre,
                        200,
                        'bordereau_1R1T',
                        a09gescodetitre
                       );
   END;

   PROCEDURE set_selection_paye (
      a01abrid           INTEGER,
      a02lesdepid        VARCHAR,
      a03lesrecid        VARCHAR,
      a04utlordre        INTEGER,
      a05exeordre        INTEGER,
      a07abrgroupby      VARCHAR,
      a08gescodemandat   VARCHAR,
      a09gescodetitre    VARCHAR
   ) IS
      boridtmp    INTEGER;
      moisordre   INTEGER;
   BEGIN
/*
 -- a07abrgroupby = mois
 select mois_ordre into moisordre from jef_paye.paye_mois where mois_complet = a07abrgroupby;

 -- CONTROLES
 -- peux t on mandater la composante --
 select count(*) into cpt from jefy_depense.papaye_compta
 where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
 and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby);

 if cpt = 0 then  raise_application_error (-20001,'PAS DE MANDATEMENT A EFFECTUER');  end if;

 select mois_ordre into moisordre from papaye.paye_mois where mois_complet = a07abrgroupby;

 -- peux t on mandater la composante --
 select count(*) into cpt from jefy_depense.papaye_compta
 where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
 and mois_ordre=moisordre and ETAT<>'LIQUIDEE';

 if (cpt = 1) then
  raise_application_error (-20001,' MANDATEMENT DEJA EFFECTUE POUR LE MOIS DE "'||a07abrgroupby||'", composante : '||a08gescodemandat);
 end if;
*/
-- ATENTION
-- tboordre : 3 salaires

      -- les mandats de papaye
      set_selection_id (a01abrid,
                        a02lesdepid,
                        NULL,
                        a04utlordre,
                        a05exeordre,
                        3,
                        'bordereau_1D1M',
                        a08gescodemandat
                       );
      boridtmp := get_selection_borid (a01abrid);
 /*
-- maj de l etat de papaye_compta et du bor_ordre -
 update jefy_depense.papaye_compta set bor_ordre=boridtmp, etat='MANDATEE'
 where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
 and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby) and ETAT='LIQUIDEE';
*/
-- Mise a jour des brouillards de paye pour le mois
--  maracuja.bordereau_papaye.maj_brouillards_payes(moisordre, boridtmp);

   -- bascule du brouillard de papaye

   -- les ORV ??????
--set_selection_id(-a01abrid ,null ,a03lesrecid  ,a04utlordre ,a05exeordre  ,200 ,'bordereau_1R1T' ,a09gescodetitre );
   END;

-- creer bordereau (tbo_ordre) + numerotation
   FUNCTION get_num_borid (tboordre INTEGER, exeordre INTEGER, gescode VARCHAR, utlordre INTEGER)
      RETURN INTEGER IS
      cpt      INTEGER;
      borid    INTEGER;
      bornum   INTEGER;
   BEGIN
-- creation du bor_id --
      SELECT bordereau_seq.NEXTVAL
        INTO borid
        FROM DUAL;

-- creation du bordereau --
      bornum := -1;

      INSERT INTO bordereau
                  (bor_date_visa,
                   bor_etat,
                   bor_id,
                   bor_num,
                   bor_ordre,
                   exe_ordre,
                   ges_code,
                   tbo_ordre,
                   utl_ordre,
                   utl_ordre_visa,
                   bor_date_creation
                  )
           VALUES (NULL,                                                                      --BOR_DATE_VISA,
                   'VALIDE',                                                                       --BOR_ETAT,
                   borid,                                                                            --BOR_ID,
                   bornum,                                                                          --BOR_NUM,
                   -borid,                                                                        --BOR_ORDRE,
--a partir de 2007 il n existe plus de bor_ordre pour conserver le constraint je met -borid
                   exeordre,                                                                      --EXE_ORDRE,
                   gescode,                                                                        --GES_CODE,
                   tboordre,                                                                      --TBO_ORDRE,
                   utlordre,                                                                      --UTL_ORDRE,
                   NULL,                                                                      --UTL_ORDRE_VISA
                   SYSDATE
                  );

      RETURN borid;
   END;

-- les algos de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
   PROCEDURE bordereau_1r1t (abrid INTEGER, monborid INTEGER) IS
      cpt          INTEGER;
      tmprecette   jefy_recette.recette_ctrl_planco%ROWTYPE;

      CURSOR rec_tit IS
         SELECT   r.*
             FROM abricot_bord_selection ab, jefy_recette.recette_ctrl_planco r
            WHERE r.rpco_id = ab.rec_id AND abr_id = abrid AND ab.abr_etat = 'ATTENTE'
         ORDER BY r.pco_num, r.rec_id ASC;
   BEGIN
      OPEN rec_tit;

      LOOP
         FETCH rec_tit
          INTO tmprecette;

         EXIT WHEN rec_tit%NOTFOUND;
         cpt := set_titre_recette (tmprecette.rpco_id, monborid);
      END LOOP;

      CLOSE rec_tit;
   END;

   PROCEDURE bordereau_nr1t (abrid INTEGER, monborid INTEGER) IS
      ht           NUMBER (12, 2);
      tva          NUMBER (12, 2);
      ttc          NUMBER (12, 2);
      pconumero    VARCHAR (20);
      nbpieces     INTEGER;
      cpt          INTEGER;
      titidtemp    INTEGER;
      tmprecette   jefy_recette.recette_ctrl_planco%ROWTYPE;

-- curseur de regroupement
      CURSOR rec_tit_group_by IS
         SELECT   r.pco_num, SUM (r.rpco_ht_saisie), SUM (r.rpco_tva_saisie), SUM (r.rpco_ttc_saisie)
             FROM abricot_bord_selection ab, jefy_recette.recette_ctrl_planco r
            WHERE r.rpco_id = ab.rec_id AND abr_id = abrid AND ab.abr_etat = 'ATTENTE'
         GROUP BY r.pco_num
         ORDER BY r.pco_num ASC;

      CURSOR rec_tit IS
         SELECT   r.*
             FROM abricot_bord_selection ab, jefy_recette.recette_ctrl_planco r
            WHERE r.rpco_id = ab.rec_id AND abr_id = abrid AND ab.abr_etat = 'ATTENTE'
                  AND r.pco_num = pconumero
         ORDER BY r.pco_num ASC, r.rec_id;
   BEGIN
      OPEN rec_tit_group_by;

      LOOP
         FETCH rec_tit_group_by
          INTO pconumero, ht, tva, ttc;

         EXIT WHEN rec_tit_group_by%NOTFOUND;
         titidtemp := 0;

         OPEN rec_tit;

         LOOP
            FETCH rec_tit
             INTO tmprecette;

            EXIT WHEN rec_tit%NOTFOUND;

            IF titidtemp = 0 THEN
               titidtemp := set_titre_recette (tmprecette.rpco_id, monborid);
            ELSE
               UPDATE jefy_recette.recette_ctrl_planco
                  SET tit_id = titidtemp
                WHERE rpco_id = tmprecette.rpco_id;
            END IF;
         END LOOP;

         CLOSE rec_tit;

-- recup du nombre de pieces
-- TODO
         nbpieces := 0;

-- le fouOrdre du titre a faire pointer sur DEBITEUR DIVERS
-- ( a definir dans le parametrage)

         -- maj des montants du titre
         UPDATE titre
            SET tit_ht = ht,
                tit_nb_piece = nbpieces,
                tit_ttc = ttc,
                tit_tva = tva,
                tit_libelle = 'TITRE COLLECTIF'
          WHERE tit_id = titidtemp;
-- mise a jour du brouillard ?
-- BORDEREAU_ABRICOT.Set_Titre_Brouillard(titidtemp);
      END LOOP;

      CLOSE rec_tit_group_by;
   END;

   PROCEDURE bordereau_nd1m (abrid INTEGER, monborid INTEGER) IS
      cpt          INTEGER;
      tmpdepense   jefy_depense.depense_ctrl_planco%ROWTYPE;
      abrgroupby   abricot_bord_selection.abr_group_by%TYPE;

-- cursor pour traites les conventions limitatives !!!!
-- 1D1M -> liaison comptabilite
      CURSOR mand_dep_convra IS
         SELECT DISTINCT d.*
                    FROM abricot_bord_selection ab,
                         jefy_depense.depense_ctrl_planco d,
                         jefy_depense.depense_budget db,
                         jefy_depense.engage_budget e,
                         maracuja.v_convention_limitative c
                   WHERE d.dpco_id = ab.dep_id
                     AND abr_id = abrid
                     AND db.dep_id = d.dep_id
                     AND e.eng_id = db.eng_id
                     AND e.org_id = c.org_id(+)
                     AND e.exe_ordre = c.exe_ordre(+)
                     AND c.org_id IS NOT NULL
                     AND d.man_id IS NULL
                     AND ab.abr_etat = 'ATTENTE'
                ORDER BY d.pco_num ASC;
-- POUR LE RESTE DE LA SELECTION :
-- un cusor par type de abr_goup_by
-- attention une selection est de base limitee a un exercice et une UB et un type de bordereau
-- dans l interface on peut restreindre a l agent qui a saisie la depense.
-- dans l interface on peut restreindre au CR ou SOUS CR qui budgetise la depense.
-- dans l interface on peut restreindre suivant les 2 criteres ci dessus.
   BEGIN
      OPEN mand_dep_convra;

      LOOP
         FETCH mand_dep_convra
          INTO tmpdepense;

         EXIT WHEN mand_dep_convra%NOTFOUND;
         cpt := set_mandat_depense (tmpdepense.dpco_id, monborid);
      END LOOP;

      CLOSE mand_dep_convra;

-- recup du group by pour traiter le reste des mandats
      SELECT DISTINCT abr_group_by
                 INTO abrgroupby
                 FROM abricot_bord_selection
                WHERE abr_id = abrid;

-- il faut traiter les autres depenses non c_convra
--IF ( abrgroupby = 'ndep_mand_org_fou_rib_pco' ) THEN
-- cpt:=ndep_mand_org_fou_rib_pco(abrid ,monborid );
--END IF;
      IF (abrgroupby = 'ndep_mand_org_fou_rib_pco_mod') THEN
         cpt := ndep_mand_org_fou_rib_pco_mod (abrid, monborid);
      END IF;

--IF ( abrgroupby = 'ndep_mand_fou_rib_pco') THEN
-- cpt:=ndep_mand_fou_rib_pco(abrid ,monborid );
--END IF;
      IF (abrgroupby = 'ndep_mand_fou_rib_pco_mod') THEN
         cpt := ndep_mand_fou_rib_pco_mod (abrid, monborid);
      END IF;
   END;

   PROCEDURE bordereau_1d1m (abrid INTEGER, monborid INTEGER) IS
      cpt          INTEGER;
      tmpdepense   jefy_depense.depense_ctrl_planco%ROWTYPE;

      CURSOR dep_mand IS
         SELECT   d.*
             FROM abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d
            WHERE d.dpco_id = ab.dep_id AND abr_id = abrid AND ab.abr_etat = 'ATTENTE'
         ORDER BY d.pco_num ASC;
   BEGIN
      OPEN dep_mand;

      LOOP
         FETCH dep_mand
          INTO tmpdepense;

         EXIT WHEN dep_mand%NOTFOUND;
         cpt := set_mandat_depense (tmpdepense.dpco_id, monborid);
      END LOOP;

      CLOSE dep_mand;
   END;

   PROCEDURE bordereau_1d1m1r1t (abrid INTEGER, boridep INTEGER, boridrec INTEGER) IS
      cpt   INTEGER;
   BEGIN
      SELECT COUNT (*)
        INTO cpt
        FROM DUAL;

      bordereau_1d1m (abrid, boridep);
      bordereau_1r1t (abrid, boridrec);
   END;

-- les mandats et titres
   FUNCTION set_mandat_depense (dpcoid INTEGER, borid INTEGER)
      RETURN INTEGER IS
      cpt               INTEGER;
      flag              INTEGER;
      ladepense         jefy_depense.depense_ctrl_planco%ROWTYPE;
      ladepensepapier   jefy_depense.depense_papier%ROWTYPE;
      leengagebudget    jefy_depense.engage_budget%ROWTYPE;
      gescode           gestion.ges_code%TYPE;
      manid             mandat.man_id%TYPE;
      manorgine_key     mandat.man_orgine_key%TYPE;
      manorigine_lib    mandat.man_origine_lib%TYPE;
      oriordre          mandat.ori_ordre%TYPE;
      prestid           mandat.prest_id%TYPE;
      torordre          mandat.tor_ordre%TYPE;
      virordre          mandat.pai_ordre%TYPE;
      mannumero         mandat.man_numero%TYPE;
   BEGIN
-- recuperation du ges_code --
      SELECT ges_code
        INTO gescode
        FROM bordereau
       WHERE bor_id = borid;

      SELECT *
        INTO ladepense
        FROM jefy_depense.depense_ctrl_planco
       WHERE dpco_id = dpcoid;

      SELECT DISTINCT dpp.*
                 INTO ladepensepapier
                 FROM jefy_depense.depense_papier dpp,
                      jefy_depense.depense_budget db,
                      jefy_depense.depense_ctrl_planco dpco
                WHERE db.dep_id = dpco.dep_id AND dpp.dpp_id = db.dpp_id AND dpco_id = dpcoid;

      SELECT eb.*
        INTO leengagebudget
        FROM jefy_depense.engage_budget eb,
             jefy_depense.depense_budget db,
             jefy_depense.depense_ctrl_planco dpco
       WHERE db.eng_id = eb.eng_id AND db.dep_id = dpco.dep_id AND dpco_id = dpcoid;

-- Verifier si ligne budgetaire ouverte sur exercice
      SELECT COUNT (*)
        INTO flag
        FROM maracuja.v_organ_exer
       WHERE org_id = leengagebudget.org_id AND exe_ordre = leengagebudget.exe_ordre;

      IF (flag = 0) THEN
         raise_application_error (-20001,
                                     'La ligne budgetaire affectee a l''engagement num. '
                                  || leengagebudget.eng_numero
                                  || ' n''est pas ouverte sur '
                                  || leengagebudget.exe_ordre
                                  || '.'
                                 );
      END IF;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
      manorgine_key := NULL;
--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
      manorigine_lib := NULL;
--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
      oriordre := gestionorigine.traiter_orgid (leengagebudget.org_id, leengagebudget.exe_ordre);

--PRESTID : PRESTATION INTERNE --
      SELECT COUNT (*)
        INTO cpt
        FROM jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
       WHERE d.pef_id = e.pef_id AND d.dep_id = ladepense.dep_id;

      IF cpt = 1 THEN
         SELECT prest_id
           INTO prestid
           FROM jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
          WHERE d.pef_id = e.pef_id AND d.dep_id = ladepense.dep_id;
      ELSE
         prestid := NULL;
      END IF;

--TORORDRE : ORIGINE DU MANDAT --
      torordre := 1;
--VIRORDRE --
      virordre := NULL;

-- creation du man_id --
      SELECT mandat_seq.NEXTVAL
        INTO manid
        FROM DUAL;

-- recup du numero de mandat
      mannumero := -1;

      INSERT INTO mandat
                  (bor_id,
                   brj_ordre,
                   exe_ordre,
                   fou_ordre,
                   ges_code,
                   man_date_remise,
                   man_date_visa_princ,
                   man_etat,
                   man_etat_remise,
                   man_ht,
                   man_id,
                   man_motif_rejet,
                   man_nb_piece,
                   man_numero,
                   man_numero_rejet,
                   man_ordre,
                   man_orgine_key,
                   man_origine_lib,
                   man_ttc,
                   man_tva,
                   mod_ordre,
                   ori_ordre,
                   pco_num,
                   prest_id,
                   tor_ordre,
                   pai_ordre,
                   org_ordre,
                   rib_ordre_ordonnateur,
                   rib_ordre_comptable
                  )
           VALUES (borid,                                                                            --BOR_ID,
                   NULL,                                                                          --BRJ_ORDRE,
                   ladepensepapier.exe_ordre,                                                     --EXE_ORDRE,
                   ladepensepapier.fou_ordre,                                                     --FOU_ORDRE,
                   gescode,                                                                        --GES_CODE,
                   NULL,                                                                    --MAN_DATE_REMISE,
                   NULL,                                                                --MAN_DATE_VISA_PRINC,
                   'ATTENTE',                                                                      --MAN_ETAT,
                   'ATTENTE',                                                               --MAN_ETAT_REMISE,
                   ladepense.dpco_montant_budgetaire,                                                --MAN_HT,
                   manid,                                                                            --MAN_ID,
                   NULL,                                                                    --MAN_MOTIF_REJET,
                   ladepensepapier.dpp_nb_piece,                                               --MAN_NB_PIECE,
                   mannumero,                                                                    --MAN_NUMERO,
                   NULL,                                                                   --MAN_NUMERO_REJET,
                   -manid,                                                                        --MAN_ORDRE,
-- a parir de 2007 plus de man_ordre mais pour conserver la contrainte je mets -manid
                   manorgine_key,                                                            --MAN_ORGINE_KEY,
                   manorigine_lib,                                                          --MAN_ORIGINE_LIB,
                   ladepense.dpco_ttc_saisie,                                                       --MAN_TTC,
                   ladepense.dpco_ttc_saisie - ladepense.dpco_montant_budgetaire,                   --MAN_TVA,
                   ladepensepapier.mod_ordre,                                                     --MOD_ORDRE,
                   oriordre,                                                                      --ORI_ORDRE,
                   ladepense.pco_num,                                                               --PCO_NUM,
                   prestid,                                                                        --PREST_ID,
                   torordre,                                                                      --TOR_ORDRE,
                   virordre,                                                                       --VIR_ORDRE
                   leengagebudget.org_id,                                                          --org_ordre
                   ladepensepapier.rib_ordre,                                                       --rib ordo
                   ladepensepapier.rib_ordre                                                  -- rib_comptable
                  );

-- maj du man_id  dans la depense
      UPDATE jefy_depense.depense_ctrl_planco
         SET man_id = manid
       WHERE dpco_id = dpcoid;

-- recup de la depense
--get_depense_jefy_depense(manid,ladepensepapier.utl_ordre);

      -- recup du brouillard
      set_mandat_brouillard (manid);
      RETURN manid;
   END;

-- lesdepid XX$FF$....$DDD$ZZZ$$
   FUNCTION set_mandat_depenses (lesdpcoid VARCHAR, borid INTEGER)
      RETURN INTEGER IS
      cpt             INTEGER;
      premier         INTEGER;
      tmpdpcoid       INTEGER;
      chaine          VARCHAR (5000);
      premierdpcoid   INTEGER;
      manid           INTEGER;
      ttc             mandat.man_ttc%TYPE;
      tva             mandat.man_tva%TYPE;
      ht              mandat.man_ht%TYPE;
      utlordre        INTEGER;
      nb_pieces       INTEGER;
   BEGIN
      SELECT COUNT (*)
        INTO cpt
        FROM DUAL;

--RAISE_APPLICATION_ERROR (-20001,'lesdpcoid'||lesdpcoid);
      premierdpcoid := NULL;

      -- traitement de la chaine des depid xx$xx$xx$.....$x$$
      IF lesdpcoid IS NOT NULL OR LENGTH (lesdpcoid) > 0 THEN
         chaine := lesdpcoid;

         LOOP
            premier := 1;

            -- On recupere le depid
            LOOP
               IF SUBSTR (chaine, premier, 1) = '$' THEN
                  tmpdpcoid := en_nombre (SUBSTR (chaine, 1, premier - 1));
                  --   IF premier=1 THEN depordre := NULL; END IF;
                  EXIT;
               ELSE
                  premier := premier + 1;
               END IF;
            END LOOP;

-- creation du mandat lie au borid
            IF premierdpcoid IS NULL THEN
               manid := set_mandat_depense (tmpdpcoid, borid);

               -- suppression du brouillard car il est uniquement sur la premiere depense
               DELETE FROM mandat_brouillard
                     WHERE man_id = manid;

               premierdpcoid := tmpdpcoid;
            ELSE
               -- maj du man_id  dans la depense
               UPDATE jefy_depense.depense_ctrl_planco
                  SET man_id = manid
                WHERE dpco_id = tmpdpcoid;

               -- recup de la depense (maracuja)
               SELECT DISTINCT dpp.utl_ordre
                          INTO utlordre
                          FROM jefy_depense.depense_papier dpp,
                               jefy_depense.depense_budget db,
                               jefy_depense.depense_ctrl_planco dpco
                         WHERE db.dep_id = dpco.dep_id AND dpp.dpp_id = db.dpp_id AND dpco_id = tmpdpcoid;
--  get_depense_jefy_depense(manid,utlordre);
            END IF;

--RECHERCHE DU CARACTERE SENTINELLE
            IF SUBSTR (chaine, premier + 1, 1) = '$' THEN
               EXIT;
            END IF;

            chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
         END LOOP;
      END IF;

-- mise a jour des montants du mandat HT TVA ET TTC nb pieces
      SELECT SUM (dpco_ttc_saisie), SUM (dpco_ttc_saisie - dpco_montant_budgetaire),
             SUM (dpco_montant_budgetaire)
        INTO ttc, tva,
             ht
        FROM jefy_depense.depense_ctrl_planco
       WHERE man_id = manid;

-- recup du nb de pieces
      SELECT SUM (dpp.dpp_nb_piece)
        INTO nb_pieces
        FROM jefy_depense.depense_papier dpp,
             jefy_depense.depense_budget db,
             jefy_depense.depense_ctrl_planco dpco
       WHERE db.dep_id = dpco.dep_id AND dpp.dpp_id = db.dpp_id AND man_id = manid;

-- maj du mandat
      UPDATE mandat
         SET man_ht = ht,
             man_tva = tva,
             man_ttc = ttc,
             man_nb_piece = nb_pieces
       WHERE man_id = manid;

-- recup du brouillard
      set_mandat_brouillard (manid);
      RETURN cpt;
   END;

   FUNCTION set_titre_recette (rpcoid INTEGER, borid INTEGER)
      RETURN INTEGER IS
      --jefytitre           jefy.titre%ROWTYPE;
      gescode             gestion.ges_code%TYPE;
      titid               titre.tit_id%TYPE;
      titorginekey        titre.tit_orgine_key%TYPE;
      titoriginelib       titre.tit_origine_lib%TYPE;
      oriordre            titre.ori_ordre%TYPE;
      prestid             titre.prest_id%TYPE;
      torordre            titre.tor_ordre%TYPE;
      modordre            titre.mod_ordre%TYPE;
      presid              INTEGER;
      cpt                 INTEGER;
      virordre            INTEGER;
      flag                INTEGER;
      recettepapier       jefy_recette.recette_papier%ROWTYPE;
      recettebudget       jefy_recette.recette_budget%ROWTYPE;
      facturebudget       jefy_recette.facture_budget%ROWTYPE;
      recettectrlplanco   jefy_recette.recette_ctrl_planco%ROWTYPE;
   BEGIN
-- recuperation du ges_code --
      SELECT ges_code
        INTO gescode
        FROM bordereau
       WHERE bor_id = borid;

--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
      SELECT *
        INTO recettectrlplanco
        FROM jefy_recette.recette_ctrl_planco
       WHERE rpco_id = rpcoid;

      SELECT *
        INTO recettebudget
        FROM jefy_recette.recette_budget
       WHERE rec_id = recettectrlplanco.rec_id;

      SELECT *
        INTO facturebudget
        FROM jefy_recette.facture_budget
       WHERE fac_id = recettebudget.fac_id;

      SELECT *
        INTO recettepapier
        FROM jefy_recette.recette_papier
       WHERE rpp_id = recettebudget.rpp_id;

-- Verifier si ligne budgetaire ouverte sur exercice
      SELECT COUNT (*)
        INTO flag
        FROM maracuja.v_organ_exer
       WHERE org_id = facturebudget.org_id AND exe_ordre = facturebudget.exe_ordre;

      IF (flag = 0) THEN
         raise_application_error (-20001,
                                     'La ligne budgetaire affectee a la recette num. '
                                  || recettebudget.rec_numero
                                  || ' n''est pas ouverte sur '
                                  || facturebudget.exe_ordre
                                  || '.'
                                 );
      END IF;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
      titorginekey := NULL;
--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
      titoriginelib := NULL;
--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
      oriordre := gestionorigine.traiter_orgid (facturebudget.org_id, facturebudget.exe_ordre);

--PRESTID : PRESTATION INTERNE --
      SELECT COUNT (*)
        INTO cpt
        FROM jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
       WHERE d.pef_id = e.pef_id AND d.rec_id = recettectrlplanco.rec_id;

      IF cpt = 1 THEN
         SELECT prest_id
           INTO prestid
           FROM jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
          WHERE d.pef_id = e.pef_id AND d.rec_id = recettectrlplanco.rec_id;
      ELSE
         prestid := NULL;
      END IF;

--TORORDRE : ORIGINE DU MANDAT --
      torordre := 1;
--VIRORDRE --
      virordre := NULL;

      SELECT titre_seq.NEXTVAL
        INTO titid
        FROM DUAL;

      INSERT INTO titre
                  (bor_id,
                   bor_ordre,
                   brj_ordre,
                   exe_ordre,
                   ges_code,
                   mod_ordre,
                   ori_ordre,
                   pco_num,
                   prest_id,
                   tit_date_remise,
                   tit_date_visa_princ,
                   tit_etat,
                   tit_etat_remise,
                   tit_ht,
                   tit_id,
                   tit_motif_rejet,
                   tit_nb_piece,
                   tit_numero,
                   tit_numero_rejet,
                   tit_ordre,
                   tit_orgine_key,
                   tit_origine_lib,
                   tit_ttc,
                   tit_tva,
                   tor_ordre,
                   utl_ordre,
                   org_ordre,
                   fou_ordre,
                   mor_ordre,
                   pai_ordre,
                   rib_ordre_ordonnateur,
                   rib_ordre_comptable,
                   tit_libelle
                  )
           VALUES (borid,                                                                            --BOR_ID,
                   -borid,                                                                        --BOR_ORDRE,
                   NULL,                                                                          --BRJ_ORDRE,
                   recettepapier.exe_ordre,                                                       --EXE_ORDRE,
                   gescode,                                                                        --GES_CODE,
                   NULL,                                   --MOD_ORDRE, n existe plus en 2007 vestige des ORVs
                   oriordre,                                                                      --ORI_ORDRE,
                   recettectrlplanco.pco_num,                                                       --PCO_NUM,
                   prestid,                                                                        --PREST_ID,
                   SYSDATE,                                                                 --TIT_DATE_REMISE,
                   NULL,                                                                --TIT_DATE_VISA_PRINC,
                   'ATTENTE',                                                                      --TIT_ETAT,
                   'ATTENTE',                                                               --TIT_ETAT_REMISE,
                   recettectrlplanco.rpco_ht_saisie,                                                 --TIT_HT,
                   titid,                                                                            --TIT_ID,
                   NULL,                                                                    --TIT_MOTIF_REJET,
                   recettepapier.rpp_nb_piece,                                                 --TIT_NB_PIECE,
                   -1,                                        --TIT_NUMERO, numerotation en fin de transaction
                   NULL,                                                                   --TIT_NUMERO_REJET,
                   -titid,                              --TIT_ORDRE,  en 2007 plus de tit_ordre on met  tit_id
                   titorginekey,                                                             --TIT_ORGINE_KEY,
                   titoriginelib,                                                           --TIT_ORIGINE_LIB,
                   recettectrlplanco.rpco_ttc_saisie,                                               --TIT_TTC,
                   recettectrlplanco.rpco_tva_saisie,                                               --TIT_TVA,
                   torordre,                                                                      --TOR_ORDRE,
                   recettepapier.utl_ordre,                                                        --UTL_ORDRE
                   facturebudget.org_id,                                                          --ORG_ORDRE,
                   recettepapier.fou_ordre,                      -- FOU_ORDRE  --TOCHECK certains sont nuls...
                   facturebudget.mor_ordre,                                                        --MOR_ORDRE
                   NULL,                                                                          -- VIR_ORDRE
                   recettepapier.rib_ordre,
                   recettepapier.rib_ordre,
                   recettebudget.rec_lib
                  );

-- maj du tit_id dans la recette
      UPDATE jefy_recette.recette_ctrl_planco
         SET tit_id = titid
       WHERE rpco_id = rpcoid;

-- recup du brouillard
--Set_Titre_Brouillard(titid);
      RETURN titid;
   END;

   FUNCTION set_titre_recettes (lesrpcoid VARCHAR, borid INTEGER)
      RETURN INTEGER IS
      cpt   INTEGER;
   BEGIN
      SELECT COUNT (*)
        INTO cpt
        FROM DUAL;

      raise_application_error (-20001, 'OPERATION NON TRAITEE');
      RETURN cpt;
   END;

/*

FUNCTION ndep_mand_org_fou_rib_pco (abrid INTEGER,borid INTEGER) RETURN INTEGER IS
cpt        INTEGER;
fouordre   v_fournisseur.FOU_ORDRE%TYPE;
ribordre   v_rib.RIB_ORDRE%TYPE;
pconum     PLAN_COMPTABLE.PCO_NUM%TYPE;
modordre   MODE_PAIEMENT.MOD_ORDRE%TYPE;
orgid      jefy_admin.organ.org_id%TYPE;
ht         MANDAT.MAN_HT%TYPE;
tva        MANDAT.MAN_HT%TYPE;
ttc        MANDAT.MAN_HT%TYPE;
budgetaire MANDAT.MAN_HT%TYPE;

CURSOR ndep_mand_org_fou_rib_pco IS
SELECT e.org_id,dpp.fou_ordre,dpp.rib_ordre,d.pco_num,
SUM(dpco_ht_saisie) ht,
SUM(dpco_tva_saisie) tva,
SUM(dpco_ttc_saisie) ttc,
SUM(dpco_montant_budgetaire) budgetaire
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
and d.man_id is null
GROUP BY e.org_id,dpp.fou_ordre,dpp.rib_ordre,d.pco_num;

CURSOR lesdpcoids IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
AND e.org_id = orgid
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre = ribordre
and d.man_id is null
AND d.pco_num = pconum;

CURSOR lesdpcoidsribnull IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
AND e.org_id = orgid
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre is null
and d.man_id is null
AND d.pco_num = pconum;

chainedpcoid VARCHAR(5000);
tmpdpcoid jefy_depense.depense_ctrl_planco.dpco_id%TYPE;

BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

OPEN ndep_mand_org_fou_rib_pco;
LOOP
FETCH ndep_mand_org_fou_rib_pco INTO
orgid,fouordre,ribordre,pconum,ht,tva,ttc,budgetaire;
EXIT WHEN ndep_mand_org_fou_rib_pco%NOTFOUND;
chainedpcoid :=NULL;
if ribordre is not null then
 OPEN lesdpcoids;
 LOOP
 FETCH lesdpcoids INTO tmpdpcoid;
 EXIT WHEN lesdpcoids%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoids;
 else
  OPEN lesdpcoidsribnull;
 LOOP
 FETCH lesdpcoidsribnull INTO tmpdpcoid;
 EXIT WHEN lesdpcoidsribnull%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoidsribnull;
 end if;
  chainedpcoid :=chainedpcoid||'$';
-- creation des mandats des pids
cpt:=set_mandat_depenses(chainedpcoid,borid);
END LOOP;

CLOSE ndep_mand_org_fou_rib_pco;

RETURN cpt;
END;

*/
   FUNCTION ndep_mand_org_fou_rib_pco_mod (abrid INTEGER, borid INTEGER)
      RETURN INTEGER IS
      cpt            INTEGER;
      fouordre       v_fournisseur.fou_ordre%TYPE;
      ribordre       v_rib.rib_ordre%TYPE;
      pconum         plan_comptable.pco_num%TYPE;
      modordre       mode_paiement.mod_ordre%TYPE;
      orgid          jefy_admin.organ.org_id%TYPE;
      ht             mandat.man_ht%TYPE;
      tva            mandat.man_ht%TYPE;
      ttc            mandat.man_ht%TYPE;
      budgetaire     mandat.man_ht%TYPE;

      CURSOR ndep_mand_org_fou_rib_pco_mod IS
         SELECT   e.org_id, dpp.fou_ordre, dpp.rib_ordre, d.pco_num, dpp.mod_ordre, SUM (dpco_ht_saisie) ht,
                  SUM (dpco_tva_saisie) tva, SUM (dpco_ttc_saisie) ttc,
                  SUM (dpco_montant_budgetaire) budgetaire
             FROM abricot_bord_selection ab,
                  jefy_depense.depense_ctrl_planco d,
                  jefy_depense.depense_budget db,
                  jefy_depense.depense_papier dpp,
                  jefy_depense.engage_budget e
            WHERE d.dpco_id = ab.dep_id
              AND dpp.dpp_id = db.dpp_id
              AND db.dep_id = d.dep_id
              AND abr_id = abrid
              AND e.eng_id = db.eng_id
              AND ab.abr_etat = 'ATTENTE'
              AND d.man_id IS NULL
         GROUP BY e.org_id, dpp.fou_ordre, dpp.rib_ordre, d.pco_num, dpp.mod_ordre;

      CURSOR lesdpcoids IS
         SELECT d.dpco_id
           FROM abricot_bord_selection ab,
                jefy_depense.depense_ctrl_planco d,
                jefy_depense.depense_budget db,
                jefy_depense.depense_papier dpp,
                jefy_depense.engage_budget e
          WHERE d.dpco_id = ab.dep_id
            AND dpp.dpp_id = db.dpp_id
            AND db.dep_id = d.dep_id
            AND abr_id = abrid
            AND e.eng_id = db.eng_id
            AND ab.abr_etat = 'ATTENTE'
            AND e.org_id = orgid
            AND dpp.fou_ordre = fouordre
            AND dpp.rib_ordre = ribordre
            AND d.pco_num = pconum
            AND dpp.mod_ordre = modordre
            AND d.man_id IS NULL;

      CURSOR lesdpcoidsribnull IS
         SELECT d.dpco_id
           FROM abricot_bord_selection ab,
                jefy_depense.depense_ctrl_planco d,
                jefy_depense.depense_budget db,
                jefy_depense.depense_papier dpp,
                jefy_depense.engage_budget e
          WHERE d.dpco_id = ab.dep_id
            AND dpp.dpp_id = db.dpp_id
            AND db.dep_id = d.dep_id
            AND abr_id = abrid
            AND e.eng_id = db.eng_id
            AND ab.abr_etat = 'ATTENTE'
            AND e.org_id = orgid
            AND dpp.fou_ordre = fouordre
            AND dpp.rib_ordre IS NULL
            AND d.pco_num = pconum
            AND dpp.mod_ordre = modordre
            AND d.man_id IS NULL;

      chainedpcoid   VARCHAR (5000);
      tmpdpcoid      jefy_depense.depense_ctrl_planco.dpco_id%TYPE;
   BEGIN
      SELECT COUNT (*)
        INTO cpt
        FROM DUAL;

      OPEN ndep_mand_org_fou_rib_pco_mod;

      LOOP
         FETCH ndep_mand_org_fou_rib_pco_mod
          INTO orgid, fouordre, ribordre, pconum, modordre, ht, tva, ttc, budgetaire;

         EXIT WHEN ndep_mand_org_fou_rib_pco_mod%NOTFOUND;
         chainedpcoid := NULL;

         IF ribordre IS NOT NULL THEN
            OPEN lesdpcoids;

            LOOP
               FETCH lesdpcoids
                INTO tmpdpcoid;

               EXIT WHEN lesdpcoids%NOTFOUND;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            END LOOP;

            CLOSE lesdpcoids;
         ELSE
            OPEN lesdpcoidsribnull;

            LOOP
               FETCH lesdpcoidsribnull
                INTO tmpdpcoid;

               EXIT WHEN lesdpcoidsribnull%NOTFOUND;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            END LOOP;

            CLOSE lesdpcoidsribnull;
         END IF;

         chainedpcoid := chainedpcoid || '$';
-- creation des mandats des pids
         cpt := set_mandat_depenses (chainedpcoid, borid);
      END LOOP;

      CLOSE ndep_mand_org_fou_rib_pco_mod;

      RETURN cpt;
   END;

/*
FUNCTION ndep_mand_fou_rib_pco  (abrid INTEGER,borid INTEGER) RETURN INTEGER IS
cpt        INTEGER;
fouordre   v_fournisseur.FOU_ORDRE%TYPE;
ribordre   v_rib.RIB_ORDRE%TYPE;
pconum     PLAN_COMPTABLE.PCO_NUM%TYPE;
modordre   MODE_PAIEMENT.MOD_ORDRE%TYPE;
orgid      jefy_admin.organ.org_id%TYPE;
ht         MANDAT.MAN_HT%TYPE;
tva        MANDAT.MAN_HT%TYPE;
ttc        MANDAT.MAN_HT%TYPE;
budgetaire MANDAT.MAN_HT%TYPE;

CURSOR ndep_mand_fou_rib_pco IS
SELECT dpp.fou_ordre,dpp.rib_ordre,d.pco_num,
SUM(dpco_ht_saisie) ht,
SUM(dpco_tva_saisie) tva,
SUM(dpco_ttc_saisie) ttc,
SUM(dpco_montant_budgetaire) budgetaire
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
and d.man_id is null
GROUP BY dpp.fou_ordre,dpp.rib_ordre,d.pco_num;


CURSOR lesdpcoids IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre = ribordre
and d.man_id is null
AND d.pco_num = pconum;


CURSOR lesdpcoidsribnull IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre is null
and d.man_id is null
AND d.pco_num = pconum;


chainedpcoid VARCHAR(5000);
tmpdpcoid jefy_depense.depense_ctrl_planco.dpco_id%TYPE;

BEGIN

OPEN ndep_mand_fou_rib_pco;
LOOP
FETCH ndep_mand_fou_rib_pco INTO
fouordre,ribordre,pconum,ht,tva,ttc,budgetaire;
EXIT WHEN ndep_mand_fou_rib_pco%NOTFOUND;
chainedpcoid :=NULL;
if ribordre is not null then
 OPEN lesdpcoids;
 LOOP
 FETCH lesdpcoids INTO tmpdpcoid;
 EXIT WHEN lesdpcoids%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoids;
 else
  OPEN lesdpcoidsribnull;
 LOOP
 FETCH lesdpcoidsribnull INTO tmpdpcoid;
 EXIT WHEN lesdpcoidsribnull%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoidsribnull;
 end if;

  chainedpcoid :=chainedpcoid||'$';
-- creation des mandats des pids
cpt:=set_mandat_depenses(chainedpcoid,borid);
END LOOP;

CLOSE ndep_mand_fou_rib_pco;

RETURN cpt;
END;
*/
   FUNCTION ndep_mand_fou_rib_pco_mod (abrid INTEGER, borid INTEGER)
      RETURN INTEGER IS
      cpt            INTEGER;
      fouordre       v_fournisseur.fou_ordre%TYPE;
      ribordre       v_rib.rib_ordre%TYPE;
      pconum         plan_comptable.pco_num%TYPE;
      modordre       mode_paiement.mod_ordre%TYPE;
      orgid          jefy_admin.organ.org_id%TYPE;
      ht             mandat.man_ht%TYPE;
      tva            mandat.man_ht%TYPE;
      ttc            mandat.man_ht%TYPE;
      budgetaire     mandat.man_ht%TYPE;

      CURSOR ndep_mand_fou_rib_pco_mod IS
         SELECT   dpp.fou_ordre, dpp.rib_ordre, d.pco_num, dpp.mod_ordre, SUM (dpco_ht_saisie) ht,
                  SUM (dpco_tva_saisie) tva, SUM (dpco_ttc_saisie) ttc,
                  SUM (dpco_montant_budgetaire) budgetaire
             FROM abricot_bord_selection ab,
                  jefy_depense.depense_ctrl_planco d,
                  jefy_depense.depense_budget db,
                  jefy_depense.depense_papier dpp
            WHERE d.dpco_id = ab.dep_id
              AND dpp.dpp_id = db.dpp_id
              AND db.dep_id = d.dep_id
              AND abr_id = abrid
              AND ab.abr_etat = 'ATTENTE'
              AND d.man_id IS NULL
         GROUP BY dpp.fou_ordre, dpp.rib_ordre, d.pco_num, dpp.mod_ordre;

      CURSOR lesdpcoids IS
         SELECT d.dpco_id
           FROM abricot_bord_selection ab,
                jefy_depense.depense_ctrl_planco d,
                jefy_depense.depense_budget db,
                jefy_depense.depense_papier dpp
          WHERE d.dpco_id = ab.dep_id
            AND dpp.dpp_id = db.dpp_id
            AND db.dep_id = d.dep_id
            AND abr_id = abrid
            AND ab.abr_etat = 'ATTENTE'
            AND dpp.fou_ordre = fouordre
            AND dpp.rib_ordre = ribordre
            AND d.pco_num = pconum
            AND d.man_id IS NULL
            AND dpp.mod_ordre = modordre;

      CURSOR lesdpcoidsnull IS
         SELECT d.dpco_id
           FROM abricot_bord_selection ab,
                jefy_depense.depense_ctrl_planco d,
                jefy_depense.depense_budget db,
                jefy_depense.depense_papier dpp
          WHERE d.dpco_id = ab.dep_id
            AND dpp.dpp_id = db.dpp_id
            AND db.dep_id = d.dep_id
            AND abr_id = abrid
            AND ab.abr_etat = 'ATTENTE'
            AND dpp.fou_ordre = fouordre
            AND dpp.rib_ordre IS NULL
            AND d.pco_num = pconum
            AND d.man_id IS NULL
            AND dpp.mod_ordre = modordre;

      chainedpcoid   VARCHAR (5000);
      tmpdpcoid      jefy_depense.depense_ctrl_planco.dpco_id%TYPE;
   BEGIN
      SELECT COUNT (*)
        INTO cpt
        FROM DUAL;

      OPEN ndep_mand_fou_rib_pco_mod;

      LOOP
         FETCH ndep_mand_fou_rib_pco_mod
          INTO fouordre, ribordre, pconum, modordre, ht, tva, ttc, budgetaire;

         EXIT WHEN ndep_mand_fou_rib_pco_mod%NOTFOUND;
         chainedpcoid := NULL;

         IF ribordre IS NOT NULL THEN
            OPEN lesdpcoids;

            LOOP
               FETCH lesdpcoids
                INTO tmpdpcoid;

               EXIT WHEN lesdpcoids%NOTFOUND;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            END LOOP;

            CLOSE lesdpcoids;
         ELSE
            OPEN lesdpcoidsnull;

            LOOP
               FETCH lesdpcoidsnull
                INTO tmpdpcoid;

               EXIT WHEN lesdpcoidsnull%NOTFOUND;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            END LOOP;

            CLOSE lesdpcoidsnull;
         END IF;

         chainedpcoid := chainedpcoid || '$';
-- creation des mandats des pids
         cpt := set_mandat_depenses (chainedpcoid, borid);
      END LOOP;

      CLOSE ndep_mand_fou_rib_pco_mod;

      RETURN cpt;
   END;

-- procedures de verifications
   FUNCTION selection_valide (abrid INTEGER)
      RETURN INTEGER IS
      cpt   INTEGER;
   BEGIN
      SELECT COUNT (*)
        INTO cpt
        FROM DUAL;

-- meme exercice

      -- si PI somme recette = somme depense

      -- recette_valides

      -- depense_valides
      RETURN cpt;
   END;

   FUNCTION recette_valide (recid INTEGER)
      RETURN INTEGER IS
      cpt   INTEGER;
   BEGIN
      SELECT COUNT (*)
        INTO cpt
        FROM DUAL;

      RETURN cpt;
   END;

   FUNCTION depense_valide (depid INTEGER)
      RETURN INTEGER IS
      cpt   INTEGER;
   BEGIN
      SELECT COUNT (*)
        INTO cpt
        FROM DUAL;

      RETURN cpt;
   END;

   FUNCTION verif_bordereau_selection (borid INTEGER, abrid INTEGER)
      RETURN INTEGER IS
      cpt   INTEGER;
   BEGIN
      SELECT COUNT (*)
        INTO cpt
        FROM DUAL;

-- verifier sum TTC depense selection  = sum TTC mandat du bord

      -- verifier sum TTC recette selection = sum TTC titre du bord

      -- verifier sum TTC depense  = sum TTC mandat du bord

      -- verifier sum TTC recette  = sum TTC titre  du bord
      RETURN cpt;
   END;

-- procedures de locks de transaction
   PROCEDURE lock_mandats IS
      cpt   INTEGER;
   BEGIN
      SELECT COUNT (*)
        INTO cpt
        FROM DUAL;
   END;

   PROCEDURE lock_titres IS
      cpt   INTEGER;
   BEGIN
      SELECT COUNT (*)
        INTO cpt
        FROM DUAL;
   END;

   PROCEDURE get_depense_jefy_depense (manid INTEGER) IS
      depid               depense.dep_id%TYPE;
      jefydepensebudget   jefy_depense.depense_budget%ROWTYPE;
      tmpdepensepapier    jefy_depense.depense_papier%ROWTYPE;
      jefydepenseplanco   jefy_depense.depense_ctrl_planco%ROWTYPE;
      lignebudgetaire     depense.dep_ligne_budgetaire%TYPE;
      fouadresse          depense.dep_adresse%TYPE;
      founom              depense.dep_fournisseur%TYPE;
      lotordre            depense.dep_lot%TYPE;
      marordre            depense.dep_marches%TYPE;
      fouordre            depense.fou_ordre%TYPE;
      gescode             depense.ges_code%TYPE;
      modordre            depense.mod_ordre%TYPE;
      cpt                 INTEGER;
      tcdordre            type_credit.tcd_ordre%TYPE;
      tcdcode             type_credit.tcd_code%TYPE;
      ecd_ordre_ema       ecriture_detail.ecd_ordre%TYPE;
      orgid               INTEGER;

      CURSOR depenses IS
         SELECT db.*
           FROM jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
          WHERE dpco.man_id = manid AND db.dep_id = dpco.dep_id;
   BEGIN
      OPEN depenses;

      LOOP
         FETCH depenses
          INTO jefydepensebudget;

         EXIT WHEN depenses%NOTFOUND;

         -- creation du depid --
         SELECT depense_seq.NEXTVAL
           INTO depid
           FROM DUAL;

         -- creation de lignebudgetaire--
         SELECT org_ub || ' ' || org_cr || ' ' || org_souscr
           INTO lignebudgetaire
           FROM jefy_admin.organ
          WHERE org_id = (SELECT org_id
                            FROM jefy_depense.engage_budget
                           WHERE eng_id = jefydepensebudget.eng_id
                                                                  --AND eng_stat !='A'
                        );

         --recuperer le type de credit a partir de la commande
         SELECT tcd_ordre
           INTO tcdordre
           FROM jefy_depense.engage_budget
          WHERE eng_id = jefydepensebudget.eng_id;

         --AND eng_stat !='A'
         SELECT org_ub, org_id
           INTO gescode, orgid
           FROM jefy_admin.organ
          WHERE org_id = (SELECT org_id
                            FROM jefy_depense.engage_budget
                           WHERE eng_id = jefydepensebudget.eng_id
                                                                  --AND eng_stat !='A'
                        );

         -- fouadresse --
         SELECT    SUBSTR ((adr_adresse1 || ' ' || adr_adresse2 || ' ' || adr_cp || ' ' || adr_ville), 1, 196)
                || '...'
           INTO fouadresse
           FROM v_fournisseur
          WHERE fou_ordre = (SELECT fou_ordre
                               FROM jefy_depense.engage_budget
                              WHERE eng_id = jefydepensebudget.eng_id
                                                                     --AND eng_stat !='A'
                           );

         -- founom --
         SELECT adr_nom || ' ' || adr_prenom
           INTO founom
           FROM v_fournisseur
          WHERE fou_ordre = (SELECT fou_ordre
                               FROM jefy_depense.engage_budget
                              WHERE eng_id = jefydepensebudget.eng_id
                                                                     --AND eng_stat !='A'
                           );

         -- fouordre --
         SELECT fou_ordre
           INTO fouordre
           FROM jefy_depense.engage_budget
          WHERE eng_id = jefydepensebudget.eng_id;

         --AND eng_stat !='A'

         -- lotordre --
         SELECT COUNT (*)
           INTO cpt
           FROM jefy_marches.attribution
          WHERE att_ordre = (SELECT att_ordre
                               FROM jefy_depense.engage_ctrl_marche
                              WHERE eng_id = jefydepensebudget.eng_id);

         IF cpt = 0 THEN
            lotordre := NULL;
         ELSE
            SELECT lot_ordre
              INTO lotordre
              FROM jefy_marches.attribution
             WHERE att_ordre = (SELECT att_ordre
                                  FROM jefy_depense.engage_ctrl_marche
                                 WHERE eng_id = jefydepensebudget.eng_id);
         END IF;

         -- marordre --
         SELECT COUNT (*)
           INTO cpt
           FROM jefy_marches.lot
          WHERE lot_ordre = lotordre;

         IF cpt = 0 THEN
            marordre := NULL;
         ELSE
            SELECT mar_ordre
              INTO marordre
              FROM jefy_marches.lot
             WHERE lot_ordre = lotordre;
         END IF;

         --MOD_ORDRE --
         SELECT mod_ordre
           INTO modordre
           FROM jefy_depense.depense_papier
          WHERE dpp_id = jefydepensebudget.dpp_id;

         -- recuperer l'ecriture_detail pour emargements semi-auto
         SELECT ecd_ordre
           INTO ecd_ordre_ema
           FROM jefy_depense.depense_ctrl_planco
          WHERE dep_id = jefydepensebudget.dep_id;

         -- recup de la depense papier
         SELECT *
           INTO tmpdepensepapier
           FROM jefy_depense.depense_papier
          WHERE dpp_id = jefydepensebudget.dpp_id;

         -- recup des infos de depense_ctrl_planco
         SELECT *
           INTO jefydepenseplanco
           FROM jefy_depense.depense_ctrl_planco
          WHERE dep_id = jefydepensebudget.dep_id;

         -- creation de la depense --
         INSERT INTO depense
              VALUES (fouadresse,                                                               --DEP_ADRESSE,
                      NULL,                                                                 --DEP_DATE_COMPTA,
                      tmpdepensepapier.dpp_date_reception,                               --DEP_DATE_RECEPTION,
                      tmpdepensepapier.dpp_date_service_fait,                              --DEP_DATE_SERVICE,
                      'VALIDE',                                                                    --DEP_ETAT,
                      founom,                                                               --DEP_FOURNISSEUR,
                      jefydepenseplanco.dpco_montant_budgetaire,                                     --DEP_HT,
                      depense_seq.NEXTVAL,                                                           --DEP_ID,
                      lignebudgetaire,                                                 --DEP_LIGNE_BUDGETAIRE,
                      lotordre,                                                                     --DEP_LOT,
                      marordre,                                                                 --DEP_MARCHES,
                      jefydepenseplanco.dpco_ttc_saisie,                              --DEP_MONTANT_DISQUETTE,
                      NULL,                     -- table N !!!jefydepensebudget.cm_ordre , --DEP_NOMENCLATURE,
                      SUBSTR (tmpdepensepapier.dpp_numero_facture, 1, 199),                      --DEP_NUMERO,
                      jefydepenseplanco.dpco_id,                                                  --DEP_ORDRE,
                      NULL,                                                                       --DEP_REJET,
                      tmpdepensepapier.rib_ordre,                                                   --DEP_RIB,
                      'NON',                                                                --DEP_SUPPRESSION,
                      jefydepenseplanco.dpco_ttc_saisie,                                            --DEP_TTC,
                      jefydepenseplanco.dpco_ttc_saisie - jefydepenseplanco.dpco_montant_budgetaire,
                                                                                                   -- DEP_TVA,
                      tmpdepensepapier.exe_ordre,                                                 --EXE_ORDRE,
                      fouordre,                                                                   --FOU_ORDRE,
                      gescode,                                                                     --GES_CODE,
                      manid,                                                                         --MAN_ID,
                      jefydepenseplanco.man_id,                                                   --MAN_ORDRE,
--            jefyfacture.mod_code,  --MOD_ORDRE,
                      modordre,
                      jefydepenseplanco.pco_num,                                                  --PCO_ORDRE,
                      tmpdepensepapier.utl_ordre,                                                  --UTL_ORDRE
                      orgid,                                                                       --org_ordre
                      tcdordre,
                      ecd_ordre_ema,            -- ecd_ordre_ema reference a l'ecriture_detail pour emargement
                      tmpdepensepapier.dpp_date_facture
                     );
      END LOOP;

      CLOSE depenses;
   END;

   PROCEDURE get_recette_jefy_recette (titid INTEGER) IS
      recettepapier          jefy_recette.recette_papier%ROWTYPE;
      recettebudget          jefy_recette.recette_budget%ROWTYPE;
      facturebudget          jefy_recette.facture_budget%ROWTYPE;
      recettectrlplanco      jefy_recette.recette_ctrl_planco%ROWTYPE;
      recettectrlplancotva   jefy_recette.recette_ctrl_planco_tva%ROWTYPE;
      maracujatitre          maracuja.titre%ROWTYPE;
      adrnom                 VARCHAR2 (200);
      letyperecette          VARCHAR2 (200);
      titinterne             VARCHAR2 (200);
      lbud                   VARCHAR2 (200);
      tboordre               INTEGER;
      cpt                    INTEGER;

      CURSOR c_recette IS
         SELECT *
           FROM jefy_recette.recette_ctrl_planco
          WHERE tit_id = titid;
   BEGIN
--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
--SELECT * INTO recettectrlplanco
--FROM  jefy_recette.RECETTE_CTRL_PLANCO
--WHERE tit_id = titid;
      OPEN c_recette;

      LOOP
         FETCH c_recette
          INTO recettectrlplanco;

         EXIT WHEN c_recette%NOTFOUND;

         SELECT *
           INTO recettebudget
           FROM jefy_recette.recette_budget
          WHERE rec_id = recettectrlplanco.rec_id;

         SELECT *
           INTO facturebudget
           FROM jefy_recette.facture_budget
          WHERE fac_id = recettebudget.fac_id;

         SELECT *
           INTO recettepapier
           FROM jefy_recette.recette_papier
          WHERE rpp_id = recettebudget.rpp_id;

         SELECT *
           INTO maracujatitre
           FROM maracuja.titre
          WHERE tit_id = titid;

         IF (recettebudget.rec_id_reduction IS NULL) THEN
            letyperecette := 'R';
         ELSE
            letyperecette := 'T';
         END IF;

         SELECT COUNT (*)
           INTO cpt
           FROM jefy_recette.pi_dep_rec
          WHERE rec_id = recettectrlplanco.rec_id;

         IF cpt > 0 THEN
            titinterne := 'O';
         ELSE
            titinterne := 'N';
         END IF;

         SELECT adr_nom
           INTO adrnom
           FROM grhum.v_fournis_grhum
          WHERE fou_ordre = recettepapier.fou_ordre;

         SELECT org_ub || '/' || org_cr || '/' || org_souscr
           INTO lbud
           FROM jefy_admin.organ
          WHERE org_id = facturebudget.org_id;

         SELECT DISTINCT tbo_ordre
                    INTO tboordre
                    FROM maracuja.titre t, maracuja.bordereau b
                   WHERE b.bor_id = t.bor_id AND t.tit_id = titid;

-- 200 bordereau de presntation interne recette
         IF tboordre = 200 THEN
            tboordre := NULL;
         ELSE
            tboordre := facturebudget.org_id;
         END IF;

         INSERT INTO recette
              VALUES (recettectrlplanco.exe_ordre,                                                --EXE_ORDRE,
                      maracujatitre.ges_code,                                                      --GES_CODE,
                      NULL,                                                                        --MOD_CODE,
                      recettectrlplanco.pco_num,                                                    --PCO_NUM,
                      recettebudget.rec_date_saisie,                         --jefytitre.tit_date,-- REC_DATE,
                      adrnom,                                                                 -- REC_DEBITEUR,
                      recette_seq.NEXTVAL,                                                          -- REC_ID,
                      NULL,                                                                   -- REC_IMPUTTVA,
                      NULL,                                                        -- REC_INTERNE, // TODO ROD
                      facturebudget.fac_lib,                                                   -- REC_LIBELLE,
                      lbud,                                                           -- REC_LIGNE_BUDGETAIRE,
                      'E',                                                                     -- REC_MONNAIE,
                      recettectrlplanco.rpco_ht_saisie,                                                  --HT,
                      recettectrlplanco.rpco_ttc_saisie,                                                --TTC,
                      recettectrlplanco.rpco_ttc_saisie,                                          --DISQUETTE,
                      recettectrlplanco.rpco_tva_saisie,                                     --   REC_MONTTVA,
                      facturebudget.fac_numero,                                                  --   REC_NUM,
                      recettectrlplanco.rpco_id,                                               --   REC_ORDRE,
                      recettepapier.rpp_nb_piece,                                              --   REC_PIECE,
                      facturebudget.fac_numero,                                                  --   REC_REF,
                      'VALIDE',                                                                 --   REC_STAT,
                      'NON',                                                 --    REC_SUPPRESSION,  Modif Rod
                      letyperecette,                                                          --     REC_TYPE,
                      NULL,                                                               --     REC_VIREMENT,
                      titid,                                                                   --      TIT_ID,
                      -titid,                                                               --      TIT_ORDRE,
                      recettebudget.utl_ordre,                                              --       UTL_ORDRE
                      facturebudget.org_id,                                     --       ORG_ORDRE --ajout rod
                      facturebudget.fou_ordre,                                         --FOU_ORDRE --ajout rod
                      NULL,                                                                        --mod_ordre
                      recettepapier.mor_ordre,                                                     --mor_ordre
                      recettepapier.rib_ordre,
                      NULL
                     );
      END LOOP;

      CLOSE c_recette;
   END;

-- procedures du brouillard
   PROCEDURE set_mandat_brouillard (manid INTEGER) IS
      lemandat                  mandat%ROWTYPE;
      pconum_ctrepartie         mandat.pco_num%TYPE;
      pconum_tva                planco_visa.pco_num_tva%TYPE;
      gescodecompta             mandat.ges_code%TYPE;
      pvicontrepartie_gestion   planco_visa.pvi_contrepartie_gestion%TYPE;
      modcontrepartie_gestion   mode_paiement.mod_contrepartie_gestion%TYPE;
      pconum_185                planco_visa.pco_num_tva%TYPE;
      parvalue                  parametre.par_value%TYPE;
      cpt                       INTEGER;
      tboordre                  type_bordereau.tbo_ordre%TYPE;
      sens                      ecriture_detail.ecd_sens%TYPE;
   BEGIN
      SELECT *
        INTO lemandat
        FROM mandat
       WHERE man_id = manid;

      SELECT DISTINCT tbo_ordre
                 INTO tboordre
                 FROM bordereau
                WHERE bor_id IN (SELECT bor_id
                                   FROM mandat
                                  WHERE man_id = manid);

--    select count(*) into cpt from v_titre_prest_interne where man_ordre=lemandat.man_ordre and tit_ordre is not null;
--    if cpt = 0 then
      IF lemandat.prest_id IS NULL THEN
         -- creation du mandat_brouillard visa DEBIT--
         sens := inverser_sens_orv (tboordre, 'D');

         INSERT INTO mandat_brouillard
              VALUES (NULL,                                                                       --ECD_ORDRE,
                      lemandat.exe_ordre,                                                         --EXE_ORDRE,
                      lemandat.ges_code,                                                           --GES_CODE,
                      ABS (lemandat.man_ht),                                                    --MAB_MONTANT,
                      'VISA MANDAT',                                                          --MAB_OPERATION,
                      mandat_brouillard_seq.NEXTVAL,                                              --MAB_ORDRE,
                      sens,                                                                        --MAB_SENS,
                      manid,                                                                         --MAN_ID,
                      lemandat.pco_num                                                                --PCO_NU
                     );

         -- credit=ctrepartie
         --debit = ordonnateur
         -- recup des infos du VISA CREDIT --
         SELECT COUNT (*)
           INTO cpt
           FROM planco_visa
          WHERE pco_num_ordonnateur = lemandat.pco_num AND exe_ordre = lemandat.exe_ordre;

         IF cpt = 0 THEN
            raise_application_error (-20001, 'PROBLEM DE CONTRE PARTIE ' || lemandat.pco_num);
         END IF;

         SELECT pco_num_ctrepartie, pco_num_tva, pvi_contrepartie_gestion
           INTO pconum_ctrepartie, pconum_tva, pvicontrepartie_gestion
           FROM planco_visa
          WHERE pco_num_ordonnateur = lemandat.pco_num AND exe_ordre = lemandat.exe_ordre;

         SELECT COUNT (*)
           INTO cpt
           FROM mode_paiement
          WHERE exe_ordre = lemandat.exe_ordre AND mod_ordre = lemandat.mod_ordre AND pco_num_visa IS NOT NULL;

         IF cpt != 0 THEN
            SELECT pco_num_visa, mod_contrepartie_gestion
              INTO pconum_ctrepartie, modcontrepartie_gestion
              FROM mode_paiement
             WHERE exe_ordre = lemandat.exe_ordre
               AND mod_ordre = lemandat.mod_ordre
               AND pco_num_visa IS NOT NULL;
         END IF;

         -- modif 15/09/2005 compatibilite avec new gestion_exercice
         SELECT c.ges_code, ge.pco_num_185
           INTO gescodecompta, pconum_185
           FROM gestion g, comptabilite c, gestion_exercice ge
          WHERE g.ges_code = lemandat.ges_code
            AND g.com_ordre = c.com_ordre
            AND g.ges_code = ge.ges_code
            AND ge.exe_ordre = lemandat.exe_ordre;

                 -- 5/12/2007
                 -- on ne prend plus le parametre mais PVICONTREPARTIE_GESTION
                 -- PVICONTREPARTIE_GESTION de la table planc_visa dans un premier temps
                 -- dans un second temps il peut etre ecras} par mod_CONTREPARTIE_GESTION de MODE_PAIEMENT
                       --SELECT par_value   INTO parvalue
         --    FROM PARAMETRE
         --    WHERE par_key ='CONTRE PARTIE VISA'
         --    AND exe_ordre = lemandat.exe_ordre;
         parvalue := pvicontrepartie_gestion;

         IF (modcontrepartie_gestion IS NOT NULL) THEN
            parvalue := modcontrepartie_gestion;
         END IF;

         IF parvalue = 'COMPOSANTE' THEN
            gescodecompta := lemandat.ges_code;
         END IF;

         IF pconum_185 IS NULL THEN
            -- creation du mandat_brouillard visa CREDIT --
            sens := inverser_sens_orv (tboordre, 'C');

            IF sens = 'D' THEN
               pconum_ctrepartie := '4632';
            END IF;

            INSERT INTO mandat_brouillard
                 VALUES (NULL,                                                                    --ECD_ORDRE,
                         lemandat.exe_ordre,                                                      --EXE_ORDRE,
                         gescodecompta,                                                            --GES_CODE,
                         ABS (lemandat.man_ttc),                                                --MAB_MONTANT,
                         'VISA MANDAT',                                                       --MAB_OPERATION,
                         mandat_brouillard_seq.NEXTVAL,                                           --MAB_ORDRE,
                         sens,                                                                     --MAB_SENS,
                         manid,                                                                      --MAN_ID,
                         pconum_ctrepartie                                                            --PCO_NU
                        );
         ELSE
            --au SACD --
            sens := inverser_sens_orv (tboordre, 'C');

            IF sens = 'D' THEN
               pconum_ctrepartie := '4632';
            END IF;

            INSERT INTO mandat_brouillard
                 VALUES (NULL,                                                                    --ECD_ORDRE,
                         lemandat.exe_ordre,                                                      --EXE_ORDRE,
                         lemandat.ges_code,                                                        --GES_CODE,
                         ABS (lemandat.man_ttc),                                                --MAB_MONTANT,
                         'VISA MANDAT',                                                       --MAB_OPERATION,
                         mandat_brouillard_seq.NEXTVAL,                                           --MAB_ORDRE,
                         sens,                                                                     --MAB_SENS,
                         manid,                                                                      --MAN_ID,
                         pconum_ctrepartie                                                            --PCO_NU
                        );
         END IF;

         IF lemandat.man_tva != 0 THEN
            -- creation du mandat_brouillard visa CREDIT TVA --
            sens := inverser_sens_orv (tboordre, 'D');

            INSERT INTO mandat_brouillard
                 VALUES (NULL,                                                                    --ECD_ORDRE,
                         lemandat.exe_ordre,                                                      --EXE_ORDRE,
                         lemandat.ges_code,                                                        --GES_CODE,
                         ABS (lemandat.man_tva),                                                --MAB_MONTANT,
                         'VISA TVA',                                                          --MAB_OPERATION,
                         mandat_brouillard_seq.NEXTVAL,                                           --MAB_ORDRE,
                         sens,                                                                     --MAB_SENS,
                         manid,                                                                      --MAN_ID,
                         pconum_tva                                                                   --PCO_NU
                        );
         END IF;
      ELSE
         bordereau_abricot.set_mandat_brouillard_intern (manid);
      END IF;
   END;

   PROCEDURE set_mandat_brouillard_intern (manid INTEGER) IS
      lemandat            mandat%ROWTYPE;
      leplancomptable     plan_comptable%ROWTYPE;
      pconum_ctrepartie   mandat.pco_num%TYPE;
      pconum_tva          planco_visa.pco_num_tva%TYPE;
      gescodecompta       mandat.ges_code%TYPE;
      ctpgescode          mandat.ges_code%TYPE;
      pconum_185          planco_visa.pco_num_tva%TYPE;
      parvalue            parametre.par_value%TYPE;
      cpt                 INTEGER;
      lepconum            plan_comptable_exer.pco_num%TYPE;
   BEGIN
      SELECT *
        INTO lemandat
        FROM mandat
       WHERE man_id = manid;

      -- modif 15/09/2005 compatibilite avec new gestion_exercice
      SELECT c.ges_code, ge.pco_num_185
        INTO gescodecompta, pconum_185
        FROM gestion g, comptabilite c, gestion_exercice ge
       WHERE g.ges_code = lemandat.ges_code
         AND g.com_ordre = c.com_ordre
         AND g.ges_code = ge.ges_code
         AND ge.exe_ordre = lemandat.exe_ordre;

      -- recup des infos du VISA CREDIT --
      SELECT COUNT (*)
        INTO cpt
        FROM planco_visa
       WHERE pco_num_ordonnateur = lemandat.pco_num AND exe_ordre = lemandat.exe_ordre;

      IF cpt = 0 THEN
         raise_application_error (-20001, 'PROBLEM DE CONTRE PARTIE ' || lemandat.pco_num);
      END IF;

      SELECT pco_num_ctrepartie, pco_num_tva
        INTO pconum_ctrepartie, pconum_tva
        FROM planco_visa
       WHERE pco_num_ordonnateur = lemandat.pco_num AND exe_ordre = lemandat.exe_ordre;

   -- verification si le compte existe !
--   SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
--          WHERE pco_num = '18'||lemandat.pco_num;

      --   IF cpt = 0 THEN
--    SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
--    WHERE pco_num = lemandat.pco_num;

      --    maj_plancomptable_mandat (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||lemandat.pco_num);
--   END IF;
--
      lepconum := api_planco.creer_planco_pi (lemandat.exe_ordre, lemandat.pco_num);

--    lemandat.pco_num := '18'||lemandat.pco_num;

      -- creation du mandat_brouillard visa DEBIT--
      INSERT INTO mandat_brouillard
           VALUES (NULL,                                                                          --ECD_ORDRE,
                   lemandat.exe_ordre,                                                            --EXE_ORDRE,
                   lemandat.ges_code,                                                              --GES_CODE,
                   ABS (lemandat.man_ht),                                                       --MAB_MONTANT,
                   'VISA MANDAT',                                                             --MAB_OPERATION,
                   mandat_brouillard_seq.NEXTVAL,                                                 --MAB_ORDRE,
                   'D',                                                                            --MAB_SENS,
                   manid,                                                                            --MAN_ID,
                   '18' || lemandat.pco_num                                                           --PCO_NU
                  );

--   SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
--          WHERE pco_num = '181';

      --   IF cpt = 0 THEN
--        maj_plancomptable_mandat (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'181');

      --   END IF;
      lepconum := api_planco.creer_planco_pi (lemandat.exe_ordre, '181');

      -- planco de CREDIT 181
      -- creation du mandat_brouillard visa CREDIT --

      -- si on est sur un sacd, la contrepartie reste sur le sacd
      IF (pconum_185 IS NOT NULL) THEN
         ctpgescode := lemandat.ges_code;
      ELSE
         ctpgescode := gescodecompta;
      END IF;

      INSERT INTO mandat_brouillard
           VALUES (NULL,                                                                          --ECD_ORDRE,
                   lemandat.exe_ordre,                                                            --EXE_ORDRE,
                   ctpgescode,                                                                     --GES_CODE,
                   ABS (lemandat.man_ttc),                                                      --MAB_MONTANT,
                   'VISA MANDAT',                                                             --MAB_OPERATION,
                   mandat_brouillard_seq.NEXTVAL,                                                 --MAB_ORDRE,
                   'C',                                                                            --MAB_SENS,
                   manid,                                                                            --MAN_ID,
                   '181'                                                                              --PCO_NU
                  );

      IF lemandat.man_tva != 0 THEN
         -- creation du mandat_brouillard visa CREDIT TVA --
         INSERT INTO mandat_brouillard
              VALUES (NULL,                                                                       --ECD_ORDRE,
                      lemandat.exe_ordre,                                                         --EXE_ORDRE,
                      lemandat.ges_code,                                                           --GES_CODE,
                      ABS (lemandat.man_tva),                                                   --MAB_MONTANT,
                      'VISA TVA',                                                             --MAB_OPERATION,
                      mandat_brouillard_seq.NEXTVAL,                                              --MAB_ORDRE,
                      'D',                                                                         --MAB_SENS,
                      manid,                                                                         --MAN_ID,
                      pconum_tva                                                                      --PCO_NU
                     );
      END IF;
   END;

--PROCEDURE maj_plancomptable_mandat (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
--IS
--    niv INTEGER;
--BEGIN

   --

   --    --calcul du niveau
--    SELECT LENGTH(pconum) INTO niv FROM dual;

   --    --
--    INSERT INTO PLAN_COMPTABLE
--         (
--         PCO_BUDGETAIRE,
--         PCO_EMARGEMENT,
--         PCO_LIBELLE,
--         PCO_NATURE,
--         PCO_NIVEAU,
--         PCO_NUM,
--         PCO_SENS_EMARGEMENT,
--         PCO_VALIDITE,
--         PCO_J_EXERCICE,
--         PCO_J_FIN_EXERCICE,
--         PCO_J_BE
--         )
--    VALUES
--     (
--     'N',--PCO_BUDGETAIRE,
--     'O',--PCO_EMARGEMENT,
--     libelle,--PCO_LIBELLE,
--     nature,--PCO_NATURE,
--     niv,--PCO_NIVEAU,
--     pconum,--PCO_NUM,
--     2,--PCO_SENS_EMARGEMENT,
--     'VALIDE',--PCO_VALIDITE,
--     'O',--PCO_J_EXERCICE,
--     'N',--PCO_J_FIN_EXERCICE,
--     'N'--PCO_J_BE
--     );
--END;
   PROCEDURE set_titre_brouillard (titid INTEGER) IS
      letitre             titre%ROWTYPE;
      recettectrlplanco   jefy_recette.recette_ctrl_planco%ROWTYPE;
      lesens              VARCHAR2 (20);
      reduction           INTEGER;
      recid               INTEGER;

      CURSOR c_recettes IS
         SELECT *
           FROM jefy_recette.recette_ctrl_planco
          WHERE tit_id = titid;
   BEGIN
      SELECT *
        INTO letitre
        FROM titre
       WHERE tit_id = titid;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
-- max car titres collectifs exact fetch return more than one row
      SELECT MAX (rb.rec_id_reduction)
        INTO reduction
        FROM jefy_recette.recette_budget rb, jefy_recette.recette_ctrl_planco rcpo
       WHERE rcpo.rec_id = rb.rec_id AND rcpo.tit_id = titid;

-- si dans le cas d une reduction
      IF (reduction IS NOT NULL) THEN
         lesens := 'D';
      ELSE
         lesens := 'C';
      END IF;

      IF letitre.prest_id IS NULL THEN
         OPEN c_recettes;

         LOOP
            FETCH c_recettes
             INTO recettectrlplanco;

            EXIT WHEN c_recettes%NOTFOUND;

            SELECT MAX (rec_id)
              INTO recid
              FROM recette
             WHERE rec_ordre = recettectrlplanco.rpco_id;

            -- creation du titre_brouillard visa --
            --  RECETTE_CTRL_PLANCO
            INSERT INTO titre_brouillard
                        (ecd_ordre,
                         exe_ordre,
                         ges_code,
                         pco_num,
                         tib_montant,
                         tib_operation,
                         tib_ordre,
                         tib_sens,
                         tit_id,
                         rec_id
                        )
               SELECT NULL,                                                                       --ECD_ORDRE,
                           recettectrlplanco.exe_ordre,                                           --EXE_ORDRE,
                                                       letitre.ges_code,                           --GES_CODE,
                                                                        recettectrlplanco.pco_num,   --PCO_NUM
                      ABS (recettectrlplanco.rpco_ht_saisie),                                   --TIB_MONTANT,
                                                             'VISA TITRE',                    --TIB_OPERATION,
                                                                          titre_brouillard_seq.NEXTVAL,
                                                                                                  --TIB_ORDRE,
                      lesens,                                                                      --TIB_SENS,
                             titid,                                                                  --TIT_ID,
                                   recid
                 FROM jefy_recette.recette_ctrl_planco
                WHERE rpco_id = recettectrlplanco.rpco_id;

            -- recette_ctrl_planco_tva
            INSERT INTO titre_brouillard
                        (ecd_ordre,
                         exe_ordre,
                         ges_code,
                         pco_num,
                         tib_montant,
                         tib_operation,
                         tib_ordre,
                         tib_sens,
                         tit_id,
                         rec_id
                        )
               SELECT NULL,                                                                       --ECD_ORDRE,
                           exe_ordre,                                                             --EXE_ORDRE,
                                     ges_code,                                                     --GES_CODE,
                                              pco_num,                                               --PCO_NUM
                                                      ABS (rpcotva_tva_saisie),                 --TIB_MONTANT,
                                                                               'VISA TITRE',  --TIB_OPERATION,
                      titre_brouillard_seq.NEXTVAL,                                               --TIB_ORDRE,
                                                   lesens,                                         --TIB_SENS,
                                                          titid,                                     --TIT_ID,
                                                                recid
                 FROM jefy_recette.recette_ctrl_planco_tva
                WHERE rpco_id = recettectrlplanco.rpco_id;

            -- recette_ctrl_planco_ctp
            INSERT INTO titre_brouillard
                        (ecd_ordre,
                         exe_ordre,
                         ges_code,
                         pco_num,
                         tib_montant,
                         tib_operation,
                         tib_ordre,
                         tib_sens,
                         tit_id,
                         rec_id
                        )
               SELECT NULL,                                                                       --ECD_ORDRE,
                           recettectrlplanco.exe_ordre,                                           --EXE_ORDRE,
                                                       ges_code,                                   --GES_CODE,
                                                                pco_num,                             --PCO_NUM
                                                                        ABS (rpcoctp_ttc_saisie),
                                                                                                --TIB_MONTANT,
                      'VISA TITRE',                                                           --TIB_OPERATION,
                                   titre_brouillard_seq.NEXTVAL,                                  --TIB_ORDRE,
                                                                inverser_sens (lesens),            --TIB_SENS,
                                                                                       titid,        --TIT_ID,
                                                                                             recid
                 FROM jefy_recette.recette_ctrl_planco_ctp
                WHERE rpco_id = recettectrlplanco.rpco_id;
         END LOOP;

         CLOSE c_recettes;
      ELSE
         set_titre_brouillard_intern (titid);
      END IF;

      -- suppression des lignes d ecritures a ZERO
      DELETE FROM titre_brouillard
            WHERE tib_montant = 0;
   END;

   PROCEDURE set_titre_brouillard_intern (titid INTEGER) IS
      letitre             titre%ROWTYPE;
      recettectrlplanco   jefy_recette.recette_ctrl_planco%ROWTYPE;
      lesens              VARCHAR2 (20);
      reduction           INTEGER;
      lepconum            maracuja.plan_comptable.pco_num%TYPE;
      libelle             maracuja.plan_comptable.pco_libelle%TYPE;
      chap                VARCHAR2 (2);
      recid               INTEGER;
      gescodecompta       maracuja.titre.ges_code%TYPE;
      ctpgescode          titre.ges_code%TYPE;
      pconum_185          gestion_exercice.pco_num_185%TYPE;

      CURSOR c_recettes IS
         SELECT *
           FROM jefy_recette.recette_ctrl_planco
          WHERE tit_id = titid;
   BEGIN
      SELECT *
        INTO letitre
        FROM titre
       WHERE tit_id = titid;

      -- modif fred 04/2007
      SELECT c.ges_code, ge.pco_num_185
        INTO gescodecompta, pconum_185
        FROM gestion g, comptabilite c, gestion_exercice ge
       WHERE g.ges_code = letitre.ges_code
         AND g.com_ordre = c.com_ordre
         AND g.ges_code = ge.ges_code
         AND ge.exe_ordre = letitre.exe_ordre;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
      SELECT rb.rec_id_reduction
        INTO reduction
        FROM jefy_recette.recette_budget rb, jefy_recette.recette_ctrl_planco rcpo
       WHERE rcpo.rec_id = rb.rec_id AND rcpo.tit_id = titid;

      -- si dans le cas d une reduction
      IF (reduction IS NOT NULL) THEN
         lesens := 'D';
      ELSE
         lesens := 'C';
      END IF;

      OPEN c_recettes;

      LOOP
         FETCH c_recettes
          INTO recettectrlplanco;

         EXIT WHEN c_recettes%NOTFOUND;

         SELECT MAX (rec_id)
           INTO recid
           FROM recette
          WHERE rec_ordre = recettectrlplanco.rpco_id;

         -- recup des 2 premiers caracteres du compte
         SELECT SUBSTR (recettectrlplanco.pco_num, 1, 2)
           INTO chap
           FROM DUAL;

         IF chap != '18' THEN
--     select pco_libelle into libelle
--     from maracuja.plan_comptable
--     where pco_num = RECETTEctrlplanco.pco_num;

            --     lepconum := '18'||RECETTEctrlplanco.pco_num;
--     maj_plancomptable_titre('R',libelle,lepconum);
            lepconum := api_planco.creer_planco_pi (recettectrlplanco.exe_ordre, recettectrlplanco.pco_num);
         END IF;

         -- creation du titre_brouillard visa --
         --  RECETTE_CTRL_PLANCO
         INSERT INTO titre_brouillard
                     (ecd_ordre,
                      exe_ordre,
                      ges_code,
                      pco_num,
                      tib_montant,
                      tib_operation,
                      tib_ordre,
                      tib_sens,
                      tit_id,
                      rec_id
                     )
            SELECT NULL,                                                                          --ECD_ORDRE,
                        recettectrlplanco.exe_ordre,                                              --EXE_ORDRE,
                                                    letitre.ges_code,                              --GES_CODE,
                                                                     lepconum,                       --PCO_NUM
                   ABS (recettectrlplanco.rpco_ht_saisie),                                      --TIB_MONTANT,
                                                          'VISA TITRE',                       --TIB_OPERATION,
                                                                       titre_brouillard_seq.NEXTVAL,
                                                                                                  --TIB_ORDRE,
                                                                                                    lesens,
                                                                                                   --TIB_SENS,
                   titid,                                                                            --TIT_ID,
                         recid
              FROM jefy_recette.recette_ctrl_planco
             WHERE rpco_id = recettectrlplanco.rpco_id;

         -- recette_ctrl_planco_tva
         INSERT INTO titre_brouillard
                     (ecd_ordre,
                      exe_ordre,
                      ges_code,
                      pco_num,
                      tib_montant,
                      tib_operation,
                      tib_ordre,
                      tib_sens,
                      tit_id,
                      rec_id
                     )
            SELECT NULL,                                                                          --ECD_ORDRE,
                        exe_ordre,                                                                --EXE_ORDRE,
                                  gescodecompta,
                                                -- ges_code,               --GES_CODE,
                                                pco_num,                                             --PCO_NUM
                                                        ABS (rpcotva_tva_saisie),               --TIB_MONTANT,
                                                                                 'VISA TITRE',
                                                                                              --TIB_OPERATION,
                   titre_brouillard_seq.NEXTVAL,                                                  --TIB_ORDRE,
                                                inverser_sens (lesens),                            --TIB_SENS,
                                                                       titid,                        --TIT_ID,
                                                                             recid
              FROM jefy_recette.recette_ctrl_planco_tva
             WHERE rpco_id = recettectrlplanco.rpco_id;

         -- si on est sur un sacd, la contrepartie reste sur le sacd
         IF (pconum_185 IS NOT NULL) THEN
            ctpgescode := letitre.ges_code;
         ELSE
            ctpgescode := gescodecompta;
         END IF;

         -- recette_ctrl_planco_ctp on force le 181
         INSERT INTO titre_brouillard
                     (ecd_ordre,
                      exe_ordre,
                      ges_code,
                      pco_num,
                      tib_montant,
                      tib_operation,
                      tib_ordre,
                      tib_sens,
                      tit_id,
                      rec_id
                     )
            SELECT NULL,                                                                          --ECD_ORDRE,
                        recettectrlplanco.exe_ordre,                                              --EXE_ORDRE,
                                                    ctpgescode,                                    --GES_CODE,
                                                               '181',                                --PCO_NUM
                                                                     ABS (rpcoctp_ttc_saisie),  --TIB_MONTANT,
                                                                                              'VISA TITRE',
                                                                                              --TIB_OPERATION,
                   titre_brouillard_seq.NEXTVAL,                                                  --TIB_ORDRE,
                                                inverser_sens (lesens),                            --TIB_SENS,
                                                                       titid,                        --TIT_ID,
                                                                             recid
              FROM jefy_recette.recette_ctrl_planco_ctp
             WHERE rpco_id = recettectrlplanco.rpco_id;
      END LOOP;

      CLOSE c_recettes;
   END;

--PROCEDURE maj_plancomptable_titre (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
--IS
--  niv INTEGER;
--  cpt integer;
--BEGIN

   --select count(*) into cpt from PLAN_COMPTABLE
--where pco_num = pconum;

   --if cpt = 0 then
--    --calcul du niveau
--    SELECT LENGTH(pconum) INTO niv FROM dual;

   --    --
--    INSERT INTO PLAN_COMPTABLE (PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE, PCO_NATURE, PCO_NIVEAU, PCO_NUM, PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, PCO_J_BE)
--        VALUES
--        (
--        'N',--PCO_BUDGETAIRE,
--        'O',--PCO_EMARGEMENT,
--        libelle,--PCO_LIBELLE,
--        nature,--PCO_NATURE,
--        niv,--PCO_NIVEAU,
--        pconum,--PCO_NUM,
--        2,--PCO_SENS_EMARGEMENT,
--        'VALIDE',--PCO_VALIDITE,
--        'O',--PCO_J_EXERCICE,
--        'N',--PCO_J_FIN_EXERCICE,
--        'N'--PCO_J_BE
--        );
--end if;

   --END;

   -- outils
   FUNCTION inverser_sens_orv (tboordre INTEGER, sens VARCHAR)
      RETURN VARCHAR IS
      cpt   INTEGER;
   BEGIN
-- si c est un bordereau de mandat li?es aux ORV
-- on inverse le sens de tous les details ecritures
-- (meme dans le cas des SACD de m.....)
      SELECT COUNT (*)
        INTO cpt
        FROM type_bordereau
       WHERE tbo_sous_type = 'REVERSEMENTS' AND tbo_ordre = tboordre;

      IF (cpt != 0) THEN
         IF (sens = 'C') THEN
            RETURN 'D';
         ELSE
            RETURN 'C';
         END IF;
      END IF;

      RETURN sens;
   END;

   FUNCTION recup_gescode (abrid INTEGER)
      RETURN VARCHAR IS
      gescode   bordereau.ges_code%TYPE;
   BEGIN
      SELECT DISTINCT ges_code
                 INTO gescode
                 FROM abricot_bord_selection
                WHERE abr_id = abrid;

      RETURN gescode;
   END;

   FUNCTION recup_utlordre (abrid INTEGER)
      RETURN INTEGER IS
      utlordre   bordereau.utl_ordre%TYPE;
   BEGIN
      SELECT DISTINCT utl_ordre
                 INTO utlordre
                 FROM abricot_bord_selection
                WHERE abr_id = abrid;

      RETURN utlordre;
   END;

   FUNCTION recup_exeordre (abrid INTEGER)
      RETURN INTEGER IS
      exeordre   bordereau.exe_ordre%TYPE;
   BEGIN
      SELECT DISTINCT exe_ordre
                 INTO exeordre
                 FROM abricot_bord_selection
                WHERE abr_id = abrid;

      RETURN exeordre;
   END;

   FUNCTION recup_tboordre (abrid INTEGER)
      RETURN INTEGER IS
      tboordre   bordereau.tbo_ordre%TYPE;
   BEGIN
      SELECT DISTINCT tbo_ordre
                 INTO tboordre
                 FROM abricot_bord_selection
                WHERE abr_id = abrid;

      RETURN tboordre;
   END;

   FUNCTION recup_groupby (abrid INTEGER)
      RETURN VARCHAR IS
      abrgroupby   abricot_bord_selection.abr_group_by%TYPE;
   BEGIN
      SELECT DISTINCT abr_group_by
                 INTO abrgroupby
                 FROM abricot_bord_selection
                WHERE abr_id = abrid;

      RETURN abrgroupby;
   END;

   FUNCTION inverser_sens (sens VARCHAR)
      RETURN VARCHAR IS
   BEGIN
      IF sens = 'D' THEN
         RETURN 'C';
      ELSE
         RETURN 'D';
      END IF;
   END;

   PROCEDURE numeroter_bordereau (borid INTEGER) IS
      cpt_mandat   INTEGER;
      cpt_titre    INTEGER;
   BEGIN
      SELECT COUNT (*)
        INTO cpt_mandat
        FROM mandat
       WHERE bor_id = borid;

      SELECT COUNT (*)
        INTO cpt_titre
        FROM titre
       WHERE bor_id = borid;

      IF cpt_mandat + cpt_titre = 0 THEN
         raise_application_error (-20001, 'Bordereau  vide');
      ELSE
         numerotationobject.numeroter_bordereau (borid);
-- boucle mandat
         numerotationobject.numeroter_mandat (borid);
-- boucle titre
         numerotationobject.numeroter_titre (borid);
      END IF;
   END;

   FUNCTION traiter_orgid (orgid INTEGER, exeordre INTEGER)
      RETURN INTEGER IS
      topordre     INTEGER;
      cpt          INTEGER;
      orilibelle   origine.ori_libelle%TYPE;
      convordre    INTEGER;
   BEGIN
      IF orgid IS NULL THEN
         RETURN NULL;
      END IF;

      SELECT COUNT (*)
        INTO cpt
        FROM accords.convention_limitative
       WHERE org_id = orgid AND exe_ordre = exeordre;

      IF cpt > 0 THEN
         -- recup du type_origine CONVENTION--
         SELECT top_ordre
           INTO topordre
           FROM type_operation
          WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

         SELECT DISTINCT con_ordre
                    INTO convordre
                    FROM accords.convention_limitative
                   WHERE org_id = orgid AND exe_ordre = exeordre;

         SELECT (exe_ordre || '-' || LPAD (con_index, 5, '0') || ' ' || con_objet)
           INTO orilibelle
           FROM accords.contrat
          WHERE con_ordre = convordre;
      ELSE
         SELECT COUNT (*)
           INTO cpt
           FROM jefy_admin.organ
          WHERE org_id = orgid AND org_lucrativite = 1;

         IF cpt = 1 THEN
            -- recup du type_origine OPERATION LUCRATIVE --
            SELECT top_ordre
              INTO topordre
              FROM type_operation
             WHERE top_libelle = 'OPERATION LUCRATIVE';

            --le libelle utilisateur pour le suivie en compta --
            SELECT org_ub || '-' || org_cr || '-' || org_souscr
              INTO orilibelle
              FROM jefy_admin.organ
             WHERE org_id = orgid;
         ELSE
            RETURN NULL;
         END IF;
      END IF;

-- l origine est t elle deja  suivie --
      SELECT COUNT (*)
        INTO cpt
        FROM origine
       WHERE ori_key_name = 'ORG_ID' AND ori_entite = 'JEFY_ADMIN.ORGAN' AND ori_key_entite = orgid;

      IF cpt >= 1 THEN
         SELECT ori_ordre
           INTO cpt
           FROM origine
          WHERE ori_key_name = 'ORG_ID'
            AND ori_entite = 'JEFY_ADMIN.ORGAN'
            AND ori_key_entite = orgid
            AND ROWNUM = 1;
      ELSE
         SELECT origine_seq.NEXTVAL
           INTO cpt
           FROM DUAL;

         INSERT INTO origine
                     (ori_entite,
                      ori_key_name,
                      ori_libelle,
                      ori_ordre,
                      ori_key_entite,
                      top_ordre
                     )
              VALUES ('JEFY_ADMIN',
                      'ORG_ID',
                      orilibelle,
                      cpt,
                      orgid,
                      topordre
                     );
      END IF;

      RETURN cpt;
   END;

   PROCEDURE controle_bordereau (borid INTEGER) IS
      ttc             maracuja.titre.tit_ttc%TYPE;
      detailttc       maracuja.titre.tit_ttc%TYPE;
      ordottc         maracuja.titre.tit_ttc%TYPE;
      debit           maracuja.titre.tit_ttc%TYPE;
      credit          maracuja.titre.tit_ttc%TYPE;
      cpt             INTEGER;
      MESSAGE         VARCHAR2 (50);
      messagedetail   VARCHAR2 (50);
   BEGIN
      SELECT COUNT (*)
        INTO cpt
        FROM maracuja.titre
       WHERE bor_id = borid;

      IF cpt = 0 THEN
-- somme des maracuja.titre
         SELECT SUM (man_ttc)
           INTO ttc
           FROM maracuja.mandat
          WHERE bor_id = borid;

--somme des maracuja.recette
         SELECT SUM (d.dep_ttc)
           INTO detailttc
           FROM maracuja.mandat m, maracuja.depense d
          WHERE m.man_id = d.man_id AND m.bor_id = borid;

-- la somme des credits
         SELECT SUM (mab_montant)
           INTO credit
           FROM maracuja.mandat m, maracuja.mandat_brouillard mb
          WHERE bor_id = borid AND m.man_id = mb.man_id AND mb.mab_sens = 'C';

-- la somme des debits
         SELECT SUM (mab_montant)
           INTO debit
           FROM maracuja.mandat m, maracuja.mandat_brouillard mb
          WHERE bor_id = borid AND m.man_id = mb.man_id AND mb.mab_sens = 'D';

-- somme des jefy.recette
         SELECT SUM (d.dpco_ttc_saisie)
           INTO ordottc
           FROM maracuja.mandat m, jefy_depense.depense_ctrl_planco d
          WHERE m.man_id = d.man_id AND m.bor_id = borid;

         MESSAGE := ' mandats ';
         messagedetail := ' depenses ';
      ELSE
-- somme des maracuja.titre
         SELECT SUM (tit_ttc)
           INTO ttc
           FROM maracuja.titre
          WHERE bor_id = borid;

--somme des maracuja.recette
         SELECT SUM (r.rec_monttva + r.rec_mont)
           INTO detailttc
           FROM maracuja.titre t, maracuja.recette r
          WHERE t.tit_id = r.tit_id AND t.bor_id = borid;

-- la somme des credits
         SELECT SUM (tib_montant)
           INTO credit
           FROM maracuja.titre t, maracuja.titre_brouillard tb
          WHERE bor_id = borid AND t.tit_id = tb.tit_id AND tb.tib_sens = 'C';

-- la somme des debits
         SELECT SUM (tib_montant)
           INTO debit
           FROM maracuja.titre t, maracuja.titre_brouillard tb
          WHERE bor_id = borid AND t.tit_id = tb.tit_id AND tb.tib_sens = 'D';

-- somme des jefy.recette
         SELECT SUM (r.rpco_ttc_saisie)
           INTO ordottc
           FROM maracuja.titre t, jefy_recette.recette_ctrl_planco r
          WHERE t.tit_id = r.tit_id AND t.bor_id = borid;

         MESSAGE := ' titres ';
         messagedetail := ' recettes ';
      END IF;

-- la somme des credits = sommes des debits
      IF (NVL (debit, 0) != NVL (credit, 0)) THEN
         raise_application_error (-20001,
                                  'PROBLEME DE ' || MESSAGE || ' :  debit <> credit : ' || debit || ' '
                                  || credit
                                 );
      END IF;

-- la somme des credits = sommes des debits
      IF (NVL (debit, 0) != NVL (credit, 0)) THEN
         raise_application_error (-20001,
                                     'PROBLEME DE '
                                  || MESSAGE
                                  || ' :  ecriture <> budgetaire : '
                                  || debit
                                  || ' '
                                  || ttc
                                 );
      END IF;

-- somme des maracuja.titre = somme des maracuja.recette
      IF (NVL (ttc, 0) != NVL (detailttc, 0)) THEN
         raise_application_error (-20001,
                                     'PROBLEME DE '
                                  || MESSAGE
                                  || ' : montant des '
                                  || MESSAGE
                                  || ' <>  du montant des '
                                  || messagedetail
                                  || ' :'
                                  || ttc
                                  || ' '
                                  || detailttc
                                 );
      END IF;

-- somme des jefy.recette = somme des maracuja.recette
      IF (NVL (ttc, 0) != NVL (ordottc, 0)) THEN
         raise_application_error (-20001,
                                     'PROBLEME DE '
                                  || MESSAGE
                                  || ' : montant des '
                                  || MESSAGE
                                  || ' <>  du montant ordonnateur des '
                                  || messagedetail
                                  || ' :'
                                  || ttc
                                  || ' '
                                  || ordottc
                                 );
      END IF;

      bordereau_abricot.ctrl_date_exercice (borid);
   END;

   PROCEDURE get_recette_prelevements (titid INTEGER) IS
      cpt                      INTEGER;
      facture_titre_data       prestation.facture_titre%ROWTYPE;
      --client_data              prelev.client%ROWTYPE;
      oriordre                 INTEGER;
      modordre                 INTEGER;
      recid                    INTEGER;
      echeid                   INTEGER;
      echeancier_data          jefy_echeancier.echeancier%ROWTYPE;
      echeancier_prelev_data   jefy_echeancier.echeancier_prelev%ROWTYPE;
      facture_data             jefy_recette.facture_budget%ROWTYPE;
      personne_data            grhum.v_personne%ROWTYPE;
      premieredate             DATE;
   BEGIN
-- verifier s il existe un echancier pour ce titre
      SELECT COUNT (*)
        INTO cpt
        FROM jefy_recette.recette_ctrl_planco pco, jefy_recette.recette r, jefy_recette.facture f
       WHERE pco.tit_id = titid
         AND pco.rec_id = r.rec_id
         AND r.fac_id = f.fac_id
         AND eche_id IS NOT NULL
         AND r.rec_id_reduction IS NULL;

      IF (cpt != 1) THEN
         RETURN;
      END IF;

-- recup du eche_id / ech_id
      SELECT eche_id
        INTO echeid
        FROM jefy_recette.recette_ctrl_planco pco, jefy_recette.recette r, jefy_recette.facture f
       WHERE pco.tit_id = titid
         AND pco.rec_id = r.rec_id
         AND r.fac_id = f.fac_id
         AND eche_id IS NOT NULL
         AND r.rec_id_reduction IS NULL;

-- recup du des infos du prelevements
      SELECT *
        INTO echeancier_data
        FROM jefy_echeancier.echeancier
       WHERE ech_id = echeid;

      SELECT *
        INTO echeancier_prelev_data
        FROM jefy_echeancier.echeancier_prelev
       WHERE ech_id = echeid;

      SELECT *
        INTO facture_data
        FROM jefy_recette.facture_budget
       WHERE eche_id = echeid;

      SELECT *
        INTO personne_data
        FROM grhum.v_personne
       WHERE pers_id = facture_data.pers_id;

      SELECT echd_date_prevue
        INTO premieredate
        FROM jefy_echeancier.echeancier_detail
       WHERE echd_numero = 1 AND ech_id = echeid;

      SELECT rec_id
        INTO recid
        FROM recette
       WHERE tit_id = titid;

/*
-- verification / mise a jour du mode de recouvrement
SELECT mor_ordre INTO modordre FROM maracuja.TITRE WHERE tit_id=titid;
IF (modordre IS NULL) THEN
   SELECT COUNT(*) INTO cpt FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;
   IF (cpt=0) THEN
         RAISE_APPLICATION_ERROR (-20001,'MODE RECOUVREMENT ECHEANCIER NON DEFINI');
   END IF;
   IF (cpt>1) THEN
         RAISE_APPLICATION_ERROR (-20001,'PLUSIEURS MODE RECOUVREMENT ECHEANCIER DEFINIS. IMPOSSIBLE DE DETERMINER.');
   END IF;

   SELECT mod_ordre INTO modordre FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;

   UPDATE TITRE SET mor_ordre=modordre WHERE tit_id=titid;
END IF;
*/

      -- recup ??
      oriordre := gestionorigine.traiter_orgid (facture_data.org_id, facture_data.exe_ordre);

      INSERT INTO maracuja.echeancier
                  (eche_autoris_signee,
                   fou_ordre_client,
                   con_ordre,
                   eche_date_1ere_echeance,
                   eche_date_creation,
                   eche_date_modif,
                   eche_echeancier_ordre,
                   eche_etat_prelevement,
                   ft_ordre,
                   eche_libelle,
                   eche_montant,
                   eche_montant_en_lettres,
                   eche_nombre_echeances,
                   eche_numero_index,
                   org_ordre,
                   prest_ordre,
                   eche_prise_en_charge,
                   eche_ref_facture_externe,
                   eche_supprime,
                   exe_ordre,
                   tit_id,
                   rec_id,
                   tit_ordre,
                   ori_ordre,
                   pers_id,
                   org_id,
                   pers_description
                  )
           VALUES ('O',                                                                  --ECHE_AUTORIS_SIGNEE
                   facture_data.fou_ordre,                                                  --FOU_ORDRE_CLIENT
                   NULL,                                              --echancier_data.CON_ORDRE  ,--CON_ORDRE
                   premieredate,               --echancier_data.DATE_1ERE_ECHEANCE  ,--ECHE_DATE_1ERE_ECHEANCE
                   SYSDATE,                              --echancier_data.DATE_CREATION  ,--ECHE_DATE_CREATION
                   SYSDATE,                                    --echancier_data.DATE_MODIF  ,--ECHE_DATE_MODIF
                   echeancier_data.ech_id,         --echancier_data.ECHEANCIER_ORDRE  ,--ECHE_ECHEANCIER_ORDRE
                   'V',                            --echancier_data.ETAT_PRELEVEMENT  ,--ECHE_ETAT_PRELEVEMENT
                   facture_data.fac_id,                                 --echancier_data.FT_ORDRE  ,--FT_ORDRE
                   echeancier_data.ech_libelle,                        --echancier_data.LIBELLE,--ECHE_LIBELLE
                   echeancier_data.ech_montant,                                                 --ECHE_MONTANT
                   echeancier_data.ech_montant_lettres,                              --ECHE_MONTANT_EN_LETTRES
                   echeancier_data.ech_nb_echeances,                                   --ECHE_NOMBRE_ECHEANCES
                   echeancier_data.ech_id,                --echeancier_data.NUMERO_INDEX  ,--ECHE_NUMERO_INDEX
                   facture_data.org_id,                              --echeancier_data.ORG_ORDRE  ,--ORG_ORDRE
                   NULL,                                         --echeancier_data.PREST_ORDRE  ,--PREST_ORDRE
                   'O',                                                                 --ECHE_PRISE_EN_CHARGE
                   facture_data.fac_lib,     --cheancier_data.REF_FACTURE_EXTERNE  ,--ECHE_REF_FACTURE_EXTERNE
                   'N',                                                                        --ECHE_SUPPRIME
                   facture_data.exe_ordre,                                                         --EXE_ORDRE
                   titid,
                   recid,                                                                            --REC_ID,
                   -titid,
                   oriordre,                                                                      --ORI_ORDRE,
                   personne_data.pers_id,                                    --CLIENT_data.pers_id  ,--PERS_ID
                   facture_data.org_id,                                          --orgid a faire plus tard....
                   personne_data.pers_libelle                                           --    PERS_DESCRIPTION
                  );

      INSERT INTO maracuja.prelevement
                  (eche_echeancier_ordre,
                   reco_ordre,
                   fou_ordre,
                   prel_commentaire,
                   prel_date_modif,
                   prel_date_prelevement,
                   prel_prelev_date_saisie,
                   prel_prelev_etat,
                   prel_numero_index,
                   prel_prelev_montant,
                   prel_prelev_ordre,
                   rib_ordre,
                   prel_etat_maracuja
                  )
         SELECT ech_id,                                                                --ECHE_ECHEANCIER_ORDRE
                       NULL,                                                                 --PREL_FICP_ORDRE
                            facture_data.fou_ordre,                                                --FOU_ORDRE
                                                   echd_commentaire,                        --PREL_COMMENTAIRE
                                                                    SYSDATE,    --DATE_MODIF,--PREL_DATE_MODIF
                                                                            echd_date_prevue,
                                                                                       --PREL_DATE_PRELEVEMENT
                                                                                             SYSDATE,
                                                                                  --,--PREL_PRELEV_DATE_SAISIE
                'ATTENTE',                                                                  --PREL_PRELEV_ETAT
                          echd_numero,                                                     --PREL_NUMERO_INDEX
                                      echd_montant,                                      --PREL_PRELEV_MONTANT
                                                   echd_id,                                --PREL_PRELEV_ORDRE
                                                           echeancier_prelev_data.rib_ordre_debiteur,
                
                --RIB_ORDRE
                'ATTENTE'                                                                 --PREL_ETAT_MARACUJA
           FROM jefy_echeancier.echeancier_detail
          WHERE ech_id = echeancier_data.ech_id;
   END;

   PROCEDURE ctrl_date_exercice (borid INTEGER) IS
      exeordre   INTEGER;
      annee      INTEGER;
   BEGIN
      SELECT TO_CHAR (bor_date_creation, 'YYYY'), exe_ordre
        INTO annee, exeordre
        FROM bordereau
       WHERE bor_id = borid AND exe_ordre >= 2007;

      IF exeordre <> annee THEN
         UPDATE bordereau
            SET bor_date_creation = TO_DATE ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          WHERE bor_id = borid;

         UPDATE mandat
            SET man_date_remise = TO_DATE ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          WHERE bor_id = borid;

         UPDATE titre
            SET tit_date_remise = TO_DATE ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          WHERE bor_id = borid;
      END IF;
   END;
END;
/








CREATE OR REPLACE PACKAGE MARACUJA.bordereau_abricot_paye is
/*
 * Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic

procedure basculer_bouillard_paye(borid integer);
procedure basculer_bouillard_paye_orv(borid integer);
procedure basculer_bouillard_paye_regul(borid integer);

procedure set_mandat_brouillard(manid integer);
procedure set_mandat_orv_brouillard(manid integer);
procedure set_mandat_regul_brouillard(manid integer);

procedure set_bord_brouillard_visa(borid integer);
procedure set_bord_brouillard_paiement(moisordre number, borid number, exeordre number);
procedure set_bord_brouillard_retenues(borid number);
procedure set_bord_brouillard_sacd(borid number);
end;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Abricot_Paye IS


PROCEDURE basculer_bouillard_paye(borid INTEGER) IS

tmpBordereau maracuja.BORDEREAU%ROWTYPE;
mois VARCHAR2(50);
moiscomplet VARCHAR2(50);
moisordre INTEGER;
cpt INTEGER;

sumdebits NUMBER;
sumcredits NUMBER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN

-- recup des infos du bordereau
SELECT * INTO tmpBordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

-- recup du mois JANVIER XXXX
SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
SELECT mois_ordre,mois_complet INTO moisordre,moiscomplet FROM jefy_paye.paye_mois
WHERE mois_complet = mois;



-- On verifie que les ecritures aient bien ete generees pour la composante en question.
SELECT COUNT(*) INTO cpt FROM jefy_paye.jefy_ecritures WHERE ecr_comp = tmpBordereau.ges_code AND mois_ordre = moisordre;

IF (cpt = 0) THEN
 RAISE_APPLICATION_ERROR(-20001,'Vous n''avez toujours pas pr?par? les ?critures pour la composante '||tmpBordereau.ges_code||' !');
END IF;

-- On verifie que le total des debits soit egal au total des credits  (Pour la composante)
 SELECT SUM(ecr_mont) INTO sumdebits FROM jefy_paye.jefy_ecritures
 WHERE ecr_comp = ges_code AND mois_ordre = moisordre AND ecr_type='64'
 AND ecr_comp = tmpBordereau.ges_code AND ecr_sens = 'D';

 SELECT SUM(ecr_mont) INTO sumcredits FROM jefy_paye.jefy_ecritures
 WHERE ecr_comp = ges_code AND mois_ordre = moisordre AND ecr_type='64'
 AND ecr_comp = tmpBordereau.ges_code AND ecr_sens = 'C';

 IF (sumcredits <> sumdebits)
 THEN
   RAISE_APPLICATION_ERROR(-20001,'Pour la composante '||tmpBordereau.ges_code||', la somme des DEBITS ('||sumdebits||') est diff?rente de la somme des CREDITS ('||sumcredits||') !');
 END IF;

  SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU
  WHERE bor_id = borid
  AND tbo_ordre = tmpBordereau.tbo_ordre
  AND exe_ordre = tmpBordereau.exe_ordre;

  IF cpt = 1 THEN
   Bordereau_Abricot_Paye.set_bord_brouillard_visa(borid);
   Bordereau_Abricot_Paye.set_bord_brouillard_retenues(borid);
   Bordereau_Abricot_Paye.set_bord_brouillard_sacd(borid);

   INSERT INTO maracuja.BORDEREAU_INFO VALUES  (borid, moiscomplet,NULL);

  END IF;
  -- Mise a jour des ecritures de paiement dans bordereau_brouillard
  -- Ces ecritures seront associees a la premiere composante qui mandatera ses payes.
  Bordereau_Abricot_Paye.set_bord_brouillard_paiement(moisordre, borid, tmpBordereau.exe_ordre);

-- misea jour dans papaye des tables apres bascule !
  -- maj de l etat de papaye_compta et du borid -

 UPDATE jefy_paye.jefy_paye_compta SET bor_id=borid, jpc_etat='MANDATEE'
    WHERE ges_code=tmpBordereau.ges_code
      AND mois_ordre=moisordre AND jpc_ETAT='LIQUIDEE';
/*
  update jefy_paye.jefy_liquidations set liq_etat='MANDATEE'
    where ges_code=tmpBordereau.ges_code
      and mois_ordre=moisordre and liq_ETAT='LIQUIDEE';
*/

-- modifications FRED -> BUG REF ECRITURES MANDAT_DETAIL_ECRITURE VU PAR RODOLPHE 23/03/2007
-- on vide la recuperation
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paye.set_mandat_brouillard(manid);
END LOOP;
CLOSE c1;

END;


PROCEDURE basculer_bouillard_paye_orv(borid INTEGER) IS
cpt INTEGER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN
-- on vide la recuperation du brouillard des mandats !
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits et credits  dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paye.set_mandat_orv_brouillard(manid);
END LOOP;
CLOSE c1;

END;


PROCEDURE basculer_bouillard_paye_regul(borid INTEGER) IS
cpt INTEGER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN
-- on vide la recuperation du brouillard des mandats !
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits et credits  dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paye.set_mandat_regul_brouillard(manid);
END LOOP;
CLOSE c1;

END;





-- Ecritures de Paiement (Type 45 dans Jefy_ecritures).
PROCEDURE set_bord_brouillard_paiement(moisordre NUMBER, borid NUMBER, exeordre NUMBER)
IS

CURSOR ecriturespaiement IS
SELECT * FROM jefy_paye.jefy_ecritures WHERE mois_ordre = moisordre AND ecr_type='45';

currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;
bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;
gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;
--moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
moiscomplet jefy_paye.paye_mois.mois_complet%TYPE;
cpt INTEGER;
mois VARCHAR2(50);

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;
-- recup du mois_complet
SELECT mois_complet INTO moiscomplet FROM jefy_paye.paye_mois
WHERE mois_ordre  = moisordre;


SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD
WHERE bob_operation LIKE '%PAIEMENT%' AND bob_libelle1 = 'PAIEMENT SALAIRES '||moiscomplet;

-- cpt = 0 ==> Aucune ecriture de paiement passee pour ce mois
IF (cpt = 0)
THEN

  OPEN ecriturespaiement;
  LOOP
    FETCH ecriturespaiement INTO currentecriture;
    EXIT WHEN ecriturespaiement%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'PAIEMENT SALAIRES',
 currentecriture.pco_num,
 'PAIEMENT SALAIRES '||moiscomplet,
 moiscomplet,
 NULL
 );

 END LOOP;
 CLOSE ecriturespaiement;
END IF;

END;

-- ECRITURES VISA - Ecritures de credit de type '64' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_visa(borid INTEGER)
IS

--cursor ecriturescredit64(mois number , gescode varchar2) is
--select * from jefy_paye.jefy_ecritures where mois_ordre = mois and ecr_comp = gescode and ecr_sens = 'C' and ecr_type='64';

CURSOR ecriturescredit64(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.JEFY_ECRITURES WHERE mois_ordre = lemois
AND ecr_comp = lacomp
AND ecr_type='64'
AND ( ecr_sens = 'C' OR (ecr_sens  = 'D' AND pco_num LIKE '4%' ));


currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
SELECT mois_ordre INTO moisordre FROM jefy_paye.paye_mois
WHERE mois_complet = mois;

SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois WHERE mois_complet  = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

dbms_output.put_line('BROUILLARD VISA : '||currentbordereau.ges_code||' , moisordre : '||mois);


  OPEN ecriturescredit64(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecriturescredit64 INTO currentecriture;
    EXIT WHEN ecriturescredit64%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'VISA SALAIRES',
 currentecriture.pco_num,
 'VISA SALAIRES '||mois,
 mois,
 NULL
 );

 END LOOP;
  CLOSE ecriturescredit64;

END;



PROCEDURE set_mandat_orv_brouillard(manid INTEGER)
IS

cpt     INTEGER;
dpcoid  INTEGER;
depid INTEGER;
classe4 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
classe6 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
montant NUMBER(12,2);

lemandat maracuja.MANDAT%ROWTYPE;

CURSOR plancos
IS SELECT dpco_id, dep_id, dpco_ttc_saisie FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

BEGIN

-- recup des infos du mandat
SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

classe6 := lemandat.pco_num;

-- creation du brouillard Crediteur classe 6 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(lemandat.man_ttc),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,              --MAB_ORDRE,
   'C',         --MAB_SENS,
   manid,        --MAN_ID,
   classe6             --PCO_NU
);


 OPEN plancos;
 LOOP
 FETCH plancos INTO dpcoid, depid, montant;
 EXIT WHEN plancos%NOTFOUND;

SELECT COUNT(*) INTO cpt
FROM jefy_paye.JEFY_ECRITURES_REVERSEMENT e, jefy_depense.depense_ctrl_planco dpco
WHERE dpco.dep_id = e.dep_id_rev AND dpco_id = dpcoid;

IF (cpt = 1 )      -- Bulletins n¿gatifs, on va chercher la contrepartie dans jefy_paye.jefy_ecritures_reversement
THEN

  SELECT PCO_NUM_CONTREPARTIE INTO classe4
  FROM jefy_paye.JEFY_ECRITURES_REVERSEMENT e, jefy_depense.depense_ctrl_planco dpco
  WHERE dpco.dep_id = e.dep_id_rev AND dpco_id = dpcoid;

ELSE                -- OR Manuel

  classe4 := jefy_paye.get_contrepartie(classe6, depid);

  IF (classe4 IS NULL)
  THEN

        SELECT pco_num_ctrepartie INTO classe4
      FROM PLANCO_VISA WHERE pco_num_ordonnateur = classe6 and exe_ordre = lemandat.exe_ordre;

  END IF;

END IF;

  -- creation du brouillard DEBITEUR CLASSE 4 !
  INSERT INTO MANDAT_BROUILLARD VALUES
   (
     NULL,          --ECD_ORDRE,
     lemandat.exe_ordre,                     --EXE_ORDRE,
     lemandat.ges_code,              --GES_CODE,
     ABS(montant),                                 --MAB_MONTANT,
     'VISA MANDAT',       --MAB_OPERATION,
     mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
     'D',         --MAB_SENS,
     manid,        --MAN_ID,
     classe4             --PCO_NU
  );

 END LOOP;
 CLOSE plancos;

END;


PROCEDURE set_mandat_regul_brouillard(manid INTEGER)
IS
cpt     INTEGER;
dpcoid  INTEGER;
classe4 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
classe6 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
montant NUMBER(12,2);

lemandat maracuja.MANDAT%ROWTYPE;

BEGIN
-- recup des infos du mandat
SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

-- recup du dpcoid de ce mandat papaye : un mandat pour un depense_ctrl_planco
SELECT dpco_id INTO dpcoid  FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

-- recup des comptes et du montant (brouillard)
SELECT PCO_NUM_CONTREPARTIE,e.pco_num ,ecr_mont INTO classe4, classe6 , montant
FROM jefy_paye.JEFY_ECRITURES_REVERSEMENT e, jefy_depense.depense_ctrl_planco dpco
WHERE dpco.dep_id = e.dep_id_rev AND dpco_id = dpcoid;


-- creation du brouillard Crediteur classe 6 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(montant),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
   'D',         --MAB_SENS,
   manid,        --MAN_ID,
   classe6             --PCO_NU
);


-- creation du brouillard DEBITEUR CLASSE 4 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(montant),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
   'C',         --MAB_SENS,
   manid,        --MAN_ID,
   classe4             --PCO_NU
);

END;


-- Ecritures de retenues / Oppositions - Ecritures de type '44' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_retenues(borid NUMBER)
IS

CURSOR ecrituresretenues(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.jefy_ecritures
WHERE mois_ordre = lemois
AND ecr_comp = lacomp AND ecr_type='44';

currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois
WHERE mois_complet = mois;

SELECT mois_ordre INTO moisordre FROM jefy_paye.paye_mois WHERE mois_complet  = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituresretenues(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecrituresretenues INTO currentecriture;
    EXIT WHEN ecrituresretenues%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'RETENUES SALAIRES',
 currentecriture.pco_num,
 'RETENUES SALAIRES '||mois,
 mois,
 NULL
 );

 END LOOP;
  CLOSE ecrituresretenues;

END;

-- Ecritures SACD - Ecritures de type '18' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_sacd(borid NUMBER)
IS

CURSOR ecrituressacd(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.jefy_ecritures
WHERE mois_ordre = lemois AND ecr_comp = lacomp AND ecr_type='18';

currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois
WHERE mois_complet = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituressacd(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecrituressacd INTO currentecriture;
    EXIT WHEN ecrituressacd%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'SACD SALAIRES',
 currentecriture.pco_num,
 'SACD SALAIRES '||mois,
 mois,
 NULL
 );

 END LOOP;
  CLOSE ecrituressacd;

END;



-- Ecritures de visa des payes (Debit 6) .
PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

lemandat     MANDAT%ROWTYPE;

BEGIN

SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

-- creation du mandat_brouillard visa DEBIT--
INSERT INTO MANDAT_BROUILLARD VALUES
(
NULL,           --ECD_ORDRE,
lemandat.exe_ordre,      --EXE_ORDRE,
lemandat.ges_code,      --GES_CODE,
lemandat.man_ht,      --MAB_MONTANT,
'VISA SALAIRES',       --MAB_OPERATION,
mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
'D',         --MAB_SENS,
manid,         --MAN_ID,
lemandat.pco_num      --PCO_NU
);

END;

PROCEDURE get_facture_jefy
(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER)
IS

--depid       DEPENSE.dep_id%TYPE;
--jefyfacture   jefy.factures%ROWTYPE;
--lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
--fouadresse    DEPENSE.dep_adresse%TYPE;
--founom     DEPENSE.dep_fournisseur%TYPE;
--lotordre     DEPENSE.dep_lot%TYPE;
--marordre   DEPENSE.dep_marches%TYPE;
--fouordre   DEPENSE.fou_ordre%TYPE;
--gescode    DEPENSE.ges_code%TYPE;
--cpt     INTEGER;
-- tcdordre   TYPE_CREDIT.TCD_ORDRE%TYPE;
-- tcdcode    TYPE_CREDIT.tcd_code%TYPE;

--lemandat MANDAT%ROWTYPE;

--CURSOR factures IS
-- SELECT * FROM jefy.factures
-- WHERE man_ordre = manordre;

BEGIN

-- procedure obsolete


--OPEN factures;
--LOOP
--FETCH factures INTO jefyfacture;
--EXIT WHEN factures%NOTFOUND;

---- creation du depid --
--SELECT depense_seq.NEXTVAL INTO depid FROM dual;


-- SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
--WHERE cde_ordre = jefyfacture.cde_ordre;

-- IF cpt = 0 THEN
--    --recuperer le type de credit a partir de la commande
--   SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

--   SELECT tc.tcd_ordre INTO tcdordre
--    FROM TYPE_CREDIT tc
--    WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

---- creation de lignebudgetaire--
--SELECT org_comp||' '||org_lbud||' '||org_uc
--INTO lignebudgetaire
--FROM jefy.organ
--WHERE org_ordre =
--(
-- SELECT org_ordre
-- FROM jefy.engage
-- WHERE cde_ordre = jefyfacture.cde_ordre
-- AND eng_stat !='A'
--);
--ELSE

--   --recuperer le type de credit a partir de la commande
--   SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

--   SELECT tc.tcd_ordre INTO tcdordre
--    FROM TYPE_CREDIT tc
--    WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

--SELECT org_comp||' '||org_lbud||' '||org_uc
--INTO lignebudgetaire
--FROM jefy.organ
--WHERE org_ordre =
--(
-- SELECT  MAX(org_ordre)
-- FROM jefy.facture_ext
-- WHERE cde_ordre = jefyfacture.cde_ordre
--);
--END IF;

---- recuperations --

---- gescode --
-- SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
--WHERE cde_ordre = jefyfacture.cde_ordre;

-- IF cpt = 0 THEN
--SELECT org_comp
--INTO gescode
--FROM jefy.organ
--WHERE org_ordre =
--(
-- SELECT org_ordre
-- FROM jefy.engage
-- WHERE cde_ordre = jefyfacture.cde_ordre
-- AND eng_stat !='A'
--);
--ELSE
--SELECT org_comp
--INTO gescode
--FROM jefy.organ
--WHERE org_ordre =
--(
--SELECT MAX(org_ordre)
-- FROM jefy.facture_ext
-- WHERE cde_ordre = jefyfacture.cde_ordre
--);
--END IF;

---- fouadresse --
--SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
--INTO fouadresse
--FROM v_fournisseur
--WHERE fou_ordre =
--(
-- SELECT fou_ordre
-- FROM jefy.commande
-- WHERE cde_ordre = jefyfacture.cde_ordre
--);

---- founom --
--SELECT adr_nom||' '||adr_prenom
--INTO founom
--FROM v_fournisseur
--WHERE fou_ordre =
--(
-- SELECT fou_ordre
-- FROM jefy.commande
-- WHERE cde_ordre = jefyfacture.cde_ordre
--);

---- fouordre --
-- SELECT fou_ordre INTO fouordre
-- FROM jefy.commande
-- WHERE cde_ordre = jefyfacture.cde_ordre;

---- lotordre --
--SELECT COUNT(*) INTO cpt
--FROM marches.attribution
--WHERE att_ordre =
--(
-- SELECT lot_ordre
-- FROM jefy.commande
-- WHERE cde_ordre = jefyfacture.cde_ordre
--);

-- IF cpt = 0 THEN
--  lotordre :=NULL;
-- ELSE
--  SELECT lot_ordre
--  INTO lotordre
--  FROM marches.attribution
--  WHERE att_ordre =
--  (
--   SELECT lot_ordre
--   FROM jefy.commande
--   WHERE cde_ordre = jefyfacture.cde_ordre
--  );
-- END IF;

---- marordre --
--SELECT COUNT(*) INTO cpt
--FROM marches.lot
--WHERE lot_ordre = lotordre;

--IF cpt = 0 THEN
--  marordre :=NULL;
--ELSE
-- SELECT mar_ordre
-- INTO marordre
-- FROM marches.lot
-- WHERE lot_ordre = lotordre;
--END IF;

--SELECT * INTO lemandat FROM MANDAT WHERE man_id=manid;




---- creation de la depense --
--INSERT INTO DEPENSE VALUES
--(
--fouadresse ,           --DEP_ADRESSE,
--NULL ,       --DEP_DATE_COMPTA,
--jefyfacture.dep_date,  --DEP_DATE_RECEPTION,
--jefyfacture.dep_date , --DEP_DATE_SERVICE,
--'VALIDE' ,      --DEP_ETAT,
--founom ,      --DEP_FOURNISSEUR,
--jefyfacture.dep_mont , --DEP_HT,
--depense_seq.NEXTVAL ,  --DEP_ID,
--lignebudgetaire ,    --DEP_LIGNE_BUDGETAIRE,
--lotordre ,      --DEP_LOT,
--marordre ,      --DEP_MARCHES,
--jefyfacture.dep_ttc ,  --DEP_MONTANT_DISQUETTE,
--jefyfacture.cm_ordre , --DEP_NOMENCLATURE,
--jefyfacture.dep_fact  ,--DEP_NUMERO,
--jefyfacture.dep_ordre ,--DEP_ORDRE,
--NULL ,       --DEP_REJET,
--jefyfacture.rib_ordre ,--DEP_RIB,
--'NON' ,       --DEP_SUPPRESSION,
--jefyfacture.dep_ttc ,  --DEP_TTC,
--jefyfacture.dep_ttc
---jefyfacture.dep_mont, -- DEP_TVA,
--exeordre ,      --EXE_ORDRE,
--fouordre,       --FOU_ORDRE,
--gescode,        --GES_CODE,
--manid ,       --MAN_ID,
--jefyfacture.man_ordre, --MAN_ORDRE,
----jefyfacture.mod_code,  --MOD_ORDRE,
--lemandat.mod_ordre,
--jefyfacture.pco_num ,  --PCO_ORDRE,
--1,         --UTL_ORDRE
--NULL, --org_ordre
--tcdordre,
--NULL, -- ecd_ordre_ema
--jefyfacture.DEP_DATE
--);

--END LOOP;
--CLOSE factures;

    return;

END;


END;
/









