begin
	delete from jefy_admin.utilisateur_fonct_exercice where uf_ordre in (select uf_ordre from jefy_admin.utilisateur_fonct where fon_ordre in (select fon_ordre from jefy_admin.fonction where fon_id_interne='REI' and tyap_id=18));
	delete from jefy_admin.utilisateur_fonct where fon_ordre in (select fon_ordre from jefy_admin.fonction where fon_id_interne='REI' and tyap_id=18);
	delete from jefy_admin.fonction where fon_id_interne='REI' and tyap_id=18;
	
	
	
    INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
    TYAV_COMMENT ) VALUES ( 
    jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 18, 'BD', '1.4.5.6',  SYSDATE, '');
    
    commit;	
end;