SET DEFINE OFF;
CREATE OR REPLACE PACKAGE MARACUJA.bordereau_abricot_paye is
/*
 * Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic

procedure basculer_bouillard_paye(borid integer);
procedure basculer_bouillard_paye_orv(borid integer);
procedure basculer_bouillard_paye_regul(borid integer);

procedure set_mandat_brouillard(manid integer);
procedure set_mandat_orv_brouillard(manid integer);
procedure set_mandat_regul_brouillard(manid integer);

procedure set_bord_brouillard_visa(borid integer);
procedure set_bord_brouillard_paiement(moisordre number, borid number, exeordre number);
procedure set_bord_brouillard_retenues(borid number);
procedure set_bord_brouillard_sacd(borid number);
end;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Abricot_Paye IS


PROCEDURE basculer_bouillard_paye(borid INTEGER) IS

tmpBordereau maracuja.BORDEREAU%ROWTYPE;
mois VARCHAR2(50);
moiscomplet VARCHAR2(50);
moisordre INTEGER;
cpt INTEGER;

sumdebits NUMBER;
sumcredits NUMBER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN

-- recup des infos du bordereau
SELECT * INTO tmpBordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

-- recup du mois JANVIER XXXX
SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
SELECT mois_ordre,mois_complet INTO moisordre,moiscomplet FROM jefy_paye.paye_mois
WHERE mois_complet = mois;



-- On verifie que les ecritures aient bien ete generees pour la composante en question.
SELECT COUNT(*) INTO cpt FROM jefy_paye.jefy_ecritures WHERE ecr_comp = tmpBordereau.ges_code AND mois_ordre = moisordre;

IF (cpt = 0) THEN
 RAISE_APPLICATION_ERROR(-20001,'Vous n''avez toujours pas pr?par? les ?critures pour la composante '||tmpBordereau.ges_code||' !');
END IF;

-- On verifie que le total des debits soit egal au total des credits  (Pour la composante)
 SELECT SUM(ecr_mont) INTO sumdebits FROM jefy_paye.jefy_ecritures
 WHERE ecr_comp = ges_code AND mois_ordre = moisordre AND ecr_type='64'
 AND ecr_comp = tmpBordereau.ges_code AND ecr_sens = 'D';

 SELECT SUM(ecr_mont) INTO sumcredits FROM jefy_paye.jefy_ecritures
 WHERE ecr_comp = ges_code AND mois_ordre = moisordre AND ecr_type='64'
 AND ecr_comp = tmpBordereau.ges_code AND ecr_sens = 'C';

 IF (sumcredits <> sumdebits)
 THEN
   RAISE_APPLICATION_ERROR(-20001,'Pour la composante '||tmpBordereau.ges_code||', la somme des DEBITS ('||sumdebits||') est diff?rente de la somme des CREDITS ('||sumcredits||') !');
 END IF;

  SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU
  WHERE bor_id = borid
  AND tbo_ordre = tmpBordereau.tbo_ordre
  AND exe_ordre = tmpBordereau.exe_ordre;

  IF cpt = 1 THEN
   Bordereau_Abricot_Paye.set_bord_brouillard_visa(borid);
   Bordereau_Abricot_Paye.set_bord_brouillard_retenues(borid);
   Bordereau_Abricot_Paye.set_bord_brouillard_sacd(borid);

   INSERT INTO maracuja.BORDEREAU_INFO VALUES  (borid, moiscomplet,NULL);

  END IF;
  -- Mise a jour des ecritures de paiement dans bordereau_brouillard
  -- Ces ecritures seront associees a la premiere composante qui mandatera ses payes.
  Bordereau_Abricot_Paye.set_bord_brouillard_paiement(moisordre, borid, tmpBordereau.exe_ordre);

-- misea jour dans papaye des tables apres bascule !
  -- maj de l etat de papaye_compta et du borid -

 UPDATE jefy_paye.jefy_paye_compta SET bor_id=borid, jpc_etat='MANDATEE'
    WHERE ges_code=tmpBordereau.ges_code
      AND mois_ordre=moisordre AND jpc_ETAT='LIQUIDEE';
/*
  update jefy_paye.jefy_liquidations set liq_etat='MANDATEE'
    where ges_code=tmpBordereau.ges_code
      and mois_ordre=moisordre and liq_ETAT='LIQUIDEE';
*/

-- modifications FRED -> BUG REF ECRITURES MANDAT_DETAIL_ECRITURE VU PAR RODOLPHE 23/03/2007
-- on vide la recuperation
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paye.set_mandat_brouillard(manid);
END LOOP;
CLOSE c1;

END;


PROCEDURE basculer_bouillard_paye_orv(borid INTEGER) IS
cpt INTEGER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN
-- on vide la recuperation du brouillard des mandats !
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits et credits  dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paye.set_mandat_orv_brouillard(manid);
END LOOP;
CLOSE c1;

END;


PROCEDURE basculer_bouillard_paye_regul(borid INTEGER) IS
cpt INTEGER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN
-- on vide la recuperation du brouillard des mandats !
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits et credits  dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paye.set_mandat_regul_brouillard(manid);
END LOOP;
CLOSE c1;

END;





-- Ecritures de Paiement (Type 45 dans Jefy_ecritures).
PROCEDURE set_bord_brouillard_paiement(moisordre NUMBER, borid NUMBER, exeordre NUMBER)
IS

CURSOR ecriturespaiement IS
SELECT * FROM jefy_paye.jefy_ecritures WHERE mois_ordre = moisordre AND ecr_type='45';

currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;
bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;
gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;
--moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
moiscomplet jefy_paye.paye_mois.mois_complet%TYPE;
cpt INTEGER;
mois VARCHAR2(50);

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;
-- recup du mois_complet
SELECT mois_complet INTO moiscomplet FROM jefy_paye.paye_mois
WHERE mois_ordre  = moisordre;


SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD
WHERE bob_operation LIKE '%PAIEMENT%' AND bob_libelle1 = 'PAIEMENT SALAIRES '||moiscomplet;

-- cpt = 0 ==> Aucune ecriture de paiement passee pour ce mois
IF (cpt = 0)
THEN

  OPEN ecriturespaiement;
  LOOP
    FETCH ecriturespaiement INTO currentecriture;
    EXIT WHEN ecriturespaiement%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'PAIEMENT SALAIRES',
 currentecriture.pco_num,
 'PAIEMENT SALAIRES '||moiscomplet,
 moiscomplet,
 NULL
 );

 END LOOP;
 CLOSE ecriturespaiement;
END IF;

END;

-- ECRITURES VISA - Ecritures de credit de type '64' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_visa(borid INTEGER)
IS

--cursor ecriturescredit64(mois number , gescode varchar2) is
--select * from jefy_paye.jefy_ecritures where mois_ordre = mois and ecr_comp = gescode and ecr_sens = 'C' and ecr_type='64';

CURSOR ecriturescredit64(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.JEFY_ECRITURES WHERE mois_ordre = lemois
AND ecr_comp = lacomp
AND ecr_type='64'
AND ( ecr_sens = 'C' OR (ecr_sens  = 'D' AND pco_num LIKE '4%' ));


currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
SELECT mois_ordre INTO moisordre FROM jefy_paye.paye_mois
WHERE mois_complet = mois;

SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois WHERE mois_complet  = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

dbms_output.put_line('BROUILLARD VISA : '||currentbordereau.ges_code||' , moisordre : '||mois);


  OPEN ecriturescredit64(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecriturescredit64 INTO currentecriture;
    EXIT WHEN ecriturescredit64%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'VISA SALAIRES',
 currentecriture.pco_num,
 'VISA SALAIRES '||mois,
 mois,
 NULL
 );

 END LOOP;
  CLOSE ecriturescredit64;

END;



PROCEDURE set_mandat_orv_brouillard(manid INTEGER)
IS

cpt     INTEGER;
dpcoid  INTEGER;
depid INTEGER;
classe4 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
classe6 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
montant NUMBER(12,2);

lemandat maracuja.MANDAT%ROWTYPE;

CURSOR plancos
IS SELECT dpco_id, dep_id, dpco_ttc_saisie FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

BEGIN

-- recup des infos du mandat
SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

classe6 := lemandat.pco_num;

-- creation du brouillard Crediteur classe 6 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(lemandat.man_ttc),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,              --MAB_ORDRE,
   'C',         --MAB_SENS,
   manid,        --MAN_ID,
   classe6             --PCO_NU
);


 OPEN plancos;
 LOOP
 FETCH plancos INTO dpcoid, depid, montant;
 EXIT WHEN plancos%NOTFOUND;

SELECT COUNT(*) INTO cpt
FROM jefy_paye.JEFY_ECRITURES_REVERSEMENT e, jefy_depense.depense_ctrl_planco dpco
WHERE dpco.dep_id = e.dep_id_rev AND dpco_id = dpcoid;

IF (cpt = 1 )      -- Bulletins n¿gatifs, on va chercher la contrepartie dans jefy_paye.jefy_ecritures_reversement
THEN

  SELECT PCO_NUM_CONTREPARTIE INTO classe4
  FROM jefy_paye.JEFY_ECRITURES_REVERSEMENT e, jefy_depense.depense_ctrl_planco dpco
  WHERE dpco.dep_id = e.dep_id_rev AND dpco_id = dpcoid;

ELSE                -- OR Manuel

  classe4 := jefy_paye.get_contrepartie(classe6, depid);

  IF (classe4 IS NULL)
  THEN

        SELECT pco_num_ctrepartie INTO classe4
      FROM PLANCO_VISA WHERE pco_num_ordonnateur = classe6 and exe_ordre = lemandat.exe_ordre;

  END IF;

END IF;

  -- creation du brouillard DEBITEUR CLASSE 4 !
  INSERT INTO MANDAT_BROUILLARD VALUES
   (
     NULL,          --ECD_ORDRE,
     lemandat.exe_ordre,                     --EXE_ORDRE,
     lemandat.ges_code,              --GES_CODE,
     ABS(montant),                                 --MAB_MONTANT,
     'VISA MANDAT',       --MAB_OPERATION,
     mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
     'D',         --MAB_SENS,
     manid,        --MAN_ID,
     classe4             --PCO_NU
  );

 END LOOP;
 CLOSE plancos;

END;


PROCEDURE set_mandat_regul_brouillard(manid INTEGER)
IS
cpt     INTEGER;
dpcoid  INTEGER;
classe4 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
classe6 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
montant NUMBER(12,2);

lemandat maracuja.MANDAT%ROWTYPE;

BEGIN
-- recup des infos du mandat
SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

-- recup du dpcoid de ce mandat papaye : un mandat pour un depense_ctrl_planco
SELECT dpco_id INTO dpcoid  FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

-- recup des comptes et du montant (brouillard)
SELECT PCO_NUM_CONTREPARTIE,e.pco_num ,ecr_mont INTO classe4, classe6 , montant
FROM jefy_paye.JEFY_ECRITURES_REVERSEMENT e, jefy_depense.depense_ctrl_planco dpco
WHERE dpco.dep_id = e.dep_id_rev AND dpco_id = dpcoid;


-- creation du brouillard Crediteur classe 6 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(montant),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
   'D',         --MAB_SENS,
   manid,        --MAN_ID,
   classe6             --PCO_NU
);


-- creation du brouillard DEBITEUR CLASSE 4 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(montant),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
   'C',         --MAB_SENS,
   manid,        --MAN_ID,
   classe4             --PCO_NU
);

END;


-- Ecritures de retenues / Oppositions - Ecritures de type '44' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_retenues(borid NUMBER)
IS

CURSOR ecrituresretenues(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.jefy_ecritures
WHERE mois_ordre = lemois
AND ecr_comp = lacomp AND ecr_type='44';

currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois
WHERE mois_complet = mois;

SELECT mois_ordre INTO moisordre FROM jefy_paye.paye_mois WHERE mois_complet  = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituresretenues(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecrituresretenues INTO currentecriture;
    EXIT WHEN ecrituresretenues%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'RETENUES SALAIRES',
 currentecriture.pco_num,
 'RETENUES SALAIRES '||mois,
 mois,
 NULL
 );

 END LOOP;
  CLOSE ecrituresretenues;

END;

-- Ecritures SACD - Ecritures de type '18' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_sacd(borid NUMBER)
IS

CURSOR ecrituressacd(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.jefy_ecritures
WHERE mois_ordre = lemois AND ecr_comp = lacomp AND ecr_type='18';

currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois
WHERE mois_complet = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituressacd(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecrituressacd INTO currentecriture;
    EXIT WHEN ecrituressacd%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'SACD SALAIRES',
 currentecriture.pco_num,
 'SACD SALAIRES '||mois,
 mois,
 NULL
 );

 END LOOP;
  CLOSE ecrituressacd;

END;



-- Ecritures de visa des payes (Debit 6) .
PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

lemandat     MANDAT%ROWTYPE;

BEGIN

SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

-- creation du mandat_brouillard visa DEBIT--
INSERT INTO MANDAT_BROUILLARD VALUES
(
NULL,           --ECD_ORDRE,
lemandat.exe_ordre,      --EXE_ORDRE,
lemandat.ges_code,      --GES_CODE,
lemandat.man_ht,      --MAB_MONTANT,
'VISA SALAIRES',       --MAB_OPERATION,
mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
'D',         --MAB_SENS,
manid,         --MAN_ID,
lemandat.pco_num      --PCO_NU
);

END;

/*
PROCEDURE get_facture_jefy
(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER)
IS

depid       DEPENSE.dep_id%TYPE;
jefyfacture   jefy.factures%ROWTYPE;
lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
fouadresse    DEPENSE.dep_adresse%TYPE;
founom     DEPENSE.dep_fournisseur%TYPE;
lotordre     DEPENSE.dep_lot%TYPE;
marordre   DEPENSE.dep_marches%TYPE;
fouordre   DEPENSE.fou_ordre%TYPE;
gescode    DEPENSE.ges_code%TYPE;
cpt     INTEGER;
 tcdordre   TYPE_CREDIT.TCD_ORDRE%TYPE;
 tcdcode    TYPE_CREDIT.tcd_code%TYPE;

lemandat MANDAT%ROWTYPE;

CURSOR factures IS
 SELECT * FROM jefy.factures
 WHERE man_ordre = manordre;

BEGIN

OPEN factures;
LOOP
FETCH factures INTO jefyfacture;
EXIT WHEN factures%NOTFOUND;

-- creation du depid --
SELECT depense_seq.NEXTVAL INTO depid FROM dual;


 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
    --recuperer le type de credit a partir de la commande
   SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

   SELECT tc.tcd_ordre INTO tcdordre
    FROM TYPE_CREDIT tc
    WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

-- creation de lignebudgetaire--
SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE

   --recuperer le type de credit a partir de la commande
   SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

   SELECT tc.tcd_ordre INTO tcdordre
    FROM TYPE_CREDIT tc
    WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT  MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- recuperations --

-- gescode --
 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
SELECT MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- fouadresse --
SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
INTO fouadresse
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- founom --
SELECT adr_nom||' '||adr_prenom
INTO founom
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- fouordre --
 SELECT fou_ordre INTO fouordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre;

-- lotordre --
SELECT COUNT(*) INTO cpt
FROM marches.attribution
WHERE att_ordre =
(
 SELECT lot_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

 IF cpt = 0 THEN
  lotordre :=NULL;
 ELSE
  SELECT lot_ordre
  INTO lotordre
  FROM marches.attribution
  WHERE att_ordre =
  (
   SELECT lot_ordre
   FROM jefy.commande
   WHERE cde_ordre = jefyfacture.cde_ordre
  );
 END IF;

-- marordre --
SELECT COUNT(*) INTO cpt
FROM marches.lot
WHERE lot_ordre = lotordre;

IF cpt = 0 THEN
  marordre :=NULL;
ELSE
 SELECT mar_ordre
 INTO marordre
 FROM marches.lot
 WHERE lot_ordre = lotordre;
END IF;

SELECT * INTO lemandat FROM MANDAT WHERE man_id=manid;




-- creation de la depense --
INSERT INTO DEPENSE VALUES
(
fouadresse ,           --DEP_ADRESSE,
NULL ,       --DEP_DATE_COMPTA,
jefyfacture.dep_date,  --DEP_DATE_RECEPTION,
jefyfacture.dep_date , --DEP_DATE_SERVICE,
'VALIDE' ,      --DEP_ETAT,
founom ,      --DEP_FOURNISSEUR,
jefyfacture.dep_mont , --DEP_HT,
depense_seq.NEXTVAL ,  --DEP_ID,
lignebudgetaire ,    --DEP_LIGNE_BUDGETAIRE,
lotordre ,      --DEP_LOT,
marordre ,      --DEP_MARCHES,
jefyfacture.dep_ttc ,  --DEP_MONTANT_DISQUETTE,
jefyfacture.cm_ordre , --DEP_NOMENCLATURE,
jefyfacture.dep_fact  ,--DEP_NUMERO,
jefyfacture.dep_ordre ,--DEP_ORDRE,
NULL ,       --DEP_REJET,
jefyfacture.rib_ordre ,--DEP_RIB,
'NON' ,       --DEP_SUPPRESSION,
jefyfacture.dep_ttc ,  --DEP_TTC,
jefyfacture.dep_ttc
-jefyfacture.dep_mont, -- DEP_TVA,
exeordre ,      --EXE_ORDRE,
fouordre,       --FOU_ORDRE,
gescode,        --GES_CODE,
manid ,       --MAN_ID,
jefyfacture.man_ordre, --MAN_ORDRE,
--jefyfacture.mod_code,  --MOD_ORDRE,
lemandat.mod_ordre,
jefyfacture.pco_num ,  --PCO_ORDRE,
1,         --UTL_ORDRE
NULL, --org_ordre
tcdordre,
NULL, -- ecd_ordre_ema
jefyfacture.DEP_DATE
);

END LOOP;
CLOSE factures;

END;
*/

END;
/


