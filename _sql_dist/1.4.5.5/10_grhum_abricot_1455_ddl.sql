set define off;

  CREATE OR REPLACE PACKAGE          Api_Plsql_Paf IS

/*
PARISDESCARTES 
Rivalland Frederic.

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja.

IL PERMET DE GENERER LES ECRITURES POUR LE
BROUILLARD DE PAYE.


*/


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER);

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR);

PROCEDURE passerEcritureSACDBord ( borid INTEGER);

-- PRIVATE --
-- permet de creer une ecriture --
FUNCTION creerEcriture (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL
  ) RETURN INTEGER;


-- permet d ajouter des details a une ecriture.
FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  ) RETURN INTEGER;



END; 
/


CREATE OR REPLACE PACKAGE BODY          Api_Plsql_Paf IS
-- PUBLIC --


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER)
IS

CURSOR mandats IS
SELECT * FROM MANDAT_BROUILLARD WHERE man_id IN (SELECT man_id FROM MANDAT WHERE bor_id = borid)
AND mab_operation = 'VISA PAF';

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'VISA PAF' AND bob_etat = 'VALIDE';

currentmab maracuja.MANDAT_BROUILLARD%ROWTYPE;
currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;
tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;

borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

bornum maracuja.BORDEREAU.bor_num%TYPE;
gescode maracuja.BORDEREAU.ges_code%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- Recuperation du libelle du mois a traiter
SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

-- Verification du type de bordereau (Bordereau de salaires ? (tbo_ordre = 3)
SELECT tbo_ordre INTO tboordre FROM BORDEREAU WHERE bor_id = borid;
-- Verification de l'etat du bordereau - Si etat != VALIDE, les ecritures ont deja ete passees.
SELECT DISTINCT bob_etat INTO boretat FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

IF (tboordre = 20 AND boretat = 'VALIDE')
THEN

--raise_application_error(-20001,'DANS TBOORDRE = 3');
   -- Creation de l'ecriture des visa (Debit 6, Credit 4).
   SELECT exe_ordre INTO exeordre FROM BORDEREAU WHERE bor_id = borid;

    ecrordre := creerecriture(
    1,
    SYSDATE,
    'VISA PAF '||moislibelle||' Bord. '||bornum||' du '||gescode,
    exeordre,
    oriordre,
    14,
    9,
    0
    );

  -- Creation des details ecritures debit 6 a partir des mandats de paye.
  OPEN mandats;
  LOOP
    FETCH mandats INTO currentmab;
    EXIT WHEN mandats%NOTFOUND;

    SELECT ori_ordre INTO oriordre FROM MANDAT WHERE man_id = currentmab.man_id;

    ecdordre := creerecrituredetail (
    NULL,
    'VISA PAF '||moislibelle||' Bord. '||bornum||' du '||gescode,
    currentmab.mab_montant,
    NULL,
    currentmab.mab_sens,
    ecrordre,
    currentmab.ges_code,
    currentmab.pco_num
    );

    SELECT mandat_detail_ecriture_seq.NEXTVAL INTO mdeordre FROM dual;

    -- Insertion des mandat_detail_ecritures
    INSERT INTO MANDAT_DETAIL_ECRITURE VALUES (
    ecdordre,
    currentmab.exe_ordre,
    currentmab.man_id,
    SYSDATE,
    mdeordre,
    'VISA',
    oriordre
    );



    END LOOP;
  CLOSE mandats;

  -- Creation des details ecritures credit 4 a partir des bordereaux_brouillards.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

    ecdordre := creerecrituredetail (
    NULL,
    'VISA PAF '||moislibelle||' Bord. '||bornum||' du '||gescode,
    currentbob.bob_montant,
    NULL,
    currentbob.bob_sens,
    ecrordre,
    currentbob.ges_code,
    currentbob.pco_num
    );

    END LOOP;
  CLOSE bordereaux;

  -- Validation de l'ecriture des visas
  Api_Plsql_Journal.validerecriture(ecrordre);

  SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;

  boretat := 'PAIEMENT';

  UPDATE MANDAT SET man_date_remise=TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') WHERE bor_id=borid;
  UPDATE BORDEREAU_BROUILLARD SET bob_etat = boretat WHERE bor_id = borid;
  UPDATE BORDEREAU SET bor_date_visa = SYSDATE,utl_ordre_visa = 0,bor_etat = boretat WHERE bor_id = borid;

 UPDATE jefy_paf.paf_etape SET pae_etat=boretat WHERE bor_id=borid;

  passerecrituresacdbord(borid);

END IF;

END ;






-- permet de passer les  ecritures de paiement d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois AND bob_operation = 'PAIEMENT PAF'
AND bob_etat = 'PAIEMENT';

CURSOR gescodenontraites IS
SELECT ges_code FROM (
SELECT DISTINCT ges_code
FROM jefy_paye.jefy_ecritures e, jefy_paye.paye_mois p
WHERE e.mois_ordre=p.mois_ordre
AND p.mois_complet = borlibelle_mois
AND e.ecr_type=64
AND e.ecr_sens='D'
MINUS
SELECT DISTINCT b.ges_code
 FROM BORDEREAU b, BORDEREAU_INFO bi
WHERE b.bor_id=bi.bor_id
AND b.tbo_ordre=20
AND bi.BOR_LIBELLE = borlibelle_mois
AND B.BOR_ETAT <> 'ANNULE'
) ORDER BY ges_code;


currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;
gescode     maracuja.BORDEREAU.ges_code%TYPE;
bornum     maracuja.BORDEREAU.bor_num%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;
curgescode maracuja.GESTION.ges_code%TYPE;
gescodes VARCHAR2(1000);

BEGIN
     gescodes := '';
     /*
-- verifier que tous les gescode sont traites
  OPEN gescodenontraites;
  LOOP
    FETCH gescodenontraites INTO curgescode;
    EXIT WHEN gescodenontraites%NOTFOUND;
         IF (LENGTH(gescodes)>0) THEN
             gescodes := gescodes || ', ';
         END IF;
          gescodes := gescodes || curgescode;
    END LOOP;
  CLOSE gescodenontraites;

  IF (LENGTH(gescodes)>0) THEN
*       RAISE_APPLICATION_ERROR (-20001,'Les composantes suivantes n''ont pas encore ete liquidees et/ou mandatees dans Papaye : ' || gescodes);
  END IF;
  */

  -- verifier qu etous les bordereaux du mois sont a l''etat PAIEMENT
SELECT COUNT(*) INTO cpt
 FROM BORDEREAU b, BORDEREAU_INFO bi
WHERE b.bor_id=bi.bor_id
AND b.tbo_ordre=20
AND bi.BOR_LIBELLE = borlibelle_mois
AND B.BOR_ETAT <> 'ANNULE' AND B.BOR_ETAT <> 'PAIEMENT' AND B.BOR_ETAT <> 'PAYE';

IF (cpt >0) THEN
 RAISE_APPLICATION_ERROR (-20001,'Certains bordereaux ne sont pas a l''etat PAIEMENT');
END IF;

-- MODIF CYRIL 
-- Pour le moment on ne pass pas les ecritures de paiement

IF passer_ecritures = 'O'
THEN

           SELECT DISTINCT b.exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=20 and  rownum=1;

           -- On verifie que les ecritures de paiement ne soient pas deja passees.
           SELECT COUNT(*) INTO cpt FROM ECRITURE WHERE ecr_libelle = 'PAIEMENT PAF '||borlibelle_mois;

           IF (cpt = 0)
           THEN
            ecrordre := creerecriture(
            1,
            SYSDATE,
            'PAIEMENT PAF '||borlibelle_mois,
            exeordre,
            oriordre,
            14,
            6,
            0
            );

          -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
          OPEN bordereaux;
          LOOP
            FETCH bordereaux INTO currentbob;
            EXIT WHEN bordereaux%NOTFOUND;

            ecdordre := creerecrituredetail (
            NULL,
            'PAIEMENT PAF '||borlibelle_mois,
            currentbob.bob_montant,
            NULL,
            currentbob.bob_sens,
            ecrordre,
            currentbob.ges_code,
            currentbob.pco_num
            );

            END LOOP;
          CLOSE bordereaux;

          Api_Plsql_Journal.validerecriture(ecrordre);
 
   
  END IF;

    UPDATE jefy_paf.paf_etape SET pae_etat='TERMINEE' WHERE MOIS_LIBELLE=borlibelle_mois;

  END IF;

  UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAYE' WHERE bor_id in (SELECT DISTINCT b.bor_id FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=20);

  UPDATE MANDAT
  SET
  man_etat = 'PAYE'
  WHERE bor_id IN (SELECT DISTINCT b.bor_id FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=20);

  UPDATE BORDEREAU
  SET
  bor_date_visa = SYSDATE,
  utl_ordre_visa = 0,
  bor_etat = 'PAYE'
  WHERE bor_id IN (SELECT DISTINCT b.bor_id FROM BORDEREAU_BROUILLARD bb, bordereau b WHERE bb.bor_id=b.bor_id and bob_libelle2 = borlibelle_mois and b.tbo_ordre=20);

END;



-- PRIVATE --
FUNCTION creerEcriture(
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  )RETURN INTEGER
IS
cpt INTEGER;

localExercice INTEGER;
localEcrDate DATE;

BEGIN

SELECT exe_exercice INTO localExercice FROM maracuja.EXERCICE WHERE exe_ordre = exeordre;

IF (SYSDATE > TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy'))
THEN
    localEcrDate := TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy');
ELSE
    localEcrDate := ecrdate;
END IF;

RETURN Api_Plsql_Journal.creerEcritureExerciceType(
  COMORDRE     ,--         NUMBER,--        NOT NULL,
  localEcrDate      ,--         DATE ,--         NOT NULL,
  ECRLIBELLE   ,--       VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE     ,--         NUMBER,--        NOT NULL,
  ORIORDRE     ,--        NUMBER,
  TOPORDRE     ,--         NUMBER,--        NOT NULL,
  UTLORDRE     ,--         NUMBER,--        NOT NULL,
  TJOORDRE         --        INTEGER
  );

END;

FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
 -- EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  )RETURN INTEGER

IS
EXEORDRE          NUMBER;
BEGIN

SELECT exe_ordre INTO EXEORDRE FROM ECRITURE
WHERE ecr_ordre = ecrordre;


RETURN Api_Plsql_Journal.creerEcritureDetail
(
  ECDCOMMENTAIRE,--   VARCHAR2,-- (200),
  ECDLIBELLE    ,--    VARCHAR2,-- (200),
  ECDMONTANT    ,--   NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE ,--  VARCHAR2,-- (20),
  ECDSENS       ,-- VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE      ,-- NUMBER,--        NOT NULL,
  GESCODE       ,--    VARCHAR2,-- (10)  NOT NULL,
  PCONUM         --   VARCHAR2-- (20)  NOT NULL
  );


END;


-- permet de passer les  ecritures SACD  d un bordereau de PAF --
PROCEDURE passerEcritureSACDBord ( borid INTEGER)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'SACD PAF';

currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

gescode     maracuja.BORDEREAU.ges_code%TYPE;
bornum     maracuja.BORDEREAU.bor_num%TYPE;
borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- TEST ; Verification du type de bordereau (Bordereau de salaires ?)

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
   SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

   SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
   SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

   SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bob_operation = 'SACD PAF' AND bor_id = borid;

   IF (cpt > 0)
   THEN

    ecrordre := creerecriture(
    1,
    SYSDATE, 
    'SACD PAF '||moislibelle||' Bord. '||bornum||' du '||gescode,
    exeordre,
    oriordre,
    14,
    9,
    0
    );

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

    ecdordre := creerecrituredetail (
    NULL,
    'SACD PAF '||moislibelle||' Bord. '||bornum||' du '||gescode,
    currentbob.bob_montant,
    NULL,
    currentbob.bob_sens,
    ecrordre,
    currentbob.ges_code,
    currentbob.pco_num
    );

    END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);

  END IF;

END ;



END; 
/


CREATE OR REPLACE PACKAGE          bordereau_abricot_paf is
/*
 * Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic

function getMoisCompletTexte(borId integer) return varchar;

procedure basculer_bouillard_paye(borid integer);
procedure basculer_bouillard_paye_orv(borid integer);
procedure basculer_bouillard_paye_regul(borid integer);

procedure set_mandat_brouillard(manid integer);
procedure set_mandat_orv_brouillard(manid integer);
procedure set_mandat_regul_brouillard(manid integer);

procedure set_bord_brouillard_visa(borid integer);
procedure set_bord_brouillard_paiement(lemois varchar, borid number, exeordre number);
procedure set_bord_brouillard_retenues(borid number);
procedure set_bord_brouillard_sacd(borid number);
end; 
/


CREATE OR REPLACE PACKAGE BODY          Bordereau_Abricot_Paf IS

function getMoisCompletTexte(borId integer) return varchar
-- renvoie le mois sous le forme JANVIER 2009 (a partir d un bordereau PAF)
is
    exeOrdre number;
    moisComplet varchar2(50);
    lemois varchar2(3);
    leMoisNum integer;
    lemoisTxt varchar2(20);
begin

    SELECT DISTINCT exe_ordre 
    INTO exeordre
    FROM maracuja.bordereau m
    WHERE m.bor_id = borid;    
    
    SELECT DISTINCT substr(dep_numero,5,3)  INTO lemois
    FROM maracuja.DEPENSE d , maracuja.MANDAT m
    WHERE d.man_id = m.man_id
    AND m.bor_id = borid
    AND ROWNUM = 1;

    --lemois := lpad(to_char(to_number(lemois)),2,'0' );
    leMoisNum := to_number(lemois);
    
    if leMoisNum = 1 then
       lemoisTxt := 'JANVIER'; 
    elsif leMoisNum = 2 then
       lemoisTxt := 'FEVRIER'; 
    elsif leMoisNum = 3 then
       lemoisTxt := 'MARS'; 
    elsif leMoisNum = 4 then
       lemoisTxt := 'AVRIL';     
    elsif leMoisNum = 5 then
       lemoisTxt := 'MAI';           
    elsif leMoisNum = 6 then
       lemoisTxt := 'JUIN';     
    elsif leMoisNum = 7 then
       lemoisTxt := 'JUILLET'; 
    elsif leMoisNum = 8 then
       lemoisTxt := 'AOUT'; 
    elsif leMoisNum = 9 then
       lemoisTxt := 'SEPTEMBRE'; 
    elsif leMoisNum = 10 then
       lemoisTxt := 'OCTOBRE'; 
    elsif leMoisNum = 11 then
       lemoisTxt := 'NOVEMBRE'; 
    elsif leMoisNum = 12 then
       lemoisTxt := 'DECEMBRE'; 
    end if;                                                     
     
    return lemoisTxt||' '||exeordre;    
end;

PROCEDURE basculer_bouillard_paye(borid INTEGER) IS

tmpBordereau maracuja.BORDEREAU%ROWTYPE;
lemois VARCHAR2(50);
moiscomplet VARCHAR2(50);
moisordre INTEGER;
cpt INTEGER;

sumdebits NUMBER;
sumcredits NUMBER;
manid INTEGER;

is_sacd gestion_exercice.pco_num_185%TYPE;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN

-- recup des infos du bordereau
SELECT * INTO tmpBordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

-- recup du mois JANVIER XXXX
SELECT DISTINCT substr(dep_numero,5,3)  INTO lemois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

SELECT DISTINCT dep_numero   INTO moiscomplet
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
--fredSELECT mois_ordre,mois_complet INTO moisordre,moiscomplet FROM jefy_paye.paye_mois
--fredWHERE mois_complet = mois;



-- On verifie que les ecritures aient bien ete generees pour la composante en question.
SELECT COUNT(*) INTO cpt FROM jefy_paf.paf_ecritures WHERE ecr_comp = tmpBordereau.ges_code AND mois = lemois and exe_ordre = tmpBordereau.exe_ordre;

IF (cpt = 0) THEN
 RAISE_APPLICATION_ERROR(-20001,'Vous n''avez toujours pas prepare les ecritures pour la composante '||tmpBordereau.ges_code||' !');
END IF;

-- On verifie que le total des debits soit egal au total des credits  (Pour la composante)
 SELECT SUM(ecr_mont) INTO sumdebits FROM jefy_paf.paf_ecritures
 WHERE ecr_comp = ges_code AND mois = lemois AND ecr_type='64'
 AND ecr_comp = tmpBordereau.ges_code AND ecr_sens = 'D'
 and exe_ordre = tmpBordereau.exe_ordre;

 SELECT SUM(ecr_mont) INTO sumcredits FROM jefy_paf.paf_ecritures
 WHERE ecr_comp = ges_code AND mois = lemois AND ecr_type='64'
 AND ecr_comp = tmpBordereau.ges_code AND ecr_sens = 'C'
 and exe_ordre = tmpBordereau.exe_ordre;

 IF (sumcredits <> sumdebits)
 THEN
   RAISE_APPLICATION_ERROR(-20001,'Pour la composante '||tmpBordereau.ges_code||', la somme des DEBITS ('||sumdebits||') est diff?rente de la somme des CREDITS ('||sumcredits||') !');
 END IF;

  SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU
  WHERE bor_id = borid
  AND tbo_ordre = tmpBordereau.tbo_ordre
  AND exe_ordre = tmpBordereau.exe_ordre;

  IF cpt = 1 THEN
   Bordereau_Abricot_Paf.set_bord_brouillard_visa(borid);

    select pco_num_185 into is_sacd from gestion_exercice where exe_ordre = tmpBordereau.exe_ordre and ges_code = tmpBordereau.ges_code;
    if (is_sacd is not null)
    then
       Bordereau_Abricot_Paf.set_bord_brouillard_sacd(borid);
    end if;

   INSERT INTO maracuja.BORDEREAU_INFO VALUES  (borid, getMoisCompletTexte(borId),NULL);
  END IF;
  -- Mise a jour des ecritures de paiement dans bordereau_brouillard
  -- Ces ecritures seront associees a la premiere composante qui mandatera ses payes.
  Bordereau_Abricot_Paf.set_bord_brouillard_paiement(lemois, borid, tmpBordereau.exe_ordre);

-- misea jour dans papaye des tables apres bascule !
  -- maj de l etat de papaye_compta et du borid -
  update jefy_paf.paf_liquidations set liq_etat='MANDATEE'
    where ges_code=tmpBordereau.ges_code
      and mois=lemois and exe_ordre=tmpBordereau.exe_ordre and liq_ETAT='LIQUIDEE';

--fred UPDATE jefy_paye.jefy_paye_compta SET bor_id=borid, jpc_etat='MANDATEE'
--fred    WHERE ges_code=tmpBordereau.ges_code
--fred      AND mois_ordre=moisordre AND jpc_ETAT='LIQUIDEE';

 UPDATE jefy_paf.paf_etape SET bor_id=borid, pae_etat='MANDATEE', mois_libelle = getMoisCompletTexte(borid)
    WHERE ges_code=tmpBordereau.ges_code
        and  mois=lemois and exe_ordre=tmpBordereau.exe_ordre
        AND pae_ETAT='LIQUIDEE';



/*
  update jefy_paye.jefy_liquidations set liq_etat='MANDATEE'
    where ges_code=tmpBordereau.ges_code
      and mois_ordre=moisordre and liq_ETAT='LIQUIDEE';
*/

-- modifications FRED -> BUG REF ECRITURES MANDAT_DETAIL_ECRITURE VU PAR RODOLPHE 23/03/2007
-- on vide la recuperation
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paf.set_mandat_brouillard(manid);
END LOOP;
CLOSE c1;

END;


PROCEDURE basculer_bouillard_paye_orv(borid INTEGER) IS
cpt INTEGER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN

select 1 into cpt from dual;

-- on vide la recuperation du brouillard des mandats !
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits et credits  dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paf.set_mandat_orv_brouillard(manid);
END LOOP;
CLOSE c1;

END;


PROCEDURE basculer_bouillard_paye_regul(borid INTEGER) IS
cpt INTEGER;
manid INTEGER;

CURSOR c1 IS
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN
-- on vide la recuperation du brouillard des mandats !
select 1 into cpt from dual;

/*
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits et credits  dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paf.set_mandat_regul_brouillard(manid);
END LOOP;
CLOSE c1;
*/

END;





-- Ecritures de Paiement (Type 45 dans Jefy_ecritures).
PROCEDURE set_bord_brouillard_paiement(lemois varchar, borid NUMBER, exeordre NUMBER)
IS


currentecriture jefy_paf.paf_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;
bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;
gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;
--moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
moiscomplet VARCHAR2(50);
mois2 varchar2(50);

CURSOR ecriturespaiement IS
SELECT * FROM jefy_paf.paf_ecritures WHERE mois = lemois AND ecr_type='45' and exe_ordre = currentbordereau.exe_ordre;

cpt INTEGER;
--fred lemois VARCHAR2(50);

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;
-- recup du mois_complet
--fred SELECT mois_complet INTO moiscomplet FROM jefy_paye.paye_mois
--fred WHERE mois_ordre  = moisordre;
SELECT DISTINCT dep_numero   INTO moiscomplet
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

mois2 := getMoisCompletTexte(borid);


--SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD
--WHERE bob_operation LIKE '%PAIEMENT%' AND bob_libelle1 = 'PAIEMENT PAF '||moiscomplet;

SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD
WHERE bob_operation LIKE '%PAIEMENT%' AND bob_libelle1 = 'PAIEMENT PAF '||mois2;

-- cpt = 0 ==> Aucune ecriture de paiement passee pour ce mois
IF (cpt = 0)
THEN

  OPEN ecriturespaiement;
  LOOP
    FETCH ecriturespaiement INTO currentecriture;
    EXIT WHEN ecriturespaiement%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'PAIEMENT PAF',
 currentecriture.pco_num,
 'PAIEMENT PAF '||mois2, -- 'PAIEMENT PAF '||moiscomplet,
 mois2, --moiscomplet,
 NULL
 );

 END LOOP;
 CLOSE ecriturespaiement;
END IF;

END;

-- ECRITURES VISA - Ecritures de credit de type '64' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_visa(borid INTEGER)
IS

currentecriture jefy_paf.paf_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

CURSOR ecriturescredit64(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paf.paf_ECRITURES WHERE mois = lemois
AND ecr_comp = lacomp
AND ecr_type='64'
AND ( ecr_sens = 'C' OR (ecr_sens  = 'D' AND pco_num LIKE '4%' ))
and exe_ordre = currentbordereau.exe_ordre;

cpt INTEGER;
moisordre INTEGER;
lemoiscomplet varchar2(50);
gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

--fred moislibelle jefy_paf.paye_mois.mois_complet%TYPE;
lemois VARCHAR2(50);
mois2 varchar2(50);
BEGIN

    SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

    SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

    SELECT DISTINCT substr(dep_numero,5,3) INTO lemois
    FROM maracuja.DEPENSE d , maracuja.MANDAT m
    WHERE d.man_id = m.man_id
    AND m.bor_id = borid
    AND ROWNUM = 1;

    SELECT DISTINCT dep_numero INTO lemoiscomplet
    FROM maracuja.DEPENSE d , maracuja.MANDAT m
    WHERE d.man_id = m.man_id
    AND m.bor_id = borid
    AND ROWNUM = 1;

    SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

    mois2 := getMoisCompletTexte(borid);

    OPEN ecriturescredit64(lemois, currentbordereau.ges_code);
    LOOP
    FETCH ecriturescredit64 INTO currentecriture;
    EXIT WHEN ecriturescredit64%NOTFOUND;

         SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

         INSERT INTO BORDEREAU_BROUILLARD VALUES
         (
         bobordre,
         borid,
         currentbordereau.exe_ordre,
         currentecriture.ges_code,
         currentecriture.ecr_mont,
         currentecriture.ecr_sens,
         'VALIDE',
         'VISA PAF',
         currentecriture.pco_num,
         'VISA PAF '|| mois2, --'VISA PAF '||lemois,
         mois2, --lemoiscomplet,
         NULL
         );

    END LOOP;
    CLOSE ecriturescredit64;

END;


/******************************************
SET_MANDAT_ORV_BROUILLARD
******************************************/
PROCEDURE set_mandat_orv_brouillard(manid INTEGER)
IS

cpt     INTEGER;
dpcoid  INTEGER;
depid INTEGER;
classe4 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
classe6 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
montant NUMBER(12,2);

contre_partie_visa plan_comptable_exer.pco_num%TYPE;
ges_code_contrepartie comptabilite.ges_code%TYPE;

lemandat maracuja.MANDAT%ROWTYPE;

CURSOR plancos
IS SELECT dpco_id, dep_id, dpco_ttc_saisie FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

BEGIN


-- recup des infos du mandat
SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

SELECT par_value INTO contre_partie_visa FROM PARAMETRE WHERE exe_ordre = lemandat.exe_ordre and par_key  = 'CONTRE PARTIE VISA';

if (contre_partie_visa = 'AGENCE')
then
    select ges_code into ges_code_contrepartie from maracuja.comptabilite where com_ordre = 1;
else
    ges_code_contrepartie := lemandat.ges_code;
end if;

classe6 := lemandat.pco_num;

-- creation du brouillard Crediteur classe 6 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,                            --ECD_ORDRE,
   lemandat.exe_ordre,              --EXE_ORDRE,
   lemandat.ges_code,               --GES_CODE,
   ABS(lemandat.man_ttc),           --MAB_MONTANT,
   'VISA MANDAT',                   --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,   --MAB_ORDRE,
   'C',                             --MAB_SENS,
   manid,                           --MAN_ID,
   classe6                          --PCO_NU
);

SELECT count(*) into cpt
FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

 OPEN plancos;
 LOOP
 FETCH plancos INTO dpcoid, depid, montant;
 EXIT WHEN plancos%NOTFOUND;

    SELECT COUNT(*) INTO cpt
    FROM jefy_paf.paf_REVERSEMENTs e, jefy_depense.depense_ctrl_planco dpco
    WHERE dpco.dep_id = e.dep_id AND dpco_id = dpcoid;

    IF (cpt = 1 )      -- Bulletins negatifs, on va chercher la contrepartie dans jefy_paf.paf_reversements
    THEN

      SELECT PCO_NUM_CONTREPARTIE INTO classe4
      FROM jefy_paf.paf_REVERSEMENTs e, jefy_depense.depense_ctrl_planco dpco
      WHERE dpco.dep_id = e.dep_id AND dpco_id = dpcoid;

    ELSE                -- OR Manuel

--      classe4 := jefy_paye.get_contrepartie(classe6, depid);
        classe4 := '4632';
    
    END IF;

      -- creation du brouillard DEBITEUR CLASSE 4 !
      INSERT INTO MANDAT_BROUILLARD VALUES (
         NULL,                          --ECD_ORDRE,
         lemandat.exe_ordre,            --EXE_ORDRE,
         ges_code_contrepartie,             --GES_CODE,
         ABS(montant),                  --MAB_MONTANT,
         'VISA MANDAT',                 --MAB_OPERATION,
         mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
         'D',                           --MAB_SENS,
         manid,                         --MAN_ID,
         classe4                        --PCO_NU
      );

 END LOOP;
 CLOSE plancos;


END;


PROCEDURE set_mandat_regul_brouillard(manid INTEGER)
IS
cpt     INTEGER;
dpcoid  INTEGER;
classe4 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
classe6 maracuja.PLAN_COMPTABLE.pco_num%TYPE;
montant NUMBER(12,2);

lemandat maracuja.MANDAT%ROWTYPE;

BEGIN
-- recup des infos du mandat
SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;
/*
-- recup du dpcoid de ce mandat papaye : un mandat pour un depense_ctrl_planco
SELECT dpco_id INTO dpcoid  FROM jefy_depense.depense_ctrl_planco
WHERE man_id = manid;

-- recup des comptes et du montant (brouillard)
SELECT PCO_NUM_CONTREPARTIE,e.pco_num ,ecr_mont INTO classe4, classe6 , montant
FROM jefy_paye.JEFY_ECRITURES_REVERSEMENT e, jefy_depense.depense_ctrl_planco dpco
WHERE dpco.dep_id = e.dep_id_rev AND dpco_id = dpcoid;


-- creation du brouillard Crediteur classe 6 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(montant),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
   'D',         --MAB_SENS,
   manid,        --MAN_ID,
   classe6             --PCO_NU
);


-- creation du brouillard DEBITEUR CLASSE 4 !
INSERT INTO MANDAT_BROUILLARD VALUES
 (
   NULL,          --ECD_ORDRE,
   lemandat.exe_ordre,                     --EXE_ORDRE,
   lemandat.ges_code,              --GES_CODE,
   ABS(montant),                                 --MAB_MONTANT,
   'VISA MANDAT',       --MAB_OPERATION,
   mandat_brouillard_seq.NEXTVAL,                         --MAB_ORDRE,
   'C',         --MAB_SENS,
   manid,        --MAN_ID,
   classe4             --PCO_NU
);
*/
END;


-- Ecritures de retenues / Oppositions - Ecritures de type '44' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_retenues(borid NUMBER)
IS


currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

CURSOR ecrituresretenues(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.jefy_ecritures
WHERE mois_ordre = lemois
AND ecr_comp = lacomp AND ecr_type='44'
and exe_ordre = currentbordereau.exe_ordre;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;
/*
SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid
AND ROWNUM = 1;

-- recup du moiordre
SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois
WHERE mois_complet = mois;

SELECT mois_ordre INTO moisordre FROM jefy_paye.paye_mois WHERE mois_complet  = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituresretenues(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecrituresretenues INTO currentecriture;
    EXIT WHEN ecrituresretenues%NOTFOUND;

 SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

 INSERT INTO BORDEREAU_BROUILLARD VALUES
 (
 bobordre,
 borid,
 currentbordereau.exe_ordre,
 currentecriture.ges_code,
 currentecriture.ecr_mont,
 currentecriture.ecr_sens,
 'VALIDE',
 'RETENUES PAF',
 currentecriture.pco_num,
 'RETENUES PAF '||mois,
 mois,
 NULL
 );

 END LOOP;
  CLOSE ecrituresretenues;
*/
END;

-- Ecritures SACD - Ecritures de type '18' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_sacd(borid NUMBER)
IS

currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

CURSOR ecrituressacd(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paf.paf_ecritures
WHERE mois = lemois AND ecr_type='18'
and exe_ordre = currentbordereau.exe_ordre;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

mois VARCHAR2(50);
mois2 varchar2(50);

BEGIN

    mois2 := getMoisCompletTexte(borid);
    select count(*) into cpt from bordereau_brouillard where bob_operation = 'SACD PAF' and bob_libelle2 = mois2;

    if (cpt = 0)        -- On a pas encore passe les ecritures SACD
    then

        SELECT DISTINCT substr(dep_numero,5,3) INTO moisordre
        FROM maracuja.DEPENSE d , maracuja.MANDAT m
        WHERE d.man_id = m.man_id
        AND m.bor_id = borid
        AND ROWNUM = 1;

        SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

        SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

        SELECT DISTINCT dep_numero INTO mois
        FROM maracuja.DEPENSE d , maracuja.MANDAT m
        WHERE d.man_id = m.man_id
        AND m.bor_id = borid
        AND ROWNUM = 1;

        SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

         OPEN ecrituressacd(moisordre, currentbordereau.ges_code);
         LOOP
         FETCH ecrituressacd INTO currentecriture;
         EXIT WHEN ecrituressacd%NOTFOUND;

             SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

             INSERT INTO BORDEREAU_BROUILLARD VALUES
             (
             bobordre,
             borid,
             currentbordereau.exe_ordre,
             currentecriture.ges_code,
             currentecriture.ecr_mont,
             currentecriture.ecr_sens,
             'VALIDE',
             'SACD PAF',
             currentecriture.pco_num,
             'SACD PAF '||mois2,
             mois2,
             NULL
             );

         END LOOP;
         CLOSE ecrituressacd;

    end if;

END;



-- Ecritures de visa des payes (Debit 6) .
PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

lemandat     MANDAT%ROWTYPE;

BEGIN

SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

-- creation du mandat_brouillard visa DEBIT--
INSERT INTO MANDAT_BROUILLARD VALUES
(
NULL,           --ECD_ORDRE,
lemandat.exe_ordre,      --EXE_ORDRE,
lemandat.ges_code,      --GES_CODE,
lemandat.man_ht,      --MAB_MONTANT,
'VISA PAF',       --MAB_OPERATION,
mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
'D',         --MAB_SENS,
manid,         --MAN_ID,
lemandat.pco_num      --PCO_NU
);

END;


/*
PROCEDURE get_facture_jefy
(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER)
IS

depid       DEPENSE.dep_id%TYPE;
jefyfacture   jefy.factures%ROWTYPE;
lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
fouadresse    DEPENSE.dep_adresse%TYPE;
founom     DEPENSE.dep_fournisseur%TYPE;
lotordre     DEPENSE.dep_lot%TYPE;
marordre   DEPENSE.dep_marches%TYPE;
fouordre   DEPENSE.fou_ordre%TYPE;
gescode    DEPENSE.ges_code%TYPE;
cpt     INTEGER;
 tcdordre   TYPE_CREDIT.TCD_ORDRE%TYPE;
 tcdcode    TYPE_CREDIT.tcd_code%TYPE;

lemandat MANDAT%ROWTYPE;

CURSOR factures IS
 SELECT * FROM jefy.factures
 WHERE man_ordre = manordre;

BEGIN

OPEN factures;
LOOP
FETCH factures INTO jefyfacture;
EXIT WHEN factures%NOTFOUND;

-- creation du depid --
SELECT depense_seq.NEXTVAL INTO depid FROM dual;


 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
    --recuperer le type de credit a partir de la commande
   SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

   SELECT tc.tcd_ordre INTO tcdordre
    FROM TYPE_CREDIT tc
    WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

-- creation de lignebudgetaire--
SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE

   --recuperer le type de credit a partir de la commande
   SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

   SELECT tc.tcd_ordre INTO tcdordre
    FROM TYPE_CREDIT tc
    WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT  MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- recuperations --

-- gescode --
 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
SELECT MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- fouadresse --
SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
INTO fouadresse
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- founom --
SELECT adr_nom||' '||adr_prenom
INTO founom
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- fouordre --
 SELECT fou_ordre INTO fouordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre;

-- lotordre --
SELECT COUNT(*) INTO cpt
FROM marches.attribution
WHERE att_ordre =
(
 SELECT lot_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

 IF cpt = 0 THEN
  lotordre :=NULL;
 ELSE
  SELECT lot_ordre
  INTO lotordre
  FROM marches.attribution
  WHERE att_ordre =
  (
   SELECT lot_ordre
   FROM jefy.commande
   WHERE cde_ordre = jefyfacture.cde_ordre
  );
 END IF;

-- marordre --
SELECT COUNT(*) INTO cpt
FROM marches.lot
WHERE lot_ordre = lotordre;

IF cpt = 0 THEN
  marordre :=NULL;
ELSE
 SELECT mar_ordre
 INTO marordre
 FROM marches.lot
 WHERE lot_ordre = lotordre;
END IF;

SELECT * INTO lemandat FROM MANDAT WHERE man_id=manid;




-- creation de la depense --
INSERT INTO DEPENSE VALUES
(
fouadresse ,           --DEP_ADRESSE,
NULL ,       --DEP_DATE_COMPTA,
jefyfacture.dep_date,  --DEP_DATE_RECEPTION,
jefyfacture.dep_date , --DEP_DATE_SERVICE,
'VALIDE' ,      --DEP_ETAT,
founom ,      --DEP_FOURNISSEUR,
jefyfacture.dep_mont , --DEP_HT,
depense_seq.NEXTVAL ,  --DEP_ID,
lignebudgetaire ,    --DEP_LIGNE_BUDGETAIRE,
lotordre ,      --DEP_LOT,
marordre ,      --DEP_MARCHES,
jefyfacture.dep_ttc ,  --DEP_MONTANT_DISQUETTE,
jefyfacture.cm_ordre , --DEP_NOMENCLATURE,
jefyfacture.dep_fact  ,--DEP_NUMERO,
jefyfacture.dep_ordre ,--DEP_ORDRE,
NULL ,       --DEP_REJET,
jefyfacture.rib_ordre ,--DEP_RIB,
'NON' ,       --DEP_SUPPRESSION,
jefyfacture.dep_ttc ,  --DEP_TTC,
jefyfacture.dep_ttc
-jefyfacture.dep_mont, -- DEP_TVA,
exeordre ,      --EXE_ORDRE,
fouordre,       --FOU_ORDRE,
gescode,        --GES_CODE,
manid ,       --MAN_ID,
jefyfacture.man_ordre, --MAN_ORDRE,
--jefyfacture.mod_code,  --MOD_ORDRE,
lemandat.mod_ordre,
jefyfacture.pco_num ,  --PCO_ORDRE,
1,         --UTL_ORDRE
NULL, --org_ordre
tcdordre,
NULL, -- ecd_ordre_ema
jefyfacture.DEP_DATE
);

END LOOP;
CLOSE factures;

END;

*/
END; 
/

