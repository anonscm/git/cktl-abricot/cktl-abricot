CREATE OR REPLACE PACKAGE MARACUJA."BORDEREAU_ABRICOT" AS

/*
 * Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic

/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
 bordereau_1R1T
 bordereau_NR1T
 bordereau_1D1M
 bordereau_1D1M1R1T
 ndep_mand_org_fou_rib_pco (bordereau_ND1M)
 ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
 ndep_mand_fou_rib_pco (bordereau_ND1M)
 ndep_mand_fou_rib_pco_mod (bordereau_ND1M)

abr_etat='ATTENTE' qd la selection n est pas sur un bordereau
abr_etat='TRAITE' qd la selection est sur le bordereau
*/

-- version du 02/03/2007

procedure creer_bordereau (abrid integer);
procedure viser_bordereau_rejet (brjordre integer);

function get_selection_id (info varchar ) return integer ;
function get_selection_borid (abrid integer) return integer ;
procedure set_selection_id (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a06tboordre integer,a07abrgroupby varchar,a08gescode varchar);
procedure set_selection_intern (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar);
procedure set_selection_paye (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar);

-- creer bordereau (tbo_ordre) + numerotation
function get_num_borid (tboordre integer,exeordre integer,gescode varchar,utlordre integer ) return integer;

-- les algo de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
procedure bordereau_1R1T(abrid integer,monborid integer);
procedure bordereau_NR1T(abrid integer,monborid integer);
procedure bordereau_ND1M(abrid integer,monborid integer);
procedure bordereau_1D1M(abrid integer,monborid integer);
procedure bordereau_1D1M1R1T(abrid integer,boridep integer,boridrec integer);

-- les mandats et titres
function set_mandat_depense (dpcoid integer,borid integer) return integer;
function set_mandat_depenses (lesdpcoid varchar,borid integer) return integer;
function set_titre_recette (rpcoid integer,borid integer) return integer;
function set_titre_recettes (lesrpcoid varchar,borid integer) return integer;


--function ndep_mand_org_fou_rib_pco (abrid integer,borid integer) return integer;
function ndep_mand_org_fou_rib_pco_mod  (abrid integer,borid integer) return integer;
--function ndep_mand_fou_rib_pco  (abrid integer,borid integer) return integer;
function ndep_mand_fou_rib_pco_mod  (abrid integer,borid integer) return integer;


-- procedures de verifications des etats
function selection_valide (abrid integer) return integer;
function recette_valide (recid integer) return integer;
function depense_valide (depid integer) return integer;
function verif_bordereau_selection(borid integer,abrid integer) return integer;


-- procedures de locks de transaction
procedure lock_mandats;
procedure lock_titres;

-- procedure de recuperation des donn?e ordonnateur
--PROCEDURE get_depense_jefy_depense (manid INTEGER,utlordre INTEGER);
PROCEDURE get_depense_jefy_depense (manid INTEGER);
PROCEDURE get_recette_jefy_recette (titid INTEGER);
procedure  Get_recette_prelevements (titid INTEGER);

-- procedures du brouillard
PROCEDURE set_mandat_brouillard(manid INTEGER);
PROCEDURE set_mandat_brouillard_intern(manid INTEGER);
--PROCEDURE maj_plancomptable_mandat (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

PROCEDURE Set_Titre_Brouillard(titid INTEGER);
PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER);
--PROCEDURE maj_plancomptable_titre (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

-- outils
function inverser_sens_orv (tboordre integer,sens varchar) return varchar;
function recup_gescode (abrid integer) return varchar;
function recup_utlordre (abrid integer) return integer;
function recup_exeordre (abrid integer) return integer;
function recup_tboordre (abrid integer) return integer;
function recup_groupby (abrid integer) return varchar;
function traiter_orgid (orgid integer,exeordre integer) return integer;
function inverser_sens (sens varchar) return varchar;

-- apres creation des bordereaux
procedure numeroter_bordereau(borid integer);
procedure controle_bordereau(borid integer);
PROCEDURE ctrl_date_exercice(borid INTEGER);

END;



CREATE OR REPLACE PACKAGE BODY MARACUJA."BORDEREAU_ABRICOT" AS

PROCEDURE creer_bordereau (abrid INTEGER) IS
cpt INTEGER;
abrgroupby ABRICOT_BORD_SELECTION.ABR_GROUP_BY%TYPE;
monborid_dep INTEGER;
monborid_rec INTEGER;

cursor lesmandats is
select man_id from mandat where bor_id = monborid_dep;

cursor lestitres is
select tit_id from titre where bor_id = monborid_rec;

tmpmandid integer;
tmptitid integer;
tboordre integer;
BEGIN

-- est ce une selection vide ???
SELECT COUNT(*) INTO cpt
FROM ABRICOT_BORD_SELECTION
where abr_id = abrid;


if cpt != 0 then
/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
 bordereau_1R1T
 bordereau_1D1M
 bordereau_1D1M1R1T
 ndep_mand_org_fou_rib_pco (bordereau_ND1M)
 ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
 ndep_mand_fou_rib_pco (bordereau_ND1M)
 ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/


-- recup du group by pour traiter les cursors
abrgroupby := recup_groupby(abrid);

IF (abrgroupby = 'bordereau_1R1T') THEN
 monborid_rec := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 bordereau_1R1T(abrid,monborid_rec);


END IF;

IF (abrgroupby = 'bordereau_NR1T') THEN
 monborid_rec := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 bordereau_NR1T(abrid,monborid_rec);

-- controle RA
 select count(*) into cpt
 from titre
 where ori_ordre is not null
 and bor_id =monborid_rec;

if cpt != 0 then
  raise_application_error (-20001,'Impossiblde traiter une recette sur convention affectee dans un bordereau collectif !');
end if;

END IF;

IF (abrgroupby = 'bordereau_1D1M') THEN
 monborid_dep := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 bordereau_1D1M(abrid,monborid_dep);


END IF;


/*
IF (abrgroupby = 'bordereau_1D1M1R1T') THEN
 monborid_dep := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 monborid_rec := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 bordereau_1D1M(abrid,monborid_dep);
 bordereau_1R1T(abrid,monborid_rec);


END IF;
*/

IF (abrgroupby NOT IN ( 'bordereau_1R1T','bordereau_NR1T','bordereau_1D1M','bordereau_1D1M1R1T')) THEN
 monborid_dep := get_num_borid(
 recup_tboordre(abrid),
 recup_exeordre(abrid),
 recup_gescode(abrid),
 recup_utlordre(abrid)
 );

 bordereau_ND1M(abrid,monborid_dep);



END IF;


IF (monborid_dep is not null) THEN
 bordereau_abricot.numeroter_bordereau(monborid_dep);

 open lesmandats;
 loop
 fetch lesmandats into tmpmandid;
 exit when lesmandats%notfound;
 get_depense_jefy_depense(tmpmandid);
 end loop;
 close lesmandats;

 controle_bordereau(monborid_dep);

END IF;

IF (monborid_rec  is not null) THEN
 bordereau_abricot.numeroter_bordereau(monborid_rec);

  open lestitres;
 loop
 fetch lestitres into tmptitid;
 exit when lestitres%notfound;
 -- recup du brouillard
 get_recette_jefy_recette(tmptitid);
 Set_Titre_Brouillard(tmptitid);
 Get_recette_prelevements(tmptitid);

 end loop;
 close lestitres;

controle_bordereau(monborid_rec);
END IF;

-- maj de l etat dans la selection
IF (monborid_dep is not null OR monborid_rec  is not null) then

 if monborid_rec  is not null then
  UPDATE ABRICOT_BORD_SELECTION SET abr_etat ='TRAITE',bor_id = monborid_rec
  WHERE abr_id = abrid;
 end if;

 if monborid_dep  is not null then
  UPDATE ABRICOT_BORD_SELECTION SET abr_etat ='TRAITE',bor_id = monborid_dep
  WHERE abr_id = abrid;

  select tbo_ordre into tboordre from bordereau where bor_id = monborid_dep;

-- pour les bordereaux de papaye on retravaille le brouillard
  if tboordre = 3  then
   bordereau_abricot_paye.basculer_bouillard_paye(monborid_dep);
  end if;


-- pour les bordereaux d'orv de papaye on retravaille le brouillard
  if tboordre = 18  then
   bordereau_abricot_paye.basculer_bouillard_paye_orv(monborid_dep);
  end if;


-- pour les bordereaux de regul de papaye on retravaille le brouillard
  if tboordre = 19  then
   bordereau_abricot_paye.basculer_bouillard_paye_regul(monborid_dep);
  end if;

-- pour les bordereaux de PAF on retravaille le brouillard
 if tboordre = 20  then
   bordereau_abricot_paf.basculer_bouillard_paye(monborid_dep);
  end if;


-- pour les bordereaux d'orv de PAF on retravaille le brouillard
-- if tboordre = 21  then
--  bordereau_abricot_paf.basculer_bouillard_paye_orv(monborid_dep);
--  end if;


-- pour les bordereaux de recette de PAF on retravaille le brouillard
--  if tboordre = -999  then
--   bordereau_abricot_paf.basculer_bouillard_paye_recettte(monborid_rec);
--  end if;


 end if;

end if;


end if;


END;

PROCEDURE viser_bordereau_rejet (brjordre INTEGER) IS
cpt INTEGER;
manid maracuja.mandat.man_id%type;
titid maracuja.titre.tit_id%type;
tboordre integer;

reduction integer;
utlordre integer;
dpcoid integer;
recId integer;
depsuppression varchar2(20);
rpcoid integer;
recsuppression varchar2(20);
exeordre integer;

cursor mandats is
select man_id from  maracuja.mandat where brj_ordre = brjordre;

cursor depenses is
select dep_ordre,dep_suppression from  maracuja.depense where man_id = manid;


cursor titres is
select tit_id from  maracuja.titre where brj_ordre = brjordre;

cursor recettes is
select rec_ordre,rec_suppression from  maracuja.recette where tit_id = titid;

deliq integer;
BEGIN

open mandats;
loop
fetch mandats into manid;
exit when mandats%notfound;

open depenses;
loop
fetch depenses into dpcoid,depsuppression;
exit when depenses%notfound;
 -- casser le liens des mand_id dans depense_ctrl_planco
   -- supprimer le liens compteble <-> depense dans l inventaire
 jefy_depense.ABRICOT.upd_depense_ctrl_planco (dpcoid,null);

select tbo_ordre,exe_ordre into tboordre,exeordre from jefy_depense.depense_ctrl_planco
where dpco_id = dpcoid;

-- suppression de la depense demand?e par la personne qui a vis? et pas un bordereau de prestation interne depense 201
 if depsuppression = 'OUI' and  tboordre != 201 then
--  select max(utl_ordre) into utlordre from jefy_depense.depense_budget jdb,jefy_depense.depense_ctrl_planco jpbp
--  where jpbp.dep_id = jdb.dep_id
--  and dpco_id = dpcoid;
/*
  deliq:=jefy_depense.Get_Fonction('DELIQ');

  SELECT max(utl_ordre ) into utlordre
  FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
  WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=exeordre  AND
  uf.fon_ordre=deliq AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng='O';

    if utlordre is null then
    deliq:=jefy_depense.Get_Fonction('DELIQINV');

  SELECT max(utl_ordre ) into utlordre
  FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
  WHERE ufe.uf_ordre=uf.uf_ordre
  AND ufe.exe_ordre=exeordre
  AND uf.fon_ordre=deliq
  AND ufe.exe_ordre=e.exe_ordre
  AND exe_stat_eng='R';
  end if;
*/
 select utl_ordre into utlordre from jefy_depense.depense_budget
    where dep_id in (select dep_id from jefy_depense.depense_ctrl_planco where dpco_id=dpcoid);

  jefy_depense.ABRICOT.del_depense_ctrl_planco (dpcoid,utlordre);
 end if;

end loop;
close depenses;

end loop;
close mandats;

open titres;
loop
fetch titres into titid;
exit when titres%notfound;

open recettes;
loop
fetch recettes into rpcoid,recsuppression;
exit when recettes%notfound;


-- casser le liens des tit_id dans recette_ctrl_planco
 SELECT r.rec_id_reduction INTO reduction
 FROM jefy_recette.RECETTE r, jefy_recette.RECETTE_CTRL_PLANCO rpco
 WHERE rpco.rpco_id = rpcoid AND rpco.rec_id = r.rec_id;

 IF reduction IS NOT NULL THEN
  jefy_recette.API.upd_reduction_ctrl_planco (rpcoid,null);
 ELSE
  jefy_recette.API.upd_recette_ctrl_planco (rpcoid,null);
 END IF;

select tbo_ordre,exe_ordre into tboordre,exeordre from jefy_recette.recette_ctrl_planco
where rpco_id = rpcoid;

-- GESTION DES SUPPRESSIONS
-- suppression de la recette demand?e par la personne qui a vis? et pas un bordereau de prestation interne recette 200
 if recsuppression = 'OUI' and  tboordre != 200 then
     select utl_ordre into utlordre from jefy_recette.recette_budget
        where rec_id in (select rec_id from jefy_recette.recette_ctrl_planco where rpco_id=rpcoid);
        
     select rec_id into recId from jefy_recette.recette_ctrl_planco where rpco_id=rpcoid;
        
      jefy_recette.API.del_recette (recId,utlordre);
 end if;


end loop;
close recettes;


end loop;
close titres;

-- on passe le brjordre a VISE
update bordereau_rejet set brj_etat = 'VISE'
where brj_ordre = brjordre;

END;



function get_selection_id (info varchar ) return integer is
selection integer;
begin
select
maracuja.ABRICOT_BORD_SELECTION_SEQ.nextval into selection from dual;
return selection;
end;


function get_selection_borid (abrid integer) return integer is
borid integer;
begin
select distinct bor_id into borid from maracuja.ABRICOT_BORD_SELECTION where abr_id = abrid;
return borid;
end;


procedure set_selection_id (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a06tboordre integer,a07abrgroupby varchar,a08gescode varchar) is

chaine varchar(32000);
premier integer;
tmpdepid integer;
tmprecid integer;
cpt integer;

begin
/*
 bordereau_1R1T
 bordereau_1D1M
 bordereau_1D1M1R1T
 ndep_mand_org_fou_rib_pco (bordereau_ND1M)
 ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
 ndep_mand_fou_rib_pco (bordereau_ND1M)
 ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

-- traitement de la chaine des depid
if a02lesdepid is not null OR length(a02lesdepid) >0 then
chaine := a02lesdepid;
LOOP
  premier:=1;
  -- On recupere le depordre
  LOOP
   IF SUBSTR(chaine,premier,1)='$' THEN
    tmpdepid:=En_Nombre(SUBSTR(chaine,1,premier-1));
    --   IF premier=1 THEN depordre := NULL; END IF;
   EXIT;
   ELSE
    premier:=premier+1;
   END IF;
  END LOOP;

insert into MARACUJA.ABRICOT_BORD_SELECTION
(ABR_ID,UTL_ORDRE,DEP_ID,REC_ID,EXE_ORDRE,TBO_ORDRE,ABR_ETAT,ABR_GROUP_BY,GES_CODE)
values
(
a01abrid,--ABR_ID
a04UTLORDRE, --ult_ordre
tmpdepid,--DEP_ID
null,--REC_ID
a05exeordre,--EXE_ORDRE
a06tboordre,--TBO_ORDRE,
'ATTENTE',--ABR_ETAT,
a07abrgroupby,--,ABR_GROUP_BY,GES_CODE
a08gescode --ges_code
);

--RECHERCHE DU CARACTERE SENTINELLE
IF SUBSTR(chaine,premier+1,1)='$'  THEN EXIT;  END IF;
chaine:=SUBSTR(chaine,premier+1,LENGTH(chaine));
END LOOP;
end if;

 -- traitement de la chaine des recid
if a03lesrecid is not null OR length(a03lesrecid) >0 then
chaine := a03lesrecid;
LOOP
  premier:=1;
  -- On recupere le depordre
  LOOP
   IF SUBSTR(chaine,premier,1)='$' THEN
    tmprecid:=En_Nombre(SUBSTR(chaine,1,premier-1));
    --   IF premier=1 THEN depordre := NULL; END IF;
   EXIT;
   ELSE
    premier:=premier+1;
   END IF;
  END LOOP;

insert into MARACUJA.ABRICOT_BORD_SELECTION
(ABR_ID,UTL_ORDRE,DEP_ID,REC_ID,EXE_ORDRE,TBO_ORDRE,ABR_ETAT,ABR_GROUP_BY,GES_CODE)
values
(
a01abrid,--ABR_ID
a04UTLORDRE, --ult_ordre
null,--DEP_ID
tmprecid,--REC_ID
a05exeordre,--EXE_ORDRE
a06tboordre,--TBO_ORDRE,
'ATTENTE',--ABR_ETAT,
a07abrgroupby,--,ABR_GROUP_BY,GES_CODE
a08gescode --ges_code
);

--RECHERCHE DU CARACTERE SENTINELLE
IF SUBSTR(chaine,premier+1,1)='$'  THEN EXIT;  END IF;
chaine:=SUBSTR(chaine,premier+1,LENGTH(chaine));
END LOOP;
end if;

select count(*) into cpt
from jefy_depense.depense_ctrl_planco
where dpco_id in ( select DEP_ID from abricot_bord_selection where abr_id = a01abrid)
and man_id is not null;


if cpt > 0 then
 raise_application_error (-20001,'VOTRE SELECTION CONTIENT UNE FACTURE DEJA SUR BORDEREAU  !');
end if;


select count(*) into cpt
from jefy_recette.recette_ctrl_planco
where rpco_id in ( select REC_ID from abricot_bord_selection where abr_id = a01abrid)
and tit_id is not null;


if cpt > 0 then
 raise_application_error (-20001,'VOTRE SELECTION CONTIENT UNE RECETTE DEJA SUR BORDEREAU !');
end if;


bordereau_abricot.creer_bordereau(a01abrid);
end;


procedure set_selection_intern (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar)
is
begin

-- ATENTION
-- tboordre : 200 recettes internes
-- tboordre : 201 mandats internes

-- les mandats
set_selection_id(a01abrid ,a02lesdepid ,null  ,a04utlordre ,a05exeordre  ,201 ,'bordereau_1D1M' ,a08gescodemandat );

-- les titres
set_selection_id(-a01abrid ,null ,a03lesrecid  ,a04utlordre ,a05exeordre  ,200 ,'bordereau_1R1T' ,a09gescodetitre );

end;


procedure set_selection_paye (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar)
is
boridtmp integer;
moisordre integer;
begin
/*
 -- a07abrgroupby = mois
 select mois_ordre into moisordre from jef_paye.paye_mois where mois_complet = a07abrgroupby;

 -- CONTROLES
 -- peux t on mandater la composante --
 select count(*) into cpt from jefy_depense.papaye_compta
 where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
 and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby);

 if cpt = 0 then  raise_application_error (-20001,'PAS DE MANDATEMENT A EFFECTUER');  end if;

 select mois_ordre into moisordre from papaye.paye_mois where mois_complet = a07abrgroupby;

 -- peux t on mandater la composante --
 select count(*) into cpt from jefy_depense.papaye_compta
 where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
 and mois_ordre=moisordre and ETAT<>'LIQUIDEE';

 if (cpt = 1) then
  raise_application_error (-20001,' MANDATEMENT DEJA EFFECTUE POUR LE MOIS DE "'||a07abrgroupby||'", composante : '||a08gescodemandat);
 end if;
*/
-- ATENTION
-- tboordre : 3 salaires

-- les mandats de papaye
 set_selection_id(a01abrid ,a02lesdepid ,null  ,a04utlordre ,a05exeordre  ,3 ,'bordereau_1D1M' ,a08gescodemandat );
 boridtmp :=get_selection_borid (a01abrid);

 /*
-- maj de l etat de papaye_compta et du bor_ordre -
 update jefy_depense.papaye_compta set bor_ordre=boridtmp, etat='MANDATEE'
 where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
 and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby) and ETAT='LIQUIDEE';
*/
-- Mise a jour des brouillards de paye pour le mois
--  maracuja.bordereau_papaye.maj_brouillards_payes(moisordre, boridtmp);

-- bascule du brouillard de papaye


-- les ORV ??????
--set_selection_id(-a01abrid ,null ,a03lesrecid  ,a04utlordre ,a05exeordre  ,200 ,'bordereau_1R1T' ,a09gescodetitre );

end;


-- creer bordereau (tbo_ordre) + numerotation
FUNCTION get_num_borid (tboordre INTEGER,exeordre INTEGER,gescode VARCHAR,utlordre INTEGER ) RETURN INTEGER IS
cpt INTEGER;
borid INTEGER;
bornum INTEGER;
BEGIN
-- creation du bor_id --
SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

-- creation du bordereau --
bornum :=-1;
INSERT INTO BORDEREAU
(
BOR_DATE_VISA,
BOR_ETAT,
BOR_ID,
BOR_NUM,
BOR_ORDRE,
EXE_ORDRE,
GES_CODE,
TBO_ORDRE,
UTL_ORDRE,
UTL_ORDRE_VISA,
BOR_DATE_CREATION
)
VALUES (
NULL ,                         --BOR_DATE_VISA,
'VALIDE',              --BOR_ETAT,
borid,                  --BOR_ID,
bornum,                              --BOR_NUM,
-borid,                             --BOR_ORDRE,
--a partir de 2007 il n existe plus de bor_ordre pour conserver le constraint je met -borid
exeordre,                --EXE_ORDRE,
gescode,                          --GES_CODE,
tboordre,              --TBO_ORDRE,
utlordre,               --UTL_ORDRE,
NULL,                  --UTL_ORDRE_VISA
sysdate
);

RETURN borid;

END;

-- les algos de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
PROCEDURE bordereau_1R1T(abrid INTEGER,monborid INTEGER) IS

cpt INTEGER;
tmprecette jefy_recette. RECETTE_CTRL_PLANCO%ROWTYPE;

CURSOR rec_tit IS
SELECT r.*
FROM ABRICOT_BORD_SELECTION ab,jefy_recette.RECETTE_CTRL_PLANCO r
WHERE r.rpco_id = ab.rec_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
order by r.pco_num, r.rec_id ASC;

BEGIN


OPEN rec_tit;
LOOP
FETCH rec_tit INTO tmprecette;
EXIT WHEN rec_tit%NOTFOUND;
cpt:=set_titre_recette(tmprecette.rpco_id,monborid);
END LOOP;
CLOSE rec_tit;

END;

PROCEDURE bordereau_NR1T(abrid INTEGER,monborid INTEGER) IS

ht  number(12,2);
tva number(12,2);
ttc number(12,2);
pconumero varchar(20);
nbpieces integer;

cpt INTEGER;
titidtemp integer;
tmprecette jefy_recette. RECETTE_CTRL_PLANCO%ROWTYPE;

-- curseur de regroupement
CURSOR rec_tit_group_by IS
SELECT
r.PCO_NUM ,
sum(r.RPCO_HT_SAISIE) ,
sum(r.RPCO_TVA_SAISIE) ,
sum(r.RPCO_TTC_SAISIE)
FROM ABRICOT_BORD_SELECTION ab,jefy_recette.RECETTE_CTRL_PLANCO r
WHERE r.rpco_id = ab.rec_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
group by r.PCO_NUM
order by r.pco_num ASC;


CURSOR rec_tit IS
SELECT r.*
FROM ABRICOT_BORD_SELECTION ab,jefy_recette.RECETTE_CTRL_PLANCO r
WHERE r.rpco_id = ab.rec_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
and r.pco_num = pconumero
order by r.pco_num ASC, r.rec_id;

BEGIN

open rec_tit_group_by;
loop
FETCH rec_tit_group_by INTO pconumero,ht,tva,ttc;
EXIT WHEN rec_tit_group_by%NOTFOUND;

titidtemp := 0;
OPEN rec_tit;
LOOP
FETCH rec_tit INTO tmprecette;
EXIT WHEN rec_tit%NOTFOUND;
 if titidtemp = 0 then
  titidtemp:=set_titre_recette(tmprecette.rpco_id,monborid);
 else
  update jefy_recette.RECETTE_CTRL_PLANCO
  set tit_id =titidtemp
  where rpco_id = tmprecette.rpco_id;
 end if;

END LOOP;
CLOSE rec_tit;

-- recup du nombre de pieces
-- TODO
 nbpieces := 0;

-- le fouOrdre du titre a faire pointer sur DEBITEUR DIVERS
-- ( a definir dans le parametrage)

-- maj des montants du titre
 update titre set
  TIT_HT = ht,
  TIT_NB_PIECE = nbpieces,
  TIT_TTC = ttc,
  TIT_TVA = tva,
  TIT_LIBELLE = 'TITRE COLLECTIF'
 where tit_id =titidtemp;

-- mise a jour du brouillard ?
-- BORDEREAU_ABRICOT.Set_Titre_Brouillard(titidtemp);

end loop;
close rec_tit_group_by;

END;

PROCEDURE bordereau_ND1M(abrid INTEGER,monborid INTEGER) IS
cpt INTEGER;

tmpdepense jefy_depense.depense_CTRL_PLANCO%ROWTYPE;
abrgroupby ABRICOT_BORD_SELECTION.ABR_GROUP_BY%TYPE;
-- cursor pour traites les conventions limitatives !!!!
-- 1D1M -> liaison comptabilite
CURSOR mand_dep_convra IS
SELECT distinct d.*
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.engage_budget e,
maracuja.V_CONVENTION_LIMITATIVE c
WHERE d.dpco_id = ab.dep_id
AND abr_id = abrid
AND  db.dep_id = d.dep_id
AND e.eng_id = db.eng_id
AND e.org_id = c.org_id (+)
AND e.exe_ordre = c.exe_ordre (+)
AND c.org_id IS NOT NULL
and d.man_id is null
AND ab.abr_etat='ATTENTE'
order by d.pco_num ASC;


-- POUR LE RESTE DE LA SELECTION :
-- un cusor par type de abr_goup_by
-- attention une selection est de base limitee a un exercice et une UB et un type de bordereau
-- dans l interface on peut restreindre a l agent qui a saisie la depense.
-- dans l interface on peut restreindre au CR ou SOUS CR qui budgetise la depense.
-- dans l interface on peut restreindre suivant les 2 criteres ci dessus.


BEGIN


OPEN mand_dep_convra;
LOOP
FETCH mand_dep_convra INTO tmpdepense;
EXIT WHEN mand_dep_convra%NOTFOUND;
cpt:=set_mandat_depense(tmpdepense.dpco_id,monborid);
END LOOP;
CLOSE mand_dep_convra;

-- recup du group by pour traiter le reste des mandats
SELECT DISTINCT abr_group_by INTO abrgroupby
FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;

-- il faut traiter les autres depenses non c_convra
--IF ( abrgroupby = 'ndep_mand_org_fou_rib_pco' ) THEN
-- cpt:=ndep_mand_org_fou_rib_pco(abrid ,monborid );
--END IF;

IF ( abrgroupby = 'ndep_mand_org_fou_rib_pco_mod'  ) THEN
 cpt:=ndep_mand_org_fou_rib_pco_mod(abrid ,monborid );
END IF;

--IF ( abrgroupby = 'ndep_mand_fou_rib_pco') THEN
-- cpt:=ndep_mand_fou_rib_pco(abrid ,monborid );
--END IF;

IF ( abrgroupby = 'ndep_mand_fou_rib_pco_mod') THEN
 cpt:=ndep_mand_fou_rib_pco_mod(abrid ,monborid );
END IF;


END;

PROCEDURE bordereau_1D1M(abrid INTEGER,monborid INTEGER) IS
cpt INTEGER;
tmpdepense jefy_depense.depense_CTRL_PLANCO%ROWTYPE;


CURSOR dep_mand IS
SELECT d.*
FROM ABRICOT_BORD_SELECTION ab,jefy_depense.depense_CTRL_PLANCO d
WHERE d.dpco_id = ab.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
order by d.pco_num ASC;

BEGIN


OPEN dep_mand;
LOOP
FETCH dep_mand INTO tmpdepense;
EXIT WHEN dep_mand%NOTFOUND;
cpt:=set_mandat_depense(tmpdepense.dpco_id,monborid);
END LOOP;
CLOSE dep_mand;

END;

PROCEDURE bordereau_1D1M1R1T(abrid INTEGER,boridep INTEGER,boridrec INTEGER) IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;
bordereau_1D1M(abrid,boridep);
bordereau_1R1T(abrid,boridrec);


END;


-- les mandats et titres


FUNCTION set_mandat_depense (dpcoid INTEGER,borid INTEGER)   RETURN INTEGER IS
cpt INTEGER;
flag integer;


ladepense       jefy_depense.depense_ctrl_planco%ROWTYPE;
ladepensepapier jefy_depense.depense_papier%ROWTYPE;
leengagebudget  jefy_depense.engage_budget%ROWTYPE;

gescode     GESTION.ges_code%TYPE;
manid         MANDAT.man_id%TYPE;

MANORGINE_KEY   MANDAT.MAN_ORGINE_KEY%TYPE;
MANORIGINE_LIB  MANDAT.MAN_ORIGINE_LIB%TYPE;
ORIORDRE     MANDAT.ORI_ORDRE%TYPE;
PRESTID     MANDAT.PREST_ID%TYPE;
TORORDRE     MANDAT.TOR_ORDRE%TYPE;
VIRORDRE     MANDAT.PAI_ORDRE%TYPE;
MANNUMERO       MANDAT.MAN_NUMERO%TYPE;
BEGIN

-- recuperation du ges_code --
SELECT ges_code INTO gescode
FROM BORDEREAU
WHERE bor_id = borid;

SELECT * INTO ladepense
FROM  jefy_depense.depense_ctrl_planco
WHERE dpco_id = dpcoid;

SELECT DISTINCT dpp.* INTO ladepensepapier
FROM jefy_depense.depense_papier dpp, jefy_depense.depense_budget db,jefy_depense.depense_ctrl_planco dpco
WHERE db.dep_id = dpco.dep_id
AND dpp.dpp_id = db.dpp_id
and dpco_id = dpcoid;

SELECT eb.* INTO leengagebudget
FROM jefy_depense.ENGAGE_BUDGET eb,jefy_depense.depense_budget db,jefy_depense.depense_ctrl_planco dpco
WHERE db.eng_id = eb.eng_id
and  db.dep_id = dpco.dep_id
and dpco_id = dpcoid;



-- Verifier si ligne budgetaire ouverte sur exercice
select count(*) into flag from maracuja.v_organ_exer where org_id = leengagebudget.org_id 
and exe_ordre=leengagebudget.exe_ordre;
if (flag=0) then
    RAISE_APPLICATION_ERROR (-20001,'La ligne budgetaire affectee a l''engagement num. '|| leengagebudget.ENG_NUMERO || ' n''est pas ouverte sur ' || leengagebudget.exe_ordre||'.');
end if;





-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
MANORGINE_KEY:=NULL;

--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
MANORIGINE_LIB:=NULL;

--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
ORIORDRE :=Gestionorigine.traiter_orgid(leengagebudget.org_id,leengagebudget.exe_ordre);

--PRESTID : PRESTATION INTERNE --
SELECT count(*) INTO cpt
FROM jefy_recette.PI_DEP_REC d, jefy_recette.PI_ENG_FAC e
WHERE  d.pef_id = e.pef_id
and d.dep_id = ladepense.dep_id;

if cpt = 1 then
 SELECT prest_id INTO PRESTID
 FROM jefy_recette.PI_DEP_REC d, jefy_recette.PI_ENG_FAC e
 WHERE  d.pef_id = e.pef_id
 and d.dep_id = ladepense.dep_id;
else
 PRESTID := null;
end if;

--TORORDRE : ORIGINE DU MANDAT --
TORORDRE := 1;

--VIRORDRE --
VIRORDRE := NULL;

-- creation du man_id --
SELECT mandat_seq.NEXTVAL INTO manid FROM dual;

-- recup du numero de mandat
MANNUMERO := -1;
INSERT INTO MANDAT
(
BOR_ID,
BRJ_ORDRE,
EXE_ORDRE,
FOU_ORDRE,
GES_CODE,
MAN_DATE_REMISE,
MAN_DATE_VISA_PRINC,
MAN_ETAT,
MAN_ETAT_REMISE,
MAN_HT,
MAN_ID,
MAN_MOTIF_REJET,
MAN_NB_PIECE,
MAN_NUMERO,
MAN_NUMERO_REJET,
MAN_ORDRE,
MAN_ORGINE_KEY,
MAN_ORIGINE_LIB,
MAN_TTC,
MAN_TVA,
MOD_ORDRE,
ORI_ORDRE,
PCO_NUM,
PREST_ID,
TOR_ORDRE,
PAI_ORDRE,
ORG_ORDRE,
RIB_ORDRE_ORDONNATEUR,
RIB_ORDRE_COMPTABLE
)
VALUES
(
borid ,                   --BOR_ID,
NULL,                        --BRJ_ORDRE,
ladepensepapier.exe_ordre,                   --EXE_ORDRE,
ladepensepapier.fou_ordre,--FOU_ORDRE,
gescode,                --GES_CODE,
NULL,                    --MAN_DATE_REMISE,
NULL,                    --MAN_DATE_VISA_PRINC,
'ATTENTE',                --MAN_ETAT,
'ATTENTE',                --MAN_ETAT_REMISE,
ladepense.dpco_montant_budgetaire, --MAN_HT,
manid,                    --MAN_ID,
NULL,                    --MAN_MOTIF_REJET,
ladepensepapier.dpp_nb_piece,--MAN_NB_PIECE,
MANNUMERO,--MAN_NUMERO,
NULL,                    --MAN_NUMERO_REJET,
-manid,--MAN_ORDRE,
-- a parir de 2007 plus de man_ordre mais pour conserver la contrainte je mets -manid
MANORGINE_KEY,            --MAN_ORGINE_KEY,
MANORIGINE_LIB,        --MAN_ORIGINE_LIB,
ladepense.dpco_ttc_saisie, --MAN_TTC,
ladepense.dpco_ttc_saisie - ladepense.dpco_montant_budgetaire,  --MAN_TVA,
ladepensepapier.mod_ordre,                --MOD_ORDRE,
ORIORDRE,                --ORI_ORDRE,
ladepense.pco_num,    --PCO_NUM,
PRESTID,                --PREST_ID,
TORORDRE,                --TOR_ORDRE,
VIRORDRE,                --VIR_ORDRE
leengagebudget.org_id,  --org_ordre
ladepensepapier.rib_ordre, --rib ordo
ladepensepapier.rib_ordre -- rib_comptable
);

-- maj du man_id  dans la depense
UPDATE jefy_depense.DEPENSE_CTRL_PLANCO
SET man_id = manid
WHERE dpco_id = dpcoid;

-- recup de la depense
--get_depense_jefy_depense(manid,ladepensepapier.utl_ordre);

-- recup du brouillard
set_mandat_brouillard(manid);


RETURN manid;

END;

-- lesdepid XX$FF$....$DDD$ZZZ$$
FUNCTION set_mandat_depenses (lesdpcoid VARCHAR,borid INTEGER)   RETURN INTEGER IS
cpt INTEGER;
premier integer;
tmpdpcoid integer;
chaine varchar(5000);
premierdpcoid integer;
manid integer;

ttc mandat.man_ttc%type;
tva mandat.man_tva%type;
ht mandat.man_ht%type;

utlordre integer;
nb_pieces integer;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

--RAISE_APPLICATION_ERROR (-20001,'lesdpcoid'||lesdpcoid);


premierdpcoid:=null;
 -- traitement de la chaine des depid xx$xx$xx$.....$x$$
if lesdpcoid is not null OR length(lesdpcoid) >0 then
chaine := lesdpcoid;
LOOP
  premier:=1;
  -- On recupere le depid
  LOOP
   IF SUBSTR(chaine,premier,1)='$' THEN
    tmpdpcoid:=En_Nombre(SUBSTR(chaine,1,premier-1));
    --   IF premier=1 THEN depordre := NULL; END IF;
   EXIT;
   ELSE
    premier:=premier+1;
   END IF;
  END LOOP;
-- creation du mandat lie au borid
 if premierdpcoid is null then
  manid:=set_mandat_depense(tmpdpcoid,borid);
  -- suppression du brouillard car il est uniquement sur la premiere depense
  delete from mandat_brouillard where man_id = manid;
  premierdpcoid:=tmpdpcoid;
 else
  -- maj du man_id  dans la depense
  UPDATE jefy_depense.DEPENSE_CTRL_PLANCO
  SET man_id = manid
  WHERE dpco_id = tmpdpcoid;

  -- recup de la depense (maracuja)
  SELECT DISTINCT dpp.utl_ordre INTO utlordre
  FROM jefy_depense.depense_papier dpp, jefy_depense.depense_budget db,jefy_depense.depense_ctrl_planco dpco
  WHERE db.dep_id = dpco.dep_id
  AND dpp.dpp_id = db.dpp_id
  and dpco_id = tmpdpcoid;

--  get_depense_jefy_depense(manid,utlordre);

 end if;

--RECHERCHE DU CARACTERE SENTINELLE
IF SUBSTR(chaine,premier+1,1)='$'  THEN EXIT;  END IF;
chaine:=SUBSTR(chaine,premier+1,LENGTH(chaine));
END LOOP;
end if;

-- mise a jour des montants du mandat HT TVA ET TTC nb pieces
select
sum(dpco_ttc_saisie),
sum(dpco_ttc_saisie - dpco_montant_budgetaire),
sum(dpco_montant_budgetaire)
into
ttc,
tva,
ht
from jefy_depense.DEPENSE_CTRL_PLANCO
where man_id = manid;

-- recup du nb de pieces
SELECT sum(dpp.dpp_nb_piece) INTO nb_pieces
FROM jefy_depense.depense_papier dpp, jefy_depense.depense_budget db,jefy_depense.depense_ctrl_planco dpco
WHERE db.dep_id = dpco.dep_id
AND dpp.dpp_id = db.dpp_id
and man_id = manid;


-- maj du mandat
update mandat set
man_ht = ht,
man_tva = tva,
man_ttc = ttc,
man_nb_piece = nb_pieces
where man_id = manid;

-- recup du brouillard
set_mandat_brouillard(manid);

RETURN cpt;

END;



FUNCTION set_titre_recette (rpcoid INTEGER,borid INTEGER)  RETURN INTEGER IS

jefytitre       jefy.TITRE%ROWTYPE;
gescode         GESTION.ges_code%TYPE;
titid           TITRE.tit_id%TYPE;

TITORGINEKEY    TITRE.tit_ORGINE_KEY%TYPE;
TITORIGINELIB   TITRE.tit_ORIGINE_LIB%TYPE;
ORIORDRE        TITRE.ORI_ORDRE%TYPE;
PRESTID         TITRE.PREST_ID%TYPE;
TORORDRE        TITRE.TOR_ORDRE%TYPE;
modordre        TITRE.mod_ordre%TYPE;
presid          INTEGER;
cpt             INTEGER;
VIRORDRE        INTEGER;
flag            INTEGER;


recettepapier   jefy_recette.recette_papier%ROWTYPE;
recettebudget   jefy_recette.recette_budget%ROWTYPE;
facturebudget   jefy_recette.facture_budget%ROWTYPE;
recettectrlplanco    jefy_recette. RECETTE_CTRL_PLANCO%rowtype;

BEGIN


-- recuperation du ges_code --
SELECT ges_code INTO gescode
FROM BORDEREAU
WHERE bor_id = borid;

--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);

SELECT * INTO recettectrlplanco
FROM  jefy_recette. RECETTE_CTRL_PLANCO
WHERE rpco_id = rpcoid;

SELECT * INTO recettebudget
FROM jefy_recette.recette_budget
WHERE rec_id = recettectrlplanco.rec_id;

SELECT * INTO facturebudget
FROM jefy_recette.facture_BUDGET
where fac_id = recettebudget.fac_id;

select * into recettepapier
from jefy_recette.recette_papier
where rpp_id = recettebudget.rpp_id;



-- Verifier si ligne budgetaire ouverte sur exercice
select count(*) into flag from maracuja.v_organ_exer where org_id = facturebudget.org_id 
and exe_ordre=facturebudget.exe_ordre;
if (flag=0) then
    RAISE_APPLICATION_ERROR (-20001,'La ligne budgetaire affectee a la recette num. '|| recettebudget.REC_NUMERO || ' n''est pas ouverte sur ' || facturebudget.exe_ordre||'.');
end if;



-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
TITORGINEKEY:=NULL;

--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
TITORIGINELIB:=NULL;

--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
ORIORDRE :=Gestionorigine.traiter_orgid(factureBUDGET.org_id,factureBUDGET.exe_ordre);

--PRESTID : PRESTATION INTERNE --
SELECT count(*) INTO cpt
FROM jefy_recette.PI_DEP_REC d, jefy_recette.PI_ENG_FAC e
WHERE  d.pef_id = e.pef_id
and d.rec_id = recettectrlplanco.rec_id;

if cpt = 1 then
 SELECT prest_id INTO PRESTID
 FROM jefy_recette.PI_DEP_REC d, jefy_recette.PI_ENG_FAC e
 WHERE  d.pef_id = e.pef_id
 AND d.rec_id = recettectrlplanco.rec_id;
else
 PRESTID := null;
end if;

--TORORDRE : ORIGINE DU MANDAT --
TORORDRE := 1;

--VIRORDRE --
VIRORDRE := NULL;

SELECT titre_seq.NEXTVAL INTO titid FROM dual;

        INSERT INTO TITRE (
           BOR_ID,
           BOR_ORDRE,
           BRJ_ORDRE,
           EXE_ORDRE,
           GES_CODE,
           MOD_ORDRE,
           ORI_ORDRE,
           PCO_NUM,
           PREST_ID,
           TIT_DATE_REMISE,
           TIT_DATE_VISA_PRINC,
           TIT_ETAT,
           TIT_ETAT_REMISE,
           TIT_HT,
           TIT_ID,
           TIT_MOTIF_REJET,
           TIT_NB_PIECE,
           TIT_NUMERO,
           TIT_NUMERO_REJET,
           TIT_ORDRE,
           TIT_ORGINE_KEY,
           TIT_ORIGINE_LIB,
           TIT_TTC,
           TIT_TVA,
           TOR_ORDRE,
           UTL_ORDRE,
           ORG_ORDRE,
           FOU_ORDRE,
           MOR_ORDRE,
           PAI_ORDRE,
           rib_ordre_ordonnateur,
           rib_ordre_comptable,
           tit_libelle)
        VALUES
            (
            borid,--BOR_ID,
            -borid,--BOR_ORDRE,
            NULL,--BRJ_ORDRE,
            recettepapier.exe_ordre,--EXE_ORDRE,
            gescode,--GES_CODE,
            null,--MOD_ORDRE, n existe plus en 2007 vestige des ORVs
            oriordre,--ORI_ORDRE,
            recettectrlplanco.pco_num,--PCO_NUM,
            PRESTID,--PREST_ID,
            sysdate,--TIT_DATE_REMISE,
            NULL,--TIT_DATE_VISA_PRINC,
            'ATTENTE',--TIT_ETAT,
            'ATTENTE',--TIT_ETAT_REMISE,
            recettectrlplanco.rpco_HT_SAISIE,--TIT_HT,
            titid,--TIT_ID,
            NULL,--TIT_MOTIF_REJET,
            recettepapier.rpp_nb_piece,--TIT_NB_PIECE,
            -1,--TIT_NUMERO, numerotation en fin de transaction
            NULL,--TIT_NUMERO_REJET,
            -titid,--TIT_ORDRE,  en 2007 plus de tit_ordre on met  tit_id
            TITORGINEKEY,--TIT_ORGINE_KEY,
            TITORIGINELIB,--TIT_ORIGINE_LIB,
            recettectrlplanco.rpco_TTC_SAISIE,--TIT_TTC,
            recettectrlplanco.rpco_TVA_SAISIE,--TIT_TVA,
            TORORDRE,--TOR_ORDRE,
            recettepapier.utl_ordre,--UTL_ORDRE
            facturebudget.org_id,        --ORG_ORDRE,
            recettepapier.fou_ordre,  -- FOU_ORDRE  --TOCHECK certains sont nuls...
            facturebudget.mor_ordre,                 --MOR_ORDRE
            NULL, -- VIR_ORDRE
            recettepapier.rib_ordre,
            recettepapier.rib_ordre,
            recettebudget.rec_lib
            );


-- maj du tit_id dans la recette
update jefy_recette.RECETTE_CTRL_PLANCO
set tit_id = titid
where rpco_id = rpcoid;

-- recup du brouillard
--Set_Titre_Brouillard(titid);

RETURN titid;
END;



FUNCTION set_titre_recettes (lesrpcoid VARCHAR,borid INTEGER) RETURN INTEGER IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;
RAISE_APPLICATION_ERROR (-20001,'OPERATION NON TRAITEE');
RETURN cpt;
END;

/*

FUNCTION ndep_mand_org_fou_rib_pco (abrid INTEGER,borid INTEGER) RETURN INTEGER IS
cpt        INTEGER;
fouordre   v_fournisseur.FOU_ORDRE%TYPE;
ribordre   v_rib.RIB_ORDRE%TYPE;
pconum     PLAN_COMPTABLE.PCO_NUM%TYPE;
modordre   MODE_PAIEMENT.MOD_ORDRE%TYPE;
orgid      jefy_admin.organ.org_id%TYPE;
ht         MANDAT.MAN_HT%TYPE;
tva        MANDAT.MAN_HT%TYPE;
ttc        MANDAT.MAN_HT%TYPE;
budgetaire MANDAT.MAN_HT%TYPE;

CURSOR ndep_mand_org_fou_rib_pco IS
SELECT e.org_id,dpp.fou_ordre,dpp.rib_ordre,d.pco_num,
SUM(dpco_ht_saisie) ht,
SUM(dpco_tva_saisie) tva,
SUM(dpco_ttc_saisie) ttc,
SUM(dpco_montant_budgetaire) budgetaire
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
and d.man_id is null
GROUP BY e.org_id,dpp.fou_ordre,dpp.rib_ordre,d.pco_num;

CURSOR lesdpcoids IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
AND e.org_id = orgid
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre = ribordre
and d.man_id is null
AND d.pco_num = pconum;

CURSOR lesdpcoidsribnull IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
AND e.org_id = orgid
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre is null
and d.man_id is null
AND d.pco_num = pconum;

chainedpcoid VARCHAR(5000);
tmpdpcoid jefy_depense.depense_ctrl_planco.dpco_id%TYPE;

BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

OPEN ndep_mand_org_fou_rib_pco;
LOOP
FETCH ndep_mand_org_fou_rib_pco INTO
orgid,fouordre,ribordre,pconum,ht,tva,ttc,budgetaire;
EXIT WHEN ndep_mand_org_fou_rib_pco%NOTFOUND;
chainedpcoid :=NULL;
if ribordre is not null then
 OPEN lesdpcoids;
 LOOP
 FETCH lesdpcoids INTO tmpdpcoid;
 EXIT WHEN lesdpcoids%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoids;
 else
  OPEN lesdpcoidsribnull;
 LOOP
 FETCH lesdpcoidsribnull INTO tmpdpcoid;
 EXIT WHEN lesdpcoidsribnull%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoidsribnull;
 end if;
  chainedpcoid :=chainedpcoid||'$';
-- creation des mandats des pids
cpt:=set_mandat_depenses(chainedpcoid,borid);
END LOOP;

CLOSE ndep_mand_org_fou_rib_pco;

RETURN cpt;
END;

*/

FUNCTION ndep_mand_org_fou_rib_pco_mod  (abrid INTEGER,borid INTEGER) RETURN INTEGER IS
cpt        INTEGER;
fouordre   v_fournisseur.FOU_ORDRE%TYPE;
ribordre   v_rib.RIB_ORDRE%TYPE;
pconum     PLAN_COMPTABLE.PCO_NUM%TYPE;
modordre   MODE_PAIEMENT.MOD_ORDRE%TYPE;
orgid      jefy_admin.organ.org_id%TYPE;
ht         MANDAT.MAN_HT%TYPE;
tva        MANDAT.MAN_HT%TYPE;
ttc        MANDAT.MAN_HT%TYPE;
budgetaire MANDAT.MAN_HT%TYPE;


CURSOR ndep_mand_org_fou_rib_pco_mod IS
SELECT e.org_id,dpp.fou_ordre,dpp.rib_ordre,d.pco_num,dpp.mod_ordre,
SUM(dpco_ht_saisie) ht,
SUM(dpco_tva_saisie) tva,
SUM(dpco_ttc_saisie) ttc,
SUM(dpco_montant_budgetaire) budgetaire
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
and d.man_id is null
GROUP BY e.org_id,dpp.fou_ordre,dpp.rib_ordre,d.pco_num,dpp.mod_ordre;


CURSOR lesdpcoids IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
AND e.org_id = orgid
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre = ribordre
AND d.pco_num = pconum
AND dpp.mod_ordre = modordre
and d.man_id is null;


CURSOR lesdpcoidsribnull IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp,
jefy_depense.engage_budget e
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND e.eng_id = db.eng_id
AND ab.abr_etat='ATTENTE'
AND e.org_id = orgid
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre is null
AND d.pco_num = pconum
AND dpp.mod_ordre = modordre
and d.man_id is null;

chainedpcoid VARCHAR(5000);
tmpdpcoid jefy_depense.depense_ctrl_planco.dpco_id%TYPE;

BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

OPEN ndep_mand_org_fou_rib_pco_mod;
LOOP
FETCH ndep_mand_org_fou_rib_pco_mod INTO
orgid,fouordre,ribordre,pconum,modordre,ht,tva,ttc,budgetaire;
EXIT WHEN ndep_mand_org_fou_rib_pco_mod%NOTFOUND;
chainedpcoid :=NULL;

if ribordre is not null then
 OPEN lesdpcoids;
 LOOP
 FETCH lesdpcoids INTO tmpdpcoid;
 EXIT WHEN lesdpcoids%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoids;
 else
  OPEN lesdpcoidsribnull;
 LOOP
 FETCH lesdpcoidsribnull INTO tmpdpcoid;
 EXIT WHEN lesdpcoidsribnull%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoidsribnull;
 end if;

  chainedpcoid :=chainedpcoid||'$';
-- creation des mandats des pids
cpt:=set_mandat_depenses(chainedpcoid,borid);
END LOOP;

CLOSE ndep_mand_org_fou_rib_pco_mod;

RETURN cpt;
END;
/*
FUNCTION ndep_mand_fou_rib_pco  (abrid INTEGER,borid INTEGER) RETURN INTEGER IS
cpt        INTEGER;
fouordre   v_fournisseur.FOU_ORDRE%TYPE;
ribordre   v_rib.RIB_ORDRE%TYPE;
pconum     PLAN_COMPTABLE.PCO_NUM%TYPE;
modordre   MODE_PAIEMENT.MOD_ORDRE%TYPE;
orgid      jefy_admin.organ.org_id%TYPE;
ht         MANDAT.MAN_HT%TYPE;
tva        MANDAT.MAN_HT%TYPE;
ttc        MANDAT.MAN_HT%TYPE;
budgetaire MANDAT.MAN_HT%TYPE;

CURSOR ndep_mand_fou_rib_pco IS
SELECT dpp.fou_ordre,dpp.rib_ordre,d.pco_num,
SUM(dpco_ht_saisie) ht,
SUM(dpco_tva_saisie) tva,
SUM(dpco_ttc_saisie) ttc,
SUM(dpco_montant_budgetaire) budgetaire
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
and d.man_id is null
GROUP BY dpp.fou_ordre,dpp.rib_ordre,d.pco_num;


CURSOR lesdpcoids IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre = ribordre
and d.man_id is null
AND d.pco_num = pconum;


CURSOR lesdpcoidsribnull IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre is null
and d.man_id is null
AND d.pco_num = pconum;


chainedpcoid VARCHAR(5000);
tmpdpcoid jefy_depense.depense_ctrl_planco.dpco_id%TYPE;

BEGIN

OPEN ndep_mand_fou_rib_pco;
LOOP
FETCH ndep_mand_fou_rib_pco INTO
fouordre,ribordre,pconum,ht,tva,ttc,budgetaire;
EXIT WHEN ndep_mand_fou_rib_pco%NOTFOUND;
chainedpcoid :=NULL;
if ribordre is not null then
 OPEN lesdpcoids;
 LOOP
 FETCH lesdpcoids INTO tmpdpcoid;
 EXIT WHEN lesdpcoids%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoids;
 else
  OPEN lesdpcoidsribnull;
 LOOP
 FETCH lesdpcoidsribnull INTO tmpdpcoid;
 EXIT WHEN lesdpcoidsribnull%NOTFOUND;
  chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
 END LOOP;
 CLOSE lesdpcoidsribnull;
 end if;

  chainedpcoid :=chainedpcoid||'$';
-- creation des mandats des pids
cpt:=set_mandat_depenses(chainedpcoid,borid);
END LOOP;

CLOSE ndep_mand_fou_rib_pco;

RETURN cpt;
END;
*/

FUNCTION ndep_mand_fou_rib_pco_mod  (abrid INTEGER,borid INTEGER) RETURN INTEGER IS
cpt        INTEGER;
fouordre   v_fournisseur.FOU_ORDRE%TYPE;
ribordre   v_rib.RIB_ORDRE%TYPE;
pconum     PLAN_COMPTABLE.PCO_NUM%TYPE;
modordre   MODE_PAIEMENT.MOD_ORDRE%TYPE;
orgid      jefy_admin.organ.org_id%TYPE;
ht         MANDAT.MAN_HT%TYPE;
tva        MANDAT.MAN_HT%TYPE;
ttc        MANDAT.MAN_HT%TYPE;
budgetaire MANDAT.MAN_HT%TYPE;

CURSOR ndep_mand_fou_rib_pco_mod IS
SELECT dpp.fou_ordre,dpp.rib_ordre,d.pco_num,dpp.mod_ordre,
SUM(dpco_ht_saisie) ht,
SUM(dpco_tva_saisie) tva,
SUM(dpco_ttc_saisie) ttc,
SUM(dpco_montant_budgetaire) budgetaire
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
and d.man_id is null
GROUP BY dpp.fou_ordre,dpp.rib_ordre,d.pco_num,dpp.mod_ordre;


CURSOR lesdpcoids IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre = ribordre
AND d.pco_num = pconum
and d.man_id is null
AND dpp.mod_ordre = modordre;

CURSOR lesdpcoidsnull IS
SELECT d.dpco_id
FROM
ABRICOT_BORD_SELECTION ab,
jefy_depense.depense_CTRL_PLANCO d ,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dpp
WHERE d.dpco_id = ab.dep_id
AND dpp.dpp_id = db.dpp_id
AND  db.dep_id = d.dep_id
AND abr_id = abrid
AND ab.abr_etat='ATTENTE'
AND dpp.fou_ordre = fouordre
AND dpp.rib_ordre is null
AND d.pco_num = pconum
and d.man_id is null
AND dpp.mod_ordre = modordre;


chainedpcoid VARCHAR(5000);
tmpdpcoid jefy_depense.depense_ctrl_planco.dpco_id%TYPE;

BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

OPEN ndep_mand_fou_rib_pco_mod;
LOOP
FETCH ndep_mand_fou_rib_pco_mod INTO
fouordre,ribordre,pconum,modordre,ht,tva,ttc,budgetaire;
EXIT WHEN ndep_mand_fou_rib_pco_mod%NOTFOUND;
chainedpcoid :=NULL;

if ribordre is not null then
   OPEN lesdpcoids;
   LOOP
   FETCH lesdpcoids INTO tmpdpcoid;
   EXIT WHEN lesdpcoids%NOTFOUND;
    chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
   END LOOP;
   CLOSE lesdpcoids;
 else
   OPEN lesdpcoidsnull;
   LOOP
   FETCH lesdpcoidsnull INTO tmpdpcoid;
   EXIT WHEN lesdpcoidsnull%NOTFOUND;
    chainedpcoid :=chainedpcoid||tmpdpcoid||'$';
   END LOOP;
   CLOSE lesdpcoidsnull;
 end if;

 chainedpcoid :=chainedpcoid||'$';
-- creation des mandats des pids
cpt:=set_mandat_depenses(chainedpcoid,borid);
END LOOP;

CLOSE ndep_mand_fou_rib_pco_mod;


RETURN cpt;
END;

-- procedures de verifications

FUNCTION selection_valide (abrid INTEGER) RETURN INTEGER IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

-- meme exercice

-- si PI somme recette = somme depense

-- recette_valides

-- depense_valides

RETURN cpt;

END;

FUNCTION recette_valide (recid INTEGER) RETURN INTEGER IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

RETURN cpt;

END;

FUNCTION depense_valide (depid INTEGER) RETURN INTEGER IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;
RETURN cpt;

END;

FUNCTION verif_bordereau_selection(borid INTEGER,abrid INTEGER) RETURN INTEGER IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

-- verifier sum TTC depense selection  = sum TTC mandat du bord

-- verifier sum TTC recette selection = sum TTC titre du bord

-- verifier sum TTC depense  = sum TTC mandat du bord

-- verifier sum TTC recette  = sum TTC titre  du bord


RETURN cpt;

END;



-- procedures de locks de transaction
PROCEDURE lock_mandats IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

END;

PROCEDURE lock_titres IS
cpt INTEGER;
BEGIN
SELECT COUNT(*) INTO cpt FROM dual;

END;

PROCEDURE get_depense_jefy_depense (manid INTEGER)
IS

    depid               DEPENSE.dep_id%TYPE;
    jefydepensebudget       jefy_depense.depense_budget%ROWTYPE;
        tmpdepensepapier        jefy_depense.depense_papier%ROWTYPE;
        jefydepenseplanco       jefy_depense.depense_ctrl_planco%ROWTYPE;
    lignebudgetaire         DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
    fouadresse              DEPENSE.dep_adresse%TYPE;
    founom          DEPENSE.dep_fournisseur%TYPE;
    lotordre          DEPENSE.dep_lot%TYPE;
    marordre        DEPENSE.dep_marches%TYPE;
    fouordre        DEPENSE.fou_ordre%TYPE;
    gescode            DEPENSE.ges_code%TYPE;
    modordre        DEPENSE.mod_ordre%TYPE;
    cpt                    INTEGER;
    tcdordre        TYPE_CREDIT.TCD_ORDRE%TYPE;
    tcdcode            TYPE_CREDIT.tcd_code%TYPE;
    ecd_ordre_ema            ECRITURE_DETAIL.ecd_ordre%TYPE;
        orgid   integer;

        CURSOR depenses IS
     SELECT db.* FROM jefy_depense.depense_budget db,jefy_depense.depense_ctrl_planco dpco
         WHERE dpco.man_id = manid
         and db.dep_id = dpco.dep_id;

BEGIN

    OPEN depenses;
    LOOP
        FETCH depenses INTO jefydepensebudget;
              EXIT WHEN depenses%NOTFOUND;

        -- creation du depid --
        SELECT depense_seq.NEXTVAL INTO depid FROM dual;

            -- creation de lignebudgetaire--
            SELECT org_ub||' '||org_cr||' '||org_souscr
                INTO lignebudgetaire
                FROM jefy_admin.organ
                WHERE org_id =
                (
                 SELECT org_id
                 FROM jefy_depense.engage_budget
                 WHERE eng_id = jefydepensebudget.eng_id
                 --AND eng_stat !='A'
                );


            --recuperer le type de credit a partir de la commande
            SELECT tcd_ordre INTO tcdordre
                        FROM jefy_depense.engage_budget
                        WHERE eng_id = jefydepensebudget.eng_id;
                        --AND eng_stat !='A'

            SELECT org_ub,org_id
                INTO gescode,orgid
                FROM jefy_admin.organ
                WHERE org_id =
                (
                 SELECT org_id
                 FROM jefy_depense.engage_budget
                 WHERE eng_id = jefydepensebudget.eng_id
                 --AND eng_stat !='A'
                );

            -- fouadresse --
            SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
                INTO fouadresse
                FROM v_fournisseur
                WHERE fou_ordre =
                (
                 SELECT fou_ordre
                 FROM jefy_depense.engage_budget
                 WHERE eng_id = jefydepensebudget.eng_id
                 --AND eng_stat !='A'
                );


            -- founom --
            SELECT adr_nom||' '||adr_prenom
                INTO founom
                FROM v_fournisseur
                WHERE fou_ordre =
                (
                 SELECT fou_ordre
                 FROM jefy_depense.engage_budget
                 WHERE eng_id = jefydepensebudget.eng_id
                 --AND eng_stat !='A'
                );

            -- fouordre --
                 SELECT fou_ordre INTO fouordre
                 FROM jefy_depense.engage_budget
                 WHERE eng_id = jefydepensebudget.eng_id;
                 --AND eng_stat !='A'

            -- lotordre --
            SELECT COUNT(*) INTO cpt
                FROM marches.attribution
                WHERE att_ordre =
                (
                 SELECT att_ordre
                 FROM jefy_depense.engage_ctrl_marche
                                 WHERE eng_id = jefydepensebudget.eng_id
                );

             IF cpt = 0 THEN
                  lotordre :=NULL;
             ELSE
                  SELECT lot_ordre
                  INTO lotordre
                  FROM marches.attribution
                  WHERE att_ordre =
                  (
                                   SELECT att_ordre
                   FROM jefy_depense.engage_ctrl_marche
                                   WHERE eng_id = jefydepensebudget.eng_id
                  );
             END IF;


        -- marordre --
        SELECT COUNT(*) INTO cpt
            FROM marches.lot
            WHERE lot_ordre = lotordre;

        IF cpt = 0 THEN
                  marordre :=NULL;
        ELSE
                        SELECT mar_ordre
                        INTO marordre
                        FROM marches.lot
                        WHERE lot_ordre = lotordre;
        END IF;



                --MOD_ORDRE --
                SELECT mod_ordre INTO modordre
                FROM jefy_depense.depense_papier
                WHERE dpp_id =jefydepensebudget.dpp_id;


        -- recuperer l'ecriture_detail pour emargements semi-auto
        SELECT ecd_ordre INTO ecd_ordre_ema
                FROM jefy_depense.depense_ctrl_planco
                WHERE dep_id = jefydepensebudget.dep_id;

                -- recup de la depense papier
                SELECT * INTO tmpdepensepapier
                FROM jefy_depense.DEPENSE_PAPIER
                WHERE dpp_id = jefydepensebudget.dpp_id;

                -- recup des infos de depense_ctrl_planco

                SELECT * INTO jefydepenseplanco
                FROM jefy_depense.depense_ctrl_planco
                WHERE dep_id = jefydepensebudget.dep_id;
        -- creation de la depense --
        INSERT INTO DEPENSE VALUES
            (
            fouadresse ,           --DEP_ADRESSE,
            NULL ,                   --DEP_DATE_COMPTA,
            tmpdepensepapier.dpp_date_reception,  --DEP_DATE_RECEPTION,
            tmpdepensepapier.dpp_date_service_fait , --DEP_DATE_SERVICE,
            'VALIDE' ,               --DEP_ETAT,
            founom ,               --DEP_FOURNISSEUR,
            jefydepenseplanco.dpco_montant_budgetaire , --DEP_HT,
            depense_seq.NEXTVAL ,  --DEP_ID,
            lignebudgetaire ,       --DEP_LIGNE_BUDGETAIRE,
            lotordre ,               --DEP_LOT,
            marordre ,               --DEP_MARCHES,
            jefydepenseplanco.dpco_ttc_saisie ,  --DEP_MONTANT_DISQUETTE,
            NULL,-- table N !!!jefydepensebudget.cm_ordre , --DEP_NOMENCLATURE,
            substr(tmpdepensepapier.dpp_numero_facture,1,199)  ,--DEP_NUMERO,
            jefydepenseplanco.dpco_id ,--DEP_ORDRE,
            NULL ,                   --DEP_REJET,
            tmpdepensepapier.rib_ordre ,--DEP_RIB,
            'NON' ,                   --DEP_SUPPRESSION,
            jefydepenseplanco.dpco_ttc_saisie ,  --DEP_TTC,
            jefydepenseplanco.dpco_ttc_saisie - jefydepenseplanco.dpco_montant_budgetaire, -- DEP_TVA,
            tmpdepensepapier.exe_ordre ,               --EXE_ORDRE,
            fouordre,                --FOU_ORDRE,
            gescode,                 --GES_CODE,
            manid ,                   --MAN_ID,
            jefydepenseplanco.man_id, --MAN_ORDRE,
--            jefyfacture.mod_code,  --MOD_ORDRE,
            modordre,
            jefydepenseplanco.pco_num ,  --PCO_ORDRE,
            tmpdepensepapier.utl_ordre,               --UTL_ORDRE
            orgid, --org_ordre
            tcdordre,
            ecd_ordre_ema, -- ecd_ordre_ema reference a l'ecriture_detail pour emargement
            tmpdepensepapier.dpp_date_facture);

    END LOOP;
    CLOSE depenses;

END;

PROCEDURE get_recette_jefy_recette (titid INTEGER) IS

recettepapier         jefy_recette.recette_papier%ROWTYPE;
recettebudget         jefy_recette.recette_budget%ROWTYPE;
facturebudget         jefy_recette.facture_budget%ROWTYPE;
recettectrlplanco     jefy_recette. RECETTE_CTRL_PLANCO%rowtype;
recettectrlplancotva  jefy_recette.recette_ctrl_planco_tva%rowtype;
maracujatitre         maracuja.titre%rowtype;

adrnom                varchar2(200);
letyperecette         varchar2(200);
titinterne            varchar2(200);
lbud            varchar2(200);
tboordre        integer;
cpt integer;

cursor c_recette is
SELECT *
FROM  jefy_recette.RECETTE_CTRL_PLANCO
WHERE tit_id = titid;

BEGIN



--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
--SELECT * INTO recettectrlplanco
--FROM  jefy_recette.RECETTE_CTRL_PLANCO
--WHERE tit_id = titid;

open c_recette;
loop
fetch c_recette into recettectrlplanco;
exit when c_recette%notfound;

SELECT * INTO recettebudget
FROM jefy_recette.recette_budget
WHERE rec_id = recettectrlplanco.rec_id;

SELECT * INTO facturebudget
FROM jefy_recette.facture_BUDGET
where fac_id = recettebudget.fac_id;

select * into recettepapier
from jefy_recette.recette_papier
where rpp_id = recettebudget.rpp_id;

select * into maracujatitre
from maracuja.titre
where tit_id = titid;

if (recettebudget.REC_ID_REDUCTION is null) then
 letyperecette := 'R';
else
 letyperecette := 'T';
end if;

select count(*) into cpt
from JEFY_RECETTE.PI_DEP_REC
where rec_id =recettectrlplanco.rec_id;

if cpt > 0 then
 titinterne := 'O';
else
 titinterne := 'N';
end if;

select adr_nom into adrnom from grhum.v_fournis_grhum where fou_ordre = recettepapier.fou_ordre;

select org_ub||'/'||org_cr||'/'||org_souscr into lbud
from jefy_admin.organ
where org_id = facturebudget.org_id;


select distinct tbo_ordre  into tboordre
from maracuja.titre t,maracuja.bordereau b
where b.bor_id = t.bor_id
and t.tit_id = titid;

-- 200 bordereau de presntation interne recette
if tboordre = 200 then
tboordre:=null;
else
tboordre:=facturebudget.org_id;
end if;



    INSERT INTO RECETTE VALUES
        (
        recettectrlplanco.exe_ordre,--EXE_ORDRE,
        maracujatitre.ges_code,--GES_CODE,
        null,--MOD_CODE,
        recettectrlplanco.pco_num,--PCO_NUM,
        recettebudget.REC_DATE_SAISIE,--jefytitre.tit_date,-- REC_DATE,
        adrnom,-- REC_DEBITEUR,
        recette_seq.nextval,-- REC_ID,
        null,-- REC_IMPUTTVA,
        null,-- REC_INTERNE, // TODO ROD
        facturebudget.FAC_LIB,-- REC_LIBELLE,
        lbud,-- REC_LIGNE_BUDGETAIRE,
        'E',-- REC_MONNAIE,
        recettectrlplanco.RPCO_HT_SAISIE,--HT,
        recettectrlplanco.RPCO_TTC_SAISIE,--TTC,
        recettectrlplanco.RPCO_TTC_SAISIE,--DISQUETTE,
        recettectrlplanco.RPCO_TVA_SAISIE,--   REC_MONTTVA,
        facturebudget.FAC_NUMERO,--   REC_NUM,
        recettectrlplanco.rpco_id,--   REC_ORDRE,
        recettepapier.RPP_NB_PIECE,--   REC_PIECE,
        facturebudget.FAC_NUMERO,--   REC_REF,
        'VALIDE',--   REC_STAT,
        'NON',--    REC_SUPPRESSION,  Modif Rod
        letyperecette,--     REC_TYPE,
        NULL,--     REC_VIREMENT,
        titid,--      TIT_ID,
        -titid,--      TIT_ORDRE,
        recettebudget.utl_ordre,--       UTL_ORDRE
        facturebudget.org_id,        --       ORG_ORDRE --ajout rod
        facturebudget.fou_ordre,   --FOU_ORDRE --ajout rod
        null, --mod_ordre
        recettepapier.mor_ordre,  --mor_ordre
        recettepapier.rib_ordre,
        NULL
        );

end loop;
close c_recette;

END;


-- procedures du brouillard

PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

   lemandat        MANDAT%ROWTYPE;
   pconum_ctrepartie MANDAT.PCO_NUM%TYPE;
   PCONUM_TVA       PLANCO_VISA.PCO_NUM_tva%TYPE;
   gescodecompta     MANDAT.ges_code%TYPE;
   PVICONTREPARTIE_GESTION      PLANCO_VISA.PVI_CONTREPARTIE_GESTION%type;
   MODCONTREPARTIE_GESTION mode_paiement.MOD_CONTREPARTIE_GESTION%type;
       pconum_185      PLANCO_VISA.PCO_NUM_tva%TYPE;
   parvalue      PARAMETRE.par_value%TYPE;
   cpt               INTEGER;
       tboordre TYPE_BORDEREAU.tbo_ordre%TYPE;
       sens ECRITURE_DETAIL.ecd_sens%TYPE;

BEGIN

   SELECT * INTO lemandat
       FROM MANDAT
       WHERE man_id = manid;

       SELECT DISTINCT tbo_ordre INTO tboordre
       FROM BORDEREAU
       WHERE bor_id IN (SELECT bor_id FROM MANDAT WHERE man_id = manid);



--    select count(*) into cpt from v_titre_prest_interne where man_ordre=lemandat.man_ordre and tit_ordre is not null;
--    if cpt = 0 then
   IF lemandat.prest_id IS NULL THEN
       -- creation du mandat_brouillard visa DEBIT--
               sens:=inverser_sens_orv(tboordre,'D');
       INSERT INTO MANDAT_BROUILLARD VALUES
           (
           NULL,                             --ECD_ORDRE,
           lemandat.exe_ordre,               --EXE_ORDRE,
           lemandat.ges_code,               --GES_CODE,
           ABS(lemandat.man_ht),               --MAB_MONTANT,
           'VISA MANDAT',                   --MAB_OPERATION,
           mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
           sens,                           --MAB_SENS,
           manid,                           --MAN_ID,
           lemandat.pco_num               --PCO_NU
           );


       -- credit=ctrepartie
       --debit = ordonnateur
       -- recup des infos du VISA CREDIT --
       SELECT COUNT(*) INTO cpt
       FROM PLANCO_VISA
       WHERE pco_num_ordonnateur = lemandat.pco_num and exe_ordre = lemandat.exe_ordre;

       IF cpt = 0 THEN
          RAISE_APPLICATION_ERROR (-20001,'PROBLEM DE CONTRE PARTIE '||lemandat.pco_num);
       END IF;

       SELECT pco_num_ctrepartie, PCO_NUM_TVA,PVI_CONTREPARTIE_GESTION
           INTO pconum_ctrepartie, PCONUM_TVA,PVICONTREPARTIE_GESTION
           FROM PLANCO_VISA
           WHERE pco_num_ordonnateur = lemandat.pco_num
           and exe_ordre = lemandat.exe_ordre;


       SELECT  COUNT(*) INTO cpt
           FROM MODE_PAIEMENT
           WHERE exe_ordre = lemandat.exe_ordre
           AND mod_ordre = lemandat.mod_ordre
           AND pco_num_visa IS NOT NULL;

       IF cpt != 0 THEN
           SELECT  pco_num_visa,mod_CONTREPARTIE_GESTION INTO pconum_ctrepartie,MODCONTREPARTIE_GESTION
           FROM MODE_PAIEMENT
           WHERE exe_ordre = lemandat.exe_ordre
           AND mod_ordre = lemandat.mod_ordre
           AND pco_num_visa IS NOT NULL;
       END IF;




           -- modif 15/09/2005 compatibilite avec new gestion_exercice
       SELECT c.ges_code,ge.PCO_NUM_185
           INTO gescodecompta,pconum_185
           FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
           WHERE g.ges_code = lemandat.ges_code
           AND g.com_ordre = c.com_ordre
           AND g.ges_code=ge.ges_code
           AND ge.exe_ordre=lemandat.EXE_ORDRE;

               -- 5/12/2007
               -- on ne prend plus le parametre mais PVICONTREPARTIE_GESTION
               -- PVICONTREPARTIE_GESTION de la table planc_visa dans un premier temps
               -- dans un second temps il peut etre ecras} par mod_CONTREPARTIE_GESTION de MODE_PAIEMENT
                     --SELECT par_value   INTO parvalue
       --    FROM PARAMETRE
       --    WHERE par_key ='CONTRE PARTIE VISA'
       --    AND exe_ordre = lemandat.exe_ordre;

       parvalue := PVICONTREPARTIE_GESTION;
       if (MODCONTREPARTIE_GESTION is not null) then
           parValue := MODCONTREPARTIE_GESTION;
       end if;
              IF parvalue = 'COMPOSANTE' THEN
          gescodecompta := lemandat.ges_code;
       END IF;

       IF pconum_185 IS NULL THEN
           -- creation du mandat_brouillard visa CREDIT --
                       sens:=inverser_sens_orv(tboordre,'C');
                       if sens = 'D' then
                        pconum_ctrepartie := '4632';
                       end if;
           INSERT INTO MANDAT_BROUILLARD VALUES (
           NULL,                            --ECD_ORDRE,
           lemandat.exe_ordre,              --EXE_ORDRE,
           gescodecompta,                  --GES_CODE,
           ABS(lemandat.man_ttc),                --MAB_MONTANT,
           'VISA MANDAT',                  --MAB_OPERATION,
           mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
           sens,                          --MAB_SENS,
           manid,                          --MAN_ID,
           pconum_ctrepartie                  --PCO_NU
           );
       ELSE
           --au SACD --
                       sens:=inverser_sens_orv(tboordre,'C');
                       if sens = 'D' then
                        pconum_ctrepartie := '4632';
                       end if;

           INSERT INTO MANDAT_BROUILLARD VALUES
           (
           NULL,                            --ECD_ORDRE,
           lemandat.exe_ordre,              --EXE_ORDRE,
           lemandat.ges_code,                  --GES_CODE,
           ABS(lemandat.man_ttc),                --MAB_MONTANT,
           'VISA MANDAT',                  --MAB_OPERATION,
           mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
           sens,                          --MAB_SENS,
           manid,                          --MAN_ID,
           pconum_ctrepartie                  --PCO_NU
           );
       END IF;

       IF lemandat.man_tva != 0 THEN
           -- creation du mandat_brouillard visa CREDIT TVA --
                       sens:=inverser_sens_orv(tboordre,'D');
           INSERT INTO MANDAT_BROUILLARD VALUES
           (
           NULL,                            --ECD_ORDRE,
           lemandat.exe_ordre,              --EXE_ORDRE,
           lemandat.ges_code,                  --GES_CODE,
           ABS(lemandat.man_tva),              --MAB_MONTANT,
           'VISA TVA',                          --MAB_OPERATION,
           mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
           sens,                          --MAB_SENS,
           manid,                          --MAN_ID,
           PCONUM_TVA                      --PCO_NU
           );
       END IF;

   ELSE
       Bordereau_abricot.set_mandat_brouillard_intern(manid);
   END IF;
END;

PROCEDURE set_mandat_brouillard_intern(manid INTEGER)
IS

   lemandat        MANDAT%ROWTYPE;
   leplancomptable   PLAN_COMPTABLE%ROWTYPE;

   pconum_ctrepartie MANDAT.PCO_NUM%TYPE;
   PCONUM_TVA       PLANCO_VISA.PCO_NUM_tva%TYPE;
   gescodecompta MANDAT.ges_code%TYPE;
   ctpGesCode MANDAT.ges_code%TYPE;
   pconum_185      PLANCO_VISA.PCO_NUM_tva%TYPE;
   parvalue      PARAMETRE.par_value%TYPE;
   cpt INTEGER;
   lepconum plan_comptable_exer.pco_num%type;

BEGIN

    SELECT * INTO lemandat
    FROM MANDAT
    WHERE man_id = manid;


       -- modif 15/09/2005 compatibilite avec new gestion_exercice
     SELECT c.ges_code,ge.PCO_NUM_185
     INTO gescodecompta,pconum_185
     FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
     WHERE g.ges_code = lemandat.ges_code
     AND g.com_ordre = c.com_ordre
     AND g.ges_code=ge.ges_code
     AND ge.exe_ordre=lemandat.EXE_ORDRE;



   -- recup des infos du VISA CREDIT --
   SELECT COUNT(*) INTO cpt
       FROM PLANCO_VISA
       WHERE pco_num_ordonnateur = lemandat.pco_num and exe_ordre=lemandat.exe_ordre;

   IF cpt = 0 THEN
      RAISE_APPLICATION_ERROR (-20001,'PROBLEM DE CONTRE PARTIE '||lemandat.pco_num);
   END IF;
   SELECT pco_num_ctrepartie, PCO_NUM_TVA
       INTO pconum_ctrepartie, PCONUM_TVA
       FROM PLANCO_VISA
       WHERE pco_num_ordonnateur = lemandat.pco_num and exe_ordre=lemandat.exe_ordre;

   -- verification si le compte existe !
--   SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
--          WHERE pco_num = '18'||lemandat.pco_num;

--   IF cpt = 0 THEN
--    SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
--    WHERE pco_num = lemandat.pco_num;

--    maj_plancomptable_mandat (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||lemandat.pco_num);
--   END IF;
--

   lepconum := api_planco.CREER_PLANCO_PI(lemandat.exe_ordre, lemandat.pco_num);




--    lemandat.pco_num := '18'||lemandat.pco_num;

   -- creation du mandat_brouillard visa DEBIT--
   INSERT INTO MANDAT_BROUILLARD VALUES
       (
       NULL,                             --ECD_ORDRE,
       lemandat.exe_ordre,               --EXE_ORDRE,
       lemandat.ges_code,               --GES_CODE,
       ABS(lemandat.man_ht),               --MAB_MONTANT,
       'VISA MANDAT',                   --MAB_OPERATION,
       mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
       'D',                           --MAB_SENS,
       manid,                           --MAN_ID,
       '18'||lemandat.pco_num               --PCO_NU
       );

--   SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
--          WHERE pco_num = '181';

--   IF cpt = 0 THEN
--        maj_plancomptable_mandat (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'181');

--   END IF;

    lepconum := api_planco.CREER_PLANCO_PI(lemandat.exe_ordre, '181');


   -- planco de CREDIT 181
   -- creation du mandat_brouillard visa CREDIT --
   
   -- si on est sur un sacd, la contrepartie reste sur le sacd
   if (pconum_185 is not null) then
    ctpGesCode := lemandat.ges_code;
   else
    ctpGesCode := gescodecompta;
   end if;
   
   INSERT INTO MANDAT_BROUILLARD VALUES
       (
       NULL,                            --ECD_ORDRE,
       lemandat.exe_ordre,              --EXE_ORDRE,
       ctpGesCode,                  --GES_CODE,
       ABS(lemandat.man_ttc),                --MAB_MONTANT,
       'VISA MANDAT',                  --MAB_OPERATION,
       mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
       'C',                          --MAB_SENS,
       manid,                          --MAN_ID,
       '181'                  --PCO_NU
       );



   IF lemandat.man_tva != 0 THEN
       -- creation du mandat_brouillard visa CREDIT TVA --
       INSERT INTO MANDAT_BROUILLARD VALUES
           (
           NULL,                            --ECD_ORDRE,
           lemandat.exe_ordre,              --EXE_ORDRE,
           lemandat.ges_code,                  --GES_CODE,
           ABS(lemandat.man_tva),              --MAB_MONTANT,
           'VISA TVA',                          --MAB_OPERATION,
           mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
           'D',                          --MAB_SENS,
           manid,                          --MAN_ID,
           PCONUM_TVA                      --PCO_NU
           );
   END IF;

END;

--PROCEDURE maj_plancomptable_mandat (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
--IS
--    niv INTEGER;
--BEGIN


--


--    --calcul du niveau
--    SELECT LENGTH(pconum) INTO niv FROM dual;

--    --
--    INSERT INTO PLAN_COMPTABLE
--         (
--         PCO_BUDGETAIRE,
--         PCO_EMARGEMENT,
--         PCO_LIBELLE,
--         PCO_NATURE,
--         PCO_NIVEAU,
--         PCO_NUM,
--         PCO_SENS_EMARGEMENT,
--         PCO_VALIDITE,
--         PCO_J_EXERCICE,
--         PCO_J_FIN_EXERCICE,
--         PCO_J_BE
--         )
--    VALUES
--     (
--     'N',--PCO_BUDGETAIRE,
--     'O',--PCO_EMARGEMENT,
--     libelle,--PCO_LIBELLE,
--     nature,--PCO_NATURE,
--     niv,--PCO_NIVEAU,
--     pconum,--PCO_NUM,
--     2,--PCO_SENS_EMARGEMENT,
--     'VALIDE',--PCO_VALIDITE,
--     'O',--PCO_J_EXERCICE,
--     'N',--PCO_J_FIN_EXERCICE,
--     'N'--PCO_J_BE
--     );
--END;



PROCEDURE Set_Titre_Brouillard(titid INTEGER)
IS
      letitre             TITRE%ROWTYPE;
      RECETTEctrlplanco   jefy_recette. RECETTE_CTRL_PLANCO%ROWTYPE;
      lesens              varchar2(20);
      REDUCTION           integer;
      recid   integer;

      cursor c_recettes is
      select * from jefy_recette.RECETTE_CTRL_PLANCO
      where tit_id = titid;

BEGIN

    SELECT * INTO letitre
    FROM TITRE
    WHERE tit_id = titid;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
-- max car titres collectifs exact fetch return more than one row
     select max(rb.REC_ID_REDUCTION) into REDUCTION
     from jefy_recette.RECETTE_budget rb,jefy_recette. RECETTE_CTRL_PLANCO rcpo
     where rcpo.rec_id = rb.rec_id
     and rcpo.tit_id = titid;

-- si dans le cas d une reduction
if (REDUCTION is not null) then
  lesens :='D';
else
  lesens :='C';
end if;


    IF letitre.prest_id IS NULL THEN

      open c_recettes;
      loop
      fetch c_recettes into RECETTEctrlplanco;
      exit when c_recettes%notfound;

    select max(rec_id) into recid from recette
    where rec_ordre = RECETTEctrlplanco.rpco_id;

        -- creation du titre_brouillard visa --
        --  RECETTE_CTRL_PLANCO
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              RECETTEctrlplanco.exe_ordre,               --EXE_ORDRE,
                letitre.ges_code,               --GES_CODE,
                  RECETTEctrlplanco.pco_num,               --PCO_NUM
                  abs(RECETTEctrlplanco.rpco_ht_saisie),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  lesens,           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette. RECETTE_CTRL_PLANCO
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;


               -- recette_ctrl_planco_tva
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              exe_ordre,               --EXE_ORDRE,
                ges_code,               --GES_CODE,
                  pco_num,               --PCO_NUM
                  abs(RPCOTVA_TVA_SAISIE),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  lesens,           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette.recette_ctrl_planco_tva
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;



               -- recette_ctrl_planco_ctp
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              RECETTEctrlplanco.exe_ordre,               --EXE_ORDRE,
                ges_code,               --GES_CODE,
                  pco_num,               --PCO_NUM
                  abs(RPCOCTP_TTC_SAISIE),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  inverser_sens(lesens),           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette.recette_ctrl_planco_ctp
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;

    end loop;
    close c_recettes;

    ELSE
     Set_Titre_Brouillard_intern(titid);
    END IF;

    -- suppression des lignes d ecritures a ZERO
    delete from TITRE_BROUILLARD where TIB_MONTANT = 0;
END;


PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER)
IS
      letitre             TITRE%ROWTYPE;
      RECETTEctrlplanco   jefy_recette. RECETTE_CTRL_PLANCO%ROWTYPE;
      lesens              varchar2(20);
      REDUCTION           integer;
      lepconum            maracuja.PLAN_COMPTABLE.pco_num%type;
      libelle             maracuja.PLAN_COMPTABLE.pco_libelle%type;
      chap                varchar2(2);
      recid               integer;
      gescodecompta       maracuja.titre.ges_code%type;
    ctpGesCode titre.ges_code%TYPE;
    pconum_185      GESTION_EXERCICE.PCO_NUM_185%TYPE;
    
      cursor c_recettes is
      select * from jefy_recette. RECETTE_CTRL_PLANCO
      where tit_id = titid;

BEGIN



    SELECT * INTO letitre
    FROM TITRE
    WHERE tit_id = titid;

    -- modif fred 04/2007
     SELECT c.ges_code,ge.PCO_NUM_185
     INTO gescodecompta,pconum_185
     FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
     WHERE g.ges_code = letitre.ges_code
     AND g.com_ordre = c.com_ordre
     AND g.ges_code=ge.ges_code
     AND ge.exe_ordre=letitre.EXE_ORDRE;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
     select rb.REC_ID_REDUCTION into REDUCTION
     from jefy_recette.RECETTE_budget rb,jefy_recette. RECETTE_CTRL_PLANCO rcpo
     where rcpo.rec_id = rb.rec_id
     and rcpo.tit_id = titid;

    -- si dans le cas d une reduction
    if (REDUCTION is not null) then
     lesens :='D';
    else
     lesens :='C';
    end if;

    open c_recettes;
    loop
    fetch c_recettes into RECETTEctrlplanco;
    exit when c_recettes%notfound;



    select max(rec_id) into recid from recette
    where rec_ordre = RECETTEctrlplanco.rpco_id;

    -- recup des 2 premiers caracteres du compte
    select substr(RECETTEctrlplanco.pco_num,1,2) into chap from dual;

    if chap != '18' then
--     select pco_libelle into libelle
--     from maracuja.plan_comptable
--     where pco_num = RECETTEctrlplanco.pco_num;

--     lepconum := '18'||RECETTEctrlplanco.pco_num;
--     maj_plancomptable_titre('R',libelle,lepconum);

        lepconum := api_planco.CREER_PLANCO_PI(RECETTEctrlplanco.exe_ordre, RECETTEctrlplanco.pco_num);


    end if;

        -- creation du titre_brouillard visa --
        --  RECETTE_CTRL_PLANCO
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              RECETTEctrlplanco.exe_ordre,               --EXE_ORDRE,
                letitre.ges_code,               --GES_CODE,
                  lepconum,               --PCO_NUM
                  abs(RECETTEctrlplanco.rpco_ht_saisie),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  lesens,           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette. RECETTE_CTRL_PLANCO
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;


               -- recette_ctrl_planco_tva
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              exe_ordre,               --EXE_ORDRE,
 gescodecompta,
               -- ges_code,               --GES_CODE,
                  pco_num,               --PCO_NUM
                  abs(RPCOTVA_TVA_SAISIE),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  inverser_sens(lesens),           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette.recette_ctrl_planco_tva
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;


   -- si on est sur un sacd, la contrepartie reste sur le sacd
   if (pconum_185 is not null) then
    ctpGesCode := letitre.ges_code;
   else
    ctpGesCode := gescodecompta;
   end if;
   
               -- recette_ctrl_planco_ctp on force le 181
        INSERT INTO TITRE_BROUILLARD
                (
                ECD_ORDRE,
                EXE_ORDRE,
                GES_CODE,
                PCO_NUM,
                TIB_MONTANT,
                TIB_OPERATION,
                TIB_ORDRE,
                TIB_SENS,
                TIT_ID,
                REC_ID
                )
        SELECT
                NULL,                             --ECD_ORDRE,
              RECETTEctrlplanco.exe_ordre,               --EXE_ORDRE,
                ctpGesCode,            --GES_CODE,
                  '181',               --PCO_NUM
                  abs(RPCOCTP_TTC_SAISIE),      --TIB_MONTANT,
                  'VISA TITRE',       --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,           --TIB_ORDRE,
                  inverser_sens(lesens),           --TIB_SENS,
                  titid,                    --TIT_ID,
                recid
        FROM jefy_recette.recette_ctrl_planco_ctp
        WHERE rpco_id = RECETTEctrlplanco.rpco_id;

    end loop;
    close c_recettes;
END;



--PROCEDURE maj_plancomptable_titre (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
--IS
--  niv INTEGER;
--  cpt integer;
--BEGIN

--select count(*) into cpt from PLAN_COMPTABLE
--where pco_num = pconum;

--if cpt = 0 then
--    --calcul du niveau
--    SELECT LENGTH(pconum) INTO niv FROM dual;

--    --
--    INSERT INTO PLAN_COMPTABLE (PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE, PCO_NATURE, PCO_NIVEAU, PCO_NUM, PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, PCO_J_BE)
--        VALUES
--        (
--        'N',--PCO_BUDGETAIRE,
--        'O',--PCO_EMARGEMENT,
--        libelle,--PCO_LIBELLE,
--        nature,--PCO_NATURE,
--        niv,--PCO_NIVEAU,
--        pconum,--PCO_NUM,
--        2,--PCO_SENS_EMARGEMENT,
--        'VALIDE',--PCO_VALIDITE,
--        'O',--PCO_J_EXERCICE,
--        'N',--PCO_J_FIN_EXERCICE,
--        'N'--PCO_J_BE
--        );
--end if;

--END;

-- outils

FUNCTION inverser_sens_orv (tboordre INTEGER,sens VARCHAR) RETURN VARCHAR IS
cpt INTEGER;

BEGIN

-- si c est un bordereau de mandat li?es aux ORV
-- on inverse le sens de tous les details ecritures
-- (meme dans le cas des SACD de m.....)
SELECT count(*)  INTO cpt
FROM TYPE_BORDEREAU
WHERE tbo_sous_type ='REVERSEMENTS'
AND tbo_ordre = tboordre;

IF (cpt != 0) THEN
 IF (sens = 'C') THEN
  RETURN 'D';
 ELSE
  RETURN 'C';
 END IF;
END IF;
RETURN sens;
END ;




FUNCTION recup_gescode (abrid INTEGER) RETURN VARCHAR IS
gescode BORDEREAU.GES_CODE%TYPE;
BEGIN

SELECT DISTINCT ges_code INTO gescode FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;
RETURN gescode;
END;

FUNCTION recup_utlordre (abrid INTEGER) RETURN INTEGER IS
utlordre BORDEREAU.utl_ordre%TYPE;
BEGIN
SELECT DISTINCT utl_ordre INTO utlordre FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;
RETURN utlordre;
END;

FUNCTION recup_exeordre (abrid INTEGER) RETURN INTEGER IS
exeordre BORDEREAU.exe_ordre%TYPE;
BEGIN
SELECT DISTINCT exe_ordre INTO exeordre FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;
RETURN exeordre;
END;

FUNCTION recup_tboordre (abrid INTEGER) RETURN INTEGER IS
tboordre BORDEREAU.tbo_ordre%TYPE;
BEGIN
SELECT DISTINCT tbo_ordre INTO tboordre FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;
RETURN tboordre;
END;

FUNCTION recup_groupby (abrid INTEGER) RETURN VARCHAR IS
abrgroupby ABRICOT_BORD_SELECTION.ABR_GROUP_BY%TYPE;
BEGIN
SELECT DISTINCT abr_group_by INTO abrgroupby
FROM ABRICOT_BORD_SELECTION
WHERE abr_id = abrid;
RETURN abrgroupby;
END;


function inverser_sens (sens varchar) return varchar is
begin
if sens = 'D' then
return 'C';
else
return 'D';
end if;

end;



PROCEDURE numeroter_bordereau(borid INTEGER) IS
cpt_mandat integer;
cpt_titre integer;
BEGIN

select count(*) into cpt_mandat from mandat
where bor_id = borid;

select count(*) into cpt_titre from titre
where bor_id = borid;

if cpt_mandat + cpt_titre = 0 then
raise_application_error (-20001,'Bordereau  vide');
else
Numerotationobject.numeroter_bordereau(borid);

-- boucle mandat
Numerotationobject.numeroter_mandat(borid);

-- boucle titre
Numerotationobject.numeroter_titre(borid);
end if;

END;




FUNCTION traiter_orgid (orgid INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
convordre INTEGER;
BEGIN

IF orgid IS NULL THEN RETURN NULL; END IF;

SELECT COUNT(*) INTO cpt
FROM accords.CONVENTION_LIMITATIVE
WHERE org_id = orgid AND exe_ordre = exeordre;

IF cpt >0 THEN
 -- recup du type_origine CONVENTION--
 SELECT top_ordre INTO topordre FROM TYPE_OPERATION
 WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

 SELECT DISTINCT con_ordre INTO convordre
 FROM accords.CONVENTION_LIMITATIVE
 WHERE org_id = orgid
 AND exe_ordre = exeordre;


 SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET)
 INTO orilibelle
 FROM accords.contrat
 WHERE con_ordre = convordre;

ELSE
 SELECT COUNT(*) INTO cpt
 FROM jefy_admin.organ
 WHERE org_id = orgid
 AND org_lucrativite = 1;

 IF cpt = 1 THEN
 -- recup du type_origine OPERATION LUCRATIVE --
 SELECT top_ordre INTO topordre FROM TYPE_OPERATION
 WHERE top_libelle = 'OPERATION LUCRATIVE';

 --le libelle utilisateur pour le suivie en compta --
 SELECT org_UB||'-'||org_CR||'-'||org_souscr
 INTO orilibelle
 FROM jefy_admin.organ
 WHERE org_id = orgid;

 ELSE
  RETURN NULL;
 END IF;
END IF;

-- l origine est t elle deja  suivie --
SELECT COUNT(*)INTO cpt FROM ORIGINE
WHERE ORI_KEY_NAME = 'ORG_ID'
AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
AND ORI_KEY_ENTITE    =orgid;

IF cpt >= 1 THEN
    SELECT ori_ordre INTO cpt FROM ORIGINE
    WHERE ORI_KEY_NAME = 'ORG_ID'
    AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
    AND ORI_KEY_ENTITE = orgid
    AND ROWNUM=1;

ELSE
    SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;

    INSERT INTO ORIGINE
    (ORI_ENTITE, ORI_KEY_NAME, ORI_LIBELLE, ORI_ORDRE, ORI_KEY_ENTITE, TOP_ORDRE)
    VALUES ('JEFY_ADMIN','ORG_ID',orilibelle,cpt,orgid,topordre);

END IF;

RETURN cpt;

END;




procedure controle_bordereau(borid integer) is

ttc         maracuja.titre.TIT_TTC%type;
detailttc   maracuja.titre.TIT_TTC%type;
ordottc     maracuja.titre.TIT_TTC%type;
debit       maracuja.titre.TIT_TTC%type;
credit      maracuja.titre.TIT_TTC%type;
cpt         integer;
message     varchar2(50);
messagedetail varchar2(50);
begin

select count(*) into cpt
from maracuja.titre
where bor_id = borid;

if cpt = 0 then
-- somme des maracuja.titre
 select sum(man_ttc) into ttc
 from maracuja.mandat
 where bor_id = borid;

--somme des maracuja.recette
 select sum(d.dep_ttc) into detailttc
 from maracuja.mandat m,maracuja.depense d
 where m.man_id = d.man_id
 and m.bor_id = borid;

-- la somme des credits
 select sum(mab_montant) into credit
 from maracuja.mandat m, maracuja.mandat_brouillard mb
 where bor_id = borid
 and m.man_id = mb.man_id
 and mb.MaB_SENS = 'C';

-- la somme des debits
 select sum(mab_montant) into debit
 from maracuja.mandat m, maracuja.mandat_brouillard mb
 where bor_id = borid
 and m.man_id = mb.man_id
 and mb.MaB_SENS = 'D';

-- somme des jefy.recette
 select sum(d.dpco_ttc_saisie) into ordottc
 from maracuja.mandat m,jefy_depense.depense_ctrl_planco d
 where m.man_id = d.man_id
 and m.bor_id = borid;

message := ' mandats ';
messagedetail := ' depenses ';

else
-- somme des maracuja.titre
 select sum(tit_ttc) into ttc
 from maracuja.titre
 where bor_id = borid;

--somme des maracuja.recette
 select sum(r.rec_monttva+r.rec_mont) into detailttc
 from maracuja.titre t,maracuja.recette r
 where t.tit_id = r.tit_id
 and t.bor_id = borid;

-- la somme des credits
 select sum(tib_montant) into credit
 from maracuja.titre t, maracuja.titre_brouillard tb
 where bor_id = borid
 and t.tit_id = tb.tit_id
 and tb.TIB_SENS = 'C';

-- la somme des debits
 select sum(tib_montant) into debit
 from maracuja.titre t, maracuja.titre_brouillard tb
 where bor_id = borid
 and t.tit_id = tb.tit_id
 and tb.TIB_SENS = 'D';

-- somme des jefy.recette
 select sum(r.rpco_ttc_saisie) into ordottc
 from maracuja.titre t,jefy_recette.RECETTE_CTRL_PLANCO r
 where t.tit_id = r.tit_id
 and t.bor_id = borid;

message := ' titres ';
messagedetail := ' recettes ';

end if;


-- la somme des credits = sommes des debits
if (nvl(debit,0) != nvl(credit,0)) then
 RAISE_APPLICATION_ERROR(-20001,'PROBLEME DE '||message||' :  debit <> credit : '||debit||' '||credit);
end if;

-- la somme des credits = sommes des debits
if (nvl(debit,0) != nvl(credit,0)) then
 RAISE_APPLICATION_ERROR(-20001,'PROBLEME DE '||message||' :  ecriture <> budgetaire : '||debit||' '||ttc);
end if;

-- somme des maracuja.titre = somme des maracuja.recette
if (nvl(ttc,0) != nvl(detailttc,0)) then
 RAISE_APPLICATION_ERROR(-20001,'PROBLEME DE '||message||' : montant des '||message||' <>  du montant des '||messagedetail||' :'||ttc||' '||detailttc);
end if;

-- somme des jefy.recette = somme des maracuja.recette
if (nvl(ttc,0) != nvl(ordottc,0)) then
 RAISE_APPLICATION_ERROR(-20001,'PROBLEME DE '||message||' : montant des '||message||' <>  du montant ordonnateur des '||messagedetail||' :'||ttc||' '||ordottc);
end if;


bordereau_abricot.ctrl_date_exercice(borid);
end;


procedure  Get_recette_prelevements (titid INTEGER)
IS
cpt INTEGER;
FACTURE_TITRE_data PRESTATION.FACTURE_titre%ROWTYPE;
CLIENT_data PRELEV.client%ROWTYPE;

ORIORDRE INTEGER;
modordre INTEGER;
recid integer;

echeid                  integer;
echeancier_data         jefy_echeancier.ECHEANCIER%ROWTYPE;
echeancier_prelev_data  jefy_echeancier.ECHEANCIER_prelev%ROWTYPE;
facture_data            jefy_recette.facture_budget%rowtype;
personne_data           grhum.v_personne%ROWTYPE;
premieredate            date;
BEGIN

-- verifier s il existe un echancier pour ce titre
select count(*) into cpt
from
jefy_recette.recette_ctrl_planco pco,
jefy_recette.recette r,
jefy_recette.facture f
where pco.tit_id =titid
and pco.rec_id = r.rec_id
and r.fac_id = f.fac_id
and eche_id is not null
AND r.rec_id_reduction IS NULL;


IF (cpt != 1) THEN
      RETURN;
END IF;


-- recup du eche_id / ech_id
select eche_id into echeid
from jefy_recette.recette_ctrl_planco pco,
jefy_recette.recette r,
jefy_recette.facture f
where pco.tit_id =titid
and pco.rec_id = r.rec_id
and r.fac_id = f.fac_id
and eche_id is not null
AND r.rec_id_reduction IS NULL;

-- recup du des infos du prelevements
SELECT * INTO echeancier_data FROM jefy_echeancier.echeancier where ech_id = echeid;
select * into echeancier_prelev_data from jefy_echeancier.echeancier_prelev where ech_id = echeid;
select * into facture_data from jefy_recette.facture_budget where eche_id = echeid;

SELECT * INTO personne_data  FROM GRHUM.V_PERSONNE WHERE pers_id = facture_data.pers_id;

select echd_date_prevue into premieredate
from jefy_echeancier.echeancier_detail
where echd_numero = 1
and ech_id = echeid;

select rec_id into recid from recette where tit_id = titid;


/*
-- verification / mise a jour du mode de recouvrement
SELECT mor_ordre INTO modordre FROM maracuja.TITRE WHERE tit_id=titid;
IF (modordre IS NULL) THEN
   SELECT COUNT(*) INTO cpt FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;
   IF (cpt=0) THEN
         RAISE_APPLICATION_ERROR (-20001,'MODE RECOUVREMENT ECHEANCIER NON DEFINI');
   END IF;
   IF (cpt>1) THEN
         RAISE_APPLICATION_ERROR (-20001,'PLUSIEURS MODE RECOUVREMENT ECHEANCIER DEFINIS. IMPOSSIBLE DE DETERMINER.');
   END IF;

   SELECT mod_ordre INTO modordre FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;

   UPDATE TITRE SET mor_ordre=modordre WHERE tit_id=titid;
END IF;
*/


-- recup ??
ORIORDRE :=Gestionorigine.traiter_orgid(facture_data.org_id,facture_data.exe_ordre);


INSERT INTO MARACUJA.ECHEANCIER (ECHE_AUTORIS_SIGNEE,
FOU_ORDRE_CLIENT, CON_ORDRE,
ECHE_DATE_1ERE_ECHEANCE, ECHE_DATE_CREATION, ECHE_DATE_MODIF,
ECHE_ECHEANCIER_ORDRE,
ECHE_ETAT_PRELEVEMENT, FT_ORDRE, ECHE_LIBELLE, ECHE_MONTANT, ECHE_MONTANT_EN_LETTRES,
ECHE_NOMBRE_ECHEANCES, ECHE_NUMERO_INDEX, ORG_ORDRE, PREST_ORDRE, ECHE_PRISE_EN_CHARGE,
ECHE_REF_FACTURE_EXTERNE, ECHE_SUPPRIME, EXE_ORDRE, TIT_ID, REC_ID, TIT_ORDRE, ORI_ORDRE,
PERS_ID, ORG_ID, PERS_DESCRIPTION)  VALUES
(
'O'  ,--ECHE_AUTORIS_SIGNEE
facture_data.FOU_ORDRE  ,--FOU_ORDRE_CLIENT
null,--echancier_data.CON_ORDRE  ,--CON_ORDRE
premieredate,--echancier_data.DATE_1ERE_ECHEANCE  ,--ECHE_DATE_1ERE_ECHEANCE
sysdate,--echancier_data.DATE_CREATION  ,--ECHE_DATE_CREATION
sysdate,--echancier_data.DATE_MODIF  ,--ECHE_DATE_MODIF
echeancier_data.ech_id,--echancier_data.ECHEANCIER_ORDRE  ,--ECHE_ECHEANCIER_ORDRE
'V',--echancier_data.ETAT_PRELEVEMENT  ,--ECHE_ETAT_PRELEVEMENT
facture_data.fac_id,--echancier_data.FT_ORDRE  ,--FT_ORDRE
echeancier_data.ech_libelle,--echancier_data.LIBELLE,--ECHE_LIBELLE
echeancier_data.ech_MONTANT  ,--ECHE_MONTANT
echeancier_data.ech_MONTANT_LETTRES  ,--ECHE_MONTANT_EN_LETTRES
echeancier_data.ech_NB_ECHEANCES  ,--ECHE_NOMBRE_ECHEANCES
echeancier_data.ech_id,--echeancier_data.NUMERO_INDEX  ,--ECHE_NUMERO_INDEX
facture_data.org_id,--echeancier_data.ORG_ORDRE  ,--ORG_ORDRE
null,--echeancier_data.PREST_ORDRE  ,--PREST_ORDRE
'O'  ,--ECHE_PRISE_EN_CHARGE
facture_data.fac_lib,--cheancier_data.REF_FACTURE_EXTERNE  ,--ECHE_REF_FACTURE_EXTERNE
'N'  ,--ECHE_SUPPRIME
facture_data.exe_ordre  ,--EXE_ORDRE
TITID,
recid ,--REC_ID,
-titid,
ORIORDRE,--ORI_ORDRE,
personne_data.pers_id, --CLIENT_data.pers_id  ,--PERS_ID
facture_data.org_id,--orgid a faire plus tard....
personne_data.PERS_LIBELLE --    PERS_DESCRIPTION
);


INSERT INTO MARACUJA.PRELEVEMENT (ECHE_ECHEANCIER_ORDRE,
RECO_ORDRE, FOU_ORDRE,
PREL_COMMENTAIRE, PREL_DATE_MODIF, PREL_DATE_PRELEVEMENT,
PREL_PRELEV_DATE_SAISIE,
PREL_PRELEV_ETAT, PREL_NUMERO_INDEX, PREL_PRELEV_MONTANT,
PREL_PRELEV_ORDRE, RIB_ORDRE, PREL_ETAT_MARACUJA)
SELECT
ech_id,--ECHE_ECHEANCIER_ORDRE
null,--PREL_FICP_ORDRE
facture_data.FOU_ORDRE,--FOU_ORDRE
echd_COMMENTAIRE,--PREL_COMMENTAIRE
sysdate,--DATE_MODIF,--PREL_DATE_MODIF
echd_date_prevue,--PREL_DATE_PRELEVEMENT
sysdate,--,--PREL_PRELEV_DATE_SAISIE
'ATTENTE',--PREL_PRELEV_ETAT
echd_numero,--PREL_NUMERO_INDEX
echd_MONTANT,--PREL_PRELEV_MONTANT
echd_id,--PREL_PRELEV_ORDRE
echeancier_prelev_data.RIB_ORDRE_DEBITEUR,--RIB_ORDRE
'ATTENTE'--PREL_ETAT_MARACUJA
FROM jefy_echeancier.echeancier_detail
WHERE ech_id = echeancier_data.ECH_id;

END;


PROCEDURE ctrl_date_exercice(borid INTEGER) IS
exeordre integer;
annee integer;
BEGIN

select to_char(bor_date_creation,'YYYY'), exe_ordre into annee, exeordre
from bordereau
where bor_id = borid
and exe_ordre >= 2007;

IF exeordre <> annee THEN
update bordereau set bor_date_creation = to_date('31/12/'||exe_ordre||' 12:00:00','DD/MM/YYYY HH24:MI:SS') where bor_id = borid;
update mandat set man_date_remise = to_date('31/12/'||exe_ordre||' 12:00:00','DD/MM/YYYY HH24:MI:SS') where bor_id = borid;
update titre set tit_date_remise = to_date('31/12/'||exe_ordre||' 12:00:00','DD/MM/YYYY HH24:MI:SS') where bor_id = borid;
end if;

END;


END;


-------------


CREATE OR REPLACE PACKAGE MARACUJA."NUMEROTATIONOBJECT" IS


-- PUBLIC -
PROCEDURE numeroter_ecriture (ecrordre INTEGER);
PROCEDURE Numeroter_Ecriture_Verif (comordre INTEGER ,exeordre INTEGER);
PROCEDURE private_numeroter_ecriture (ecrordre INTEGER);

PROCEDURE numeroter_brouillard (ecrordre INTEGER);

PROCEDURE numeroter_bordereauRejet (brjordre INTEGER);
PROCEDURE numeroter_bordereau (borid INTEGER);

PROCEDURE numeroter_bordereaucheques (borid INTEGER);

PROCEDURE numeroter_emargement (emaordre INTEGER);

PROCEDURE numeroter_ordre_paiement (odpordre INTEGER);

PROCEDURE numeroter_paiement (paiordre INTEGER);

PROCEDURE numeroter_retenue (retordre INTEGER);

PROCEDURE numeroter_reimputation (reiordre INTEGER);

PROCEDURE numeroter_recouvrement (recoordre INTEGER);


-- PRIVATE --
PROCEDURE numeroter_mandat_rejete (manid INTEGER);

PROCEDURE numeroter_titre_rejete (titid INTEGER);

PROCEDURE numeroter_mandat (borid INTEGER);

PROCEDURE numeroter_titre (borid INTEGER);

PROCEDURE numeroter_orv (borid INTEGER);

PROCEDURE numeroter_aor (borid INTEGER);

FUNCTION numeroter(
COMORDRE COMPTABILITE.com_ordre%TYPE,
EXEORDRE EXERCICE.exe_ordre%TYPE,
GESCODE GESTION.ges_ordre%TYPE,
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE
) RETURN INTEGER;

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA."NUMEROTATIONOBJECT" IS


PROCEDURE private_numeroter_ecriture (ecrordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt  FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ECRITURE INEXISTANTE , CLE '||ecrordre);
END IF;

SELECT ecr_numero INTO cpt  FROM ECRITURE WHERE ecr_ordre = ecrordre;

IF cpt = 0 THEN
-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO comordre,exeordre
FROM ECRITURE
WHERE ecr_ordre = ecrordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ECRITURE DU JOURNAL';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ECRITURE SET ecr_numero = lenumero
WHERE ecr_ordre = ecrordre;
END IF;
END;

PROCEDURE numeroter_ecriture (ecrordre INTEGER)
IS
monecriture ECRITURE%ROWTYPE;

BEGIN
Numerotationobject.private_numeroter_ecriture(ecrordre);
SELECT * INTO monecriture  FROM ECRITURE WHERE ecr_ordre = ecrordre;
Numerotationobject.Numeroter_Ecriture_Verif (monecriture.com_ordre,monecriture.exe_ordre);
END;

PROCEDURE Numeroter_Ecriture_Verif (comordre INTEGER ,exeordre INTEGER)
IS
ecrordre INTEGER;
lenumero INTEGER;

GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

CURSOR lesEcrituresNonNumerotees IS
SELECT ecr_ordre
FROM ECRITURE
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ecr_numero =0
AND bro_ordre IS NULL
ORDER BY ecr_ordre;

BEGIN

OPEN lesEcrituresNonNumerotees;
LOOP
FETCH lesEcrituresNonNumerotees INTO ecrordre;
EXIT WHEN lesEcrituresNonNumerotees%NOTFOUND;
Numerotationobject.private_numeroter_ecriture(ecrordre);
END LOOP;
CLOSE lesEcrituresNonNumerotees;

END;

PROCEDURE numeroter_brouillard (ecrordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ECRITURE BROUILLARD INEXISTANTE , CLE '||ecrordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO comordre,exeordre
FROM ECRITURE
WHERE ecr_ordre = ecrordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='BROUILLARD';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ECRITURE SET ecr_numero_brouillard = lenumero
WHERE ecr_ordre = ecrordre;


END;


PROCEDURE numeroter_bordereauRejet (brjordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
parvalue PARAMETRE.par_value%TYPE;

mandat_rejet MANDAT%ROWTYPE;
titre_rejet TITRE%ROWTYPE;

CURSOR mdt_rejet IS
SELECT *
FROM MANDAT WHERE brj_ordre = brjordre;

CURSOR tit_rejet IS
SELECT *
FROM TITRE WHERE brj_ordre = brjordre;

BEGIN

SELECT COUNT(*) INTO cpt FROM BORDEREAU_REJET WHERE brj_ordre = brjordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU REJET INEXISTANT , CLE '||brjordre);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,comordre,exeordre,gescode
FROM BORDEREAU_REJET b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.brj_ordre = brjordre
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;



IF (tbotype ='BTMNA') THEN
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORD. DEPENSE NON ADMIS';
ELSE
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORD. RECETTE NON ADMIS';
END IF;


-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;


-- affectation du numero -
UPDATE BORDEREAU_REJET SET brj_num = lenumero
WHERE brj_ordre = brjordre;

-- numeroter les titres rejetes -
IF (tbotype ='BTMNA') THEN
OPEN mdt_rejet;
LOOP
FETCH  mdt_rejet INTO mandat_rejet;
EXIT WHEN mdt_rejet%NOTFOUND;
Numerotationobject.numeroter_mandat_rejete (mandat_rejet.man_id);
END LOOP;
CLOSE mdt_rejet;

END IF;

-- numeroter les mandats rejetes -
IF (tbotype ='BTTNA') THEN

OPEN tit_rejet;
LOOP
FETCH  tit_rejet INTO titre_rejet;
EXIT WHEN  tit_rejet%NOTFOUND;
Numerotationobject.numeroter_titre_rejete (titre_rejet.tit_id);
END LOOP;
CLOSE tit_rejet;

END IF;

END;


PROCEDURE numeroter_bordereaucheques (borid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
parvalue PARAMETRE.par_value%TYPE;



BEGIN

SELECT COUNT(*) INTO cpt FROM BORDEREAU WHERE bor_id = borid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU DE CHEQUES INEXISTANT , CLE '||borid);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;


-- TNU_ORDRE A DEFINIR
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORDEREAU CHEQUE';



-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

--IF parvalue = 'COMPOSANTE' THEN
--lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
--ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
--END IF;


-- affectation du numero -
UPDATE BORDEREAU SET bor_num = lenumero
WHERE bor_id = borid;



END;


PROCEDURE numeroter_emargement (emaordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM EMARGEMENT WHERE ema_ordre = emaordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' EMARGEMENT INEXISTANT , CLE '||emaordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO COMORDRE,EXEORDRE
FROM EMARGEMENT
WHERE ema_ordre = emaordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RAPPROCHEMENT / LETTRAGE';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -

UPDATE EMARGEMENT  SET ema_numero = lenumero
WHERE ema_ordre = emaordre;


END;


PROCEDURE numeroter_ordre_paiement (odpordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM ORDRE_DE_PAIEMENT WHERE odp_ordre = odpordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ORDRE DE PAIEMENT INEXISTANT , CLE '||odpordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO COMORDRE,EXEORDRE
FROM ORDRE_DE_PAIEMENT
WHERE odp_ordre = odpordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ORDRE DE PAIEMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ORDRE_DE_PAIEMENT SET odp_numero = lenumero
WHERE odp_ordre = odpordre;

END;


PROCEDURE numeroter_paiement (paiordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM PAIEMENT WHERE pai_ordre = paiordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' PAIEMENT INEXISTANT , CLE '||paiordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
--select 1,1
INTO COMORDRE,EXEORDRE
FROM PAIEMENT
WHERE pai_ordre = paiordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='VIREMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE PAIEMENT SET pai_numero = lenumero
WHERE pai_ordre = paiordre;

END;


PROCEDURE numeroter_recouvrement (recoordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM RECOUVREMENT WHERE reco_ordre = recoordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' RECOUVREMENT INEXISTANT , CLE '||RECOordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
--select 1,1
INTO COMORDRE,EXEORDRE
FROM RECOUVREMENT
WHERE RECO_ordre = recoordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RECOUVREMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE RECOUVREMENT SET reco_numero = lenumero
WHERE reco_ordre = recoordre;

END;



PROCEDURE numeroter_retenue (retordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM RETENUE WHERE ret_id = retordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' RETENUE INEXISTANTE , CLE '||retordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre, exe_ordre
INTO COMORDRE,EXEORDRE
FROM RETENUE
WHERE ret_id = retordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RETENUE';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

--affectation du numero -
UPDATE RETENUE SET ret_numero = lenumero
WHERE RET_ID=retordre;

END;

PROCEDURE numeroter_reimputation (reiordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
TNULIBELLE TYPE_NUMEROTATION.TNU_libelle%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM REIMPUTATION WHERE rei_ordre = reiordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' REIMPUTATION INEXISTANTE , CLE '||reiordre);
END IF;

SELECT COUNT(*)
--select 1,1
INTO  cpt
FROM REIMPUTATION r,MANDAT m,GESTION g
WHERE rei_ordre = reiordre
AND m.man_id = r.man_id
AND m.ges_code = g.ges_code;

IF cpt != 0 THEN

-- recup des infos de l objet -
--select com_ordre,exe_ordre
 SELECT DISTINCT g.com_ordre,r.exe_ordre,'REIMPUTATION MANDAT'
--select 1,1
 INTO  COMORDRE,EXEORDRE,TNULIBELLE
 FROM REIMPUTATION r,MANDAT m,GESTION g
 WHERE rei_ordre = reiordre
 AND m.man_id = r.man_id
 AND m.ges_code = g.ges_code;
ELSE
--select com_ordre,exe_ordre
SELECT DISTINCT g.com_ordre,r.exe_ordre,'REIMPUTATION TITRE'
--select 1,1
 INTO  COMORDRE,EXEORDRE,TNULIBELLE
 FROM REIMPUTATION r,TITRE t,GESTION g
 WHERE rei_ordre = reiordre
 AND t.tit_id = r.tit_id
 AND t.ges_code = g.ges_code;
END IF;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle =TNULIBELLE;

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

--affectation du numero -
UPDATE REIMPUTATION SET rei_numero = lenumero
WHERE rei_ordre = reiordre;

END;




PROCEDURE numeroter_mandat_rejete (manid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM MANDAT WHERE man_id=manid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' MANDAT REJETE INEXISTANT , CLE '||manid);
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,m.exe_ordre,m.ges_code
--select 1,1
INTO COMORDRE,EXEORDRE,GESCODE
FROM MANDAT m ,GESTION g
WHERE man_id = manid
AND m.ges_code = g.ges_code;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='MANDAT REJET';

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU' AND exe_ordre=EXEORDRE;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;



-- affectation du numero -
UPDATE MANDAT  SET man_numero_rejet = lenumero
WHERE man_id=manid;

END;



PROCEDURE numeroter_titre_rejete (titid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM TITRE WHERE tit_id=titid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' TITRE REJETE INEXISTANT , CLE '||titid);
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,t.exe_ordre,t.ges_code
--select 1,1
INTO COMORDRE,EXEORDRE,GESCODE
FROM TITRE t ,GESTION g
WHERE t.tit_id = titid
AND t.ges_code = g.ges_code;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='TITRE REJET';

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU' AND exe_ordre=EXEORDRE;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;

-- affectation du numero -
UPDATE TITRE  SET tit_numero_rejet = lenumero
WHERE tit_id=titid;

END;



FUNCTION numeroter (
COMORDRE COMPTABILITE.com_ordre%TYPE,
EXEORDRE EXERCICE.exe_ordre%TYPE,
GESCODE GESTION.ges_ordre%TYPE,
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE
)
RETURN INTEGER
IS
cpt INTEGER;
numordre INTEGER;
BEGIN

LOCK TABLE NUMEROTATION IN EXCLUSIVE MODE;

--COM_ORDRE, EXE_ORDRE, GES_CODE, NUM_NUMERO, NUM_ORDRE, TNU_ORDRE

IF gescode IS NULL THEN
SELECT COUNT(*) INTO cpt
FROM NUMEROTATION
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ges_code IS NULL
AND tnu_ordre = tnuordre;
ELSE
SELECT COUNT(*) INTO cpt
FROM NUMEROTATION
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ges_code = gescode
AND tnu_ordre = tnuordre;
END IF;

IF cpt  = 0 THEN
--raise_application_error (-20002,'trace:'||COMORDRE||''||EXEORDRE||''||GESCODE||''||numordre||''||TNUORDRE);
 SELECT numerotation_seq.NEXTVAL INTO numordre FROM dual;


 INSERT INTO NUMEROTATION
 ( COM_ORDRE, EXE_ORDRE, GES_CODE, NUM_NUMERO, NUM_ORDRE, TNU_ORDRE)
 VALUES (COMORDRE, EXEORDRE, GESCODE, 1,numordre, TNUORDRE);
 RETURN 1;
ELSE
 IF gescode IS NOT NULL THEN
  SELECT num_numero+1 INTO cpt
  FROM NUMEROTATION
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code = gescode
  AND tnu_ordre = tnuordre;

  UPDATE NUMEROTATION SET num_numero = cpt
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code = gescode
  AND tnu_ordre = tnuordre;
 ELSE
  SELECT num_numero+1 INTO cpt
  FROM NUMEROTATION
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code IS NULL
  AND tnu_ordre = tnuordre;

  UPDATE NUMEROTATION SET num_numero = cpt
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code IS NULL
  AND tnu_ordre = tnuordre;
 END IF;
 RETURN cpt;
END IF;
END ;




PROCEDURE numeroter_bordereau (borid INTEGER) IS


cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
tboordre TYPE_BORDEREAU.tbo_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;

lebordereau BORDEREAU%ROWTYPE;


BEGIN

SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU WHERE bor_id = borid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU  INEXISTANT , CLE '||borid);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,t.tbo_ordre,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,tboordre,comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;



SELECT  COUNT(*) INTO cpt
FROM maracuja.TYPE_BORDEREAU tb , maracuja.TYPE_NUMEROTATION tn
WHERE tn.tnu_ordre = tb.tnu_ordre
AND tb.tbo_ordre = tboordre;

IF cpt = 1 THEN
 SELECT tn.tnu_ordre INTO tnuordre
 FROM maracuja.TYPE_BORDEREAU tb , TYPE_NUMEROTATION tn
 WHERE tn.tnu_ordre = tb.tnu_ordre
 AND tb.tbo_ordre = tboordre;
ELSE
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE DETERMINER LE TYPE DE NUMEROTATION'||borid);
END IF;


-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;

IF lenumero IS NULL THEN
 RAISE_APPLICATION_ERROR (-20001,'PROBLEME DE NUMEROTATION'||borid);
END IF;


-- affectation du numero -
UPDATE maracuja.BORDEREAU SET bor_num = lenumero
WHERE bor_id = borid;

END;


PROCEDURE numeroter_mandat (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
manid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
tboordre BORDEREAU.tbo_ordre%TYPE;

CURSOR a_numeroter IS
SELECT man_id FROM MANDAT WHERE bor_id = borid ORDER BY pco_num;

BEGIN

-- si c un bordereau orv
SELECT tbo_ordre INTO tboordre FROM BORDEREAU WHERE bor_id=borid;
IF (tboordre in (8,18) ) THEN
   numeroter_orv(borid);
   RETURN;
END IF;



-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;



-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='MANDAT';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO manid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;
 UPDATE maracuja.MANDAT SET man_numero = lenumero
 WHERE bor_id = borid AND man_id = manid;
END LOOP;
CLOSE a_numeroter;
END ;

PROCEDURE numeroter_titre (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
titid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
tboordre BORDEREAU.tbo_ordre%TYPE;

CURSOR a_numeroter IS
SELECT tit_id FROM TITRE WHERE bor_id = borid ORDER BY pco_num, tit_id;


BEGIN


-- si c un bordereau aor
SELECT tbo_ordre INTO  tboordre FROM BORDEREAU WHERE bor_id=borid;
IF (tboordre=9) THEN
   numeroter_aor(borid);
   RETURN;
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;


-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='TITRE';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO titid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;

 UPDATE maracuja.TITRE SET tit_numero =  lenumero
 WHERE bor_id = borid AND tit_id = titid;
END LOOP;
CLOSE a_numeroter;

END;



PROCEDURE numeroter_orv (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
manid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
CURSOR a_numeroter IS
SELECT man_id FROM MANDAT WHERE bor_id = borid;

BEGIN

-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;


-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ORV';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO manid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;
 UPDATE maracuja.MANDAT SET man_numero = lenumero
 WHERE bor_id = borid AND man_id = manid;
END LOOP;
CLOSE a_numeroter;
END ;

PROCEDURE numeroter_aor (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
titid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
CURSOR a_numeroter IS
SELECT tit_id FROM TITRE WHERE bor_id = borid;

BEGIN

-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;


-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='AOR';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO titid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;
 UPDATE maracuja.TITRE SET tit_numero =  lenumero
 WHERE bor_id = borid AND tit_id = titid;
END LOOP;
CLOSE a_numeroter;

END;

END;
/


GRANT EXECUTE ON  MARACUJA.NUMEROTATIONOBJECT TO JEFY_PAYE;

----------

CREATE OR REPLACE VIEW MARACUJA.V_ABRICOT_DEPENSE_A_MANDATER
(C_BANQUE, C_GUICHET, NO_COMPTE, IBAN, BIC, 
 CLE_RIB, DOMICILIATION, MOD_LIBELLE, MOD_CODE, MOD_DOM, 
 PERS_TYPE, PERS_LIBELLE, PERS_LC, EXE_EXERCICE, ORG_ID, 
 ORG_UB, ORG_CR, ORG_SOUSCR, TCD_ORDRE, TCD_CODE, 
 TCD_LIBELLE, DPP_NUMERO_FACTURE, DPP_ID, DPP_HT_SAISIE, DPP_TVA_SAISIE, 
 DPP_TTC_SAISIE, DPP_DATE_FACTURE, DPP_DATE_SAISIE, DPP_DATE_RECEPTION, DPP_DATE_SERVICE_FAIT, 
 DPP_NB_PIECE, UTL_ORDRE, TBO_ORDRE, DEP_ID, PCO_NUM, 
 DPCO_ID, DPCO_HT_SAISIE, DPCO_TVA_SAISIE, DPCO_TTC_SAISIE, ADR_CIVILITE, 
 ADR_NOM, ADR_PRENOM, ADR_ADRESSE1, ADR_ADRESSE2, ADR_VILLE, 
 ADR_CP, UTLNOMPRENOM)
AS 
select 
r.C_BANQUE,
r.C_GUICHET,
r.NO_COMPTE,
r.iban,
r.bic,
r.cle_rib,
bq.DOMICILIATION,
mp.MOD_libelle,
mp.MOD_CODE,
mp.MOD_DOM,
p.pers_type,
p.pers_libelle,
p.pers_lc,
e.EXE_EXERCICE,
eb.org_id,
o.ORG_UB,
o.ORG_CR,
o.ORG_SOUSCR,
eb.tcd_ordre,
tc.tcd_code,
tc.tcd_libelle,
dp.dpp_numero_facture,
dp.DPP_ID,
dp.DPP_HT_SAISIE,
dp.DPP_TVA_SAISIE,
dp.DPP_TTC_SAISIE,
dp.DPP_DATE_FACTURE,
dp.DPP_DATE_SAISIE,
dp.DPP_DATE_RECEPTION,
dp.DPP_DATE_SERVICE_FAIT,
dp.DPP_NB_PIECE,
dp.utl_ordre,
dcp.tbo_ordre,
dcp.dep_id dep_id,
dcp.pco_num,
dcp.DPCO_id  DPCO_id ,
dcp.DPCO_HT_SAISIE,
dcp.DPCO_TVA_SAISIE,
dcp.DPCO_TTC_SAISIE,
f.ADR_CIVILITE,
f.ADR_NOM,
f.ADR_PRENOM,
f.ADR_ADRESSE1,
f.ADR_ADRESSE2,
f.ADR_VILLE,
f.ADR_CP,
pu.pers_libelle||' '||pu.pers_lc utlNomPrenom
from
jefy_depense.engage_budget eb,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dp,
jefy_depense.depense_ctrl_planco dcp,
grhum.v_fournis_grhum f,
jefy_admin.EXERCICE e,
jefy_admin.utilisateur u,
grhum.personne p,
grhum.personne pu,
grhum.ribfour_ulr r,
grhum.banque bq,
maracuja.MODE_PAIEMENT mp,
jefy_admin.type_credit tc,
jefy_admin.organ o --,
--jefy_admin.type_credit tcd
where eb.eng_id = db.eng_id
and   db.dpp_id = dp.dpp_id
and   db.dep_id = dcp.dep_id
and   dp.fou_ordre = f.fou_ordre
and   dp.exe_ordre = e.exe_ordre
and   dp.utl_ordre = u.utl_ordre
and   p.pers_id = u.pers_id
and   dp.rib_ordre = r.rib_ordre(+)
and   r.C_BANQUE = bq.C_BANQUE(+)
and   r.C_GUICHET = bq.C_GUICHET(+)
and   mp.mod_ordre = dp.mod_ordre
and   eb.tcd_ordre = tc.tcd_ordre
and   o.org_id = eb.org_id
and u.pers_id = pu.pers_id
--and   r.rib_valide ='O'
and   man_id is null
and tbo_ordre != 201;



---------------

CREATE OR REPLACE PACKAGE MARACUJA.Abricot_Impressions  IS

   -- il s'agit du dep_id de DEPENSE_budget
   FUNCTION GET_DEPENSE_CN (depid INTEGER)  RETURN VARCHAR2;
   FUNCTION GET_DEPENSE_CONV (depid INTEGER) RETURN VARCHAR2;
   FUNCTION GET_RECETTE_CONV (recid INTEGER) RETURN VARCHAR2;
   FUNCTION GET_DEPENSE_actions (depid INTEGER)    RETURN VARCHAR2;
   FUNCTION GET_RECETTE_ACTIONS (recid INTEGER)   RETURN VARCHAR2;
   FUNCTION GET_DEPENSE_inventaire(dpcoid INTEGER) RETURN VARCHAR2;
   FUNCTION GET_DEPENSE_lbud(depid INTEGER)    RETURN VARCHAR2;  
   FUNCTION GET_DEPENSE_infos(depid INTEGER)    RETURN VARCHAR2;
END; 
/

CREATE OR REPLACE PACKAGE BODY MARACUJA.Abricot_Impressions IS


    FUNCTION GET_depense_CN (depid INTEGER)
    RETURN VARCHAR2 AS

            ceordre INTEGER;
            lescodes VARCHAR2(2000);
            lescodesdepenses VARCHAR2(2000);
            CURSOR c1 IS
            SELECT ce_ordre FROM jefy_depense.DEPENSE_CTRL_HORS_MARCHE WHERE dep_id = depid;
            BEGIN

            lescodesdepenses :=' ';
            OPEN c1;
            LOOP
            FETCH c1 INTO ceordre;
            EXIT WHEN c1%NOTFOUND;

            SELECT cm.cm_code||' '||cm_lib INTO lescodes
                        --SELECT cm.cm_code INTO lescodes
            FROM jefy_marches.CODE_EXER ce,jefy_marches.CODE_MARCHE cm
            WHERE ce.cm_ordre = cm.cm_ordre
            AND ce.ce_ordre  = ceordre;

            lescodesdepenses:=lescodesdepenses||' '||lescodes;

            END LOOP;
            CLOSE c1;


              RETURN lescodesdepenses;
    END;

    FUNCTION GET_DEPENSE_CONV (depid INTEGER)
    RETURN VARCHAR2 AS
           lesconv VARCHAR2(2000);
           convref VARCHAR2(500);
            CURSOR c1 IS
                    SELECT conv_reference FROM jefy_depense.DEPENSE_ctrl_convention dcv, jefy_depense.v_convention c WHERE dcv.dep_id = depid AND dcv.conv_ordre = c.conv_ordre;
    BEGIN
         lesconv := null;

         OPEN c1;
            LOOP
                FETCH c1 INTO convref;
                      EXIT WHEN c1%NOTFOUND;

                IF lesconv IS NOT NULL THEN
                   lesconv := lesconv||', ';
                END IF;
                lesconv := lesconv || convref;
            END LOOP;
            CLOSE c1;

           RETURN lesconv;
    END;

      FUNCTION GET_recette_CONV (recid INTEGER)
          RETURN VARCHAR2 AS
           lesconv VARCHAR2(2000);
           convref VARCHAR2(500);
           CURSOR c1 IS
                --SELECT con_reference_externe FROM jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c WHERE dcv.rec_id = recid AND dcv.con_ordre = c.con_ordre;
                SELECT con_reference_externe
                FROM jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
                WHERE dcv.con_ordre = c.con_ordre and dcv.rec_id=rpco.rec_id
                and rpco.TIT_ID=t.tit_id and t.tit_id=mr.tit_id and mr.rec_id = recid;
                
    BEGIN
         lesconv := null;

         OPEN c1;
            LOOP
                FETCH c1 INTO convref;
                      EXIT WHEN c1%NOTFOUND;

                IF lesconv IS NOT NULL THEN
                   lesconv := lesconv||', ';
                END IF;
                lesconv := lesconv || convref;
            END LOOP;
            CLOSE c1;

           RETURN lesconv;
    END;



    FUNCTION GET_DEPENSE_actions (depid INTEGER)
    RETURN VARCHAR2 AS
           leslolf VARCHAR2(2000);
           lolfcode VARCHAR2(500);
            CURSOR c1 IS
                    SELECT lolf_code FROM jefy_depense.DEPENSE_ctrl_action dca, jefy_admin.v_lolf_nomenclature_depense c WHERE dca.dep_id = depid AND dca.tyac_id = c.lolf_id AND dca.exe_ordre=c.exe_ordre;
    BEGIN
         leslolf := null;

         OPEN c1;
            LOOP
                FETCH c1 INTO lolfcode;
                      EXIT WHEN c1%NOTFOUND;

                IF leslolf IS NOT NULL THEN
                   leslolf := leslolf||', ';
                END IF;
                leslolf := leslolf || lolfcode;
            END LOOP;
            CLOSE c1;

           RETURN leslolf;
    END;
        
    FUNCTION GET_RECETTE_ACTIONS (recid INTEGER)
          RETURN VARCHAR2 AS
           leslolf VARCHAR2(2000);
           lolfcode VARCHAR2(500);
           CURSOR c1 IS
                --SELECT con_reference_externe FROM jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c WHERE dcv.rec_id = recid AND dcv.con_ordre = c.con_ordre;
                SELECT lolf_code
                FROM jefy_recette.recette_ctrl_action rca, jefy_recette.V_LOLF_NOMENCLATURE_RECETTE l, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
                WHERE rca.LOLF_ID= l.lolf_id and rca.rec_id=rpco.rec_id and rca.EXE_ORDRE = l.exe_ordre
                and rpco.TIT_ID=t.tit_id and t.tit_id=mr.tit_id and mr.rec_id = recid;
                
    BEGIN
         leslolf := null;

         OPEN c1;
            LOOP
                FETCH c1 INTO lolfcode;
                      EXIT WHEN c1%NOTFOUND;

                IF leslolf IS NOT NULL THEN
                   leslolf := leslolf||', ';
                END IF;
                leslolf := leslolf || lolfcode;
            END LOOP;
            CLOSE c1;

           RETURN leslolf;
    END;
    
    
        FUNCTION GET_DEPENSE_inventaire(dpcoid INTEGER)
    RETURN VARCHAR2 AS
        lesinv VARCHAR2(2000);
        leinv VARCHAR2(2000);

    CURSOR c1 IS
        SELECT ci.clic_num_complet FROM 
        jefy_inventaire.inventaire_comptable ic , jefy_inventaire.CLE_INVENTAIRE_COMPTABLE ci
        where ic.clic_id = ci.clic_id
        and dpco_id = dpcoid;
        BEGIN

    lesinv := null;

     OPEN c1;
      LOOP
      FETCH c1 INTO leinv;
       EXIT WHEN c1%NOTFOUND;
        IF leinv IS NOT NULL THEN
         leinv := leinv||' ';
        END IF;
       lesinv := lesinv || leinv;
      END LOOP;
     CLOSE c1;
    RETURN lesinv;
        END;
        
        
    FUNCTION GET_DEPENSE_lbud (depid INTEGER)
    RETURN VARCHAR2 AS
           lbudlib VARCHAR2(2000);
    BEGIN

  
   SELECT o.org_ub || ' / ' ||org_cr || ' / ' ||org_souscr lbud into lbudlib
FROM
jefy_depense.depense_budget db, 
jefy_depense.engage_budget eb,
jefy_admin.organ o
WHERE  db.dep_id = depid
and o.org_id = eb.org_id
AND eb.ENG_ID=db.ENG_ID;

        return lbudlib;


    END;

    FUNCTION GET_DEPENSE_infos (depid INTEGER)
    RETURN VARCHAR2 AS

    BEGIN

 
        return Abricot_Impressions.GET_depense_CN(depid)||' '||Abricot_Impressions.GET_depense_lbud(depid);


    END;

END; 




