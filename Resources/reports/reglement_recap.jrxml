<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="reglement_recap"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="595"
		 pageHeight="842"
		 columnWidth="567"
		 columnSpacing="0"
		 leftMargin="14"
		 rightMargin="14"
		 topMargin="14"
		 bottomMargin="14"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="BORDEREAU" isForPrompting="true" class="java.lang.Integer">
		<defaultValueExpression ><![CDATA[new Integer(13299)]]></defaultValueExpression>
	</parameter>
	<parameter name="NB_MANDATS" isForPrompting="false" class="java.lang.Integer"/>
	<queryString><![CDATA[SELECT 
jefy_admin.API_JASPER_PARAM.get_param_admin('UNIV',b.EXE_ORDRE) ETABLISSEMENT,
jefy_admin.API_JASPER_PARAM.get_param_admin('VILLE',b.EXE_ORDRE) LIEU,
jefy_admin.API_JASPER_PARAM.get_param_maracuja('AGENT_COMPTABLE',b.EXE_ORDRE) AGENT_COMPTABLE,
jefy_admin.API_JASPER_PARAM.get_param_maracuja('COMPTE_TPG',b.EXE_ORDRE) COMPTE_TPG,
b.EXE_ORDRE, b.GES_CODE,b.BOR_NUM, 
m.mod_ordre, mp.MOD_CODE, mp.MOD_LIBELLE, mp.MOD_DOM,
m.MAN_NUMERO, m.man_id, m.PCO_NUM, m.MAN_HT, m.MAN_TVA, m.MAN_TTC,D.DEP_ID,
d.DEP_FOURNISSEUR, d.DEP_NUMERO, d.DEP_HT, d.DEP_TVA, d.DEP_TTC,d.dep_adresse,
ROUND(d.DEP_DATE_reception) DEP_DATE, 
m.man_DATE_remise BOR_date,
vr.RIB_CODBANC C_BANQUE, vr.RIB_GUICH C_GUICHET, vr.RIB_NUM NO_COMPTE, vr.RIB_CLE CLE_RIB, vr.RIB_DOMICIL, trim(grhum.format_iban(RIB_IBAN)) IBAN, nvl(RIB_BIC, ' ') BIC
FROM
maracuja.BORDEREAU b, maracuja.MANDAT m, maracuja.DEPENSE d, maracuja.MODE_PAIEMENT mp,  maracuja.V_RIB vr
WHERE
b.BOR_ID = $P{BORDEREAU}
AND m.BOR_ID = b.BOR_ID
AND d.MAN_ID = m.MAN_ID
AND d.mod_ordre = mp.mod_ordre
AND m.RIB_ORDRE_comptable = vr.RIB_ORDRE (+)
ORDER BY b.EXE_ORDRE, b.GES_CODE,b.BOR_NUM, mp.MOD_DOM, mp.mod_code, m.MAN_NUMERO, d.DEP_DATE_reception]]></queryString>

	<field name="ETABLISSEMENT" class="java.lang.String"/>
	<field name="LIEU" class="java.lang.String"/>
	<field name="AGENT_COMPTABLE" class="java.lang.String"/>
	<field name="COMPTE_TPG" class="java.lang.String"/>
	<field name="EXE_ORDRE" class="java.math.BigDecimal"/>
	<field name="GES_CODE" class="java.lang.String"/>
	<field name="BOR_NUM" class="java.math.BigDecimal"/>
	<field name="MOD_ORDRE" class="java.math.BigDecimal"/>
	<field name="MOD_CODE" class="java.lang.String"/>
	<field name="MOD_LIBELLE" class="java.lang.String"/>
	<field name="MOD_DOM" class="java.lang.String"/>
	<field name="MAN_NUMERO" class="java.math.BigDecimal"/>
	<field name="MAN_ID" class="java.math.BigDecimal"/>
	<field name="PCO_NUM" class="java.lang.String"/>
	<field name="MAN_HT" class="java.math.BigDecimal"/>
	<field name="MAN_TVA" class="java.math.BigDecimal"/>
	<field name="MAN_TTC" class="java.math.BigDecimal"/>
	<field name="DEP_ID" class="java.math.BigDecimal"/>
	<field name="DEP_FOURNISSEUR" class="java.lang.String"/>
	<field name="DEP_NUMERO" class="java.lang.String"/>
	<field name="DEP_HT" class="java.math.BigDecimal"/>
	<field name="DEP_TVA" class="java.math.BigDecimal"/>
	<field name="DEP_TTC" class="java.math.BigDecimal"/>
	<field name="DEP_ADRESSE" class="java.lang.String"/>
	<field name="DEP_DATE" class="java.sql.Timestamp"/>
	<field name="BOR_DATE" class="java.sql.Timestamp"/>
	<field name="C_BANQUE" class="java.lang.String"/>
	<field name="C_GUICHET" class="java.lang.String"/>
	<field name="NO_COMPTE" class="java.lang.String"/>
	<field name="CLE_RIB" class="java.lang.String"/>
	<field name="RIB_DOMICIL" class="java.lang.String"/>
	<field name="IBAN" class="java.lang.String"/>
	<field name="BIC" class="java.lang.String"/>

	<variable name="TOTAL_TTC" class="java.math.BigDecimal" resetType="Group" resetGroup="MOD_CODE" calculation="Sum">
		<variableExpression><![CDATA[$F{DEP_TTC}]]></variableExpression>
	</variable>

		<group  name="MOD_CODE" isStartNewPage="true" minHeightToStartNewPage="38" >
			<groupExpression><![CDATA[$F{MOD_CODE}]]></groupExpression>
			<groupHeader>
			<band height="23"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="5"
						width="565"
						height="14"
						key="textField-34"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Arial" pdfFontName="Helvetica-BoldOblique" size="12" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{MOD_CODE} + " " + $F{MOD_LIBELLE}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="100"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true" evaluationTime="Group" evaluationGroup="MOD_CODE"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="460"
						y="5"
						width="105"
						height="16"
						key="textField-21"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="3" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-BoldOblique" size="12" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TOTAL_TTC}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="7"
						y="5"
						width="223"
						height="16"
						key="textField-36"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" pdfFontName="Helvetica-BoldOblique" size="12" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Total pour le "  + $F{MOD_CODE} +" : "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="0" isBlankWhenNull="true" evaluationTime="Group" evaluationGroup="MOD_CODE"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="339"
						y="5"
						width="36"
						height="16"
						key="textField-37"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" pdfFontName="Helvetica-BoldOblique" size="12" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$V{MOD_CODE_COUNT}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="380"
						y="5"
						width="73"
						height="16"
						key="staticText-28"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-BoldOblique" size="12" isItalic="true"/>
					</textElement>
				<text><![CDATA[Dépense(s)]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="6"
						y="62"
						width="519"
						height="12"
						key="textField-22"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{AGENT_COMPTABLE}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="6"
						y="30"
						width="28"
						height="14"
						key="staticText-22"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font/>
					</textElement>
				<text><![CDATA[Fait à ]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="35"
						y="30"
						width="270"
						height="14"
						key="textField-30"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{LIEU}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="3"
						y="28"
						width="560"
						height="1"
						key="line-2"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="21"
						y="46"
						width="80"
						height="14"
						key="textField-31"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date())]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="7"
						y="46"
						width="12"
						height="14"
						key="staticText-23"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font/>
					</textElement>
				<text><![CDATA[Le ]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" pattern="0" isBlankWhenNull="true" evaluationTime="Group" evaluationGroup="MOD_CODE"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="235"
						y="5"
						width="38"
						height="16"
						key="textField-42"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" pdfFontName="Helvetica-BoldOblique" size="12" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$P{NB_MANDATS}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="278"
						y="5"
						width="61"
						height="16"
						key="staticText-29"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-BoldOblique" size="12" isItalic="true"/>
					</textElement>
				<text><![CDATA[Mandat(s)
]]></text>
				</staticText>
			</band>
			</groupFooter>
		</group>
		<group  name="BORDEREAU" >
			<groupExpression><![CDATA[$F{BOR_NUM}]]></groupExpression>
			<groupHeader>
			<band height="33"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="1"
						y="5"
						width="565"
						height="14"
						backcolor="#CCCCFF"
						key="textField-3"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="1Point" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font fontName="Arial" pdfFontName="Helvetica-Oblique" size="12" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["UB " +$F{GES_CODE} + " Bd. n° " +$F{BOR_NUM}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="0"
						y="24"
						width="344"
						height="8"
						key="staticText-5"/>
					<box topBorder="Thin" topBorderColor="#000000" leftBorder="Thin" leftBorderColor="#000000" leftPadding="10" rightBorder="Thin" rightBorderColor="#000000" bottomBorder="Thin" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica-Oblique" size="6" isItalic="true"/>
					</textElement>
				<text><![CDATA[CREANCIER, COMPTE A CREDITER]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="344"
						y="24"
						width="50"
						height="8"
						key="staticText-24"/>
					<box topBorder="Thin" topBorderColor="#000000" leftBorder="Thin" leftBorderColor="#000000" leftPadding="10" rightBorder="Thin" rightBorderColor="#000000" bottomBorder="Thin" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica-Oblique" size="6" isItalic="true"/>
					</textElement>
				<text><![CDATA[Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="394"
						y="24"
						width="50"
						height="8"
						key="staticText-25"/>
					<box topBorder="Thin" topBorderColor="#000000" leftBorder="Thin" leftBorderColor="#000000" leftPadding="10" rightBorder="Thin" rightBorderColor="#000000" bottomBorder="Thin" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica-Oblique" size="6" isItalic="true"/>
					</textElement>
				<text><![CDATA[Mandat]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="444"
						y="24"
						width="50"
						height="8"
						key="staticText-26"/>
					<box topBorder="Thin" topBorderColor="#000000" leftBorder="Thin" leftBorderColor="#000000" leftPadding="10" rightBorder="Thin" rightBorderColor="#000000" bottomBorder="Thin" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica-Oblique" size="6" isItalic="true"/>
					</textElement>
				<text><![CDATA[Imputation]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="495"
						y="24"
						width="71"
						height="8"
						key="staticText-27"/>
					<box topBorder="Thin" topBorderColor="#000000" leftBorder="Thin" leftBorderColor="#000000" leftPadding="10" rightBorder="Thin" rightBorderColor="#000000" bottomBorder="Thin" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica-Oblique" size="6" isItalic="true"/>
					</textElement>
				<text><![CDATA[Montant TTC]]></text>
				</staticText>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="DEP_ID" >
			<groupExpression><![CDATA[$F{DEP_ID}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="20"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="1"
						width="372"
						height="16"
						backcolor="#FFFFFF"
						key="staticText-2"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" leftPadding="4" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[RECAPITULATIF DES DEPENSES]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="395"
						y="1"
						width="169"
						height="16"
						key="textField-4"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Exercice " + $F{EXE_ORDRE}.intValue()]]></textFieldExpression>
				</textField>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="31"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="340"
						height="12"
						key="textField-7"/>
					<box topBorder="None" topBorderColor="#000000" topPadding="2" leftBorder="None" leftBorderColor="#000000" leftPadding="3" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{DEP_FOURNISSEUR}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="495"
						y="0"
						width="71"
						height="24"
						key="textField-16"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="4" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{DEP_TTC}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="136"
						y="12"
						width="204"
						height="12"
						key="textField-23">
							<printWhenExpression><![CDATA[new Boolean("VIREMENT".equals($F{MOD_DOM}))]]></printWhenExpression>
						</reportElement>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[(
($F{IBAN}==null || $F{IBAN}=="")?
$F{C_BANQUE} + "  " +$F{C_GUICHET} + "  " +$F{NO_COMPTE} + "  " +$F{CLE_RIB}:
$F{BIC}+" - " + $F{IBAN}
)]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="12"
						width="136"
						height="12"
						key="textField-27">
							<printWhenExpression><![CDATA[new Boolean("VIREMENT".equals($F{MOD_DOM}))]]></printWhenExpression>
						</reportElement>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" leftPadding="6" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{RIB_DOMICIL}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="444"
						y="0"
						width="50"
						height="24"
						key="textField-38"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="4" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{PCO_NUM}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="395"
						y="0"
						width="50"
						height="24"
						key="textField-39"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="4" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{MAN_NUMERO}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="345"
						y="0"
						width="50"
						height="24"
						key="textField-40"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="4" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.sql.Timestamp"><![CDATA[$F{DEP_DATE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="16"
						width="338"
						height="11"
						key="textField-41">
							<printWhenExpression><![CDATA[new Boolean(!"VIREMENT".equals($F{MOD_DOM}))]]></printWhenExpression>
						</reportElement>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" leftPadding="6" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{DEP_ADRESSE}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="3"
						y="30"
						width="560"
						height="1"
						key="line-3"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="15"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="463"
						y="3"
						width="50"
						height="10"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " / "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="515"
						y="3"
						width="50"
						height="10"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER} + ""]]></textFieldExpression>
				</textField>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
