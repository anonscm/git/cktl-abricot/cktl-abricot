<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="Bord_journalM_or"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="842"
		 pageHeight="595"
		 columnWidth="782"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="UNIV" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA["UNIVERSITE DE PROVENCE"]]></defaultValueExpression>
	</parameter>
	<parameter name="AGENCE_COMPTABLE" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA["AGENCE COMPTABLE DE L'UNIVERSITE DE PROVENCE"]]></defaultValueExpression>
	</parameter>
	<parameter name="ORDONNATEUR_UNIV" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA["PAUL TORDO"]]></defaultValueExpression>
	</parameter>
	<parameter name="REQUETE_SQL" isForPrompting="false" class="java.lang.String"/>
	<parameter name="BORDEREAU" isForPrompting="false" class="java.lang.Integer">
		<defaultValueExpression ><![CDATA[new Integer(13299)]]></defaultValueExpression>
	</parameter>
	<parameter name="EXERCICE" isForPrompting="false" class="java.lang.Integer">
		<defaultValueExpression ><![CDATA[new Integer(2007)]]></defaultValueExpression>
	</parameter>
	<queryString><![CDATA[SELECT vo.exe_ordre, b.ges_code,
       TO_CHAR (bor_date_creation, 'DD/MM/YYYY') bor_date, bor_num, abs(ant_ht) ant_ht,
       abs(ant_tva) ant_tva, abs(ant_ttc) ant_ttc, abs(bor_ht) bor_ht, abs(bor_tva) bor_tva, abs(bor_ttc) bor_ttc,
       jefy_admin.api_jasper_param.get_param_admin ('UNIV', b.exe_ordre) univ,
       jefy_admin.api_jasper_param.get_param_maracuja
                                        ('AGENCE_COMPTABLE',
                                         vo.exe_ordre
                                        ) agence_comptable,
       jefy_admin.api_jasper_param.get_ordo_bord_dep
                                          (vo.org_etab,
                                           vo.org_ub,
                                           bor_date_creation
                                          ) ordonnateur_univ
  FROM (SELECT   exe_ordre, ges_code, SUM (ant_ht) ant_ht,
                 SUM (ant_tva) ant_tva, SUM (ant_ttc) ant_ttc,
                 SUM (bor_ht) bor_ht, SUM (bor_tva) bor_tva,
                 SUM (bor_ttc) bor_ttc
            FROM (SELECT   exe_ordre, ges_code, SUM (ant_ht) ant_ht,
                           SUM (ant_tva) ant_tva, SUM (ant_ttc) ant_ttc,
                           SUM (bor_ht) bor_ht, SUM (bor_tva) bor_tva,
                           SUM (bor_ttc) bor_ttc
                      FROM (SELECT b.exe_ordre, b.ges_code, 0 ant_ht,
                                   0 ant_tva, 0 ant_ttc, man_ht bor_ht,
                                   man_tva bor_tva, man_ttc bor_ttc
                              FROM maracuja.BORDEREAU b,
                                   maracuja.MANDAT m
                             WHERE b.bor_id = $P{BORDEREAU}
				AND b.bor_id=m.bor_id
                               AND m.brj_ordre IS NULL
                            UNION ALL
                            SELECT bb.exe_ordre, bb.ges_code, man_ht ant_ht,
                                   man_tva ant_tva, man_ttc ant_ttc, 0 bor_ht,
                                   0 bor_tva, 0 bor_ttc
                              FROM maracuja.BORDEREAU b, MARACUJA.TYPE_BORDEREAU tb, maracuja.TYPE_BORDEREAU tbb,
                                   maracuja.BORDEREAU bb,
                                   maracuja.MANDAT m
                             WHERE b.bor_id = $P{BORDEREAU}
							 	  AND b.tbo_ordre = tb.tbo_ordre
								  AND bb.tbo_ordre = tbb.tbo_ordre
								  AND tb.tnu_ordre=tbb.tnu_ordre
                               AND bb.exe_ordre = b.exe_ordre
                               AND bb.ges_code = b.ges_code
                               AND bb.bor_id = m.bor_id
                               AND bb.bor_num < b.bor_num
							   AND bb.bor_etat<>'ANNULE'
                               AND m.brj_ordre IS NULL)
                  GROUP BY exe_ordre, ges_code)
        GROUP BY exe_ordre, ges_code) b,
       maracuja.v_organ_exer vo,
       maracuja.BORDEREAU bb
 WHERE vo.org_niv = 2
   AND vo.exe_ordre = b.exe_ordre
   AND vo.org_ub = b.ges_code
   AND bb.bor_id = $P{BORDEREAU}
   AND bb.exe_ordre = b.exe_ordre
   AND bb.ges_code = b.ges_code]]></queryString>

	<field name="EXE_ORDRE" class="java.math.BigDecimal"/>
	<field name="GES_CODE" class="java.lang.String"/>
	<field name="BOR_DATE" class="java.lang.String"/>
	<field name="BOR_NUM" class="java.math.BigDecimal"/>
	<field name="ANT_HT" class="java.math.BigDecimal"/>
	<field name="ANT_TVA" class="java.math.BigDecimal"/>
	<field name="ANT_TTC" class="java.math.BigDecimal"/>
	<field name="BOR_HT" class="java.math.BigDecimal"/>
	<field name="BOR_TVA" class="java.math.BigDecimal"/>
	<field name="BOR_TTC" class="java.math.BigDecimal"/>
	<field name="UNIV" class="java.lang.String"/>
	<field name="AGENCE_COMPTABLE" class="java.lang.String"/>
	<field name="ORDONNATEUR_UNIV" class="java.lang.String"/>

	<variable name="BOR_LIBELLE" class="java.lang.String" resetType="None" calculation="Nothing">
		<variableExpression><![CDATA[$F{BOR_NUM}.intValue()== 0 ? "Total des bordereaux antérieurs" : "Total du bordereau"]]></variableExpression>
	</variable>
	<variable name="TOTAL_HT" class="java.math.BigDecimal" resetType="Group" resetGroup="GES_CODE" calculation="Nothing">
		<variableExpression><![CDATA[$F{ANT_HT}.add($F{BOR_HT})]]></variableExpression>
	</variable>
	<variable name="TOTAL_TVA" class="java.math.BigDecimal" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA[$F{BOR_TVA}.add($F{ANT_TVA})]]></variableExpression>
	</variable>
	<variable name="TOTAL_TTC" class="java.math.BigDecimal" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA[$F{BOR_TTC}.add($F{ANT_TTC})]]></variableExpression>
	</variable>

		<group  name="GES_CODE" isStartNewPage="true" >
			<groupExpression><![CDATA[$F{GES_CODE}]]></groupExpression>
			<groupHeader>
			<band height="147"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="83"
						y="0"
						width="349"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-1"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{UNIV}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="0"
						y="35"
						width="344"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-3"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{AGENCE_COMPTABLE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="638"
						y="3"
						width="135"
						height="16"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-4"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Exercice "+$F{EXE_ORDRE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="638"
						y="21"
						width="134"
						height="16"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-5"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["UB "+$F{GES_CODE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="638"
						y="41"
						width="140"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-6"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Bordereau N° "+$F{BOR_NUM}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						mode="Transparent"
						x="639"
						y="62"
						width="28"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-1"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="0"
						y="0"
						width="79"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-3"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Etablissement :]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="0"
						y="16"
						width="79"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-4"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Ordonnateur :]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="83"
						y="16"
						width="239"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-7"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{ORDONNATEUR_UNIV}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						mode="Transparent"
						x="455"
						y="125"
						width="100"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-9"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Montant HT]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="655"
						y="125"
						width="100"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-10"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Montant TTC]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="555"
						y="125"
						width="100"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-12"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Montant TVA]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="314"
						y="78"
						width="303"
						height="25"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true" isItalic="true" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["DES REVERSEMENTS"]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						mode="Transparent"
						x="49"
						y="22"
						width="0"
						height="0"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-13"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Static text]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="85"
						y="78"
						width="226"
						height="25"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-16"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true" isItalic="true" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[BORDEREAU JOURNAL :]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						mode="Opaque"
						x="16"
						y="142"
						width="753"
						height="1"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="line-1"/>
					<graphicElement stretchType="NoStretch" pen="Thin" fill="Solid" />
				</line>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="671"
						y="62"
						width="100"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{BOR_DATE}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="122"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="GES_CODE"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="655"
						y="15"
						width="100"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="5" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TOTAL_TTC}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="GES_CODE"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="555"
						y="15"
						width="100"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="5" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TOTAL_TVA}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Group" evaluationGroup="GES_CODE"  hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="455"
						y="15"
						width="100"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-15"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="5" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TOTAL_HT}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="48"
						y="92"
						width="239"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-2"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{ORDONNATEUR_UNIV}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						mode="Transparent"
						x="48"
						y="75"
						width="79"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-14"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[L'Ordonnateur]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="166"
						y="15"
						width="278"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-8"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Totaux des bordereaux pour l'UB " + $F{GES_CODE}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						mode="Opaque"
						x="16"
						y="6"
						width="753"
						height="1"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="line-2"/>
					<graphicElement stretchType="NoStretch" pen="Thin" fill="Solid" />
				</line>
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="1"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="98"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="555"
						y="65"
						width="100"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-13"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="5" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{BOR_TVA}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="655"
						y="65"
						width="100"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-14"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="5" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{BOR_TTC}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="455"
						y="65"
						width="100"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="5" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{BOR_HT}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						mode="Transparent"
						x="52"
						y="5"
						width="98"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-17"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[RECAPITULATIF]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="127"
						y="24"
						width="300"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-18"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Totaux des bordereaux précédents]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="655"
						y="24"
						width="100"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-16"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="5" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{ANT_TTC}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="455"
						y="24"
						width="100"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-17"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="5" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{ANT_HT}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="555"
						y="24"
						width="100"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-18"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="5" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{ANT_TVA}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="127"
						y="65"
						width="300"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-19"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Totaux du bordereau N° "+ $F{BOR_NUM}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
