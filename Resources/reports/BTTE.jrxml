<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="BTTE"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="842"
		 pageHeight="595"
		 columnWidth="782"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="REP_BASE_PATH" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="BORDEREAU" isForPrompting="true" class="java.lang.Integer">
		<defaultValueExpression ><![CDATA[new Integer(15163)]]></defaultValueExpression>
	</parameter>
	<queryString><![CDATA[select distinct v.EXE_ORDRE,TBO_ORDRE,BOR_ID,BOR_NUM,TIT_ID,TIT_NB_PIECE,TIT_NUMERO,
abs(TIT_HT) tit_ht, abs(TIT_TTC) tit_ttc ,abs(TIT_TVA) tit_tva,
v.GES_CODE,
decode(tit_libelle,'TITRE COLLECTIF',null,LIGNE_BUDG) LIGNE_BUDG,
decode(tit_libelle,'TITRE COLLECTIF',null,v.ORG_LIB) ORG_LIB,
TIT_DATE,PCO_CHAPITRE,
decode(tit_libelle,'TITRE COLLECTIF',null,DEBITEUR) DEBITEUR,
decode(tit_libelle,'TITRE COLLECTIF',null,FOU_ORDRE) FOU_ORDRE,
TIT_LIBELLE,MONTANT_LETTRE,
decode(tit_libelle,'TITRE COLLECTIF',null,TIT_ADRESSE) TIT_ADRESSE,
decode(tit_libelle,'TITRE COLLECTIF',null,FAC_NUMERO) FAC_NUMERO,
decode(tit_libelle,'TITRE COLLECTIF',null,rec_numero) rec_numero,
jefy_admin.API_JASPER_PARAM.get_param_admin('UNIV',v.EXE_ORDRE) UNIV,
jefy_admin.API_JASPER_PARAM.get_param_maracuja('AGENCE_COMPTABLE',v.EXE_ORDRE) AGENCE_COMPTABLE,
jefy_admin.API_JASPER_PARAM.get_ordo_bord_rec(vo.org_etab,vo.org_ub,tit_date) ORDONNATEUR_UNIV
from maracuja.v_edition_titre v, maracuja.V_ORGAN_EXER vo
where v.bor_id = $P{BORDEREAU}
and vo.ORG_NIV = 2
and vo.EXE_ORDRE = v.EXE_ORDRE
and vo.ORG_UB = v.GES_CODE
order by tit_numero]]></queryString>

	<field name="EXE_ORDRE" class="java.math.BigDecimal"/>
	<field name="TBO_ORDRE" class="java.math.BigDecimal"/>
	<field name="BOR_ID" class="java.math.BigDecimal"/>
	<field name="BOR_NUM" class="java.math.BigDecimal"/>
	<field name="TIT_ID" class="java.lang.Integer"/>
	<field name="TIT_NB_PIECE" class="java.math.BigDecimal"/>
	<field name="TIT_NUMERO" class="java.lang.Integer"/>
	<field name="TIT_HT" class="java.math.BigDecimal"/>
	<field name="TIT_TTC" class="java.math.BigDecimal"/>
	<field name="TIT_TVA" class="java.math.BigDecimal"/>
	<field name="GES_CODE" class="java.lang.String"/>
	<field name="LIGNE_BUDG" class="java.lang.String"/>
	<field name="ORG_LIB" class="java.lang.String"/>
	<field name="TIT_DATE" class="java.sql.Timestamp"/>
	<field name="PCO_CHAPITRE" class="java.lang.String"/>
	<field name="DEBITEUR" class="java.lang.String"/>
	<field name="FOU_ORDRE" class="java.lang.String"/>
	<field name="TIT_LIBELLE" class="java.lang.String"/>
	<field name="MONTANT_LETTRE" class="java.lang.String"/>
	<field name="TIT_ADRESSE" class="java.lang.String"/>
	<field name="FAC_NUMERO" class="java.lang.String"/>
	<field name="REC_NUMERO" class="java.lang.String"/>
	<field name="UNIV" class="java.lang.String"/>
	<field name="AGENCE_COMPTABLE" class="java.lang.String"/>
	<field name="ORDONNATEUR_UNIV" class="java.lang.String"/>

	<variable name="BOR_TYPE" class="java.lang.String" resetType="Group" resetGroup="BOR_ID" calculation="Nothing">
		<variableExpression><![CDATA[$F{TBO_ORDRE}.intValue()== 7 ? "TITRE DE RECETTES" : $F{TBO_ORDRE}.intValue()== 9 ? "REDUCTION / ANNULATION DE RECETTES" : "PRESTATIONS INTERNES"]]></variableExpression>
	</variable>
	<variable name="TOTAL_HT" class="java.math.BigDecimal" resetType="Group" resetGroup="BOR_ID" calculation="Sum">
		<variableExpression><![CDATA[$F{TIT_HT}]]></variableExpression>
	</variable>
	<variable name="TOTAL_TVA" class="java.math.BigDecimal" resetType="Group" resetGroup="BOR_ID" calculation="Sum">
		<variableExpression><![CDATA[$F{TIT_TVA}]]></variableExpression>
	</variable>
	<variable name="TOTAL_TTC" class="java.math.BigDecimal" resetType="Group" resetGroup="BOR_ID" calculation="Sum">
		<variableExpression><![CDATA[$F{TIT_TTC}]]></variableExpression>
	</variable>

		<group  name="BOR_ID" isStartNewPage="true" >
			<groupExpression><![CDATA[$F{BOR_ID}]]></groupExpression>
			<groupHeader>
			<band height="119"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="83"
						y="0"
						width="349"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-1"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{UNIV}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="0"
						y="35"
						width="344"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-3"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{AGENCE_COMPTABLE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="638"
						y="3"
						width="135"
						height="16"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-4"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Exercice "+$F{EXE_ORDRE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="638"
						y="21"
						width="134"
						height="16"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-5"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["UB "+$F{GES_CODE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="638"
						y="41"
						width="140"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-6"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Bordereau N° "+$F{BOR_NUM}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						mode="Transparent"
						x="639"
						y="62"
						width="28"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-1"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Date]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="dd/MM/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="673"
						y="62"
						width="100"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{TIT_DATE}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						mode="Transparent"
						x="0"
						y="0"
						width="79"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-3"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Etablissement :]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="0"
						y="16"
						width="79"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-4"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Ordonnateur :]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="83"
						y="16"
						width="239"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-7"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{ORDONNATEUR_UNIV}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						mode="Transparent"
						x="0"
						y="92"
						width="41"
						height="25"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-5"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[N° Fact
/Rec]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="84"
						y="103"
						width="178"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-6"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" leftPadding="20" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Débiteur]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="262"
						y="103"
						width="130"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-7"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Imputation Budgétaire]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="392"
						y="103"
						width="90"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-8"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Imputation Cptble]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="482"
						y="103"
						width="100"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-9"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Montant HT]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="682"
						y="103"
						width="100"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-10"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Montant TTC]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="582"
						y="103"
						width="100"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-12"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Montant TVA]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="334"
						y="56"
						width="303"
						height="25"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true" isItalic="true" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{TBO_ORDRE}.intValue()== 7 ? "TITRE DE RECETTES" : $F{TBO_ORDRE}.intValue()== 9 ? "REDUCTION / ANNULATION DE RECETTES" : "PRESTATIONS INTERNES"]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						mode="Transparent"
						x="49"
						y="22"
						width="0"
						height="0"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-13"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[Static text]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="105"
						y="56"
						width="226"
						height="25"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-16"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true" isItalic="true" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[BORDEREAU DE TRANSMISSION :]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="41"
						y="92"
						width="43"
						height="25"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-18"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[ N° du Titre
]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						mode="Opaque"
						x="0"
						y="118"
						width="782"
						height="1"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="line-2"/>
					<graphicElement stretchType="NoStretch" pen="Dotted" fill="Solid" />
				</line>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="132"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="192"
						y="57"
						width="239"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-2"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{ORDONNATEUR_UNIV}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="482"
						y="7"
						width="100"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="5" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TOTAL_HT}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="682"
						y="7"
						width="100"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="5" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TOTAL_TTC}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.00;-#,##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="582"
						y="7"
						width="100"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="5" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{TOTAL_TVA}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						mode="Transparent"
						x="192"
						y="40"
						width="79"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-14"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<text><![CDATA[L'Ordonnateur]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="212"
						y="7"
						width="256"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-8"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Totaux du bordereau N° "+ $F{BOR_NUM}]]></textFieldExpression>
				</textField>
			</band>
			</groupFooter>
		</group>
		<group  name="TIT_ID" >
			<groupExpression><![CDATA[$F{TIT_ID}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="9"  isSplitAllowed="true" >
				<line direction="TopDown">
					<reportElement
						mode="Opaque"
						x="0"
						y="4"
						width="782"
						height="1"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="line-1"/>
					<graphicElement stretchType="NoStretch" pen="Dotted" fill="Solid" />
				</line>
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="1"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="60"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="41"
						y="0"
						width="42"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" leftPadding="10" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.Integer"><![CDATA[$F{TIT_NUMERO}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="84"
						y="0"
						width="179"
						height="13"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"
						isPrintWhenDetailOverflows="true"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="Cp1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{TIT_LIBELLE}.equals("TITRE COLLECTIF")? "DEBITEURS DIVERS":$F{DEBITEUR})]]></textFieldExpression>
				</textField>
				<subreport  isUsingCache="true">
					<reportElement
						mode="Opaque"
						x="262"
						y="43"
						width="520"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="subreport-1"/>
					<subreportParameter  name="TITID">
						<subreportParameterExpression><![CDATA[$F{TIT_ID}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression  class="java.lang.String"><![CDATA[$P{REP_BASE_PATH}+"btte_liquidation.jasper"]]></subreportExpression>
				</subreport>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="268"
						y="0"
						width="514"
						height="12"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{TIT_LIBELLE}]]></textFieldExpression>
				</textField>
				<subreport  isUsingCache="true">
					<reportElement
						x="2"
						y="28"
						width="210"
						height="16"
						key="subreport-2"/>
					<subreportParameter  name="TITID">
						<subreportParameterExpression><![CDATA[$F{TIT_ID}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression  class="java.lang.String"><![CDATA[$P{REP_BASE_PATH}+"btte_liquidation_Brouillard.jasper"]]></subreportExpression>
				</subreport>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="304"
						y="13"
						width="394"
						height="26"
						key="textField">
							<printWhenExpression><![CDATA[($F{TIT_LIBELLE}.equals("TITRE COLLECTIF")? new Boolean(false):new Boolean(true))]]></printWhenExpression>
						</reportElement>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{TIT_ADRESSE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="41"
						height="26"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{FAC_NUMERO} == null? " ":$F{FAC_NUMERO}+" / "+$F{REC_NUMERO})]]></textFieldExpression>
				</textField>
				<rectangle>
					<reportElement
						x="79"
						y="41"
						width="0"
						height="0"
						key="rectangle-1"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<line direction="TopDown">
					<reportElement
						x="7"
						y="28"
						width="179"
						height="0"
						key="line-3"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="28"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="560"
						y="9"
						width="100"
						height="15"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right">
						<font size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " de "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="660"
						y="9"
						width="100"
						height="15"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER} + ""]]></textFieldExpression>
				</textField>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
